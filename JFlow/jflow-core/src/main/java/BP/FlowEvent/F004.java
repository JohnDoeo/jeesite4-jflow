package BP.FlowEvent;

import BP.DA.*;
import BP.DTS.*;
import BP.En.*;
import BP.Web.*;
import BP.Sys.*;
import BP.WF.*;
import BP.Port.*;

public class F004 extends BP.WF.FlowEventBase
{
	/** 
	 重写流程标记
	 
	*/
	@Override
	public String getFlowMark()
	{
		return "004";
	}

	public F004()
	{
	}
	/** 
	 发送成功时
	 
	 @return 
	 * @throws Exception 
	*/
	@Override
	public String SendSuccess() throws Exception
	{
		//根据WorkID获取当前节点编号
		GenerWorkFlow gwf = new GenerWorkFlow(this.getWorkID());
		//判断节点事件
		switch (gwf.getFK_Node())
		{
			case 403: //对未处理的人进行信息抄送
				SendCC(gwf.getFK_Node(), gwf.getTitle());
				break;
		}
		return super.SendSuccess();
	}
	/** 
	 发送成功后，执行抄送
	 * @throws Exception 
	 
	*/
	public final void SendCC(int FK_Node, String title) throws Exception
	{
		//查询当前节点的处理人
		Paras ps = new Paras();
		//防注入式sql语句写法
		String dbstr = SystemConfig.getAppCenterDBVarStr();
		ps.SQL = "SELECT FK_Emp from WF_NodeEmp Where FK_Node=" + dbstr + "FK_Node";
		ps.Add("FK_Node", FK_Node);

		//获取当前节点的处理人
		DataTable dt = BP.DA.DBAccess.RunSQLReturnTable(ps);

		String empNo = "";
		String empName="";
		if (dt.Rows.size() > 0)
		{
			//获取未执行操作的人员账号与名称
			for (DataRow row : dt.Rows)
			{
				//排除当前执行人的帐号
				if (row.getValue("FK_Emp").toString().equals(WebUser.getNo()))
				{
					continue;
				}
				empNo += row.getValue("FK_Emp") + ",";

				//获取人员名称
				Emp emp = new Emp(row.getValue("FK_Emp").toString());
				empName += emp.getName() + ",";
			}
		}
		//如果有未执行的处理人，执行抄送
		if (!DataType.IsNullOrEmpty(empNo))
		{
			empNo = DotNetToJavaStringHelper.trimEnd(empNo, ',');
			//写入抄送列表
			BP.WF.Dev2Interface.Node_CC_WriteTo_CClist(FK_Node, this.getWorkID(), empNo, empName, title, "业务办理通知");
			//写入待办列表
			BP.WF.Dev2Interface.Node_CC_WriteTo_Todolist(Integer.parseInt(this.HisNode.getNo()),FK_Node, this.getWorkID(), empNo, empName);
		}

	}
}
