package BP.WF.Template;

import BP.En.EntitiesMyPK;
import BP.En.Entity;

public class Parts extends EntitiesMyPK
{
	/** 
	 得到它的 Entity 
	 
	*/
	@Override
	public Entity getGetNewEntity()
	{
		return new Part();
	}
	
	/** 
	 配件集合
	 
	*/
	public Parts()
	{
	}
	/** 
	 配件集合.
	 
	 @param FlowNo
	 * @throws Exception 
	*/
	public Parts(String fk_flow) throws Exception
	{
		this.Retrieve(PartAttr.FK_Flow, fk_flow);
	}
	/** 
	 转化成 java list,C#不能调用.
	 
	 @return List
	*/
	public final java.util.List<Part> ToJavaList()
	{
		return (java.util.List<Part>)(Object)this;
	}
	/** 
	 转化成list
	 
	 @return List
	*/
	public final java.util.ArrayList<Part> Tolist()
	{
		java.util.ArrayList<Part> list = new java.util.ArrayList<Part>();
		for (int i = 0; i < this.size(); i++)
		{
			list.add((Part)this.get(i));
		}
		return list;
	}


}
