﻿function InitHomeCount() {
    var handler = new HttpHandler("BP.WF.HttpHandler.WF_AppClassic");
    var data = handler.DoMethodReturnString("Home_Init");

    if (data.indexOf('err@') == 0) {
        alert(data);
        return;
    }

    data = JSON.parse(data);
    GenerFullAllCtrlsVal(data);
}


/*为页面的所有字段属性赋值. */
function GenerFullAllCtrlsVal(data) {
    if (data == null)
        return;
    //判断data是否是一个数组，如果是一个数组，就取第1个对象.
    var json = data;
    if ($.isArray(data) && data.length > 0)
        json = data[0];

    var unSetCtrl = "";
    for (var attr in json) {

        var val = json[attr]; //值

        var div = window.parent.document.getElementById(attr);
        if (div != null) {
            div.innerHTML = val;
            continue;
        }
    }    
}