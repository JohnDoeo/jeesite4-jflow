-- MySQL dump 10.13  Distrib 5.6.42, for Win64 (x86_64)
--
-- Host: localhost    Database: jflow
-- ------------------------------------------------------
-- Server version	5.6.42-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ccfrm_ceshibiaodan`
--

DROP TABLE IF EXISTS `ccfrm_ceshibiaodan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ccfrm_ceshibiaodan` (
  `OID` int(11) NOT NULL DEFAULT '0',
  `RDT` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`OID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ccfrm_ceshibiaodan`
--

LOCK TABLES `ccfrm_ceshibiaodan` WRITE;
/*!40000 ALTER TABLE `ccfrm_ceshibiaodan` DISABLE KEYS */;
/*!40000 ALTER TABLE `ccfrm_ceshibiaodan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cn_area`
--

DROP TABLE IF EXISTS `cn_area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cn_area` (
  `No` varchar(50) NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(50) DEFAULT NULL COMMENT '名称',
  `Names` varchar(50) DEFAULT NULL COMMENT '小名',
  `Grade` int(11) DEFAULT NULL COMMENT 'Grade',
  `FK_SF` varchar(100) DEFAULT NULL COMMENT '省份,外键:对应物理表:CN_SF,表描述:省份',
  `FK_PQ` varchar(100) DEFAULT NULL COMMENT '片区,外键:对应物理表:CN_PQ,表描述:片区',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='城市编码';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cn_area`
--

LOCK TABLES `cn_area` WRITE;
/*!40000 ALTER TABLE `cn_area` DISABLE KEYS */;
/*!40000 ALTER TABLE `cn_area` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cn_city`
--

DROP TABLE IF EXISTS `cn_city`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cn_city` (
  `No` varchar(50) NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(50) DEFAULT NULL COMMENT '名称',
  `Names` varchar(50) DEFAULT NULL COMMENT '小名',
  `Grade` int(11) DEFAULT NULL COMMENT 'Grade',
  `FK_SF` varchar(100) DEFAULT NULL COMMENT '省份,外键:对应物理表:CN_SF,表描述:省份',
  `FK_PQ` varchar(100) DEFAULT NULL COMMENT '片区,外键:对应物理表:CN_PQ,表描述:片区',
  `PinYin` varchar(200) DEFAULT NULL COMMENT '搜索拼音',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='城市';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cn_city`
--

LOCK TABLES `cn_city` WRITE;
/*!40000 ALTER TABLE `cn_city` DISABLE KEYS */;
INSERT INTO `cn_city` VALUES ('1101','北京市东城区','东城',2,'11','AA',NULL),('1102','北京市西城区','西城',2,'11','AA',NULL),('1103','北京市崇文区','崇文',2,'11','AA',NULL),('1104','北京市宣武区','宣武',2,'11','AA',NULL),('1105','北京市朝阳区','朝阳',2,'11','AA',NULL),('1106','北京市丰台区','丰台',2,'11','AA',NULL),('1107','北京市石景山区','石景山',2,'11','AA',NULL),('1108','北京市海淀区','海淀',2,'11','AA',NULL),('1109','北京市门头沟区','门头沟',2,'11','AA',NULL),('1111','北京市房山区','房山',2,'11','AA',NULL),('1112','北京市通州区','通州',2,'11','AA',NULL),('1113','北京市顺义区','顺义',2,'11','AA',NULL),('1114','北京市昌平区','昌平',2,'11','AA',NULL),('1115','北京市大兴区','大兴',2,'11','AA',NULL),('1116','北京市怀柔区','怀柔',2,'11','AA',NULL),('1117','北京市平谷区','平谷',2,'11','AA',NULL),('1128','北京市密云县','密云',2,'11','AA',NULL),('1129','北京市延庆县','延庆',2,'11','AA',NULL),('1201','天津市和平区','和平区',2,'12','AA',NULL),('1202','天津市河东区','河东区',2,'12','AA',NULL),('1203','天津市河西区','河西区',2,'12','AA',NULL),('1204','天津市南开区','南开区',2,'12','AA',NULL),('1205','天津市河北区','河北区',2,'12','AA',NULL),('1206','天津市红桥区','红桥区',2,'12','AA',NULL),('1207','天津市塘沽区','塘沽区',2,'12','AA',NULL),('1208','天津市汉沽区','汉沽区',2,'12','AA',NULL),('1209','天津市大港区','大港区',2,'12','AA',NULL),('1210','天津市东丽区','东丽区',2,'12','AA',NULL),('1211','天津市西青区','西青区',2,'12','AA',NULL),('1212','天津市津南区','津南区',2,'12','AA',NULL),('1213','天津市北辰区','北辰区',2,'12','AA',NULL),('1214','天津市武清区','武清区',2,'12','AA',NULL),('1215','天津市宝坻区','宝坻区',2,'12','AA',NULL),('1221','天津市宁河县','宁河区',2,'12','AA',NULL),('1223','天津市静海县','静海区',2,'12','AA',NULL),('1225','天津市蓟县','蓟县',2,'12','AA',NULL),('1301','河北省石家庄市','石家庄',2,'13','HB',NULL),('1302','河北省唐山市','唐山',2,'13','HB',NULL),('1303','河北省秦皇岛市','秦皇岛',2,'13','HB',NULL),('1304','河北省邯郸市','邯郸',2,'13','HB',NULL),('1305','河北省邢台市','邢台',2,'13','HB',NULL),('1306','河北省保定市','保定',2,'13','HB',NULL),('1307','河北省张家口市','张家口',2,'13','HB',NULL),('1308','河北省承德市','承德',2,'13','HB',NULL),('1309','河北省沧州市','沧州',2,'13','HB',NULL),('1310','河北省廊坊市','廊坊',2,'13','HB',NULL),('1311','河北省衡水市','衡水',2,'13','HB',NULL),('1401','山西省太原市','太原',2,'14','HB',NULL),('1402','山西省大同市','大同',2,'14','HB',NULL),('1403','山西省阳泉市','阳泉',2,'14','HB',NULL),('1404','山西省长治市','长治',2,'14','HB',NULL),('1405','山西省晋城市','晋城',2,'14','HB',NULL),('1406','山西省朔州市','朔州',2,'14','HB',NULL),('1407','山西省晋中市','晋中',2,'14','HB',NULL),('1408','山西省运城市','运城',2,'14','HB',NULL),('1409','山西省忻州市','忻州',2,'14','HB',NULL),('1410','山西省临汾市','临汾',2,'14','HB',NULL),('1411','山西省吕梁市','吕梁',2,'14','HB',NULL),('1412','山西省临汾市尧都区','临汾尧都区',2,'14','HB',NULL),('1501','内蒙古呼和浩特市','呼和浩特',2,'15','HB',NULL),('1502','内蒙古包头市','包头',2,'15','HB',NULL),('1503','内蒙古乌海市','乌海',2,'15','HB',NULL),('1504','内蒙古赤峰市','赤峰',2,'15','HB',NULL),('1505','内蒙古通辽市','通辽',2,'15','HB',NULL),('1506','内蒙古鄂尔多斯市','鄂尔多斯',2,'15','HB',NULL),('1507','内蒙古呼伦贝尔市','呼伦贝尔',2,'15','HB',NULL),('1508','内蒙古巴彦淖尔市','巴彦淖尔',2,'15','HB',NULL),('1509','内蒙古乌兰察布市','乌兰察布',2,'15','HB',NULL),('1522','内蒙古兴安盟','兴安盟',2,'15','HB',NULL),('1525','内蒙古锡林郭勒盟','锡林郭勒盟',2,'15','HB',NULL),('1526','内蒙古乌兰察布盟','乌兰察布盟',2,'15','HB',NULL),('1529','内蒙古阿拉善盟','阿拉善盟',2,'15','HB',NULL),('2101','辽宁省沈阳市','沈阳',2,'21','DB',NULL),('2102','辽宁省大连市','大连',2,'21','DB',NULL),('2103','辽宁省鞍山市','鞍山',2,'21','DB',NULL),('2104','辽宁省抚顺市','抚顺',2,'21','DB',NULL),('2105','辽宁省本溪市','本溪',2,'21','DB',NULL),('2106','辽宁省丹东市','丹东',2,'21','DB',NULL),('2107','辽宁省锦州市','锦州',2,'21','DB',NULL),('2108','辽宁省营口市','营口',2,'21','DB',NULL),('2109','辽宁省阜新市','阜新',2,'21','DB',NULL),('2110','辽宁省辽阳市','辽阳',2,'21','DB',NULL),('2111','辽宁省盘锦市','盘锦市',2,'21','DB',NULL),('2112','辽宁省铁岭市','铁岭',2,'21','DB',NULL),('2113','辽宁省朝阳市','朝阳',2,'21','DB',NULL),('2114','辽宁省葫芦岛市','葫芦岛市',2,'21','DB',NULL),('2201','吉林省长春市','长春',2,'22','DB',NULL),('2202','吉林省吉林市','吉林',2,'22','DB',NULL),('2203','吉林省四平市','四平',2,'22','DB',NULL),('2204','吉林省辽源市','辽源',2,'22','DB',NULL),('2205','吉林省通化市','通化',2,'22','DB',NULL),('2206','吉林省白山市','白山',2,'22','DB',NULL),('2207','吉林省松原市','松原',2,'22','DB',NULL),('2208','吉林省白城市','白城',2,'22','DB',NULL),('2224','吉林省延边朝鲜族自治州','延边',2,'22','DB',NULL),('2301','黑龙江省哈尔滨市','哈尔滨',2,'23','DB',NULL),('2302','黑龙江省齐齐哈尔市','齐齐哈尔',2,'23','DB',NULL),('2303','黑龙江省鸡西市','鸡西',2,'23','DB',NULL),('2304','黑龙江省鹤岗市','鹤岗',2,'23','DB',NULL),('2305','黑龙江省双鸭山市','双鸭山',2,'23','DB',NULL),('2306','黑龙江省大庆市','大庆',2,'23','DB',NULL),('2307','黑龙江省伊春市','伊春',2,'23','DB',NULL),('2308','黑龙江省佳木斯市','佳木斯',2,'23','DB',NULL),('2309','黑龙江省七台河市','七台河',2,'23','DB',NULL),('2310','黑龙江省牡丹江市','牡丹江',2,'23','DB',NULL),('2311','黑龙江省黑河市','黑河',2,'23','DB',NULL),('2312','黑龙江省绥化市','绥化',2,'23','DB',NULL),('2327','黑龙江省大兴安岭地区','大兴安岭地区',2,'23','DB',NULL),('3101','上海市黄浦区','黄浦区',2,'31','AA',NULL),('3103','上海市卢湾区','卢湾区',2,'31','AA',NULL),('3104','上海市徐汇区','徐汇区',2,'31','AA',NULL),('3105','上海市长宁区','长宁区',2,'31','AA',NULL),('3106','上海市静安区','静安区',2,'31','AA',NULL),('3107','上海市普陀区','普陀区',2,'31','AA',NULL),('3108','上海市闸北区','闸北区',2,'31','AA',NULL),('3109','上海市虹口区','虹口区',2,'31','AA',NULL),('3110','上海市杨浦区','杨浦区',2,'31','AA',NULL),('3112','上海市闵行区','闵行区',2,'31','AA',NULL),('3113','上海市宝山区','宝山区',2,'31','AA',NULL),('3114','上海市嘉定区','嘉定区',2,'31','AA',NULL),('3115','上海市浦东新区','浦东新区',2,'31','AA',NULL),('3116','上海市金山区','金山区',2,'31','AA',NULL),('3117','上海市松江区','松江区',2,'31','AA',NULL),('3118','上海市青浦区','青浦区',2,'31','AA',NULL),('3119','上海市南汇区','南汇区',2,'31','AA',NULL),('3120','上海市奉贤区','奉贤区',2,'31','AA',NULL),('3130','上海市崇明县','崇明县',2,'31','AA',NULL),('3201','江苏省南京市','南京',2,'32','HD',NULL),('3202','江苏省无锡市','无锡',2,'32','HD',NULL),('3203','江苏省徐州市','徐州',2,'32','HD',NULL),('3204','江苏省常州市','常州',2,'32','HD',NULL),('3205','江苏省苏州市','苏州',2,'32','HD',NULL),('3206','江苏省南通市','南通',2,'32','HD',NULL),('3207','江苏省连云港市','连云港',2,'32','HD',NULL),('3208','江苏省淮安市','淮安',2,'32','HD',NULL),('3209','江苏省盐城市','盐城',2,'32','HD',NULL),('3210','江苏省扬州市','扬州',2,'32','HD',NULL),('3211','江苏省镇江市','镇江',2,'32','HD',NULL),('3212','江苏省泰州市','泰州',2,'32','HD',NULL),('3213','江苏省宿迁市','宿迁',2,'32','HD',NULL),('3301','浙江省杭州市','杭州',2,'33','HD',NULL),('3302','浙江省宁波市','宁波',2,'33','HD',NULL),('3303','浙江省温州市','温州',2,'33','HD',NULL),('3304','浙江省嘉兴市','嘉兴',2,'33','HD',NULL),('3305','浙江省湖州市','湖州',2,'33','HD',NULL),('3306','浙江省绍兴市','绍兴',2,'33','HD',NULL),('3307','浙江省金华市','金华',2,'33','HD',NULL),('3308','浙江省衢州市','衢州',2,'33','HD',NULL),('3309','浙江省舟山市','舟山',2,'33','HD',NULL),('3310','浙江省台州市','台州',2,'33','HD',NULL),('3311','浙江省丽水市','丽水',2,'33','HD',NULL),('3401','安徽省合肥市','合肥',2,'34','HD',NULL),('3402','安徽省芜湖市','芜湖',2,'34','HD',NULL),('3403','安徽省蚌埠市','蚌埠',2,'34','HD',NULL),('3404','安徽省淮南市','淮南',2,'34','HD',NULL),('3405','安徽省马鞍山市','马鞍山',2,'34','HD',NULL),('3406','安徽省淮北市','淮北',2,'34','HD',NULL),('3407','安徽省铜陵市','铜陵',2,'34','HD',NULL),('3408','安徽省安庆市','安庆',2,'34','HD',NULL),('3410','安徽省黄山市','黄山',2,'34','HD',NULL),('3411','安徽省滁州市','滁州',2,'34','HD',NULL),('3412','安徽省阜阳市','阜阳',2,'34','HD',NULL),('3413','安徽省宿州市','宿州',2,'34','HD',NULL),('3414','安徽省巢湖市','巢湖',2,'34','HD',NULL),('3415','安徽省六安市','六安',2,'34','HD',NULL),('3416','安徽省亳州市','亳州',2,'34','HD',NULL),('3417','安徽省池州市','池州',2,'34','HD',NULL),('3418','安徽省宣城市','宣城',2,'34','HD',NULL),('3501','福建省福州市','福州',2,'35','HD',NULL),('3502','福建省厦门市','厦门',2,'35','HD',NULL),('3503','福建省莆田市','莆田',2,'35','HD',NULL),('3504','福建省三明市','三明',2,'35','HD',NULL),('3505','福建省泉州市','泉州',2,'35','HD',NULL),('3506','福建省漳州市','漳州',2,'35','HD',NULL),('3507','福建省南平市','南平',2,'35','HD',NULL),('3508','福建省龙岩市','龙岩',2,'35','HD',NULL),('3509','福建省宁德市','宁德',2,'35','HD',NULL),('3601','江西省南昌市','南昌',2,'36','HD',NULL),('3602','江西省景德镇市','景德镇',2,'36','HD',NULL),('3603','江西省萍乡市','萍乡',2,'36','HD',NULL),('3604','江西省九江市','九江',2,'36','HD',NULL),('3605','江西省新余市','新余',2,'36','HD',NULL),('3606','江西省鹰潭市','鹰潭',2,'36','HD',NULL),('3607','江西省赣州市','赣州',2,'36','HD',NULL),('3608','江西省吉安市','吉安',2,'36','HD',NULL),('3609','江西省宜春市','宜春',2,'36','HD',NULL),('3610','江西省抚州市','抚州',2,'36','HD',NULL),('3611','江西省上饶市','上饶',2,'36','HD',NULL),('3701','山东省济南市','济南',2,'37','HD',NULL),('3702','山东省青岛市','青岛',2,'37','HD',NULL),('3703','山东省淄博市','淄博',2,'37','HD',NULL),('3704','山东省枣庄市','枣庄',2,'37','HD',NULL),('3705','山东省东营市','东营',2,'37','HD',NULL),('3706','山东省烟台市','烟台',2,'37','HD',NULL),('3707','山东省潍坊市','潍坊',2,'37','HD',NULL),('3708','山东省济宁市','济宁',2,'37','HD',NULL),('3709','山东省泰安市','泰安',2,'37','HD',NULL),('3710','山东省威海市','威海',2,'37','HD',NULL),('3711','山东省日照市','日照',2,'37','HD',NULL),('3712','山东省莱芜市','莱芜',2,'37','HD',NULL),('3713','山东省临沂市','临沂',2,'37','HD',NULL),('3714','山东省德州市','德州',2,'37','HD',NULL),('3715','山东省聊城市','聊城',2,'37','HD',NULL),('3716','山东省滨州市','滨州',2,'37','HD',NULL),('3717','山东省荷泽市','荷泽',2,'37','HD',NULL),('4101','河南省郑州市','郑州',2,'41','ZN',NULL),('4102','河南省开封市','开封',2,'41','ZN',NULL),('4103','河南省洛阳市','洛阳',2,'41','ZN',NULL),('4104','河南省平顶山市','平顶山',2,'41','ZN',NULL),('4105','河南省安阳市','安阳',2,'41','ZN',NULL),('4106','河南省鹤壁市','鹤壁',2,'41','ZN',NULL),('4107','河南省新乡市','新乡',2,'41','ZN',NULL),('4108','河南省焦作市','焦作',2,'41','ZN',NULL),('4109','河南省濮阳市','濮阳',2,'41','ZN',NULL),('4110','河南省许昌市','许昌',2,'41','ZN',NULL),('4111','河南省漯河市','漯河',2,'41','ZN',NULL),('4112','河南省三门峡市','三门峡',2,'41','ZN',NULL),('4113','河南省南阳市','南阳',2,'41','ZN',NULL),('4114','河南省商丘市','商丘',2,'41','ZN',NULL),('4115','河南省信阳市','信阳',2,'41','ZN',NULL),('4116','河南省周口市','周口',2,'41','ZN',NULL),('4117','河南省驻马店市','驻马店',2,'41','ZN',NULL),('4201','湖北省武汉市','武汉',2,'42','',NULL),('4202','湖北省黄石市','黄石',2,'42','',NULL),('4203','湖北省十堰市','十堰',2,'42','',NULL),('4205','湖北省宜昌市','宜昌',2,'42','',NULL),('4206','湖北省襄樊市','襄樊',2,'42','',NULL),('4207','湖北省鄂州市','鄂州',2,'42','',NULL),('4208','湖北省荆门市','荆门',2,'42','',NULL),('4209','湖北省孝感市','孝感',2,'42','',NULL),('4210','湖北省荆州市','荆州',2,'42','',NULL),('4211','湖北省荆州市市辖区','荆州辖区',2,'42','',NULL),('4212','湖北省咸宁市','咸宁',2,'42','',NULL),('4213','湖北省随州市','随州',2,'42','',NULL),('4228','湖北省恩施土家族苗族自治州','恩施',2,'42','',NULL),('4290','湖北省省直辖行政单位','直辖行政单位',2,'42','',NULL),('4294','湖北省仙桃市','仙桃',2,'42','',NULL),('4295','湖北省潜江市','潜江',2,'42','',NULL),('4296','湖北省天门市','天门',2,'42','',NULL),('4301','湖南省长沙市','长沙',2,'43','ZN',NULL),('4302','湖南省株洲市','株洲',2,'43','ZN',NULL),('4303','湖南省湘潭市','湘潭',2,'43','ZN',NULL),('4304','湖南省衡阳市','衡阳',2,'43','ZN',NULL),('4305','湖南省邵阳市','邵阳',2,'43','ZN',NULL),('4306','湖南省岳阳市','岳阳',2,'43','ZN',NULL),('4307','湖南省常德市','常德',2,'43','ZN',NULL),('4308','湖南省张家界市','张家界',2,'43','ZN',NULL),('4309','湖南省益阳市','益阳',2,'43','ZN',NULL),('4310','湖南省郴州市','郴州',2,'43','ZN',NULL),('4311','湖南省永州市','永州',2,'43','ZN',NULL),('4312','湖南省郴州市北湖区','郴州北湖区',2,'43','ZN',NULL),('4313','湖南省郴州市苏仙区','郴州苏仙区',2,'43','ZN',NULL),('4331','湖南省湘西土家族苗族自治州','湘西',2,'43','ZN',NULL),('4401','广东省广州市','广州',2,'44','ZN',NULL),('4402','广东省韶关市','韶关',2,'44','ZN',NULL),('4403','广东省深圳市','深圳',2,'44','ZN',NULL),('4404','广东省珠海市','珠海',2,'44','ZN',NULL),('4405','广东省汕头市','汕头',2,'44','ZN',NULL),('4406','广东省佛山市','佛山',2,'44','ZN',NULL),('4407','广东省江门市','江门',2,'44','ZN',NULL),('4408','广东省湛江市','湛江',2,'44','ZN',NULL),('4409','广东省茂名市','茂名',2,'44','ZN',NULL),('4412','广东省肇庆市','肇庆',2,'44','ZN',NULL),('4413','广东省惠州市','惠州',2,'44','ZN',NULL),('4414','广东省梅州市','梅州',2,'44','ZN',NULL),('4415','广东省汕尾市','汕尾',2,'44','ZN',NULL),('4416','广东省河源市','河源',2,'44','ZN',NULL),('4417','广东省阳江市','阳江',2,'44','ZN',NULL),('4418','广东省清远市','清远',2,'44','ZN',NULL),('4419','广东省东莞市','东莞',2,'44','ZN',NULL),('4420','广东省中山市','中山',2,'44','ZN',NULL),('4451','广东省潮州市','潮州',2,'44','ZN',NULL),('4452','广东省揭阳市','揭阳',2,'44','ZN',NULL),('4453','广东省云浮市','云浮',2,'44','ZN',NULL),('4501','广西南宁市','南宁市',2,'45','ZN',NULL),('4502','广西柳州市','柳州市',2,'45','ZN',NULL),('4503','广西桂林市','桂林市',2,'45','ZN',NULL),('4504','广西梧州市','梧州市',2,'45','ZN',NULL),('4505','广西北海市','北海市',2,'45','ZN',NULL),('4506','广西防城港市','城港',2,'45','ZN',NULL),('4507','广西钦州市','钦州市',2,'45','ZN',NULL),('4508','广西贵港市','贵港市',2,'45','ZN',NULL),('4509','广西玉林市','玉林市',2,'45','ZN',NULL),('4510','广西百色市','百色市',2,'45','ZN',NULL),('4511','广西贺州市','贺州市',2,'45','ZN',NULL),('4512','广西河池市','河池市',2,'45','ZN',NULL),('4513','广西来宾市','来宾市',2,'45','ZN',NULL),('4514','广西崇左市','崇左市',2,'45','ZN',NULL),('4601','海南省海口市','海口',2,'46','ZN',NULL),('4602','海南省三亚市','三亚',2,'46','ZN',NULL),('4691','海南省五指山市','五指山',2,'46','ZN',NULL),('4692','海南省琼海市','琼海',2,'46','ZN',NULL),('4693','海南省儋州市','儋州',2,'46','ZN',NULL),('4695','海南省文昌市','文昌',2,'46','ZN',NULL),('4696','海南省万宁市','万宁',2,'46','ZN',NULL),('4697','海南省东方市','东方',2,'46','ZN',NULL),('5001','重庆市万州区','万州区',2,'50','AA',NULL),('5002','重庆市涪陵区','涪陵区',2,'50','AA',NULL),('5003','重庆市渝中区','渝中区',2,'50','AA',NULL),('5004','重庆市大渡口区','大渡口区',2,'50','AA',NULL),('5005','重庆市江北区','江北区',2,'50','AA',NULL),('5006','重庆市沙坪坝区','沙坪坝区',2,'50','AA',NULL),('5007','重庆市九龙坡区','九龙坡区',2,'50','AA',NULL),('5008','重庆市南岸区','南岸区',2,'50','AA',NULL),('5009','重庆市北碚区','北碚区',2,'50','AA',NULL),('5010','重庆市万盛区','万盛区',2,'50','AA',NULL),('5011','重庆市双桥区','双桥区',2,'50','AA',NULL),('5012','重庆市渝北区','渝北区',2,'50','AA',NULL),('5013','重庆市巴南区','巴南区',2,'50','AA',NULL),('5014','重庆市黔江区','黔江区',2,'50','AA',NULL),('5015','重庆市长寿区','长寿区',2,'50','AA',NULL),('5022','重庆市綦江县','綦江县',2,'50','AA',NULL),('5023','重庆市潼南县','潼南县',2,'50','AA',NULL),('5024','重庆市铜梁县','铜梁县',2,'50','AA',NULL),('5025','重庆市大足县','大足县',2,'50','AA',NULL),('5026','重庆市荣昌县','荣昌县',2,'50','AA',NULL),('5027','重庆市璧山县','璧山县',2,'50','AA',NULL),('5028','重庆市梁平县','梁平县',2,'50','AA',NULL),('5029','重庆市城口县','城口县',2,'50','AA',NULL),('5030','重庆市丰都县','丰都县',2,'50','AA',NULL),('5031','重庆市垫江县','垫江县',2,'50','AA',NULL),('5032','重庆市武隆县','武隆县',2,'50','AA',NULL),('5033','重庆市忠县','市忠县',2,'50','AA',NULL),('5034','重庆市开县','市开县',2,'50','AA',NULL),('5035','重庆市云阳县','云阳县',2,'50','AA',NULL),('5036','重庆市奉节县','奉节县',2,'50','AA',NULL),('5037','重庆市巫山县','巫山县',2,'50','AA',NULL),('5038','重庆市巫溪县','巫溪县',2,'50','AA',NULL),('5040','重庆市石柱土家族自治县','石柱',2,'50','AA',NULL),('5041','重庆市秀山土家族苗族自治县','秀山',2,'50','AA',NULL),('5042','重庆市酉阳土家族苗族自治县','酉阳',2,'50','AA',NULL),('5043','重庆市彭水苗族土家族自治县','彭水',2,'50','AA',NULL),('5081','重庆市江津市','江津市',2,'50','AA',NULL),('5082','重庆市合川市','合川市',2,'50','AA',NULL),('5083','重庆市永川市','永川市',2,'50','AA',NULL),('5084','重庆市南川市','南川市',2,'50','AA',NULL),('5101','四川省成都市','成都',2,'51','XN',NULL),('5103','四川省自贡市','自贡',2,'51','XN',NULL),('5104','四川省攀枝花市','攀枝花',2,'51','XN',NULL),('5105','四川省泸州市','泸州',2,'51','XN',NULL),('5106','四川省德阳市','德阳',2,'51','XN',NULL),('5107','四川省绵阳市','绵阳',2,'51','XN',NULL),('5108','四川省广元市','广元',2,'51','XN',NULL),('5109','四川省遂宁市','遂宁',2,'51','XN',NULL),('5110','四川省内江市','内江',2,'51','XN',NULL),('5111','四川省内江市市辖区','内江辖区',2,'51','XN',NULL),('5112','四川省内江市市中区','内江中区',2,'51','XN',NULL),('5113','四川省南充市','南充',2,'51','XN',NULL),('5114','四川省眉山市','眉山',2,'51','XN',NULL),('5115','四川省宜宾市','宜宾',2,'51','XN',NULL),('5116','四川省广安市','广安',2,'51','XN',NULL),('5117','四川省达州市','达州',2,'51','XN',NULL),('5118','四川省雅安市','雅安',2,'51','XN',NULL),('5119','四川省巴中市','巴中',2,'51','XN',NULL),('5120','四川省资阳市','资阳',2,'51','XN',NULL),('5121','四川省资阳市市辖区','资阳辖区',2,'51','XN',NULL),('5122','四川省资阳市雁江区','资阳雁江区',2,'51','XN',NULL),('5132','四川省阿坝藏族羌族自治州','阿坝',2,'51','XN',NULL),('5133','四川省甘孜藏族自治州','甘孜',2,'51','XN',NULL),('5134','四川省凉山彝族自治州','凉山',2,'51','XN',NULL),('5201','贵州省贵阳市','贵阳',2,'52','XN',NULL),('5202','贵州省六盘水市','六盘水',2,'52','XN',NULL),('5203','贵州省遵义市','遵义',2,'52','XN',NULL),('5204','贵州省安顺市','安顺',2,'52','XN',NULL),('5222','贵州省铜仁地区','铜仁地区',2,'52','XN',NULL),('5223','贵州省黔西南布依族苗族自治州','黔西南',2,'52','XN',NULL),('5224','贵州省毕节地区','毕节地区',2,'52','XN',NULL),('5226','贵州省黔东南苗族侗族自治州','黔东南',2,'52','XN',NULL),('5227','贵州省黔南布依族苗族自治州','黔南',2,'52','XN',NULL),('5301','云南省昆明市','昆明',2,'53','XN',NULL),('5303','云南省曲靖市','曲靖',2,'53','XN',NULL),('5304','云南省玉溪市','玉溪',2,'53','XN',NULL),('5305','云南省保山市','保山',2,'53','XN',NULL),('5306','云南省昭通市','昭通',2,'53','XN',NULL),('5307','云南省丽江市','丽江',2,'53','XN',NULL),('5308','云南省思茅市','思茅',2,'53','XN',NULL),('5309','云南省临沧市','临沧',2,'53','XN',NULL),('5323','云南省楚雄彝族自治州','楚雄',2,'53','XN',NULL),('5325','云南省红河哈尼族彝族自治州','红河',2,'53','XN',NULL),('5326','云南省文山壮族苗族自治州','文山',2,'53','XN',NULL),('5328','云南省西双版纳傣族自治州','西双版纳',2,'53','XN',NULL),('5329','云南省大理白族自治州','大理',2,'53','XN',NULL),('5331','云南省德宏傣族景颇族自治州','德宏',2,'53','XN',NULL),('5333','云南省怒江僳僳族自治州','怒江僳',2,'53','XN',NULL),('5334','云南省迪庆藏族自治州','迪庆',2,'53','XN',NULL),('5401','西藏拉萨市','拉萨',2,'54','XN',NULL),('5421','西藏昌都地区','昌都',2,'54','XN',NULL),('5422','西藏山南地区','山南',2,'54','XN',NULL),('5423','西藏日喀则地区','日喀则',2,'54','XN',NULL),('5424','西藏那曲地区','那曲',2,'54','XN',NULL),('5425','西藏阿里地区','阿里',2,'54','XN',NULL),('5426','西藏林芝地区','林芝',2,'54','XN',NULL),('6101','陕西省西安市','西安',2,'61','XB',NULL),('6102','陕西省铜川市','铜川',2,'61','XB',NULL),('6103','陕西省宝鸡市','宝鸡',2,'61','XB',NULL),('6104','陕西省咸阳市','咸阳',2,'61','XB',NULL),('6105','陕西省渭南市','渭南',2,'61','XB',NULL),('6106','陕西省延安市','延安',2,'61','XB',NULL),('6107','陕西省汉中市','汉中',2,'61','XB',NULL),('6108','陕西省榆林市','榆林',2,'61','XB',NULL),('6109','陕西省安康市','安康',2,'61','XB',NULL),('6110','陕西省商洛市','商洛',2,'61','XB',NULL),('6201','甘肃省兰州市','兰州',2,'62','XB',NULL),('6202','甘肃省嘉峪关市','嘉峪关',2,'62','XB',NULL),('6203','甘肃省金昌市','金昌',2,'62','XB',NULL),('6204','甘肃省白银市','白银',2,'62','XB',NULL),('6205','甘肃省天水市','天水',2,'62','XB',NULL),('6206','甘肃省武威市','武威',2,'62','XB',NULL),('6207','甘肃省张掖市','张掖',2,'62','XB',NULL),('6208','甘肃省平凉市','平凉',2,'62','XB',NULL),('6209','甘肃省酒泉市','酒泉',2,'62','XB',NULL),('6210','甘肃省庆阳市','庆阳',2,'62','XB',NULL),('6211','甘肃省庆阳市市辖区','庆阳辖区',2,'62','XB',NULL),('6212','甘肃省陇南市','陇南',2,'62','XB',NULL),('6229','甘肃省临夏回族自治州','临夏',2,'62','XB',NULL),('6230','甘肃省甘南藏族自治州','甘南',2,'62','XB',NULL),('6231','甘肃省合作市','合作',2,'62','XB',NULL),('6301','青海省西宁市','西宁',2,'63','XB',NULL),('6321','青海省海东地区','海东',2,'63','XB',NULL),('6322','青海省海北藏族自治州','海北州',2,'63','XB',NULL),('6323','青海省黄南藏族自治州','黄南州',2,'63','XB',NULL),('6325','青海省海南藏族自治州','海南州',2,'63','XB',NULL),('6326','青海省果洛藏族自治州','果洛州',2,'63','XB',NULL),('6327','青海省玉树藏族自治州','玉树州',2,'63','XB',NULL),('6328','青海省海西蒙古族藏族自治州','海西州',2,'63','XB',NULL),('6401','宁夏银川市','银川市',2,'64','XB',NULL),('6402','宁夏石嘴山市','嘴山',2,'64','XB',NULL),('6403','宁夏吴忠市','吴忠市',2,'64','XB',NULL),('6404','宁夏固原市','固原市',2,'64','XB',NULL),('6405','宁夏中卫市','中卫市',2,'64','XB',NULL),('6501','新疆乌鲁木齐市','乌鲁木齐',2,'65','XB',NULL),('6502','新疆克拉玛依市','克拉玛依',2,'65','XB',NULL),('6521','新疆克拉玛依市吐鲁番地区','吐鲁番',2,'65','XB',NULL),('6522','新疆哈密地区','哈密',2,'65','XB',NULL),('6523','新疆昌吉回族自治州','昌吉',2,'65','XB',NULL),('6527','新疆博尔塔拉蒙古自治州','博尔塔拉州',2,'65','XB',NULL),('6528','新疆巴音郭楞蒙古自治州','巴音郭楞州',2,'65','XB',NULL),('6529','新疆阿克苏地区','阿克苏',2,'65','XB',NULL),('6530','新疆克孜勒苏柯尔克孜自治州','克孜勒苏柯尔克孜州',2,'65','XB',NULL),('6531','新疆喀什地区','喀什',2,'65','XB',NULL),('6532','新疆和田地区','和田',2,'65','XB',NULL),('6540','新疆伊宁市','伊宁',2,'65','XB',NULL),('6542','新疆塔城地区','塔城',2,'65','XB',NULL),('6543','新疆奎屯市','奎屯市',2,'65','XB',NULL),('6591','新疆石河子市','石河子',2,'65','XB',NULL),('6592','新疆阿拉尔市','阿拉尔',2,'65','XB',NULL),('6593','新疆图木舒克市','图木舒克',2,'65','XB',NULL),('6594','新疆五家渠市','五家渠',2,'65','XB',NULL);
/*!40000 ALTER TABLE `cn_city` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cn_pq`
--

DROP TABLE IF EXISTS `cn_pq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cn_pq` (
  `No` varchar(50) NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(50) DEFAULT NULL COMMENT '名称',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='片区';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cn_pq`
--

LOCK TABLES `cn_pq` WRITE;
/*!40000 ALTER TABLE `cn_pq` DISABLE KEYS */;
INSERT INTO `cn_pq` VALUES ('AA','城市'),('DB','东北'),('HB','华北'),('HD','华东'),('XB','西北'),('XN','西南'),('ZN','中南'),('ZZ','香澳台');
/*!40000 ALTER TABLE `cn_pq` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cn_sf`
--

DROP TABLE IF EXISTS `cn_sf`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cn_sf` (
  `No` varchar(2) NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(50) DEFAULT NULL COMMENT '名称',
  `Names` varchar(50) DEFAULT NULL COMMENT '小名称',
  `JC` varchar(50) DEFAULT NULL COMMENT '简称',
  `FK_PQ` varchar(100) DEFAULT NULL COMMENT '片区,外键:对应物理表:CN_PQ,表描述:片区',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='省份';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cn_sf`
--

LOCK TABLES `cn_sf` WRITE;
/*!40000 ALTER TABLE `cn_sf` DISABLE KEYS */;
INSERT INTO `cn_sf` VALUES ('11','北京','北京市','京','AA'),('12','天津','天津市','津','AA'),('13','河北','河北省','冀','HB'),('14','山西','山西省','晋','HB'),('15','内蒙','内蒙古自治区','蒙','HB'),('21','辽宁','辽宁省','辽','DB'),('22','吉林','吉林省','吉','DB'),('23','黑龙江','黑龙江省','黑','DB'),('31','上海','上海市','沪','AA'),('32','江苏','江苏省','苏','HD'),('33','浙江','浙江省','浙','HD'),('34','安徽','安徽省','皖','HD'),('35','福建','福建省','闽','HD'),('36','江西','江西省','赣','HD'),('37','山东','山东省','鲁','HD'),('41','河南','河南省','豫','ZN'),('42','湖北','湖北省','鄂','ZN'),('43','湖南','湖南省','湘','ZN'),('44','广东','广东省','粤','ZN'),('45','广西','广西壮族自治区','桂','ZN'),('46','海南','海南省','琼','ZN'),('50','重庆','重庆市','渝','AA'),('51','四川','四川省','川','XN'),('52','贵州','贵州省','贵','XN'),('53','云南','云南省','云','XN'),('54','西藏','西藏自治区','藏','XN'),('61','陕西','陕西省','陕','XB'),('62','甘肃','甘肃省','甘','XB'),('63','青海','青海省','青','XB'),('64','宁夏','宁夏回族自治区','宁','XB'),('65','新疆','新疆维吾尔自治区','新','XB'),('71','台湾','台湾省','台','ZZ'),('81','香港','香港特别行政区','港','ZZ'),('82','澳门','澳门特别行政区','澳','ZZ');
/*!40000 ALTER TABLE `cn_sf` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gpm_app`
--

DROP TABLE IF EXISTS `gpm_app`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gpm_app` (
  `No` varchar(30) NOT NULL COMMENT '编号 - 主键',
  `AppModel` int(11) DEFAULT NULL COMMENT '应用类型,枚举类型:0 BS系统;1 CS系统;',
  `Name` text COMMENT '名称',
  `FK_AppSort` varchar(100) DEFAULT NULL COMMENT '类别,外键:对应物理表:GPM_AppSort,表描述:系统类别',
  `IsEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `Url` text COMMENT '默认连接',
  `SubUrl` text COMMENT '第二连接',
  `UidControl` varchar(100) DEFAULT NULL COMMENT '用户名控件',
  `PwdControl` varchar(100) DEFAULT NULL COMMENT '密码控件',
  `ActionType` int(11) DEFAULT NULL COMMENT '提交类型,枚举类型:0 GET;1 POST;',
  `SSOType` int(11) DEFAULT NULL COMMENT '登录方式,枚举类型:0 SID验证;1 连接;2 表单提交;3 不传值;',
  `OpenWay` int(11) DEFAULT NULL COMMENT '打开方式,枚举类型:0 新窗口;1 本窗口;2 覆盖新窗口;',
  `RefMenuNo` text COMMENT '关联菜单编号',
  `AppRemark` varchar(500) DEFAULT NULL COMMENT '备注',
  `Idx` int(11) DEFAULT NULL COMMENT '显示顺序',
  `MyFileName` varchar(300) DEFAULT NULL COMMENT 'ICON',
  `MyFilePath` varchar(300) DEFAULT NULL COMMENT 'MyFilePath',
  `MyFileExt` varchar(20) DEFAULT NULL COMMENT 'MyFileExt',
  `WebPath` varchar(300) DEFAULT NULL COMMENT 'WebPath',
  `MyFileH` int(11) DEFAULT NULL COMMENT 'MyFileH',
  `MyFileW` int(11) DEFAULT NULL COMMENT 'MyFileW',
  `MyFileSize` float DEFAULT NULL COMMENT 'MyFileSize',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gpm_app`
--

LOCK TABLES `gpm_app` WRITE;
/*!40000 ALTER TABLE `gpm_app` DISABLE KEYS */;
/*!40000 ALTER TABLE `gpm_app` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gpm_appsort`
--

DROP TABLE IF EXISTS `gpm_appsort`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gpm_appsort` (
  `No` varchar(2) NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(300) DEFAULT NULL COMMENT '名称',
  `Idx` int(11) DEFAULT NULL COMMENT '显示顺序',
  `RefMenuNo` varchar(300) DEFAULT NULL COMMENT '关联的菜单编号',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统类别';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gpm_appsort`
--

LOCK TABLES `gpm_appsort` WRITE;
/*!40000 ALTER TABLE `gpm_appsort` DISABLE KEYS */;
/*!40000 ALTER TABLE `gpm_appsort` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gpm_bar`
--

DROP TABLE IF EXISTS `gpm_bar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gpm_bar` (
  `No` varchar(200) NOT NULL COMMENT '编号 - 主键',
  `Name` text COMMENT '名称',
  `Title` text COMMENT '标题',
  `OpenWay` int(11) DEFAULT NULL COMMENT '打开方式,枚举类型:0 新窗口;1 本窗口;2 覆盖新窗口;',
  `IsLine` int(11) DEFAULT '0' COMMENT '是否独占一行',
  `MoreUrl` text COMMENT '更多标签Url',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='信息块';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gpm_bar`
--

LOCK TABLES `gpm_bar` WRITE;
/*!40000 ALTER TABLE `gpm_bar` DISABLE KEYS */;
/*!40000 ALTER TABLE `gpm_bar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gpm_baremp`
--

DROP TABLE IF EXISTS `gpm_baremp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gpm_baremp` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `FK_Bar` varchar(90) DEFAULT NULL COMMENT '信息块编号',
  `FK_Emp` varchar(90) DEFAULT NULL COMMENT '人员编号',
  `Title` text COMMENT '标题',
  `IsShow` int(11) DEFAULT NULL COMMENT '是否显示',
  `Idx` int(11) DEFAULT NULL COMMENT '显示顺序',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='人员信息块';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gpm_baremp`
--

LOCK TABLES `gpm_baremp` WRITE;
/*!40000 ALTER TABLE `gpm_baremp` DISABLE KEYS */;
/*!40000 ALTER TABLE `gpm_baremp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gpm_empapp`
--

DROP TABLE IF EXISTS `gpm_empapp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gpm_empapp` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `FK_Emp` varchar(50) DEFAULT NULL COMMENT '操作员',
  `FK_App` varchar(50) DEFAULT NULL COMMENT '系统',
  `Name` text COMMENT '系统-名称',
  `Url` text COMMENT '连接',
  `MyFileName` varchar(300) DEFAULT NULL COMMENT '图标',
  `MyFilePath` varchar(300) DEFAULT NULL COMMENT 'MyFilePath',
  `MyFileExt` varchar(20) DEFAULT NULL COMMENT 'MyFileExt',
  `WebPath` varchar(300) DEFAULT NULL COMMENT 'WebPath',
  `MyFileH` int(11) DEFAULT NULL COMMENT 'MyFileH',
  `MyFileW` int(11) DEFAULT NULL COMMENT 'MyFileW',
  `MyFileSize` float DEFAULT NULL COMMENT 'MyFileSize',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='管理员与系统权限';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gpm_empapp`
--

LOCK TABLES `gpm_empapp` WRITE;
/*!40000 ALTER TABLE `gpm_empapp` DISABLE KEYS */;
/*!40000 ALTER TABLE `gpm_empapp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gpm_empmenu`
--

DROP TABLE IF EXISTS `gpm_empmenu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gpm_empmenu` (
  `FK_Menu` varchar(50) NOT NULL COMMENT '菜单功能 - 主键',
  `FK_Emp` varchar(100) NOT NULL COMMENT '操作员 - 主键',
  `IsChecked` int(11) DEFAULT NULL COMMENT '是否选中',
  PRIMARY KEY (`FK_Menu`,`FK_Emp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='人员菜单功能';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gpm_empmenu`
--

LOCK TABLES `gpm_empmenu` WRITE;
/*!40000 ALTER TABLE `gpm_empmenu` DISABLE KEYS */;
/*!40000 ALTER TABLE `gpm_empmenu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gpm_group`
--

DROP TABLE IF EXISTS `gpm_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gpm_group` (
  `No` varchar(3) NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(300) DEFAULT NULL COMMENT '名称',
  `Idx` int(11) DEFAULT NULL COMMENT '显示顺序',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='权限组';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gpm_group`
--

LOCK TABLES `gpm_group` WRITE;
/*!40000 ALTER TABLE `gpm_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `gpm_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gpm_groupemp`
--

DROP TABLE IF EXISTS `gpm_groupemp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gpm_groupemp` (
  `FK_Group` varchar(50) NOT NULL COMMENT '权限组 - 主键',
  `FK_Emp` varchar(100) NOT NULL COMMENT '人员,主外键:对应物理表:Port_Emp,表描述:用户',
  PRIMARY KEY (`FK_Group`,`FK_Emp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='权限组人员';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gpm_groupemp`
--

LOCK TABLES `gpm_groupemp` WRITE;
/*!40000 ALTER TABLE `gpm_groupemp` DISABLE KEYS */;
/*!40000 ALTER TABLE `gpm_groupemp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gpm_groupmenu`
--

DROP TABLE IF EXISTS `gpm_groupmenu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gpm_groupmenu` (
  `FK_Group` varchar(50) NOT NULL COMMENT '权限组 - 主键',
  `FK_Menu` varchar(50) NOT NULL COMMENT '菜单 - 主键',
  `IsChecked` int(11) DEFAULT NULL COMMENT '是否选中',
  PRIMARY KEY (`FK_Group`,`FK_Menu`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='权限组菜单';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gpm_groupmenu`
--

LOCK TABLES `gpm_groupmenu` WRITE;
/*!40000 ALTER TABLE `gpm_groupmenu` DISABLE KEYS */;
/*!40000 ALTER TABLE `gpm_groupmenu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gpm_groupstation`
--

DROP TABLE IF EXISTS `gpm_groupstation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gpm_groupstation` (
  `FK_Group` varchar(50) NOT NULL COMMENT '权限组 - 主键',
  `FK_Station` varchar(100) NOT NULL COMMENT '岗位,主外键:对应物理表:Port_Station,表描述:岗位',
  PRIMARY KEY (`FK_Group`,`FK_Station`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='权限组岗位';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gpm_groupstation`
--

LOCK TABLES `gpm_groupstation` WRITE;
/*!40000 ALTER TABLE `gpm_groupstation` DISABLE KEYS */;
/*!40000 ALTER TABLE `gpm_groupstation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gpm_menu`
--

DROP TABLE IF EXISTS `gpm_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gpm_menu` (
  `No` varchar(4) NOT NULL COMMENT '功能编号 - 主键',
  `ParentNo` varchar(100) DEFAULT NULL COMMENT '父节点,外键:对应物理表:GPM_Menu,表描述:系统',
  `Name` varchar(300) DEFAULT NULL COMMENT '名称',
  `Idx` int(11) DEFAULT NULL COMMENT '顺序号',
  `MenuType` int(11) DEFAULT NULL COMMENT '菜单类型,枚举类型:0 系统根目录;1 系统类别;2 系统;3 目录;4 功能;5 功能控制点;',
  `FK_App` varchar(100) DEFAULT NULL COMMENT '系统,外键:对应物理表:GPM_App,表描述:系统',
  `OpenWay` int(11) DEFAULT NULL COMMENT '打开方式,枚举类型:0 新窗口;1 本窗口;2 覆盖新窗口;',
  `Url` text COMMENT '连接',
  `IsEnable` int(11) DEFAULT NULL COMMENT '是否启用?',
  `Icon` varchar(500) DEFAULT NULL COMMENT 'Icon',
  `MenuCtrlWay` int(11) DEFAULT '0' COMMENT '控制方式',
  `Flag` varchar(500) DEFAULT NULL COMMENT '标记',
  `Tag1` varchar(500) DEFAULT NULL COMMENT 'Tag1',
  `Tag2` varchar(500) DEFAULT NULL COMMENT 'Tag2',
  `Tag3` varchar(500) DEFAULT NULL COMMENT 'Tag3',
  `MyFileName` varchar(300) DEFAULT NULL COMMENT '图标',
  `MyFilePath` varchar(300) DEFAULT NULL COMMENT 'MyFilePath',
  `MyFileExt` varchar(20) DEFAULT NULL COMMENT 'MyFileExt',
  `WebPath` varchar(300) DEFAULT NULL COMMENT '图标',
  `MyFileH` int(11) DEFAULT NULL COMMENT 'MyFileH',
  `MyFileW` int(11) DEFAULT NULL COMMENT 'MyFileW',
  `MyFileSize` float DEFAULT NULL COMMENT 'MyFileSize',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gpm_menu`
--

LOCK TABLES `gpm_menu` WRITE;
/*!40000 ALTER TABLE `gpm_menu` DISABLE KEYS */;
/*!40000 ALTER TABLE `gpm_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gpm_persetting`
--

DROP TABLE IF EXISTS `gpm_persetting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gpm_persetting` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `FK_Emp` varchar(200) DEFAULT NULL COMMENT '人员',
  `FK_App` varchar(200) DEFAULT NULL COMMENT '系统',
  `UserNo` varchar(200) DEFAULT NULL COMMENT 'UserNo',
  `UserPass` varchar(200) DEFAULT NULL COMMENT 'UserPass',
  `Idx` int(11) DEFAULT NULL COMMENT '显示顺序',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='个人设置';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gpm_persetting`
--

LOCK TABLES `gpm_persetting` WRITE;
/*!40000 ALTER TABLE `gpm_persetting` DISABLE KEYS */;
/*!40000 ALTER TABLE `gpm_persetting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gpm_stationmenu`
--

DROP TABLE IF EXISTS `gpm_stationmenu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gpm_stationmenu` (
  `FK_Station` varchar(50) NOT NULL COMMENT '岗位 - 主键',
  `FK_Menu` varchar(50) NOT NULL COMMENT '菜单 - 主键',
  `IsChecked` int(11) DEFAULT NULL COMMENT '是否选中',
  PRIMARY KEY (`FK_Station`,`FK_Menu`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='岗位菜单';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gpm_stationmenu`
--

LOCK TABLES `gpm_stationmenu` WRITE;
/*!40000 ALTER TABLE `gpm_stationmenu` DISABLE KEYS */;
/*!40000 ALTER TABLE `gpm_stationmenu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hy_dataincomeway`
--

DROP TABLE IF EXISTS `hy_dataincomeway`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hy_dataincomeway` (
  `No` varchar(20) NOT NULL COMMENT '编号',
  `Name` varchar(300) DEFAULT NULL COMMENT '名称',
  `BeiZhui` text COMMENT '适应对象&优缺点',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hy_dataincomeway`
--

LOCK TABLES `hy_dataincomeway` WRITE;
/*!40000 ALTER TABLE `hy_dataincomeway` DISABLE KEYS */;
/*!40000 ALTER TABLE `hy_dataincomeway` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hy_datasort`
--

DROP TABLE IF EXISTS `hy_datasort`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hy_datasort` (
  `No` varchar(20) NOT NULL COMMENT '编号',
  `Name` varchar(300) DEFAULT NULL COMMENT '名称',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hy_datasort`
--

LOCK TABLES `hy_datasort` WRITE;
/*!40000 ALTER TABLE `hy_datasort` DISABLE KEYS */;
/*!40000 ALTER TABLE `hy_datasort` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hy_deptsort`
--

DROP TABLE IF EXISTS `hy_deptsort`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hy_deptsort` (
  `FK_Dept` varchar(100) NOT NULL COMMENT '部门',
  `FK_Sort` varchar(100) NOT NULL COMMENT '类别',
  PRIMARY KEY (`FK_Dept`,`FK_Sort`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hy_deptsort`
--

LOCK TABLES `hy_deptsort` WRITE;
/*!40000 ALTER TABLE `hy_deptsort` DISABLE KEYS */;
/*!40000 ALTER TABLE `hy_deptsort` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_gen_table`
--

DROP TABLE IF EXISTS `js_gen_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `js_gen_table` (
  `table_name` varchar(64) NOT NULL COMMENT '表名',
  `class_name` varchar(100) NOT NULL COMMENT '实体类名称',
  `comments` varchar(500) NOT NULL COMMENT '表说明',
  `parent_table_name` varchar(64) DEFAULT NULL COMMENT '关联父表的表名',
  `parent_table_fk_name` varchar(64) DEFAULT NULL COMMENT '本表关联父表的外键名',
  `data_source_name` varchar(64) DEFAULT NULL COMMENT '数据源名称',
  `tpl_category` varchar(200) DEFAULT NULL COMMENT '使用的模板',
  `package_name` varchar(500) DEFAULT NULL COMMENT '生成包路径',
  `module_name` varchar(30) DEFAULT NULL COMMENT '生成模块名',
  `sub_module_name` varchar(30) DEFAULT NULL COMMENT '生成子模块名',
  `function_name` varchar(200) DEFAULT NULL COMMENT '生成功能名',
  `function_name_simple` varchar(50) DEFAULT NULL COMMENT '生成功能名（简写）',
  `function_author` varchar(50) DEFAULT NULL COMMENT '生成功能作者',
  `gen_base_dir` varchar(1000) DEFAULT NULL COMMENT '生成基础路径',
  `options` varchar(1000) DEFAULT NULL COMMENT '其它生成选项',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) DEFAULT NULL COMMENT '备注信息',
  PRIMARY KEY (`table_name`),
  KEY `idx_gen_table_ptn` (`parent_table_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='代码生成表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_gen_table`
--

LOCK TABLES `js_gen_table` WRITE;
/*!40000 ALTER TABLE `js_gen_table` DISABLE KEYS */;
INSERT INTO `js_gen_table` VALUES ('test_data','TestData','测试数据',NULL,NULL,NULL,'crud','com.jeesite.modules','test','','测试数据','数据','ThinkGem',NULL,'{\"isHaveDelete\":\"1\",\"isFileUpload\":\"1\",\"isHaveDisableEnable\":\"1\",\"isImageUpload\":\"1\"}','system','2018-12-04 11:24:16','system','2018-12-04 11:24:16',NULL),('test_data_child','TestDataChild','测试数据子表','test_data','test_data_id',NULL,'crud','com.jeesite.modules','test','','测试子表','数据','ThinkGem',NULL,NULL,'system','2018-12-04 11:24:20','system','2018-12-04 11:24:20',NULL),('test_tree','TestTree','测试树表',NULL,NULL,NULL,'treeGrid','com.jeesite.modules','test','','测试树表','数据','ThinkGem',NULL,'{\"treeViewName\":\"tree_name\",\"isHaveDelete\":\"1\",\"treeViewCode\":\"tree_code\",\"isFileUpload\":\"1\",\"isHaveDisableEnable\":\"1\",\"isImageUpload\":\"1\"}','system','2018-12-04 11:24:23','system','2018-12-04 11:24:23',NULL);
/*!40000 ALTER TABLE `js_gen_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_gen_table_column`
--

DROP TABLE IF EXISTS `js_gen_table_column`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `js_gen_table_column` (
  `id` varchar(64) NOT NULL COMMENT '编号',
  `table_name` varchar(64) NOT NULL COMMENT '表名',
  `column_name` varchar(64) NOT NULL COMMENT '列名',
  `column_sort` decimal(10,0) DEFAULT NULL COMMENT '列排序（升序）',
  `column_type` varchar(100) NOT NULL COMMENT '类型',
  `column_label` varchar(50) DEFAULT NULL COMMENT '列标签名',
  `comments` varchar(500) NOT NULL COMMENT '列备注说明',
  `attr_name` varchar(200) NOT NULL COMMENT '类的属性名',
  `attr_type` varchar(200) NOT NULL COMMENT '类的属性类型',
  `is_pk` char(1) DEFAULT NULL COMMENT '是否主键',
  `is_null` char(1) DEFAULT NULL COMMENT '是否可为空',
  `is_insert` char(1) DEFAULT NULL COMMENT '是否插入字段',
  `is_update` char(1) DEFAULT NULL COMMENT '是否更新字段',
  `is_list` char(1) DEFAULT NULL COMMENT '是否列表字段',
  `is_query` char(1) DEFAULT NULL COMMENT '是否查询字段',
  `query_type` varchar(200) DEFAULT NULL COMMENT '查询方式',
  `is_edit` char(1) DEFAULT NULL COMMENT '是否编辑字段',
  `show_type` varchar(200) DEFAULT NULL COMMENT '表单类型',
  `options` varchar(1000) DEFAULT NULL COMMENT '其它生成选项',
  PRIMARY KEY (`id`),
  KEY `idx_gen_table_column_tn` (`table_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='代码生成表列';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_gen_table_column`
--

LOCK TABLES `js_gen_table_column` WRITE;
/*!40000 ALTER TABLE `js_gen_table_column` DISABLE KEYS */;
INSERT INTO `js_gen_table_column` VALUES ('1069794484653826048','test_data','id',10,'varchar(64)','编号','编号','id','String','1','0','1',NULL,NULL,NULL,NULL,'1','hidden','{\"fieldValid\":\"abc\"}'),('1069794484678991872','test_data','test_input',20,'varchar(200)','单行文本','单行文本','testInput','String',NULL,'1','1','1','1','1','LIKE','1','input',NULL),('1069794487002636288','test_data','test_textarea',30,'varchar(200)','多行文本','多行文本','testTextarea','String',NULL,'1','1','1','1','1','LIKE','1','textarea','{\"isNewLine\":\"1\",\"gridRowCol\":\"12/2/10\"}'),('1069794487015219200','test_data','test_select',40,'varchar(10)','下拉框','下拉框','testSelect','String',NULL,'1','1','1','1','1',NULL,'1','select','{\"dictName\":\"sys_menu_type\",\"dictType\":\"sys_menu_type\"}'),('1069794487027802112','test_data','test_select_multiple',50,'varchar(200)','下拉多选','下拉多选','testSelectMultiple','String',NULL,'1','1','1','1','1',NULL,'1','select_multiple','{\"dictName\":\"sys_menu_type\",\"dictType\":\"sys_menu_type\"}'),('1069794488772632576','test_data','test_radio',60,'varchar(10)','单选框','单选框','testRadio','String',NULL,'1','1','1','1','1',NULL,'1','radio','{\"dictName\":\"sys_menu_type\",\"dictType\":\"sys_menu_type\"}'),('1069794488781021184','test_data','test_checkbox',70,'varchar(200)','复选框','复选框','testCheckbox','String',NULL,'1','1','1','1','1',NULL,'1','checkbox','{\"dictName\":\"sys_menu_type\",\"dictType\":\"sys_menu_type\"}'),('1069794491796725760','test_data','test_date',80,'datetime','日期选择','日期选择','testDate','java.util.Date',NULL,'1','1','1','1','1','BETWEEN','1','date',NULL),('1069794491809308672','test_data','test_datetime',90,'datetime','日期时间','日期时间','testDatetime','java.util.Date',NULL,'1','1','1','1','1','BETWEEN','1','datetime',NULL),('1069794491821891584','test_data','test_user_code',100,'varchar(64)','用户选择','用户选择','testUser','com.jeesite.modules.sys.entity.User',NULL,'1','1','1','1','1',NULL,'1','userselect',NULL),('1069794494032289792','test_data','test_office_code',110,'varchar(64)','机构选择','机构选择','testOffice','com.jeesite.modules.sys.entity.Office',NULL,'1','1','1','1','1',NULL,'1','officeselect',NULL),('1069794494040678400','test_data','test_area_code',120,'varchar(64)','区域选择','区域选择','testAreaCode|testAreaName','String',NULL,'1','1','1','1','1',NULL,'1','areaselect',NULL),('1069794496527900672','test_data','test_area_name',130,'varchar(100)','区域名称','区域名称','testAreaName','String',NULL,'1','1','1','1','0','LIKE','0','input',NULL),('1069794496540483584','test_data','status',140,'char(1)','状态','状态（0正常 1删除 2停用）','status','String',NULL,'0','1',NULL,'1','1',NULL,NULL,'select','{\"dictName\":\"sys_search_status\",\"dictType\":\"sys_search_status\"}'),('1069794496548872192','test_data','create_by',150,'varchar(64)','创建者','创建者','createBy','String',NULL,'0','1',NULL,NULL,NULL,NULL,NULL,'input',NULL),('1069794497979129856','test_data','create_date',160,'datetime','创建时间','创建时间','createDate','java.util.Date',NULL,'0','1',NULL,'1',NULL,NULL,NULL,'dateselect',NULL),('1069794497991712768','test_data','update_by',170,'varchar(64)','更新者','更新者','updateBy','String',NULL,'0','1','1',NULL,NULL,NULL,NULL,'input',NULL),('1069794498000101376','test_data','update_date',180,'datetime','更新时间','更新时间','updateDate','java.util.Date',NULL,'0','1','1',NULL,NULL,NULL,NULL,'dateselect',NULL),('1069794499916898304','test_data','remarks',190,'varchar(500)','备注信息','备注信息','remarks','String',NULL,'1','1','1','1','1',NULL,'1','textarea','{\"isNewLine\":\"1\",\"gridRowCol\":\"12/2/10\"}'),('1069794502936797184','test_data_child','id',10,'varchar(64)','编号','编号','id','String','1','0','1',NULL,NULL,NULL,NULL,'1','hidden','{\"fieldValid\":\"abc\"}'),('1069794502949380096','test_data_child','test_sort',20,'int(11)','排序号','排序号','testSort','Long',NULL,'1','1','1','1','1',NULL,'1','input','{\"fieldValid\":\"digits\"}'),('1069794506170605568','test_data_child','test_data_id',30,'varchar(64)','父表主键','父表主键','testData','String',NULL,'1','1','1','1','1',NULL,'1','input',NULL),('1069794506183188480','test_data_child','test_input',40,'varchar(200)','单行文本','单行文本','testInput','String',NULL,'1','1','1','1','1','LIKE','1','input',NULL),('1069794506191577088','test_data_child','test_textarea',50,'varchar(200)','多行文本','多行文本','testTextarea','String',NULL,'1','1','1','1','1','LIKE','1','textarea','{\"isNewLine\":\"1\",\"gridRowCol\":\"12/2/10\"}'),('1069794508922068992','test_data_child','test_select',60,'varchar(10)','下拉框','下拉框','testSelect','String',NULL,'1','1','1','1','1',NULL,'1','select','{\"dictName\":\"sys_menu_type\",\"dictType\":\"sys_menu_type\"}'),('1069794508934651904','test_data_child','test_select_multiple',70,'varchar(200)','下拉多选','下拉多选','testSelectMultiple','String',NULL,'1','1','1','1','1',NULL,'1','select_multiple','{\"dictName\":\"sys_menu_type\",\"dictType\":\"sys_menu_type\"}'),('1069794510037753856','test_data_child','test_radio',80,'varchar(10)','单选框','单选框','testRadio','String',NULL,'1','1','1','1','1',NULL,'1','radio','{\"dictName\":\"sys_menu_type\",\"dictType\":\"sys_menu_type\"}'),('1069794510050336768','test_data_child','test_checkbox',90,'varchar(200)','复选框','复选框','testCheckbox','String',NULL,'1','1','1','1','1',NULL,'1','checkbox','{\"dictName\":\"sys_menu_type\",\"dictType\":\"sys_menu_type\"}'),('1069794510058725376','test_data_child','test_date',100,'datetime','日期选择','日期选择','testDate','java.util.Date',NULL,'1','1','1','1','1','BETWEEN','1','date',NULL),('1069794512097157120','test_data_child','test_datetime',110,'datetime','日期时间','日期时间','testDatetime','java.util.Date',NULL,'1','1','1','1','1','BETWEEN','1','datetime',NULL),('1069794512105545728','test_data_child','test_user_code',120,'varchar(64)','用户选择','用户选择','testUser','com.jeesite.modules.sys.entity.User',NULL,'1','1','1','1','1',NULL,'1','userselect',NULL),('1069794514341109760','test_data_child','test_office_code',130,'varchar(64)','机构选择','机构选择','testOffice','com.jeesite.modules.sys.entity.Office',NULL,'1','1','1','1','1',NULL,'1','officeselect',NULL),('1069794514353692672','test_data_child','test_area_code',140,'varchar(64)','区域选择','区域选择','testAreaCode|testAreaName','String',NULL,'1','1','1','1','1',NULL,'1','areaselect',NULL),('1069794514362081280','test_data_child','test_area_name',150,'varchar(100)','区域名称','区域名称','testAreaName','String',NULL,'1','1','1','1','0','LIKE','0','input',NULL),('1069794519835648000','test_tree','tree_code',10,'varchar(64)','节点编码','节点编码','treeCode','String','1','0','1',NULL,NULL,NULL,NULL,'1','input','{\"fieldValid\":\"abc\"}'),('1069794519848230912','test_tree','parent_code',20,'varchar(64)','父级编号','父级编号','parentCode|parentName','This',NULL,'0','1','1','1','1',NULL,'1','treeselect',NULL),('1069794519856619520','test_tree','parent_codes',30,'varchar(1000)','所有父级编号','所有父级编号','parentCodes','String',NULL,'0','1','1','1','1','LIKE','0','input',NULL),('1069794521215574016','test_tree','tree_sort',40,'decimal(10,0)','本级排序号','本级排序号（升序）','treeSort','Integer',NULL,'0','1','1','1','1',NULL,'1','input','{\"fieldValid\":\"digits\"}'),('1069794521223962624','test_tree','tree_sorts',50,'varchar(1000)','所有级别排序号','所有级别排序号','treeSorts','String',NULL,'0','1','1','0','1',NULL,'0','input',NULL),('1069794521232351232','test_tree','tree_leaf',60,'char(1)','是否最末级','是否最末级','treeLeaf','String',NULL,'0','1','1',NULL,NULL,NULL,NULL,'input',NULL),('1069794524206112768','test_tree','tree_level',70,'decimal(4,0)','层次级别','层次级别','treeLevel','Integer',NULL,'0','1','1',NULL,NULL,NULL,NULL,'input','{\"fieldValid\":\"digits\"}'),('1069794524218695680','test_tree','tree_names',80,'varchar(1000)','全节点名','全节点名','treeNames','String',NULL,'0','1','1','1','1','LIKE','1','input',NULL),('1069794526701723648','test_tree','tree_name',90,'varchar(200)','节点名称','节点名称','treeName','String',NULL,'0','1','1','1','1','LIKE','1','input',NULL),('1069794526710112256','test_tree','status',100,'char(1)','状态','状态（0正常 1删除 2停用）','status','String',NULL,'0','1',NULL,'1','1',NULL,NULL,'select','{\"dictName\":\"sys_search_status\",\"dictType\":\"sys_search_status\"}'),('1069794526722695168','test_tree','create_by',110,'varchar(64)','创建者','创建者','createBy','String',NULL,'0','1',NULL,NULL,NULL,NULL,NULL,'input',NULL),('1069794529201528832','test_tree','create_date',120,'datetime','创建时间','创建时间','createDate','java.util.Date',NULL,'0','1',NULL,'1',NULL,NULL,NULL,'dateselect',NULL),('1069794529209917440','test_tree','update_by',130,'varchar(64)','更新者','更新者','updateBy','String',NULL,'0','1','1',NULL,NULL,NULL,NULL,'input',NULL),('1069794529218306048','test_tree','update_date',140,'datetime','更新时间','更新时间','updateDate','java.util.Date',NULL,'0','1','1',NULL,NULL,NULL,NULL,'dateselect',NULL),('1069794531390955520','test_tree','remarks',150,'varchar(500)','备注信息','备注信息','remarks','String',NULL,'1','1','1','1','1',NULL,'1','textarea','{\"isNewLine\":\"1\",\"gridRowCol\":\"12/2/10\"}');
/*!40000 ALTER TABLE `js_gen_table_column` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_job_blob_triggers`
--

DROP TABLE IF EXISTS `js_job_blob_triggers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `js_job_blob_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `BLOB_DATA` blob,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  KEY `SCHED_NAME` (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `js_job_blob_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `js_job_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_job_blob_triggers`
--

LOCK TABLES `js_job_blob_triggers` WRITE;
/*!40000 ALTER TABLE `js_job_blob_triggers` DISABLE KEYS */;
/*!40000 ALTER TABLE `js_job_blob_triggers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_job_calendars`
--

DROP TABLE IF EXISTS `js_job_calendars`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `js_job_calendars` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `CALENDAR_NAME` varchar(200) NOT NULL,
  `CALENDAR` blob NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`CALENDAR_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_job_calendars`
--

LOCK TABLES `js_job_calendars` WRITE;
/*!40000 ALTER TABLE `js_job_calendars` DISABLE KEYS */;
/*!40000 ALTER TABLE `js_job_calendars` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_job_cron_triggers`
--

DROP TABLE IF EXISTS `js_job_cron_triggers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `js_job_cron_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `CRON_EXPRESSION` varchar(120) NOT NULL,
  `TIME_ZONE_ID` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `js_job_cron_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `js_job_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_job_cron_triggers`
--

LOCK TABLES `js_job_cron_triggers` WRITE;
/*!40000 ALTER TABLE `js_job_cron_triggers` DISABLE KEYS */;
/*!40000 ALTER TABLE `js_job_cron_triggers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_job_fired_triggers`
--

DROP TABLE IF EXISTS `js_job_fired_triggers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `js_job_fired_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `ENTRY_ID` varchar(95) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `INSTANCE_NAME` varchar(200) NOT NULL,
  `FIRED_TIME` bigint(13) NOT NULL,
  `SCHED_TIME` bigint(13) NOT NULL,
  `PRIORITY` int(11) NOT NULL,
  `STATE` varchar(16) NOT NULL,
  `JOB_NAME` varchar(200) DEFAULT NULL,
  `JOB_GROUP` varchar(200) DEFAULT NULL,
  `IS_NONCONCURRENT` varchar(1) DEFAULT NULL,
  `REQUESTS_RECOVERY` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`,`ENTRY_ID`),
  KEY `IDX_QRTZ_FT_TRIG_INST_NAME` (`SCHED_NAME`,`INSTANCE_NAME`),
  KEY `IDX_QRTZ_FT_INST_JOB_REQ_RCVRY` (`SCHED_NAME`,`INSTANCE_NAME`,`REQUESTS_RECOVERY`),
  KEY `IDX_QRTZ_FT_J_G` (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_FT_JG` (`SCHED_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_FT_T_G` (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  KEY `IDX_QRTZ_FT_TG` (`SCHED_NAME`,`TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_job_fired_triggers`
--

LOCK TABLES `js_job_fired_triggers` WRITE;
/*!40000 ALTER TABLE `js_job_fired_triggers` DISABLE KEYS */;
/*!40000 ALTER TABLE `js_job_fired_triggers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_job_job_details`
--

DROP TABLE IF EXISTS `js_job_job_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `js_job_job_details` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `JOB_NAME` varchar(200) NOT NULL,
  `JOB_GROUP` varchar(200) NOT NULL,
  `DESCRIPTION` varchar(250) DEFAULT NULL,
  `JOB_CLASS_NAME` varchar(250) NOT NULL,
  `IS_DURABLE` varchar(1) NOT NULL,
  `IS_NONCONCURRENT` varchar(1) NOT NULL,
  `IS_UPDATE_DATA` varchar(1) NOT NULL,
  `REQUESTS_RECOVERY` varchar(1) NOT NULL,
  `JOB_DATA` blob,
  PRIMARY KEY (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_J_REQ_RECOVERY` (`SCHED_NAME`,`REQUESTS_RECOVERY`),
  KEY `IDX_QRTZ_J_GRP` (`SCHED_NAME`,`JOB_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_job_job_details`
--

LOCK TABLES `js_job_job_details` WRITE;
/*!40000 ALTER TABLE `js_job_job_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `js_job_job_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_job_locks`
--

DROP TABLE IF EXISTS `js_job_locks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `js_job_locks` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `LOCK_NAME` varchar(40) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`LOCK_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_job_locks`
--

LOCK TABLES `js_job_locks` WRITE;
/*!40000 ALTER TABLE `js_job_locks` DISABLE KEYS */;
/*!40000 ALTER TABLE `js_job_locks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_job_paused_trigger_grps`
--

DROP TABLE IF EXISTS `js_job_paused_trigger_grps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `js_job_paused_trigger_grps` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_job_paused_trigger_grps`
--

LOCK TABLES `js_job_paused_trigger_grps` WRITE;
/*!40000 ALTER TABLE `js_job_paused_trigger_grps` DISABLE KEYS */;
/*!40000 ALTER TABLE `js_job_paused_trigger_grps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_job_scheduler_state`
--

DROP TABLE IF EXISTS `js_job_scheduler_state`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `js_job_scheduler_state` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `INSTANCE_NAME` varchar(200) NOT NULL,
  `LAST_CHECKIN_TIME` bigint(13) NOT NULL,
  `CHECKIN_INTERVAL` bigint(13) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`INSTANCE_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_job_scheduler_state`
--

LOCK TABLES `js_job_scheduler_state` WRITE;
/*!40000 ALTER TABLE `js_job_scheduler_state` DISABLE KEYS */;
/*!40000 ALTER TABLE `js_job_scheduler_state` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_job_simple_triggers`
--

DROP TABLE IF EXISTS `js_job_simple_triggers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `js_job_simple_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `REPEAT_COUNT` bigint(7) NOT NULL,
  `REPEAT_INTERVAL` bigint(12) NOT NULL,
  `TIMES_TRIGGERED` bigint(10) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `js_job_simple_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `js_job_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_job_simple_triggers`
--

LOCK TABLES `js_job_simple_triggers` WRITE;
/*!40000 ALTER TABLE `js_job_simple_triggers` DISABLE KEYS */;
/*!40000 ALTER TABLE `js_job_simple_triggers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_job_simprop_triggers`
--

DROP TABLE IF EXISTS `js_job_simprop_triggers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `js_job_simprop_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `STR_PROP_1` varchar(512) DEFAULT NULL,
  `STR_PROP_2` varchar(512) DEFAULT NULL,
  `STR_PROP_3` varchar(512) DEFAULT NULL,
  `INT_PROP_1` int(11) DEFAULT NULL,
  `INT_PROP_2` int(11) DEFAULT NULL,
  `LONG_PROP_1` bigint(20) DEFAULT NULL,
  `LONG_PROP_2` bigint(20) DEFAULT NULL,
  `DEC_PROP_1` decimal(13,4) DEFAULT NULL,
  `DEC_PROP_2` decimal(13,4) DEFAULT NULL,
  `BOOL_PROP_1` varchar(1) DEFAULT NULL,
  `BOOL_PROP_2` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `js_job_simprop_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `js_job_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_job_simprop_triggers`
--

LOCK TABLES `js_job_simprop_triggers` WRITE;
/*!40000 ALTER TABLE `js_job_simprop_triggers` DISABLE KEYS */;
/*!40000 ALTER TABLE `js_job_simprop_triggers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_job_triggers`
--

DROP TABLE IF EXISTS `js_job_triggers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `js_job_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `JOB_NAME` varchar(200) NOT NULL,
  `JOB_GROUP` varchar(200) NOT NULL,
  `DESCRIPTION` varchar(250) DEFAULT NULL,
  `NEXT_FIRE_TIME` bigint(13) DEFAULT NULL,
  `PREV_FIRE_TIME` bigint(13) DEFAULT NULL,
  `PRIORITY` int(11) DEFAULT NULL,
  `TRIGGER_STATE` varchar(16) NOT NULL,
  `TRIGGER_TYPE` varchar(8) NOT NULL,
  `START_TIME` bigint(13) NOT NULL,
  `END_TIME` bigint(13) DEFAULT NULL,
  `CALENDAR_NAME` varchar(200) DEFAULT NULL,
  `MISFIRE_INSTR` smallint(2) DEFAULT NULL,
  `JOB_DATA` blob,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  KEY `IDX_QRTZ_T_J` (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_T_JG` (`SCHED_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_T_C` (`SCHED_NAME`,`CALENDAR_NAME`),
  KEY `IDX_QRTZ_T_G` (`SCHED_NAME`,`TRIGGER_GROUP`),
  KEY `IDX_QRTZ_T_STATE` (`SCHED_NAME`,`TRIGGER_STATE`),
  KEY `IDX_QRTZ_T_N_STATE` (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`,`TRIGGER_STATE`),
  KEY `IDX_QRTZ_T_N_G_STATE` (`SCHED_NAME`,`TRIGGER_GROUP`,`TRIGGER_STATE`),
  KEY `IDX_QRTZ_T_NEXT_FIRE_TIME` (`SCHED_NAME`,`NEXT_FIRE_TIME`),
  KEY `IDX_QRTZ_T_NFT_ST` (`SCHED_NAME`,`TRIGGER_STATE`,`NEXT_FIRE_TIME`),
  KEY `IDX_QRTZ_T_NFT_MISFIRE` (`SCHED_NAME`,`MISFIRE_INSTR`,`NEXT_FIRE_TIME`),
  KEY `IDX_QRTZ_T_NFT_ST_MISFIRE` (`SCHED_NAME`,`MISFIRE_INSTR`,`NEXT_FIRE_TIME`,`TRIGGER_STATE`),
  KEY `IDX_QRTZ_T_NFT_ST_MISFIRE_GRP` (`SCHED_NAME`,`MISFIRE_INSTR`,`NEXT_FIRE_TIME`,`TRIGGER_GROUP`,`TRIGGER_STATE`),
  CONSTRAINT `js_job_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) REFERENCES `js_job_job_details` (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_job_triggers`
--

LOCK TABLES `js_job_triggers` WRITE;
/*!40000 ALTER TABLE `js_job_triggers` DISABLE KEYS */;
/*!40000 ALTER TABLE `js_job_triggers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_sys_area`
--

DROP TABLE IF EXISTS `js_sys_area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `js_sys_area` (
  `area_code` varchar(100) NOT NULL COMMENT '区域编码',
  `parent_code` varchar(64) NOT NULL COMMENT '父级编号',
  `parent_codes` varchar(1000) NOT NULL COMMENT '所有父级编号',
  `tree_sort` decimal(10,0) NOT NULL COMMENT '本级排序号（升序）',
  `tree_sorts` varchar(1000) NOT NULL COMMENT '所有级别排序号',
  `tree_leaf` char(1) NOT NULL COMMENT '是否最末级',
  `tree_level` decimal(4,0) NOT NULL COMMENT '层次级别',
  `tree_names` varchar(1000) NOT NULL COMMENT '全节点名',
  `area_name` varchar(100) NOT NULL COMMENT '区域名称',
  `area_type` char(1) DEFAULT NULL COMMENT '区域类型',
  `status` char(1) NOT NULL DEFAULT '0' COMMENT '状态（0正常 1删除 2停用）',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) DEFAULT NULL COMMENT '备注信息',
  PRIMARY KEY (`area_code`),
  KEY `idx_sys_area_pc` (`parent_code`),
  KEY `idx_sys_area_ts` (`tree_sort`),
  KEY `idx_sys_area_status` (`status`),
  KEY `idx_sys_area_pcs` (`parent_codes`(255)),
  KEY `idx_sys_area_tss` (`tree_sorts`(255))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='行政区划';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_sys_area`
--

LOCK TABLES `js_sys_area` WRITE;
/*!40000 ALTER TABLE `js_sys_area` DISABLE KEYS */;
INSERT INTO `js_sys_area` VALUES ('370000','0','0,',370000,'0000370000,','0',0,'山东省','山东省','1','0','system','2018-12-04 11:21:30','system','2018-12-04 11:21:30',NULL),('370100','370000','0,370000,',370100,'0000370000,0000370100,','0',1,'山东省/济南市','济南市','2','0','system','2018-12-04 11:21:31','system','2018-12-04 11:21:31',NULL),('370102','370100','0,370000,370100,',370102,'0000370000,0000370100,0000370102,','1',2,'山东省/济南市/历下区','历下区','3','0','system','2018-12-04 11:21:31','system','2018-12-04 11:21:31',NULL),('370103','370100','0,370000,370100,',370103,'0000370000,0000370100,0000370103,','1',2,'山东省/济南市/市中区','市中区','3','0','system','2018-12-04 11:21:31','system','2018-12-04 11:21:31',NULL),('370104','370100','0,370000,370100,',370104,'0000370000,0000370100,0000370104,','1',2,'山东省/济南市/槐荫区','槐荫区','3','0','system','2018-12-04 11:21:31','system','2018-12-04 11:21:31',NULL),('370105','370100','0,370000,370100,',370105,'0000370000,0000370100,0000370105,','1',2,'山东省/济南市/天桥区','天桥区','3','0','system','2018-12-04 11:21:31','system','2018-12-04 11:21:31',NULL),('370112','370100','0,370000,370100,',370112,'0000370000,0000370100,0000370112,','1',2,'山东省/济南市/历城区','历城区','3','0','system','2018-12-04 11:21:31','system','2018-12-04 11:21:31',NULL),('370113','370100','0,370000,370100,',370113,'0000370000,0000370100,0000370113,','1',2,'山东省/济南市/长清区','长清区','3','0','system','2018-12-04 11:21:31','system','2018-12-04 11:21:31',NULL),('370114','370100','0,370000,370100,',370114,'0000370000,0000370100,0000370114,','1',2,'山东省/济南市/章丘区','章丘区','3','0','system','2018-12-04 11:21:31','system','2018-12-04 11:21:31',NULL),('370124','370100','0,370000,370100,',370124,'0000370000,0000370100,0000370124,','1',2,'山东省/济南市/平阴县','平阴县','3','0','system','2018-12-04 11:21:31','system','2018-12-04 11:21:31',NULL),('370125','370100','0,370000,370100,',370125,'0000370000,0000370100,0000370125,','1',2,'山东省/济南市/济阳县','济阳县','3','0','system','2018-12-04 11:21:31','system','2018-12-04 11:21:31',NULL),('370126','370100','0,370000,370100,',370126,'0000370000,0000370100,0000370126,','1',2,'山东省/济南市/商河县','商河县','3','0','system','2018-12-04 11:21:31','system','2018-12-04 11:21:31',NULL),('370200','370000','0,370000,',370200,'0000370000,0000370200,','0',1,'山东省/青岛市','青岛市','2','0','system','2018-12-04 11:21:31','system','2018-12-04 11:21:31',NULL),('370202','370200','0,370000,370200,',370202,'0000370000,0000370200,0000370202,','1',2,'山东省/青岛市/市南区','市南区','3','0','system','2018-12-04 11:21:31','system','2018-12-04 11:21:31',NULL),('370203','370200','0,370000,370200,',370203,'0000370000,0000370200,0000370203,','1',2,'山东省/青岛市/市北区','市北区','3','0','system','2018-12-04 11:21:31','system','2018-12-04 11:21:31',NULL),('370211','370200','0,370000,370200,',370211,'0000370000,0000370200,0000370211,','1',2,'山东省/青岛市/黄岛区','黄岛区','3','0','system','2018-12-04 11:21:31','system','2018-12-04 11:21:31',NULL),('370212','370200','0,370000,370200,',370212,'0000370000,0000370200,0000370212,','1',2,'山东省/青岛市/崂山区','崂山区','3','0','system','2018-12-04 11:21:31','system','2018-12-04 11:21:31',NULL),('370213','370200','0,370000,370200,',370213,'0000370000,0000370200,0000370213,','1',2,'山东省/青岛市/李沧区','李沧区','3','0','system','2018-12-04 11:21:31','system','2018-12-04 11:21:31',NULL),('370214','370200','0,370000,370200,',370214,'0000370000,0000370200,0000370214,','1',2,'山东省/青岛市/城阳区','城阳区','3','0','system','2018-12-04 11:21:31','system','2018-12-04 11:21:31',NULL),('370281','370200','0,370000,370200,',370281,'0000370000,0000370200,0000370281,','1',2,'山东省/青岛市/胶州市','胶州市','3','0','system','2018-12-04 11:21:31','system','2018-12-04 11:21:31',NULL),('370282','370200','0,370000,370200,',370282,'0000370000,0000370200,0000370282,','1',2,'山东省/青岛市/即墨区','即墨区','3','0','system','2018-12-04 11:21:31','system','2018-12-04 11:21:31',NULL),('370283','370200','0,370000,370200,',370283,'0000370000,0000370200,0000370283,','1',2,'山东省/青岛市/平度市','平度市','3','0','system','2018-12-04 11:21:31','system','2018-12-04 11:21:31',NULL),('370285','370200','0,370000,370200,',370285,'0000370000,0000370200,0000370285,','1',2,'山东省/青岛市/莱西市','莱西市','3','0','system','2018-12-04 11:21:31','system','2018-12-04 11:21:31',NULL);
/*!40000 ALTER TABLE `js_sys_area` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_sys_company`
--

DROP TABLE IF EXISTS `js_sys_company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `js_sys_company` (
  `company_code` varchar(64) NOT NULL COMMENT '公司编码',
  `parent_code` varchar(64) NOT NULL COMMENT '父级编号',
  `parent_codes` varchar(1000) NOT NULL COMMENT '所有父级编号',
  `tree_sort` decimal(10,0) NOT NULL COMMENT '本级排序号（升序）',
  `tree_sorts` varchar(1000) NOT NULL COMMENT '所有级别排序号',
  `tree_leaf` char(1) NOT NULL COMMENT '是否最末级',
  `tree_level` decimal(4,0) NOT NULL COMMENT '层次级别',
  `tree_names` varchar(1000) NOT NULL COMMENT '全节点名',
  `view_code` varchar(100) NOT NULL COMMENT '公司代码',
  `company_name` varchar(200) NOT NULL COMMENT '公司名称',
  `full_name` varchar(200) NOT NULL COMMENT '公司全称',
  `area_code` varchar(100) DEFAULT NULL COMMENT '区域编码',
  `status` char(1) NOT NULL DEFAULT '0' COMMENT '状态（0正常 1删除 2停用）',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) DEFAULT NULL COMMENT '备注信息',
  `corp_code` varchar(64) NOT NULL DEFAULT '0' COMMENT '租户代码',
  `corp_name` varchar(100) NOT NULL DEFAULT 'JeeSite' COMMENT '租户名称',
  `extend_s1` varchar(500) DEFAULT NULL COMMENT '扩展 String 1',
  `extend_s2` varchar(500) DEFAULT NULL COMMENT '扩展 String 2',
  `extend_s3` varchar(500) DEFAULT NULL COMMENT '扩展 String 3',
  `extend_s4` varchar(500) DEFAULT NULL COMMENT '扩展 String 4',
  `extend_s5` varchar(500) DEFAULT NULL COMMENT '扩展 String 5',
  `extend_s6` varchar(500) DEFAULT NULL COMMENT '扩展 String 6',
  `extend_s7` varchar(500) DEFAULT NULL COMMENT '扩展 String 7',
  `extend_s8` varchar(500) DEFAULT NULL COMMENT '扩展 String 8',
  `extend_i1` decimal(19,0) DEFAULT NULL COMMENT '扩展 Integer 1',
  `extend_i2` decimal(19,0) DEFAULT NULL COMMENT '扩展 Integer 2',
  `extend_i3` decimal(19,0) DEFAULT NULL COMMENT '扩展 Integer 3',
  `extend_i4` decimal(19,0) DEFAULT NULL COMMENT '扩展 Integer 4',
  `extend_f1` decimal(19,4) DEFAULT NULL COMMENT '扩展 Float 1',
  `extend_f2` decimal(19,4) DEFAULT NULL COMMENT '扩展 Float 2',
  `extend_f3` decimal(19,4) DEFAULT NULL COMMENT '扩展 Float 3',
  `extend_f4` decimal(19,4) DEFAULT NULL COMMENT '扩展 Float 4',
  `extend_d1` datetime DEFAULT NULL COMMENT '扩展 Date 1',
  `extend_d2` datetime DEFAULT NULL COMMENT '扩展 Date 2',
  `extend_d3` datetime DEFAULT NULL COMMENT '扩展 Date 3',
  `extend_d4` datetime DEFAULT NULL COMMENT '扩展 Date 4',
  PRIMARY KEY (`company_code`),
  KEY `idx_sys_company_cc` (`corp_code`),
  KEY `idx_sys_company_pc` (`parent_code`),
  KEY `idx_sys_company_ts` (`tree_sort`),
  KEY `idx_sys_company_status` (`status`),
  KEY `idx_sys_company_vc` (`view_code`),
  KEY `idx_sys_company_pcs` (`parent_codes`(255)),
  KEY `idx_sys_company_tss` (`tree_sorts`(255))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='公司表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_sys_company`
--

LOCK TABLES `js_sys_company` WRITE;
/*!40000 ALTER TABLE `js_sys_company` DISABLE KEYS */;
INSERT INTO `js_sys_company` VALUES ('SD','0','0,',40,'0000000040,','0',0,'山东公司','SD','山东公司','山东公司',NULL,'0','system','2018-12-04 11:23:34','system','2018-12-04 11:23:34',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('SDJN','SD','0,SD,',30,'0000000040,0000000030,','1',1,'山东公司/济南公司','SDJN','济南公司','山东济南公司',NULL,'0','system','2018-12-04 11:23:35','system','2018-12-04 11:23:35',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('SDQD','SD','0,SD,',40,'0000000040,0000000040,','1',1,'山东公司/青岛公司','SDQD','青岛公司','山东青岛公司',NULL,'0','system','2018-12-04 11:23:36','system','2018-12-04 11:23:36',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `js_sys_company` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_sys_company_office`
--

DROP TABLE IF EXISTS `js_sys_company_office`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `js_sys_company_office` (
  `company_code` varchar(64) NOT NULL COMMENT '公司编码',
  `office_code` varchar(64) NOT NULL COMMENT '机构编码',
  PRIMARY KEY (`company_code`,`office_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='公司部门关联表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_sys_company_office`
--

LOCK TABLES `js_sys_company_office` WRITE;
/*!40000 ALTER TABLE `js_sys_company_office` DISABLE KEYS */;
/*!40000 ALTER TABLE `js_sys_company_office` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_sys_config`
--

DROP TABLE IF EXISTS `js_sys_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `js_sys_config` (
  `id` varchar(64) NOT NULL COMMENT '编号',
  `config_name` varchar(100) NOT NULL COMMENT '名称',
  `config_key` varchar(100) NOT NULL COMMENT '参数键',
  `config_value` varchar(1000) DEFAULT NULL COMMENT '参数值',
  `is_sys` char(1) NOT NULL COMMENT '系统内置（1是 0否）',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) DEFAULT NULL COMMENT '备注信息',
  PRIMARY KEY (`id`),
  KEY `idx_sys_config_key` (`config_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='参数配置表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_sys_config`
--

LOCK TABLES `js_sys_config` WRITE;
/*!40000 ALTER TABLE `js_sys_config` DISABLE KEYS */;
INSERT INTO `js_sys_config` VALUES ('1069793797945597952','研发工具-代码生成默认包名','gen.defaultPackageName','com.jeesite.modules','0','system','2018-12-04 11:21:32','system','2018-12-04 11:21:32','新建项目后，修改该键值，在生成代码的时候就不要再修改了'),('1069793798197256192','主框架页-桌面仪表盘首页地址','sys.index.desktopUrl','/desktop','0','system','2018-12-04 11:21:32','system','2018-12-04 11:21:32','主页面的第一个页签首页桌面地址'),('1069793798411165696','主框架页-侧边栏的默认显示样式','sys.index.sidebarStyle','1','0','system','2018-12-04 11:21:32','system','2018-12-04 11:21:32','1：默认显示侧边栏；2：默认折叠侧边栏'),('1069793798599909376','主框架页-默认皮肤样式名称','sys.index.skinName','skin-blue-light','0','system','2018-12-04 11:21:32','system','2018-12-04 11:21:32','skin-black-light、skin-black、skin-blue-light、skin-blue、skin-green-light、skin-green、skin-purple-light、skin-purple、skin-red-light、skin-red、skin-yellow-light、skin-yellow'),('1069793798742515712','用户登录-登录失败多少次数后显示验证码','sys.login.failedNumAfterValidCode','100','0','system','2018-12-04 11:21:32','system','2018-12-04 11:21:32','设置为0强制使用验证码登录'),('1069793799086448640','用户登录-登录失败多少次数后锁定账号','sys.login.failedNumAfterLockAccount','200','0','system','2018-12-04 11:21:32','system','2018-12-04 11:21:32','登录失败多少次数后锁定账号'),('1069793799241637888','用户登录-登录失败多少次数后锁定账号的时间','sys.login.failedNumAfterLockMinute','20','0','system','2018-12-04 11:21:32','system','2018-12-04 11:21:32','锁定账号的时间（单位：分钟）'),('1069793799375855616','账号自助-是否开启用户注册功能','sys.account.registerUser','true','0','system','2018-12-04 11:21:32','system','2018-12-04 11:21:32','是否开启注册用户功能'),('1069793799531044864','账号自助-允许自助注册的用户类型','sys.account.registerUser.userTypes','-1','0','system','2018-12-04 11:21:32','system','2018-12-04 11:21:32','允许注册的用户类型（多个用逗号隔开，如果注册时不选择用户类型，则第一个为默认类型）'),('1069793799665262592','账号自助-验证码有效时间（分钟）','sys.account.validCodeTimeout','10','0','system','2018-12-04 11:21:32','system','2018-12-04 11:21:32','找回密码时，短信/邮件验证码有效时间（单位：分钟，0表示不限制）'),('1069793799799480320','用户管理-账号默认角色-员工类型','sys.user.defaultRoleCodes.employee','default','0','system','2018-12-04 11:21:32','system','2018-12-04 11:21:32','所有员工用户都拥有的角色权限（适用于菜单授权查询）'),('1069793799937892352','用户管理-账号初始密码','sys.user.initPassword','123456','0','system','2018-12-04 11:21:32','system','2018-12-04 11:21:32','创建用户和重置密码的时候用户的密码'),('1069793800088887296','用户管理-初始密码修改策略','sys.user.initPasswordModify','1','0','system','2018-12-04 11:21:32','system','2018-12-04 11:21:32','0：初始密码修改策略关闭，没有任何提示；1：提醒用户，如果未修改初始密码，则在登录时和点击菜单就会提醒修改密码对话框；2：强制实行初始密码修改，登录后若不修改密码则不能进行系统操作'),('1069793800227299328','用户管理-账号密码修改策略','sys.user.passwordModify','1','0','system','2018-12-04 11:21:32','system','2018-12-04 11:21:32','0：密码修改策略关闭，没有任何提示；1：提醒用户，如果未修改初始密码，则在登录时和点击菜单就会提醒修改密码对话框；2：强制实行初始密码修改，登录后若不修改密码则不能进行系统操作。'),('1069793800365711360','用户管理-账号密码修改策略验证周期','sys.user.passwordModifyCycle','30','0','system','2018-12-04 11:21:32','system','2018-12-04 11:21:32','密码安全修改周期是指定时间内提醒或必须修改密码（例如设置30天）的验证时间（天），超过这个时间登录系统时需，提醒用户修改密码或强制修改密码才能继续使用系统。单位：天，如果设置30天，则第31天开始强制修改密码'),('1069793800499929088','用户管理-密码修改多少次内不允许重复','sys.user.passwordModifyNotRepeatNum','1','0','system','2018-12-04 11:21:33','system','2018-12-04 11:21:33','默认1次，表示不能与上次密码重复；若设置为3，表示并与前3次密码重复'),('1069793800634146816','用户管理-账号密码修改最低安全等级','sys.user.passwordModifySecurityLevel','0','0','system','2018-12-04 11:21:33','system','2018-12-04 11:21:33','0：不限制等级（用户在修改密码的时候不进行等级验证）\r；1：限制最低等级为很弱\r；2：限制最低等级为弱\r；3：限制最低等级为安全\r；4：限制最低等级为很安全');
/*!40000 ALTER TABLE `js_sys_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_sys_dict_data`
--

DROP TABLE IF EXISTS `js_sys_dict_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `js_sys_dict_data` (
  `dict_code` varchar(64) NOT NULL COMMENT '字典编码',
  `parent_code` varchar(64) NOT NULL COMMENT '父级编号',
  `parent_codes` varchar(1000) NOT NULL COMMENT '所有父级编号',
  `tree_sort` decimal(10,0) NOT NULL COMMENT '本级排序号（升序）',
  `tree_sorts` varchar(1000) NOT NULL COMMENT '所有级别排序号',
  `tree_leaf` char(1) NOT NULL COMMENT '是否最末级',
  `tree_level` decimal(4,0) NOT NULL COMMENT '层次级别',
  `tree_names` varchar(1000) NOT NULL COMMENT '全节点名',
  `dict_label` varchar(100) NOT NULL COMMENT '字典标签',
  `dict_value` varchar(100) NOT NULL COMMENT '字典键值',
  `dict_type` varchar(100) NOT NULL COMMENT '字典类型',
  `is_sys` char(1) NOT NULL COMMENT '系统内置（1是 0否）',
  `description` varchar(500) DEFAULT NULL COMMENT '字典描述',
  `css_style` varchar(500) DEFAULT NULL COMMENT 'css样式（如：color:red)',
  `css_class` varchar(500) DEFAULT NULL COMMENT 'css类名（如：red）',
  `status` char(1) NOT NULL DEFAULT '0' COMMENT '状态（0正常 1删除 2停用）',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) DEFAULT NULL COMMENT '备注信息',
  `corp_code` varchar(64) NOT NULL DEFAULT '0' COMMENT '租户代码',
  `corp_name` varchar(100) NOT NULL DEFAULT 'JeeSite' COMMENT '租户名称',
  `extend_s1` varchar(500) DEFAULT NULL COMMENT '扩展 String 1',
  `extend_s2` varchar(500) DEFAULT NULL COMMENT '扩展 String 2',
  `extend_s3` varchar(500) DEFAULT NULL COMMENT '扩展 String 3',
  `extend_s4` varchar(500) DEFAULT NULL COMMENT '扩展 String 4',
  `extend_s5` varchar(500) DEFAULT NULL COMMENT '扩展 String 5',
  `extend_s6` varchar(500) DEFAULT NULL COMMENT '扩展 String 6',
  `extend_s7` varchar(500) DEFAULT NULL COMMENT '扩展 String 7',
  `extend_s8` varchar(500) DEFAULT NULL COMMENT '扩展 String 8',
  `extend_i1` decimal(19,0) DEFAULT NULL COMMENT '扩展 Integer 1',
  `extend_i2` decimal(19,0) DEFAULT NULL COMMENT '扩展 Integer 2',
  `extend_i3` decimal(19,0) DEFAULT NULL COMMENT '扩展 Integer 3',
  `extend_i4` decimal(19,0) DEFAULT NULL COMMENT '扩展 Integer 4',
  `extend_f1` decimal(19,4) DEFAULT NULL COMMENT '扩展 Float 1',
  `extend_f2` decimal(19,4) DEFAULT NULL COMMENT '扩展 Float 2',
  `extend_f3` decimal(19,4) DEFAULT NULL COMMENT '扩展 Float 3',
  `extend_f4` decimal(19,4) DEFAULT NULL COMMENT '扩展 Float 4',
  `extend_d1` datetime DEFAULT NULL COMMENT '扩展 Date 1',
  `extend_d2` datetime DEFAULT NULL COMMENT '扩展 Date 2',
  `extend_d3` datetime DEFAULT NULL COMMENT '扩展 Date 3',
  `extend_d4` datetime DEFAULT NULL COMMENT '扩展 Date 4',
  PRIMARY KEY (`dict_code`),
  KEY `idx_sys_dict_data_cc` (`corp_code`),
  KEY `idx_sys_dict_data_dt` (`dict_type`),
  KEY `idx_sys_dict_data_pc` (`parent_code`),
  KEY `idx_sys_dict_data_status` (`status`),
  KEY `idx_sys_dict_data_pcs` (`parent_codes`(255)),
  KEY `idx_sys_dict_data_ts` (`tree_sort`),
  KEY `idx_sys_dict_data_tss` (`tree_sorts`(255)),
  KEY `idx_sys_dict_data_dv` (`dict_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='字典数据表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_sys_dict_data`
--

LOCK TABLES `js_sys_dict_data` WRITE;
/*!40000 ALTER TABLE `js_sys_dict_data` DISABLE KEYS */;
INSERT INTO `js_sys_dict_data` VALUES ('1069793805692477440','0','0,',30,'0000000030,','1',0,'是','是','1','sys_yes_no','1','','','','0','system','2018-12-04 11:21:34','system','2018-12-04 11:21:34',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793805944135680','0','0,',40,'0000000040,','1',0,'否','否','0','sys_yes_no','1','','color:#aaa;','','0','system','2018-12-04 11:21:34','system','2018-12-04 11:21:34',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793806111907840','0','0,',20,'0000000020,','1',0,'正常','正常','0','sys_status','1','','','','0','system','2018-12-04 11:21:34','system','2018-12-04 11:21:34',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793806258708480','0','0,',30,'0000000030,','1',0,'删除','删除','1','sys_status','1','','color:#f00;','','0','system','2018-12-04 11:21:34','system','2018-12-04 11:21:34',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793806489395200','0','0,',40,'0000000040,','1',0,'停用','停用','2','sys_status','1','','color:#f00;','','0','system','2018-12-04 11:21:34','system','2018-12-04 11:21:34',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793806669750272','0','0,',50,'0000000050,','1',0,'冻结','冻结','3','sys_status','1','','color:#fa0;','','0','system','2018-12-04 11:21:34','system','2018-12-04 11:21:34',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793806850105344','0','0,',60,'0000000060,','1',0,'待审','待审','4','sys_status','1','','','','0','system','2018-12-04 11:21:34','system','2018-12-04 11:21:34',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793807022071808','0','0,',70,'0000000070,','1',0,'驳回','驳回','5','sys_status','1','','','','0','system','2018-12-04 11:21:34','system','2018-12-04 11:21:34',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793807206621184','0','0,',80,'0000000080,','1',0,'草稿','草稿','9','sys_status','1','','color:#aaa;','','0','system','2018-12-04 11:21:34','system','2018-12-04 11:21:34',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793807382781952','0','0,',30,'0000000030,','1',0,'显示','显示','1','sys_show_hide','1','','','','0','system','2018-12-04 11:21:34','system','2018-12-04 11:21:34',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793807546359808','0','0,',40,'0000000040,','1',0,'隐藏','隐藏','0','sys_show_hide','1','','color:#aaa;','','0','system','2018-12-04 11:21:34','system','2018-12-04 11:21:34',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793807718326272','0','0,',30,'0000000030,','1',0,'简体中文','简体中文','zh_CN','sys_lang_type','1','','','','0','system','2018-12-04 11:21:34','system','2018-12-04 11:21:34',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793807860932608','0','0,',40,'0000000040,','1',0,'英语','英语','en','sys_lang_type','1','','','','0','system','2018-12-04 11:21:34','system','2018-12-04 11:21:34',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793808011927552','0','0,',30,'0000000030,','1',0,'PC电脑','PC电脑','pc','sys_device_type','1','','','','0','system','2018-12-04 11:21:34','system','2018-12-04 11:21:34',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793808158728192','0','0,',40,'0000000040,','1',0,'手机APP','手机APP','mobileApp','sys_device_type','1','','','','0','system','2018-12-04 11:21:34','system','2018-12-04 11:21:34',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793808292945920','0','0,',50,'0000000050,','1',0,'手机Web','手机Web','mobileWeb','sys_device_type','1','','','','0','system','2018-12-04 11:21:34','system','2018-12-04 11:21:34',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793808448135168','0','0,',60,'0000000060,','1',0,'微信设备','微信设备','weixin','sys_device_type','1','','','','0','system','2018-12-04 11:21:34','system','2018-12-04 11:21:34',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793808641073152','0','0,',30,'0000000030,','1',0,'主导航菜单','主导航菜单','default','sys_menu_sys_code','1','','','','0','system','2018-12-04 11:21:34','system','2018-12-04 11:21:34',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793808800456704','0','0,',30,'0000000030,','1',0,'菜单','菜单','1','sys_menu_type','1','','','','0','system','2018-12-04 11:21:34','system','2018-12-04 11:21:35',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793808955645952','0','0,',40,'0000000040,','1',0,'权限','权限','2','sys_menu_type','1','','color:#c243d6;','','0','system','2018-12-04 11:21:35','system','2018-12-04 11:21:35',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793809102446592','0','0,',30,'0000000030,','1',0,'默认权重','默认权重','20','sys_menu_weight','1','','','','0','system','2018-12-04 11:21:35','system','2018-12-04 11:21:35',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793809270218752','0','0,',40,'0000000040,','1',0,'二级管理员','二级管理员','40','sys_menu_weight','1','','','','0','system','2018-12-04 11:21:35','system','2018-12-04 11:21:35',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793809421213696','0','0,',50,'0000000050,','1',0,'系统管理员','系统管理员','60','sys_menu_weight','1','','','','0','system','2018-12-04 11:21:35','system','2018-12-04 11:21:35',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793809614151680','0','0,',60,'0000000060,','1',0,'超级管理员','超级管理员','80','sys_menu_weight','1','','color:#c243d6;','','0','system','2018-12-04 11:21:35','system','2018-12-04 11:21:35',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793809890975744','0','0,',30,'0000000030,','1',0,'国家','国家','0','sys_area_type','1','','','','0','system','2018-12-04 11:21:35','system','2018-12-04 11:21:35',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793810050359296','0','0,',40,'0000000040,','1',0,'省份直辖市','省份直辖市','1','sys_area_type','1','','','','0','system','2018-12-04 11:21:35','system','2018-12-04 11:21:35',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793810230714368','0','0,',50,'0000000050,','1',0,'地市','地市','2','sys_area_type','1','','','','0','system','2018-12-04 11:21:35','system','2018-12-04 11:21:35',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793810411069440','0','0,',60,'0000000060,','1',0,'区县','区县','3','sys_area_type','1','','','','0','system','2018-12-04 11:21:35','system','2018-12-04 11:21:35',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793810612396032','0','0,',30,'0000000030,','1',0,'省级公司','省级公司','1','sys_office_type','1','','','','0','system','2018-12-04 11:21:35','system','2018-12-04 11:21:35',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793810801139712','0','0,',40,'0000000040,','1',0,'市级公司','市级公司','2','sys_office_type','1','','','','0','system','2018-12-04 11:21:35','system','2018-12-04 11:21:35',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793811019243520','0','0,',50,'0000000050,','1',0,'部门','部门','3','sys_office_type','1','','','','0','system','2018-12-04 11:21:35','system','2018-12-04 11:21:35',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793811233153024','0','0,',30,'0000000030,','1',0,'正常','正常','0','sys_search_status','1','','','','0','system','2018-12-04 11:21:35','system','2018-12-04 11:21:35',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793811442868224','0','0,',40,'0000000040,','1',0,'停用','停用','2','sys_search_status','1','','color:#f00;','','0','system','2018-12-04 11:21:35','system','2018-12-04 11:21:35',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793811610640384','0','0,',30,'0000000030,','1',0,'男','男','1','sys_user_sex','1','','','','0','system','2018-12-04 11:21:35','system','2018-12-04 11:21:35',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793811807772672','0','0,',40,'0000000040,','1',0,'女','女','2','sys_user_sex','1','','','','0','system','2018-12-04 11:21:35','system','2018-12-04 11:21:35',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793812051042304','0','0,',30,'0000000030,','1',0,'正常','正常','0','sys_user_status','1','','','','0','system','2018-12-04 11:21:35','system','2018-12-04 11:21:35',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793812235591680','0','0,',40,'0000000040,','1',0,'停用','停用','2','sys_user_status','1','','color:#f00;','','0','system','2018-12-04 11:21:35','system','2018-12-04 11:21:36',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793813674237952','0','0,',50,'0000000050,','1',0,'冻结','冻结','3','sys_user_status','1','','color:#fa0;','','0','system','2018-12-04 11:21:36','system','2018-12-04 11:21:36',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793813854593024','0','0,',30,'0000000030,','1',0,'员工','员工','employee','sys_user_type','1','','','','0','system','2018-12-04 11:21:36','system','2018-12-04 11:21:36',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793813997199360','0','0,',40,'0000000040,','1',0,'会员','会员','member','sys_user_type','1','','','','0','system','2018-12-04 11:21:36','system','2018-12-04 11:21:36',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793814202720256','0','0,',50,'0000000050,','1',0,'单位','单位','btype','sys_user_type','1','','','','0','system','2018-12-04 11:21:36','system','2018-12-04 11:21:36',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793814399852544','0','0,',60,'0000000060,','1',0,'个人','个人','persion','sys_user_type','1','','','','0','system','2018-12-04 11:21:36','system','2018-12-04 11:21:36',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793814571819008','0','0,',70,'0000000070,','1',0,'专家','专家','expert','sys_user_type','1','','','','0','system','2018-12-04 11:21:36','system','2018-12-04 11:21:36',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793814727008256','0','0,',30,'0000000030,','1',0,'高管','高管','1','sys_role_type','1','','','','0','system','2018-12-04 11:21:36','system','2018-12-04 11:21:36',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793814915751936','0','0,',40,'0000000040,','1',0,'中层','中层','2','sys_role_type','1','','','','0','system','2018-12-04 11:21:36','system','2018-12-04 11:21:36',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793815129661440','0','0,',50,'0000000050,','1',0,'基层','基层','3','sys_role_type','1','','','','0','system','2018-12-04 11:21:36','system','2018-12-04 11:21:36',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793815335182336','0','0,',60,'0000000060,','1',0,'其它','其它','4','sys_role_type','1','','','','0','system','2018-12-04 11:21:36','system','2018-12-04 11:21:36',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793815658143744','0','0,',30,'0000000030,','1',0,'未设置','未设置','0','sys_role_data_scope','1','','','','0','system','2018-12-04 11:21:36','system','2018-12-04 11:21:36',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793815901413376','0','0,',40,'0000000040,','1',0,'全部数据','全部数据','1','sys_role_data_scope','1','','','','0','system','2018-12-04 11:21:36','system','2018-12-04 11:21:36',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793816115322880','0','0,',50,'0000000050,','1',0,'自定义数据','自定义数据','2','sys_role_data_scope','1','','','','0','system','2018-12-04 11:21:36','system','2018-12-04 11:21:36',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793816299872256','0','0,',60,'0000000060,','1',0,'本部门数据','本部门数据','3','sys_role_data_scope','1','','','','0','system','2018-12-04 11:21:36','system','2018-12-04 11:21:36',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793816492810240','0','0,',70,'0000000070,','1',0,'本公司数据','本公司数据','4','sys_role_data_scope','1','','','','0','system','2018-12-04 11:21:36','system','2018-12-04 11:21:36',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793816715108352','0','0,',80,'0000000080,','1',0,'本部门和本公司数据','本部门和本公司数据','5','sys_role_data_scope','1','','','','0','system','2018-12-04 11:21:36','system','2018-12-04 11:21:36',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793816908046336','0','0,',30,'0000000030,','1',0,'高管','高管','1','sys_post_type','1','','','','0','system','2018-12-04 11:21:36','system','2018-12-04 11:21:36',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793817092595712','0','0,',40,'0000000040,','1',0,'中层','中层','2','sys_post_type','1','','','','0','system','2018-12-04 11:21:36','system','2018-12-04 11:21:37',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793817281339392','0','0,',50,'0000000050,','1',0,'基层','基层','3','sys_post_type','1','','','','0','system','2018-12-04 11:21:37','system','2018-12-04 11:21:37',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793817474277376','0','0,',60,'0000000060,','1',0,'其它','其它','4','sys_post_type','1','','','','0','system','2018-12-04 11:21:37','system','2018-12-04 11:21:37',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793817663021056','0','0,',30,'0000000030,','1',0,'接入日志','接入日志','access','sys_log_type','1','','','','0','system','2018-12-04 11:21:37','system','2018-12-04 11:21:37',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793817855959040','0','0,',40,'0000000040,','1',0,'修改日志','修改日志','update','sys_log_type','1','','','','0','system','2018-12-04 11:21:37','system','2018-12-04 11:21:37',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793818036314112','0','0,',50,'0000000050,','1',0,'查询日志','查询日志','select','sys_log_type','1','','','','0','system','2018-12-04 11:21:37','system','2018-12-04 11:21:37',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793818195697664','0','0,',60,'0000000060,','1',0,'登录登出','登录登出','loginLogout','sys_log_type','1','','','','0','system','2018-12-04 11:21:37','system','2018-12-04 11:21:37',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793818380247040','0','0,',30,'0000000030,','1',0,'默认','默认','DEFAULT','sys_job_group','1','','','','0','system','2018-12-04 11:21:37','system','2018-12-04 11:21:37',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793818522853376','0','0,',40,'0000000040,','1',0,'系统','系统','SYSTEM','sys_job_group','1','','','','0','system','2018-12-04 11:21:37','system','2018-12-04 11:21:37',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793818657071104','0','0,',30,'0000000030,','1',0,'错过计划等待本次计划完成后立即执行一次','错过计划等待本次计划完成后立即执行一次','1','sys_job_misfire_instruction','1','','','','0','system','2018-12-04 11:21:37','system','2018-12-04 11:21:37',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793818808066048','0','0,',40,'0000000040,','1',0,'本次执行时间根据上次结束时间重新计算（时间间隔方式）','本次执行时间根据上次结束时间重新计算（时间间隔方式）','2','sys_job_misfire_instruction','1','','','','0','system','2018-12-04 11:21:37','system','2018-12-04 11:21:37',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793818954866688','0','0,',30,'0000000030,','1',0,'正常','正常','0','sys_job_status','1','','','','0','system','2018-12-04 11:21:37','system','2018-12-04 11:21:37',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793819076501504','0','0,',40,'0000000040,','1',0,'删除','删除','1','sys_job_status','1','','','','0','system','2018-12-04 11:21:37','system','2018-12-04 11:21:37',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793819219107840','0','0,',50,'0000000050,','1',0,'暂停','暂停','2','sys_job_status','1','','color:#f00;','','0','system','2018-12-04 11:21:37','system','2018-12-04 11:21:37',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793819437211648','0','0,',30,'0000000030,','1',0,'完成','完成','3','sys_job_status','1','','','','0','system','2018-12-04 11:21:37','system','2018-12-04 11:21:37',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793819600789504','0','0,',40,'0000000040,','1',0,'错误','错误','4','sys_job_status','1','','color:#f00;','','0','system','2018-12-04 11:21:37','system','2018-12-04 11:21:37',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793819730812928','0','0,',50,'0000000050,','1',0,'运行','运行','5','sys_job_status','1','','','','0','system','2018-12-04 11:21:37','system','2018-12-04 11:21:37',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793819848253440','0','0,',30,'0000000030,','1',0,'计划日志','计划日志','scheduler','sys_job_type','1','','','','0','system','2018-12-04 11:21:37','system','2018-12-04 11:21:37',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793819990859776','0','0,',40,'0000000040,','1',0,'任务日志','任务日志','job','sys_job_type','1','','','','0','system','2018-12-04 11:21:37','system','2018-12-04 11:21:37',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793820141854720','0','0,',50,'0000000050,','1',0,'触发日志','触发日志','trigger','sys_job_type','1','','','','0','system','2018-12-04 11:21:37','system','2018-12-04 11:21:37',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793820263489536','0','0,',30,'0000000030,','1',0,'计划创建','计划创建','jobScheduled','sys_job_event','1','','','','0','system','2018-12-04 11:21:37','system','2018-12-04 11:21:37',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793820372541440','0','0,',40,'0000000040,','1',0,'计划移除','计划移除','jobUnscheduled','sys_job_event','1','','','','0','system','2018-12-04 11:21:37','system','2018-12-04 11:21:37',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793820494176256','0','0,',50,'0000000050,','1',0,'计划暂停','计划暂停','triggerPaused','sys_job_event','1','','','','0','system','2018-12-04 11:21:37','system','2018-12-04 11:21:37',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793820645171200','0','0,',60,'0000000060,','1',0,'计划恢复','计划恢复','triggerResumed','sys_job_event','1','','','','0','system','2018-12-04 11:21:37','system','2018-12-04 11:21:37',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793820771000320','0','0,',70,'0000000070,','1',0,'调度错误','调度错误','schedulerError','sys_job_event','1','','','','0','system','2018-12-04 11:21:37','system','2018-12-04 11:21:37',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793820917800960','0','0,',80,'0000000080,','1',0,'任务执行','任务执行','jobToBeExecuted','sys_job_event','1','','','','0','system','2018-12-04 11:21:37','system','2018-12-04 11:21:37',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793821056212992','0','0,',90,'0000000090,','1',0,'任务结束','任务结束','jobWasExecuted','sys_job_event','1','','','','0','system','2018-12-04 11:21:37','system','2018-12-04 11:21:37',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793821249150976','0','0,',100,'0000000100,','1',0,'任务停止','任务停止','jobExecutionVetoed','sys_job_event','1','','','','0','system','2018-12-04 11:21:37','system','2018-12-04 11:21:37',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793821383368704','0','0,',110,'0000000110,','1',0,'触发计划','触发计划','triggerFired','sys_job_event','1','','','','0','system','2018-12-04 11:21:37','system','2018-12-04 11:21:38',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793821492420608','0','0,',120,'0000000120,','1',0,'触发验证','触发验证','vetoJobExecution','sys_job_event','1','','','','0','system','2018-12-04 11:21:38','system','2018-12-04 11:21:38',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793821605666816','0','0,',130,'0000000130,','1',0,'触发完成','触发完成','triggerComplete','sys_job_event','1','','','','0','system','2018-12-04 11:21:38','system','2018-12-04 11:21:38',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793821714718720','0','0,',140,'0000000140,','1',0,'触发错过','触发错过','triggerMisfired','sys_job_event','1','','','','0','system','2018-12-04 11:21:38','system','2018-12-04 11:21:38',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793821882490880','0','0,',30,'0000000030,','1',0,'PC','PC','pc','sys_msg_type','1','消息类型','','','0','system','2018-12-04 11:21:38','system','2018-12-04 11:21:38',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793822037680128','0','0,',40,'0000000040,','1',0,'APP','APP','app','sys_msg_type','1','','','','0','system','2018-12-04 11:21:38','system','2018-12-04 11:21:38',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793822150926336','0','0,',50,'0000000050,','1',0,'短信','短信','sms','sys_msg_type','1','','','','0','system','2018-12-04 11:21:38','system','2018-12-04 11:21:38',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793822268366848','0','0,',60,'0000000060,','1',0,'邮件','邮件','email','sys_msg_type','1','','','','0','system','2018-12-04 11:21:38','system','2018-12-04 11:21:38',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793822402584576','0','0,',70,'0000000070,','1',0,'微信','微信','weixin','sys_msg_type','1','','','','0','system','2018-12-04 11:21:38','system','2018-12-04 11:21:38',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793822532608000','0','0,',30,'0000000030,','1',0,'待推送','待推送','0','sys_msg_push_status','1','推送状态','','','0','system','2018-12-04 11:21:38','system','2018-12-04 11:21:38',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793822654242816','0','0,',30,'0000000030,','1',0,'成功','成功','1','sys_msg_push_status','1','','','','0','system','2018-12-04 11:21:38','system','2018-12-04 11:21:38',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793822813626368','0','0,',40,'0000000040,','1',0,'失败','失败','2','sys_msg_push_status','1','','color:#f00;','','0','system','2018-12-04 11:21:38','system','2018-12-04 11:21:38',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793822947844096','0','0,',30,'0000000030,','1',0,'未送达','未送达','0','sys_msg_read_status','1','读取状态','','','0','system','2018-12-04 11:21:38','system','2018-12-04 11:21:38',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793823077867520','0','0,',40,'0000000040,','1',0,'已读','已读','1','sys_msg_read_status','1','','','','0','system','2018-12-04 11:21:38','system','2018-12-04 11:21:38',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793823237251072','0','0,',50,'0000000050,','1',0,'未读','未读','2','sys_msg_read_status','1','','','','0','system','2018-12-04 11:21:38','system','2018-12-04 11:21:38',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793823375663104','0','0,',30,'0000000030,','1',0,'普通','普通','1','msg_inner_content_level','1','内容级别','','','0','system','2018-12-04 11:21:38','system','2018-12-04 11:21:38',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793823509880832','0','0,',40,'0000000040,','1',0,'一般','一般','2','msg_inner_content_level','1','','','','0','system','2018-12-04 11:21:38','system','2018-12-04 11:21:38',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793823631515648','0','0,',50,'0000000050,','1',0,'紧急','紧急','3','msg_inner_content_level','1','','color:#f00;','','0','system','2018-12-04 11:21:38','system','2018-12-04 11:21:38',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793823761539072','0','0,',30,'0000000030,','1',0,'公告','公告','1','msg_inner_content_type','1','内容类型','','','0','system','2018-12-04 11:21:38','system','2018-12-04 11:21:38',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793823887368192','0','0,',40,'0000000040,','1',0,'新闻','新闻','2','msg_inner_content_type','1','','','','0','system','2018-12-04 11:21:38','system','2018-12-04 11:21:38',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793824017391616','0','0,',50,'0000000050,','1',0,'会议','会议','3','msg_inner_content_type','1','','','','0','system','2018-12-04 11:21:38','system','2018-12-04 11:21:38',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793824227106816','0','0,',60,'0000000060,','1',0,'其它','其它','4','msg_inner_content_type','1','','','','0','system','2018-12-04 11:21:38','system','2018-12-04 11:21:38',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793824365518848','0','0,',30,'0000000030,','1',0,'用户','用户','1','msg_inner_receiver_type','1','接受类型','','','0','system','2018-12-04 11:21:38','system','2018-12-04 11:21:38',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793824520708096','0','0,',40,'0000000040,','1',0,'部门','部门','2','msg_inner_receiver_type','1','','','','0','system','2018-12-04 11:21:38','system','2018-12-04 11:21:38',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793824638148608','0','0,',50,'0000000050,','1',0,'角色','角色','3','msg_inner_receiver_type','1','','','','0','system','2018-12-04 11:21:38','system','2018-12-04 11:21:38',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793824772366336','0','0,',60,'0000000060,','1',0,'岗位','岗位','4','msg_inner_receiver_type','1','','','','0','system','2018-12-04 11:21:38','system','2018-12-04 11:21:38',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793824919166976','0','0,',30,'0000000030,','1',0,'正常','正常','0','msg_inner_msg_status','1','消息状态','','','0','system','2018-12-04 11:21:38','system','2018-12-04 11:21:38',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793825065967616','0','0,',40,'0000000040,','1',0,'删除','删除','1','msg_inner_msg_status','1','','','','0','system','2018-12-04 11:21:38','system','2018-12-04 11:21:38',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793825216962560','0','0,',50,'0000000050,','1',0,'审核','审核','4','msg_inner_msg_status','1','','','','0','system','2018-12-04 11:21:38','system','2018-12-04 11:21:38',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793825384734720','0','0,',60,'0000000060,','1',0,'驳回','驳回','5','msg_inner_msg_status','1','','color:#f00;','','0','system','2018-12-04 11:21:38','system','2018-12-04 11:21:38',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793825531535360','0','0,',70,'0000000070,','1',0,'草稿','草稿','9','msg_inner_msg_status','1','','','','0','system','2018-12-04 11:21:38','system','2018-12-04 11:21:38',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `js_sys_dict_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_sys_dict_type`
--

DROP TABLE IF EXISTS `js_sys_dict_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `js_sys_dict_type` (
  `id` varchar(64) NOT NULL COMMENT '编号',
  `dict_name` varchar(100) NOT NULL COMMENT '字典名称',
  `dict_type` varchar(100) NOT NULL COMMENT '字典类型',
  `is_sys` char(1) NOT NULL COMMENT '是否系统字典',
  `status` char(1) NOT NULL DEFAULT '0' COMMENT '状态（0正常 1删除 2停用）',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) DEFAULT NULL COMMENT '备注信息',
  PRIMARY KEY (`id`),
  KEY `idx_sys_dict_type_is` (`is_sys`),
  KEY `idx_sys_dict_type_status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='字典类型表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_sys_dict_type`
--

LOCK TABLES `js_sys_dict_type` WRITE;
/*!40000 ALTER TABLE `js_sys_dict_type` DISABLE KEYS */;
INSERT INTO `js_sys_dict_type` VALUES ('1069793802970374144','是否','sys_yes_no','1','0','system','2018-12-04 11:21:33','system','2018-12-04 11:21:33',NULL),('1069793803112980480','状态','sys_status','1','0','system','2018-12-04 11:21:33','system','2018-12-04 11:21:33',NULL),('1069793803171700736','显示隐藏','sys_show_hide','1','0','system','2018-12-04 11:21:33','system','2018-12-04 11:21:33',NULL),('1069793803243003904','国际化语言类型','sys_lang_type','1','0','system','2018-12-04 11:21:33','system','2018-12-04 11:21:33',NULL),('1069793803314307072','客户端设备类型','sys_device_type','1','0','system','2018-12-04 11:21:33','system','2018-12-04 11:21:33',NULL),('1069793803385610240','菜单归属系统','sys_menu_sys_code','1','0','system','2018-12-04 11:21:33','system','2018-12-04 11:21:33',NULL),('1069793803448524800','菜单类型','sys_menu_type','1','0','system','2018-12-04 11:21:33','system','2018-12-04 11:21:33',NULL),('1069793803511439360','菜单权重','sys_menu_weight','1','0','system','2018-12-04 11:21:33','system','2018-12-04 11:21:33',NULL),('1069793803574353920','区域类型','sys_area_type','1','0','system','2018-12-04 11:21:33','system','2018-12-04 11:21:33',NULL),('1069793803633074176','机构类型','sys_office_type','1','0','system','2018-12-04 11:21:33','system','2018-12-04 11:21:33',NULL),('1069793803691794432','查询状态','sys_search_status','1','0','system','2018-12-04 11:21:33','system','2018-12-04 11:21:33',NULL),('1069793803746320384','用户性别','sys_user_sex','1','0','system','2018-12-04 11:21:33','system','2018-12-04 11:21:33',NULL),('1069793803792457728','用户状态','sys_user_status','1','0','system','2018-12-04 11:21:33','system','2018-12-04 11:21:33',NULL),('1069793803846983680','用户类型','sys_user_type','1','0','system','2018-12-04 11:21:33','system','2018-12-04 11:21:33',NULL),('1069793803905703936','角色分类','sys_role_type','1','0','system','2018-12-04 11:21:33','system','2018-12-04 11:21:33',NULL),('1069793803960229888','角色数据范围','sys_role_data_scope','1','0','system','2018-12-04 11:21:33','system','2018-12-04 11:21:33',NULL),('1069793804018950144','岗位分类','sys_post_type','1','0','system','2018-12-04 11:21:33','system','2018-12-04 11:21:33',NULL),('1069793804119613440','日志类型','sys_log_type','1','0','system','2018-12-04 11:21:33','system','2018-12-04 11:21:33',NULL),('1069793804199305216','作业分组','sys_job_group','1','0','system','2018-12-04 11:21:33','system','2018-12-04 11:21:33',NULL),('1069793804262219776','作业错过策略','sys_job_misfire_instruction','1','0','system','2018-12-04 11:21:33','system','2018-12-04 11:21:33',NULL),('1069793804325134336','作业状态','sys_job_status','1','0','system','2018-12-04 11:21:33','system','2018-12-04 11:21:33',NULL),('1069793804383854592','作业任务类型','sys_job_type','1','0','system','2018-12-04 11:21:33','system','2018-12-04 11:21:33',NULL),('1069793804446769152','作业任务事件','sys_job_event','1','0','system','2018-12-04 11:21:33','system','2018-12-04 11:21:33',NULL),('1069793804509683712','消息类型','sys_msg_type','1','0','system','2018-12-04 11:21:33','system','2018-12-04 11:21:33',NULL),('1069793804564209664','推送状态','sys_msg_push_status','1','0','system','2018-12-04 11:21:33','system','2018-12-04 11:21:33',NULL),('1069793804618735616','读取状态','sys_msg_read_status','1','0','system','2018-12-04 11:21:33','system','2018-12-04 11:21:33',NULL);
/*!40000 ALTER TABLE `js_sys_dict_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_sys_employee`
--

DROP TABLE IF EXISTS `js_sys_employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `js_sys_employee` (
  `emp_code` varchar(64) NOT NULL COMMENT '员工编码',
  `emp_name` varchar(100) NOT NULL COMMENT '员工姓名',
  `emp_name_en` varchar(100) DEFAULT NULL COMMENT '英文名',
  `office_code` varchar(64) NOT NULL COMMENT '机构编码',
  `office_name` varchar(100) NOT NULL COMMENT '机构名称',
  `company_code` varchar(64) DEFAULT NULL COMMENT '公司编码',
  `company_name` varchar(200) DEFAULT NULL COMMENT '公司名称',
  `status` char(1) NOT NULL COMMENT '状态（0在职 1删除 2离职）',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) DEFAULT NULL COMMENT '备注信息',
  `corp_code` varchar(64) NOT NULL DEFAULT '0' COMMENT '租户代码',
  `corp_name` varchar(100) NOT NULL DEFAULT 'JeeSite' COMMENT '租户名称',
  PRIMARY KEY (`emp_code`),
  KEY `idx_sys_employee_cco` (`company_code`),
  KEY `idx_sys_employee_cc` (`corp_code`),
  KEY `idx_sys_employee_ud` (`update_date`),
  KEY `idx_sys_employee_oc` (`office_code`),
  KEY `idx_sys_employee_status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='员工表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_sys_employee`
--

LOCK TABLES `js_sys_employee` WRITE;
/*!40000 ALTER TABLE `js_sys_employee` DISABLE KEYS */;
INSERT INTO `js_sys_employee` VALUES ('user10_e2ie','用户10',NULL,'SDJN03','研发部','SDJN','济南公司','0','system','2018-12-04 11:23:53','system','2018-12-04 11:23:53',NULL,'0','JeeSite'),('user11_us21','用户11',NULL,'SDJN03','研发部','SDJN','济南公司','0','system','2018-12-04 11:23:55','system','2018-12-04 11:23:55',NULL,'0','JeeSite'),('user12_u9li','用户12',NULL,'SDJN03','研发部','SDJN','济南公司','0','system','2018-12-04 11:23:57','system','2018-12-04 11:23:57',NULL,'0','JeeSite'),('user13_5ael','用户13',NULL,'SDJN03','研发部','SDJN','济南公司','0','system','2018-12-04 11:23:58','system','2018-12-04 11:23:58',NULL,'0','JeeSite'),('user14_iyau','用户14',NULL,'SDJN03','研发部','SDJN','济南公司','0','system','2018-12-04 11:24:00','system','2018-12-04 11:24:00',NULL,'0','JeeSite'),('user15_luzt','用户15',NULL,'SDQD01','企管部','SDQD','青岛公司','0','system','2018-12-04 11:24:01','system','2018-12-04 11:24:01',NULL,'0','JeeSite'),('user16_tqgx','用户16',NULL,'SDQD01','企管部','SDQD','青岛公司','0','system','2018-12-04 11:24:03','system','2018-12-04 11:24:03',NULL,'0','JeeSite'),('user17_pt7y','用户17',NULL,'SDQD01','企管部','SDQD','青岛公司','0','system','2018-12-04 11:24:05','system','2018-12-04 11:24:05',NULL,'0','JeeSite'),('user18_3g45','用户18',NULL,'SDQD02','财务部','SDQD','青岛公司','0','system','2018-12-04 11:24:06','system','2018-12-04 11:24:06',NULL,'0','JeeSite'),('user19_ni2v','用户19',NULL,'SDQD02','财务部','SDQD','青岛公司','0','system','2018-12-04 11:24:08','system','2018-12-04 11:24:08',NULL,'0','JeeSite'),('user1_7ppw','用户01',NULL,'SDJN01','企管部','SDJN','济南公司','0','system','2018-12-04 11:23:39','system','2018-12-04 11:23:39',NULL,'0','JeeSite'),('user20_z6bi','用户20',NULL,'SDQD02','财务部','SDQD','青岛公司','0','system','2018-12-04 11:24:09','system','2018-12-04 11:24:09',NULL,'0','JeeSite'),('user21_szwr','用户21',NULL,'SDQD03','研发部','SDQD','青岛公司','0','system','2018-12-04 11:24:11','system','2018-12-04 11:24:11',NULL,'0','JeeSite'),('user22_u7dq','用户22',NULL,'SDQD03','研发部','SDQD','青岛公司','0','system','2018-12-04 11:24:13','system','2018-12-04 11:24:13',NULL,'0','JeeSite'),('user23_btwa','用户23',NULL,'SDQD03','研发部','SDQD','青岛公司','0','system','2018-12-04 11:24:14','system','2018-12-04 11:24:14',NULL,'0','JeeSite'),('user2_7nxn','用户02',NULL,'SDJN01','企管部','SDJN','济南公司','0','system','2018-12-04 11:23:41','system','2018-12-04 11:23:41',NULL,'0','JeeSite'),('user3_ebwj','用户03',NULL,'SDJN01','企管部','SDJN','济南公司','0','system','2018-12-04 11:23:42','system','2018-12-04 11:23:42',NULL,'0','JeeSite'),('user4_62k1','用户04',NULL,'SDJN02','财务部','SDJN','济南公司','0','system','2018-12-04 11:23:44','system','2018-12-04 11:23:44',NULL,'0','JeeSite'),('user5_x004','用户05',NULL,'SDJN02','财务部','SDJN','济南公司','0','system','2018-12-04 11:23:45','system','2018-12-04 11:23:45',NULL,'0','JeeSite'),('user6_hmvr','用户06',NULL,'SDJN02','财务部','SDJN','济南公司','0','system','2018-12-04 11:23:47','system','2018-12-04 11:23:47',NULL,'0','JeeSite'),('user7_ptwy','用户07',NULL,'SDJN03','研发部','SDJN','济南公司','0','system','2018-12-04 11:23:49','system','2018-12-04 11:23:49',NULL,'0','JeeSite'),('user8_as3r','用户08',NULL,'SDJN03','研发部','SDJN','济南公司','0','system','2018-12-04 11:23:50','system','2018-12-04 11:23:50',NULL,'0','JeeSite'),('user9_eqac','用户09',NULL,'SDJN03','研发部','SDJN','济南公司','0','system','2018-12-04 11:23:52','system','2018-12-04 11:23:52',NULL,'0','JeeSite');
/*!40000 ALTER TABLE `js_sys_employee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_sys_employee_post`
--

DROP TABLE IF EXISTS `js_sys_employee_post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `js_sys_employee_post` (
  `emp_code` varchar(64) NOT NULL COMMENT '员工编码',
  `post_code` varchar(64) NOT NULL COMMENT '岗位编码',
  PRIMARY KEY (`emp_code`,`post_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='员工与岗位关联表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_sys_employee_post`
--

LOCK TABLES `js_sys_employee_post` WRITE;
/*!40000 ALTER TABLE `js_sys_employee_post` DISABLE KEYS */;
INSERT INTO `js_sys_employee_post` VALUES ('user10_e2ie','user'),('user11_us21','user'),('user12_u9li','user'),('user13_5ael','user'),('user14_iyau','dept'),('user15_luzt','dept'),('user16_tqgx','user'),('user17_pt7y','user'),('user18_3g45','dept'),('user19_ni2v','user'),('user1_7ppw','dept'),('user20_z6bi','user'),('user21_szwr','dept'),('user22_u7dq','user'),('user23_btwa','user'),('user2_7nxn','user'),('user3_ebwj','user'),('user4_62k1','dept'),('user5_x004','user'),('user6_hmvr','user'),('user7_ptwy','dept'),('user8_as3r','user'),('user9_eqac','user');
/*!40000 ALTER TABLE `js_sys_employee_post` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_sys_file_entity`
--

DROP TABLE IF EXISTS `js_sys_file_entity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `js_sys_file_entity` (
  `file_id` varchar(64) NOT NULL COMMENT '文件编号',
  `file_md5` varchar(64) NOT NULL COMMENT '文件MD5',
  `file_path` varchar(1000) NOT NULL COMMENT '文件相对路径',
  `file_content_type` varchar(200) NOT NULL COMMENT '文件内容类型',
  `file_extension` varchar(100) NOT NULL COMMENT '文件后缀扩展名',
  `file_size` decimal(31,0) NOT NULL COMMENT '文件大小(单位B)',
  PRIMARY KEY (`file_id`),
  UNIQUE KEY `file_md5` (`file_md5`),
  KEY `idx_sys_file_entity_md5` (`file_md5`),
  KEY `idx_sys_file_entity_size` (`file_size`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='文件实体表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_sys_file_entity`
--

LOCK TABLES `js_sys_file_entity` WRITE;
/*!40000 ALTER TABLE `js_sys_file_entity` DISABLE KEYS */;
/*!40000 ALTER TABLE `js_sys_file_entity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_sys_file_upload`
--

DROP TABLE IF EXISTS `js_sys_file_upload`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `js_sys_file_upload` (
  `id` varchar(64) NOT NULL COMMENT '编号',
  `file_id` varchar(64) NOT NULL COMMENT '文件编号',
  `file_name` varchar(500) NOT NULL COMMENT '文件名称',
  `file_type` varchar(20) NOT NULL COMMENT '文件分类（image、media、file）',
  `biz_key` varchar(64) DEFAULT NULL COMMENT '业务主键',
  `biz_type` varchar(64) DEFAULT NULL COMMENT '业务类型',
  `status` char(1) NOT NULL DEFAULT '0' COMMENT '状态（0正常 1删除 2停用）',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) DEFAULT NULL COMMENT '备注信息',
  PRIMARY KEY (`id`),
  KEY `idx_sys_file_biz_ft` (`file_type`),
  KEY `idx_sys_file_biz_fi` (`file_id`),
  KEY `idx_sys_file_biz_status` (`status`),
  KEY `idx_sys_file_biz_cb` (`create_by`),
  KEY `idx_sys_file_biz_ud` (`update_date`),
  KEY `idx_sys_file_biz_bt` (`biz_type`),
  KEY `idx_sys_file_biz_bk` (`biz_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='文件上传表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_sys_file_upload`
--

LOCK TABLES `js_sys_file_upload` WRITE;
/*!40000 ALTER TABLE `js_sys_file_upload` DISABLE KEYS */;
/*!40000 ALTER TABLE `js_sys_file_upload` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_sys_job`
--

DROP TABLE IF EXISTS `js_sys_job`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `js_sys_job` (
  `job_name` varchar(64) NOT NULL COMMENT '任务名称',
  `job_group` varchar(64) NOT NULL COMMENT '任务组名',
  `description` varchar(100) NOT NULL COMMENT '任务描述',
  `invoke_target` varchar(1000) NOT NULL COMMENT '调用目标字符串',
  `cron_expression` varchar(255) NOT NULL COMMENT 'Cron执行表达式',
  `misfire_instruction` decimal(1,0) NOT NULL COMMENT '计划执行错误策略',
  `concurrent` char(1) NOT NULL COMMENT '是否并发执行',
  `status` char(1) NOT NULL COMMENT '状态（0正常 1删除 2暂停）',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) DEFAULT NULL COMMENT '备注信息',
  PRIMARY KEY (`job_name`,`job_group`),
  KEY `idx_sys_job_status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='作业调度表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_sys_job`
--

LOCK TABLES `js_sys_job` WRITE;
/*!40000 ALTER TABLE `js_sys_job` DISABLE KEYS */;
INSERT INTO `js_sys_job` VALUES ('MsgLocalMergePushTask','SYSTEM','消息推送服务 (延迟推送)','msgLocalMergePushTask.execute()','0 0/30 * * * ?',2,'0','2','system','2018-12-04 11:24:14','system','2018-12-04 11:24:14',NULL),('MsgLocalPushTask','SYSTEM','消息推送服务 (实时推送)','msgLocalPushTask.execute()','0/3 * * * * ?',2,'0','2','system','2018-12-04 11:24:14','system','2018-12-04 11:24:14',NULL);
/*!40000 ALTER TABLE `js_sys_job` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_sys_job_log`
--

DROP TABLE IF EXISTS `js_sys_job_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `js_sys_job_log` (
  `id` varchar(64) NOT NULL COMMENT '编号',
  `job_name` varchar(64) NOT NULL COMMENT '任务名称',
  `job_group` varchar(64) NOT NULL COMMENT '任务组名',
  `job_type` varchar(50) DEFAULT NULL COMMENT '日志类型',
  `job_event` varchar(200) DEFAULT NULL COMMENT '日志事件',
  `job_message` varchar(500) DEFAULT NULL COMMENT '日志信息',
  `is_exception` char(1) DEFAULT NULL COMMENT '是否异常',
  `exception_info` text COMMENT '异常信息',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `idx_sys_job_log_jn` (`job_name`),
  KEY `idx_sys_job_log_jg` (`job_group`),
  KEY `idx_sys_job_log_t` (`job_type`),
  KEY `idx_sys_job_log_e` (`job_event`),
  KEY `idx_sys_job_log_ie` (`is_exception`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='作业调度日志表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_sys_job_log`
--

LOCK TABLES `js_sys_job_log` WRITE;
/*!40000 ALTER TABLE `js_sys_job_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `js_sys_job_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_sys_lang`
--

DROP TABLE IF EXISTS `js_sys_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `js_sys_lang` (
  `id` varchar(64) NOT NULL COMMENT '编号',
  `module_code` varchar(64) NOT NULL COMMENT '归属模块',
  `lang_code` varchar(500) NOT NULL COMMENT '语言编码',
  `lang_text` varchar(500) NOT NULL COMMENT '语言译文',
  `lang_type` varchar(50) NOT NULL COMMENT '语言类型',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) DEFAULT NULL COMMENT '备注信息',
  PRIMARY KEY (`id`),
  KEY `idx_sys_lang_code` (`lang_code`(255)),
  KEY `idx_sys_lang_type` (`lang_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='国际化语言';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_sys_lang`
--

LOCK TABLES `js_sys_lang` WRITE;
/*!40000 ALTER TABLE `js_sys_lang` DISABLE KEYS */;
/*!40000 ALTER TABLE `js_sys_lang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_sys_log`
--

DROP TABLE IF EXISTS `js_sys_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `js_sys_log` (
  `id` varchar(64) NOT NULL COMMENT '编号',
  `log_type` varchar(50) NOT NULL COMMENT '日志类型',
  `log_title` varchar(500) NOT NULL COMMENT '日志标题',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_by_name` varchar(100) NOT NULL COMMENT '用户名称',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `request_uri` varchar(500) DEFAULT NULL COMMENT '请求URI',
  `request_method` varchar(10) DEFAULT NULL COMMENT '操作方式',
  `request_params` longtext COMMENT '操作提交的数据',
  `diff_modify_data` text COMMENT '新旧数据比较结果',
  `biz_key` varchar(64) DEFAULT NULL COMMENT '业务主键',
  `biz_type` varchar(64) DEFAULT NULL COMMENT '业务类型',
  `remote_addr` varchar(255) NOT NULL COMMENT '操作IP地址',
  `server_addr` varchar(255) NOT NULL COMMENT '请求服务器地址',
  `is_exception` char(1) DEFAULT NULL COMMENT '是否异常',
  `exception_info` text COMMENT '异常信息',
  `user_agent` varchar(500) DEFAULT NULL COMMENT '用户代理',
  `device_name` varchar(100) DEFAULT NULL COMMENT '设备名称/操作系统',
  `browser_name` varchar(100) DEFAULT NULL COMMENT '浏览器名称',
  `execute_time` decimal(19,0) DEFAULT NULL COMMENT '执行时间',
  `corp_code` varchar(64) NOT NULL DEFAULT '0' COMMENT '租户代码',
  `corp_name` varchar(100) NOT NULL DEFAULT 'JeeSite' COMMENT '租户名称',
  PRIMARY KEY (`id`),
  KEY `idx_sys_log_cb` (`create_by`),
  KEY `idx_sys_log_cc` (`corp_code`),
  KEY `idx_sys_log_lt` (`log_type`),
  KEY `idx_sys_log_bk` (`biz_key`),
  KEY `idx_sys_log_bt` (`biz_type`),
  KEY `idx_sys_log_ie` (`is_exception`),
  KEY `idx_sys_log_cd` (`create_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='操作日志表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_sys_log`
--

LOCK TABLES `js_sys_log` WRITE;
/*!40000 ALTER TABLE `js_sys_log` DISABLE KEYS */;
INSERT INTO `js_sys_log` VALUES ('1069798143897161728','loginLogout','系统登录','system','超级管理员','2018-12-04 11:38:48','/js/a/login','POST','username=F3EDC7D2C193E0B8DCF554C726719ED2&password=*&validCode=',NULL,NULL,NULL,'127.0.0.1','http://127.0.0.1:8980','0','','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36','Windows 10','Chrome',0,'0','JeeSite'),('1069799066500554752','loginLogout','系统登录','system','超级管理员','2018-12-04 11:42:28','/js/a/login','POST','username=F3EDC7D2C193E0B8DCF554C726719ED2&password=*&validCode=',NULL,NULL,NULL,'127.0.0.1','http://127.0.0.1:8980','0','','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36','Windows 10','Chrome',0,'0','JeeSite'),('1069804079817908224','access','系统管理/组织管理/用户管理','system','超级管理员','2018-12-04 12:02:23','/js/a/sys/empUser/index','GET','',NULL,'','EmpUser','127.0.0.1','http://127.0.0.1:8980','0','','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36','Windows 10','Chrome',46,'0','JeeSite'),('1069804081126531072','select','系统管理/组织管理/用户管理/查看','system','超级管理员','2018-12-04 12:02:24','/js/a/sys/empUser/list','GET','',NULL,'','EmpUser','127.0.0.1','http://127.0.0.1:8980','0','','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36','Windows 10','Chrome',158,'0','JeeSite'),('1069804083135602688','select','系统管理/组织管理/用户管理','system','超级管理员','2018-12-04 12:02:24','/js/a/sys/empUser/listData','POST','ctrlPermi=2&loginCode=&userName=&email=&mobile=&phone=&refName=&employee.office.officeName=&employee.office.officeCode=&employee.company.companyName=&employee.company.companyCode=&employee.postCode=&status=&pageNo=&pageSize=&orderBy=',NULL,'','EmpUser','127.0.0.1','http://127.0.0.1:8980','0','','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36','Windows 10','Chrome',206,'0','JeeSite'),('1069804102232268800','select','系统管理/组织管理/用户管理/查看','system','超级管理员','2018-12-04 12:02:29','/js/a/sys/empUser/form','GET','op=add',NULL,'','EmpUser','127.0.0.1','http://127.0.0.1:8980','0','','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36','Windows 10','Chrome',49,'0','JeeSite'),('1069826444401131520','loginLogout','系统登录','system','超级管理员','2018-12-04 13:31:15','/js/a/login','POST','username=F3EDC7D2C193E0B8DCF554C726719ED2&password=*&validCode=&rememberUserCode=on',NULL,NULL,NULL,'119.251.16.17','http://140.143.236.168:8980','0','','Mozilla/5.0 (Windows NT 6.1; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0','Windows 7','Firefox 49',0,'0','JeeSite'),('1073486829954969600','loginLogout','系统登录','system','超级管理员','2018-12-14 15:56:19','/js/a/login','POST','username=12D443AAC95D9747DCF554C726719ED2&password=*&validCode=',NULL,NULL,NULL,'0:0:0:0:0:0:0:1','http://localhost:8980','0','','Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36','Windows 10','Chrome 65',0,'0','JeeSite'),('1073509222680698880','loginLogout','系统登录','system','超级管理员','2018-12-14 17:25:18','/js/a/login','POST','username=12D443AAC95D9747DCF554C726719ED2&password=*&validCode=',NULL,NULL,NULL,'0:0:0:0:0:0:0:1','http://localhost:8980','0','','Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36','Windows 10','Chrome 65',0,'0','JeeSite'),('1073510825282314240','loginLogout','系统登录','system','超级管理员','2018-12-14 17:31:40','/js/a/login','POST','username=12D443AAC95D9747DCF554C726719ED2&password=*&validCode=&rememberUserCode=on',NULL,NULL,NULL,'0:0:0:0:0:0:0:1','http://localhost:8980','0','','Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36','Windows 10','Chrome 65',0,'0','JeeSite'),('1073511025040236544','loginLogout','系统登录','system','超级管理员','2018-12-14 17:32:28','/js/a/login','POST','username=12D443AAC95D9747DCF554C726719ED2&password=*&validCode=',NULL,NULL,NULL,'0:0:0:0:0:0:0:1','http://localhost:8980','0','','Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36','Windows 10','Chrome 65',0,'0','JeeSite'),('1073513373344043008','loginLogout','系统登录','system','超级管理员','2018-12-14 17:41:48','/js/a/login','POST','username=12D443AAC95D9747DCF554C726719ED2&password=*&validCode=&rememberUserCode=on&rememberMe=on',NULL,NULL,NULL,'0:0:0:0:0:0:0:1','http://localhost:8980','0','','Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36','Windows 10','Chrome 65',0,'0','JeeSite'),('1073774000192212992','loginLogout','系统登录','system','超级管理员','2018-12-15 10:57:26','/jflow/a/login','POST','username=F3EDC7D2C193E0B8DCF554C726719ED2&password=*&validCode=',NULL,NULL,NULL,'0:0:0:0:0:0:0:1','http://localhost:8980','0','','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','Windows 7','Chrome 63',0,'0','JeeSite'),('1073774438861885440','access','系统管理/组织管理/用户管理','system','超级管理员','2018-12-15 10:59:11','/jflow/a/sys/empUser/index','GET','',NULL,'','EmpUser','0:0:0:0:0:0:0:1','http://localhost:8980','0','','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','Windows 7','Chrome 63',167,'0','JeeSite'),('1073774441797898240','select','系统管理/组织管理/用户管理/查看','system','超级管理员','2018-12-15 10:59:11','/jflow/a/sys/empUser/list','GET','',NULL,'','EmpUser','0:0:0:0:0:0:0:1','http://localhost:8980','0','','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','Windows 7','Chrome 63',479,'0','JeeSite'),('1073774446671679488','select','系统管理/组织管理/用户管理','system','超级管理员','2018-12-15 10:59:13','/jflow/a/sys/empUser/listData','POST','ctrlPermi=2&loginCode=&userName=&email=&mobile=&phone=&refName=&employee.office.officeName=&employee.office.officeCode=&employee.company.companyName=&employee.company.companyCode=&employee.postCode=&status=&pageNo=&pageSize=&orderBy=',NULL,'','EmpUser','0:0:0:0:0:0:0:1','http://localhost:8980','0','','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','Windows 7','Chrome 63',680,'0','JeeSite'),('1073774782350217216','access','系统管理/系统设置/菜单管理','system','超级管理员','2018-12-15 11:00:33','/jflow/a/sys/menu/list','GET','',NULL,'','Menu','0:0:0:0:0:0:0:1','http://localhost:8980','0','','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','Windows 7','Chrome 63',72,'0','JeeSite'),('1073774784183128064','select','系统管理/系统设置/菜单管理','system','超级管理员','2018-12-15 11:00:33','/jflow/a/sys/menu/listData','POST','moduleCodes=&sysCode=default&menuNameOrig=&pageNo=&pageSize=&orderBy=',NULL,'','Menu','0:0:0:0:0:0:0:1','http://localhost:8980','0','','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','Windows 7','Chrome 63',173,'0','JeeSite'),('1078211541163913216','loginLogout','系统登录','system','超级管理员','2018-12-27 16:50:38','/js/a/login','POST','username=F3EDC7D2C193E0B8DCF554C726719ED2&password=*&validCode=',NULL,NULL,NULL,'0:0:0:0:0:0:0:1','http://localhost:8980','0','','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','Windows 7','Chrome 63',0,'0','JeeSite'),('1078211565377630208','access','系统管理/组织管理/用户管理','system','超级管理员','2018-12-27 16:50:44','/js/a/sys/empUser/index','GET','',NULL,'','EmpUser','0:0:0:0:0:0:0:1','http://localhost:8980','0','','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','Windows 7','Chrome 63',140,'0','JeeSite'),('1078211568082956288','select','系统管理/组织管理/用户管理/查看','system','超级管理员','2018-12-27 16:50:45','/js/a/sys/empUser/list','GET','',NULL,'','EmpUser','0:0:0:0:0:0:0:1','http://localhost:8980','0','','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','Windows 7','Chrome 63',341,'0','JeeSite'),('1078211571081883648','select','系统管理/组织管理/用户管理','system','超级管理员','2018-12-27 16:50:45','/js/a/sys/empUser/listData','POST','ctrlPermi=2&loginCode=&userName=&email=&mobile=&phone=&refName=&employee.office.officeName=&employee.office.officeCode=&employee.company.companyName=&employee.company.companyCode=&employee.postCode=&status=&pageNo=&pageSize=&orderBy=',NULL,'','EmpUser','0:0:0:0:0:0:0:1','http://localhost:8980','0','','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','Windows 7','Chrome 63',372,'0','JeeSite'),('1078211585887776768','access','系统管理/组织管理/机构管理','system','超级管理员','2018-12-27 16:50:49','/js/a/sys/office/list','GET','',NULL,'','Office','0:0:0:0:0:0:0:1','http://localhost:8980','0','','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','Windows 7','Chrome 63',64,'0','JeeSite'),('1078211586965712896','select','系统管理/组织管理/机构管理','system','超级管理员','2018-12-27 16:50:49','/js/a/sys/office/listData','POST','ctrlPermi=2&viewCode=&officeName=&fullName=&officeType=&status=&pageNo=&pageSize=&orderBy=',NULL,'','Office','0:0:0:0:0:0:0:1','http://localhost:8980','0','','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','Windows 7','Chrome 63',49,'0','JeeSite'),('1078211604669870080','select','系统管理/组织管理/机构管理/查看','system','超级管理员','2018-12-27 16:50:53','/js/a/sys/office/form','GET','',NULL,'','Office','0:0:0:0:0:0:0:1','http://localhost:8980','0','','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','Windows 7','Chrome 63',217,'0','JeeSite'),('1078212270544990208','access','系统管理/组织管理/公司管理','system','超级管理员','2018-12-27 16:53:32','/js/a/sys/company/list','GET','',NULL,'','Company','0:0:0:0:0:0:0:1','http://localhost:8980','0','','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','Windows 7','Chrome 63',151,'0','JeeSite'),('1078212272109465600','select','系统管理/组织管理/公司管理','system','超级管理员','2018-12-27 16:53:33','/js/a/sys/company/listData','POST','ctrlPermi=2&viewCode=&companyName=&fullName=&status=&pageNo=&pageSize=&orderBy=',NULL,'','Company','0:0:0:0:0:0:0:1','http://localhost:8980','0','','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','Windows 7','Chrome 63',82,'0','JeeSite'),('1078212283308257280','select','系统管理/组织管理/公司管理/查看','system','超级管理员','2018-12-27 16:53:35','/js/a/sys/company/form','GET','',NULL,'','Company','0:0:0:0:0:0:0:1','http://localhost:8980','0','','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','Windows 7','Chrome 63',91,'0','JeeSite'),('1078212413059051520','select','系统管理/组织管理/机构管理/查看','system','超级管理员','2018-12-27 16:54:06','/js/a/sys/office/form','GET','officeCode=SD',NULL,'SD','Office','0:0:0:0:0:0:0:1','http://localhost:8980','0','','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','Windows 7','Chrome 63',72,'0','JeeSite'),('1078212453353730048','access','系统管理/组织管理/公司管理','system','超级管理员','2018-12-27 16:54:16','/js/a/sys/company/list','GET','',NULL,'','Company','0:0:0:0:0:0:0:1','http://localhost:8980','0','','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','Windows 7','Chrome 63',25,'0','JeeSite'),('1078212454121287680','select','系统管理/组织管理/公司管理','system','超级管理员','2018-12-27 16:54:16','/js/a/sys/company/listData','POST','ctrlPermi=2&viewCode=&companyName=&fullName=&status=&pageNo=&pageSize=&orderBy=',NULL,'','Company','0:0:0:0:0:0:0:1','http://localhost:8980','0','','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','Windows 7','Chrome 63',37,'0','JeeSite'),('1078212495737171968','select','系统管理/组织管理/机构管理','system','超级管理员','2018-12-27 16:54:26','/js/a/sys/office/listData','POST','id=&nodeid=SD&parentid=0&n_level=0&_search=false&nd=1545900865809&pageSize=&pageNo=&orderBy=&sord=asc&parentCode=SD&ctrlPermi=2&viewCode=&officeName=&fullName=&officeType=&status=',NULL,'','Office','0:0:0:0:0:0:0:1','http://localhost:8980','0','','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','Windows 7','Chrome 63',36,'0','JeeSite'),('1078212501315596288','select','系统管理/组织管理/机构管理','system','超级管理员','2018-12-27 16:54:27','/js/a/sys/office/listData','POST','id=&nodeid=SDJN&parentid=SD&n_level=1&_search=false&nd=1545900867159&pageSize=&pageNo=&orderBy=&sord=asc&parentCode=SDJN&ctrlPermi=2&viewCode=&officeName=&fullName=&officeType=&status=',NULL,'','Office','0:0:0:0:0:0:0:1','http://localhost:8980','0','','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','Windows 7','Chrome 63',25,'0','JeeSite'),('1078212507707715584','select','系统管理/组织管理/机构管理','system','超级管理员','2018-12-27 16:54:29','/js/a/sys/office/listData','POST','id=&nodeid=SDQD&parentid=SD&n_level=1&_search=false&nd=1545900868679&pageSize=&pageNo=&orderBy=&sord=asc&parentCode=SDQD&ctrlPermi=2&viewCode=&officeName=&fullName=&officeType=&status=',NULL,'','Office','0:0:0:0:0:0:0:1','http://localhost:8980','0','','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','Windows 7','Chrome 63',29,'0','JeeSite'),('1078212578302046208','select','系统管理/组织管理/用户管理','system','超级管理员','2018-12-27 16:54:46','/js/a/sys/empUser/listData','POST','ctrlPermi=2&loginCode=&userName=&email=&mobile=&phone=&refName=&employee.office.officeName=&employee.office.officeCode=&employee.company.companyName=济南公司&employee.company.companyCode=SDJN&employee.postCode=&status=&pageNo=&pageSize=&orderBy=',NULL,'','EmpUser','0:0:0:0:0:0:0:1','http://localhost:8980','0','','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','Windows 7','Chrome 63',134,'0','JeeSite'),('1078463109417070592','loginLogout','系统登录','system','超级管理员','2018-12-28 09:30:17','/js/a/login','POST','username=F3EDC7D2C193E0B8DCF554C726719ED2&password=*&validCode=',NULL,NULL,NULL,'127.0.0.1','http://localhost:8980','0','','Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0','Windows 7','Firefox',0,'0','JeeSite'),('1078562273741701120','loginLogout','系统登录','system','超级管理员','2018-12-28 16:04:19','/js/a/login','POST','username=F3EDC7D2C193E0B8DCF554C726719ED2&password=*&validCode=',NULL,NULL,NULL,'0:0:0:0:0:0:0:1','http://localhost:8980','0','','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.101 Safari/537.36','Windows 7','Chrome 41',0,'0','JeeSite');
/*!40000 ALTER TABLE `js_sys_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_sys_menu`
--

DROP TABLE IF EXISTS `js_sys_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `js_sys_menu` (
  `menu_code` varchar(64) NOT NULL COMMENT '菜单编码',
  `parent_code` varchar(64) NOT NULL COMMENT '父级编号',
  `parent_codes` varchar(1000) NOT NULL COMMENT '所有父级编号',
  `tree_sort` decimal(10,0) NOT NULL COMMENT '本级排序号（升序）',
  `tree_sorts` varchar(1000) NOT NULL COMMENT '所有级别排序号',
  `tree_leaf` char(1) NOT NULL COMMENT '是否最末级',
  `tree_level` decimal(4,0) NOT NULL COMMENT '层次级别',
  `tree_names` varchar(1000) NOT NULL COMMENT '全节点名',
  `menu_name` varchar(100) NOT NULL COMMENT '菜单名称',
  `menu_type` char(1) NOT NULL COMMENT '菜单类型（1菜单 2权限 3开发）',
  `menu_href` varchar(1000) DEFAULT NULL COMMENT '链接',
  `menu_target` varchar(20) DEFAULT NULL COMMENT '目标',
  `menu_icon` varchar(100) DEFAULT NULL COMMENT '图标',
  `menu_color` varchar(50) DEFAULT NULL COMMENT '颜色',
  `permission` varchar(1000) DEFAULT NULL COMMENT '权限标识',
  `weight` decimal(4,0) DEFAULT NULL COMMENT '菜单权重',
  `is_show` char(1) NOT NULL COMMENT '是否显示（1显示 0隐藏）',
  `sys_code` varchar(64) NOT NULL COMMENT '归属系统（default:主导航菜单、mobileApp:APP菜单）',
  `module_codes` varchar(500) NOT NULL COMMENT '归属模块（多个用逗号隔开）',
  `status` char(1) NOT NULL DEFAULT '0' COMMENT '状态（0正常 1删除 2停用）',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) DEFAULT NULL COMMENT '备注信息',
  `extend_s1` varchar(500) DEFAULT NULL COMMENT '扩展 String 1',
  `extend_s2` varchar(500) DEFAULT NULL COMMENT '扩展 String 2',
  `extend_s3` varchar(500) DEFAULT NULL COMMENT '扩展 String 3',
  `extend_s4` varchar(500) DEFAULT NULL COMMENT '扩展 String 4',
  `extend_s5` varchar(500) DEFAULT NULL COMMENT '扩展 String 5',
  `extend_s6` varchar(500) DEFAULT NULL COMMENT '扩展 String 6',
  `extend_s7` varchar(500) DEFAULT NULL COMMENT '扩展 String 7',
  `extend_s8` varchar(500) DEFAULT NULL COMMENT '扩展 String 8',
  `extend_i1` decimal(19,0) DEFAULT NULL COMMENT '扩展 Integer 1',
  `extend_i2` decimal(19,0) DEFAULT NULL COMMENT '扩展 Integer 2',
  `extend_i3` decimal(19,0) DEFAULT NULL COMMENT '扩展 Integer 3',
  `extend_i4` decimal(19,0) DEFAULT NULL COMMENT '扩展 Integer 4',
  `extend_f1` decimal(19,4) DEFAULT NULL COMMENT '扩展 Float 1',
  `extend_f2` decimal(19,4) DEFAULT NULL COMMENT '扩展 Float 2',
  `extend_f3` decimal(19,4) DEFAULT NULL COMMENT '扩展 Float 3',
  `extend_f4` decimal(19,4) DEFAULT NULL COMMENT '扩展 Float 4',
  `extend_d1` datetime DEFAULT NULL COMMENT '扩展 Date 1',
  `extend_d2` datetime DEFAULT NULL COMMENT '扩展 Date 2',
  `extend_d3` datetime DEFAULT NULL COMMENT '扩展 Date 3',
  `extend_d4` datetime DEFAULT NULL COMMENT '扩展 Date 4',
  PRIMARY KEY (`menu_code`),
  KEY `idx_sys_menu_pc` (`parent_code`),
  KEY `idx_sys_menu_ts` (`tree_sort`),
  KEY `idx_sys_menu_status` (`status`),
  KEY `idx_sys_menu_mt` (`menu_type`),
  KEY `idx_sys_menu_pss` (`parent_codes`(255)),
  KEY `idx_sys_menu_tss` (`tree_sorts`(255)),
  KEY `idx_sys_menu_sc` (`sys_code`),
  KEY `idx_sys_menu_is` (`is_show`),
  KEY `idx_sys_menu_mcs` (`module_codes`(255)),
  KEY `idx_sys_menu_wt` (`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='菜单表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_sys_menu`
--

LOCK TABLES `js_sys_menu` WRITE;
/*!40000 ALTER TABLE `js_sys_menu` DISABLE KEYS */;
INSERT INTO `js_sys_menu` VALUES ('1028924699103330304','0','0,',9030,'0000009030,','0',0,'JFlow流程','JFlow流程','1','','','','','',40,'1','default','jflow','0','system','2018-08-13 16:42:19','system','2018-12-28 16:03:30','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1028924891047264256','1028924699103330304','0,1028924699103330304,',30,'0000009030,0000000030,','1',1,'JFlow流程/流程设计器','流程设计器','1','//WF/Admin/CCBPMDesigner/Default.htm','','','','',40,'1','default','jflow','0','system','2018-08-13 16:43:05','system','2018-12-28 16:03:30','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1028925472004505600','1028924699103330304','0,1028924699103330304,',60,'0000009030,0000000060,','0',1,'JFlow流程/流程办理','流程办理','1','','','','','',40,'1','default','jflow','0','system','2018-08-13 16:45:24','system','2018-12-28 16:03:30','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1028933723626536960','1028925472004505600','0,1028924699103330304,1028925472004505600,',30,'0000009030,0000000060,0000000030,','1',2,'JFlow流程/流程办理/发起','发起','1','//WF/Start.htm','','','','',40,'1','default','jflow','0','system','2018-08-13 20:18:11','system','2018-12-28 16:03:30','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1028933804492718080','1028925472004505600','0,1028924699103330304,1028925472004505600,',60,'0000009030,0000000060,0000000060,','1',2,'JFlow流程/流程办理/待办','待办','1','//WF/Todolist.htm','','','','',40,'1','default','jflow','0','system','2018-08-13 20:18:30','system','2018-12-28 16:03:30','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1028933895135821824','1028925472004505600','0,1028924699103330304,1028925472004505600,',90,'0000009030,0000000060,0000000090,','1',2,'JFlow流程/流程办理/会签','会签','1','//WF/HuiQianList.htm','','','','',40,'1','default','jflow','0','system','2018-08-13 20:18:52','system','2018-12-28 16:03:30','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1028933988475863040','1028925472004505600','0,1028924699103330304,1028925472004505600,',120,'0000009030,0000000060,0000000120,','1',2,'JFlow流程/流程办理/在途','在途','1','//WF/Runing.htm','','','','',40,'1','default','jflow','0','system','2018-08-13 20:19:14','system','2018-12-28 16:03:30','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1028934263999692800','1028924699103330304','0,1028924699103330304,',90,'0000009030,0000000090,','0',1,'JFlow流程/流程查询','流程查询','1','','','','','',40,'1','default','jflow','0','system','2018-08-13 20:20:20','system','2018-12-28 16:03:30','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1028934340843536384','1028934263999692800','0,1028924699103330304,1028934263999692800,',30,'0000009030,0000000090,0000000030,','1',2,'JFlow流程/流程查询/我发起的','我发起的','1','//WF/Comm/Search.htm?EnsName=BP.WF.Data.MyStartFlows','','','','',40,'1','default','jflow','0','system','2018-08-13 20:20:38','system','2018-12-28 16:03:30','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1028934445013270528','1028934263999692800','0,1028924699103330304,1028934263999692800,',60,'0000009030,0000000090,0000000060,','1',2,'JFlow流程/流程查询/我审批的','我审批的','1','//WF/Comm/Search.htm?EnsName=BP.WF.Data.MyJoinFlows','','','','',40,'1','default','jflow','0','system','2018-08-13 20:21:03','system','2018-12-28 16:03:30','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1028934560448905216','1028934263999692800','0,1028924699103330304,1028934263999692800,',90,'0000009030,0000000090,0000000090,','1',2,'JFlow流程/流程查询/流程分布','流程分布','1','//WF/RptSearch/DistributedOfMy.htm','','','','',40,'1','default','jflow','0','system','2018-08-13 20:21:31','system','2018-12-28 16:03:30','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1028934755261743104','1028934263999692800','0,1028924699103330304,1028934263999692800,',120,'0000009030,0000000090,0000000120,','1',2,'JFlow流程/流程查询/我的流程','我的流程','1','//WF/Search.htm','','','','',40,'1','default','jflow','0','system','2018-08-13 20:22:17','system','2018-12-28 16:03:30','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1028934869279703040','1028934263999692800','0,1028924699103330304,1028934263999692800,',150,'0000009030,0000000090,0000000150,','1',2,'JFlow流程/流程查询/单流程查询','单流程查询','1','//WF/RptDfine/Flowlist.htm','','','','',40,'1','default','jflow','0','system','2018-08-13 20:22:44','system','2018-12-28 16:03:30','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1028935178097917952','1028934263999692800','0,1028924699103330304,1028934263999692800,',180,'0000009030,0000000090,0000000180,','1',2,'JFlow流程/流程查询/综合查询','综合查询','1','//WF/RptSearch/Default.htm','','','','',40,'1','default','jflow','0','system','2018-08-13 20:23:58','system','2018-12-28 16:03:30','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1028935301783748608','1028925472004505600','0,1028924699103330304,1028925472004505600,',150,'0000009030,0000000060,0000000150,','1',2,'JFlow流程/流程办理/共享任务','共享任务','1','//WF/TaskPoolSharing.htm','','','','',40,'1','default','jflow','0','system','2018-08-13 20:24:27','system','2018-12-28 16:03:30','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1028935378346573824','1028925472004505600','0,1028924699103330304,1028925472004505600,',180,'0000009030,0000000060,0000000180,','1',2,'JFlow流程/流程办理/申请下来的任务','申请下来的任务','1','//WF/TaskPoolApply.htm','','','','',40,'1','default','jflow','0','system','2018-08-13 20:24:46','system','2018-12-28 16:03:30','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1028935550501781504','1028924699103330304','0,1028924699103330304,',120,'0000009030,0000000120,','0',1,'JFlow流程/高级功能','高级功能','1','','','','','',40,'1','default','jflow','0','system','2018-08-13 20:25:27','system','2018-12-28 16:03:30','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1028935644458385408','1028935550501781504','0,1028924699103330304,1028935550501781504,',30,'0000009030,0000000120,0000000030,','1',2,'JFlow流程/高级功能/我的草稿','我的草稿','1','//WF/Draft.htm','','','','',40,'1','default','jflow','0','system','2018-08-13 20:25:49','system','2018-12-28 16:03:30','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1028935717149868032','1028935550501781504','0,1028924699103330304,1028935550501781504,',60,'0000009030,0000000120,0000000060,','1',2,'JFlow流程/高级功能/抄送','抄送','1','//WF/CC.htm','','','','',40,'1','default','jflow','0','system','2018-08-13 20:26:06','system','2018-12-28 16:03:30','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1028935775526191104','1028935550501781504','0,1028924699103330304,1028935550501781504,',90,'0000009030,0000000120,0000000090,','1',2,'JFlow流程/高级功能/我的关注','我的关注','1','//WF/Focus.htm','','','','',40,'1','default','jflow','0','system','2018-08-13 20:26:20','system','2018-12-28 16:03:30','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1028935931373944832','1028935550501781504','0,1028924699103330304,1028935550501781504,',120,'0000009030,0000000120,0000000120,','1',2,'JFlow流程/高级功能/授权待办','授权待办','1','//WF/TodolistOfAuth.htm','','','','',40,'1','default','jflow','0','system','2018-08-13 20:26:57','system','2018-12-28 16:03:30','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1028936085451702272','1028935550501781504','0,1028924699103330304,1028935550501781504,',150,'0000009030,0000000120,0000000150,','1',2,'JFlow流程/高级功能/挂起的工作','挂起的工作','1','//WF/HungUpList.htm','','','','',40,'1','default','jflow','0','system','2018-08-13 20:27:34','system','2018-12-28 16:03:30','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793827855179776','0','0,',9000,'0000009000,','0',0,'系统管理','系统管理','1','','','icon-settings','','',40,'1','default','core','0','system','2018-12-04 11:21:39','system','2018-12-28 16:03:30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793828132003840','1069793827855179776','0,1069793827855179776,',300,'0000009000,0000000300,','0',1,'系统管理/组织管理','组织管理','1','','','icon-grid','','',40,'1','default','core','0','system','2018-12-04 11:21:39','system','2018-12-28 16:03:30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793828333330432','1069793828132003840','0,1069793827855179776,1069793828132003840,',40,'0000009000,0000000300,0000000040,','0',2,'系统管理/组织管理/用户管理','用户管理','1','/sys/empUser/index','','icon-user','','',40,'1','default','core','0','system','2018-12-04 11:21:39','system','2018-12-28 16:03:30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793828547239936','1069793828333330432','0,1069793827855179776,1069793828132003840,1069793828333330432,',30,'0000009000,0000000300,0000000040,0000000030,','1',3,'系统管理/组织管理/用户管理/查看','查看','2','','','','','sys:empUser:view',40,'1','default','core','0','system','2018-12-04 11:21:39','system','2018-12-28 16:03:30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793828735983616','1069793828333330432','0,1069793827855179776,1069793828132003840,1069793828333330432,',40,'0000009000,0000000300,0000000040,0000000040,','1',3,'系统管理/组织管理/用户管理/编辑','编辑','2','','','','','sys:empUser:edit',40,'1','default','core','0','system','2018-12-04 11:21:39','system','2018-12-28 16:03:30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793828937310208','1069793828333330432','0,1069793827855179776,1069793828132003840,1069793828333330432,',60,'0000009000,0000000300,0000000040,0000000060,','1',3,'系统管理/组织管理/用户管理/分配角色','分配角色','2','','','','','sys:empUser:authRole',40,'1','default','core','0','system','2018-12-04 11:21:39','system','2018-12-28 16:03:30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793829147025408','1069793828333330432','0,1069793827855179776,1069793828132003840,1069793828333330432,',50,'0000009000,0000000300,0000000040,0000000050,','1',3,'系统管理/组织管理/用户管理/分配数据','分配数据','2','','','','','sys:empUser:authDataScope',40,'1','default','core','0','system','2018-12-04 11:21:39','system','2018-12-28 16:03:30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793829339963392','1069793828333330432','0,1069793827855179776,1069793828132003840,1069793828333330432,',60,'0000009000,0000000300,0000000040,0000000060,','1',3,'系统管理/组织管理/用户管理/停用启用','停用启用','2','','','','','sys:empUser:updateStatus',40,'1','default','core','0','system','2018-12-04 11:21:39','system','2018-12-28 16:03:30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793829541289984','1069793828333330432','0,1069793827855179776,1069793828132003840,1069793828333330432,',70,'0000009000,0000000300,0000000040,0000000070,','1',3,'系统管理/组织管理/用户管理/重置密码','重置密码','2','','','','','sys:empUser:resetpwd',40,'1','default','core','0','system','2018-12-04 11:21:39','system','2018-12-28 16:03:30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793829818114048','1069793828132003840','0,1069793827855179776,1069793828132003840,',50,'0000009000,0000000300,0000000050,','0',2,'系统管理/组织管理/机构管理','机构管理','1','/sys/office/list','','icon-grid','','',60,'1','default','core','0','system','2018-12-04 11:21:40','system','2018-12-28 16:03:30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793830040412160','1069793829818114048','0,1069793827855179776,1069793828132003840,1069793829818114048,',30,'0000009000,0000000300,0000000050,0000000030,','1',3,'系统管理/组织管理/机构管理/查看','查看','2','','','','','sys:office:view',60,'1','default','core','0','system','2018-12-04 11:21:40','system','2018-12-28 16:03:30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793830271098880','1069793829818114048','0,1069793827855179776,1069793828132003840,1069793829818114048,',40,'0000009000,0000000300,0000000050,0000000040,','1',3,'系统管理/组织管理/机构管理/编辑','编辑','2','','','','','sys:office:edit',60,'1','default','core','0','system','2018-12-04 11:21:40','system','2018-12-28 16:03:30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793830438871040','1069793828132003840','0,1069793827855179776,1069793828132003840,',70,'0000009000,0000000300,0000000070,','0',2,'系统管理/组织管理/公司管理','公司管理','1','/sys/company/list','','icon-fire','','',60,'1','default','core','0','system','2018-12-04 11:21:40','system','2018-12-28 16:03:30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793830615031808','1069793830438871040','0,1069793827855179776,1069793828132003840,1069793830438871040,',30,'0000009000,0000000300,0000000070,0000000030,','1',3,'系统管理/组织管理/公司管理/查看','查看','2','','','','','sys:company:view',60,'1','default','core','0','system','2018-12-04 11:21:40','system','2018-12-28 16:03:30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793830791192576','1069793830438871040','0,1069793827855179776,1069793828132003840,1069793830438871040,',40,'0000009000,0000000300,0000000070,0000000040,','1',3,'系统管理/组织管理/公司管理/编辑','编辑','2','','','','','sys:company:edit',60,'1','default','core','0','system','2018-12-04 11:21:40','system','2018-12-28 16:03:30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793830954770432','1069793828132003840','0,1069793827855179776,1069793828132003840,',70,'0000009000,0000000300,0000000070,','0',2,'系统管理/组织管理/岗位管理','岗位管理','1','/sys/post/list','','icon-trophy','','',60,'1','default','core','0','system','2018-12-04 11:21:40','system','2018-12-28 16:03:30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793831114153984','1069793830954770432','0,1069793827855179776,1069793828132003840,1069793830954770432,',30,'0000009000,0000000300,0000000070,0000000030,','1',3,'系统管理/组织管理/岗位管理/查看','查看','2','','','','','sys:post:view',60,'1','default','core','0','system','2018-12-04 11:21:40','system','2018-12-28 16:03:30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793831281926144','1069793830954770432','0,1069793827855179776,1069793828132003840,1069793830954770432,',40,'0000009000,0000000300,0000000070,0000000040,','1',3,'系统管理/组织管理/岗位管理/编辑','编辑','2','','','','','sys:post:edit',60,'1','default','core','0','system','2018-12-04 11:21:40','system','2018-12-28 16:03:30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793831453892608','1069793827855179776','0,1069793827855179776,',400,'0000009000,0000000400,','0',1,'系统管理/权限管理','权限管理','1','','','icon-social-dropbox','','',60,'1','default','core','0','system','2018-12-04 11:21:40','system','2018-12-28 16:03:30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793831613276160','1069793831453892608','0,1069793827855179776,1069793831453892608,',30,'0000009000,0000000400,0000000030,','1',2,'系统管理/权限管理/角色管理','角色管理','1','/sys/role/list','','icon-people','','sys:role',60,'1','default','core','0','system','2018-12-04 11:21:40','system','2018-12-28 16:03:30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793831781048320','1069793831453892608','0,1069793827855179776,1069793831453892608,',40,'0000009000,0000000400,0000000040,','1',2,'系统管理/权限管理/二级管理员','二级管理员','1','/sys/secAdmin/list','','icon-user-female','','sys:secAdmin',60,'1','default','core','0','system','2018-12-04 11:21:40','system','2018-12-28 16:03:30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793831948820480','1069793831453892608','0,1069793827855179776,1069793831453892608,',50,'0000009000,0000000400,0000000050,','1',2,'系统管理/权限管理/系统管理员','系统管理员','1','/sys/corpAdmin/list','','icon-badge','','sys:corpAdmin',80,'1','default','core','0','system','2018-12-04 11:21:40','system','2018-12-28 16:03:30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793832187895808','1069793827855179776','0,1069793827855179776,',500,'0000009000,0000000500,','0',1,'系统管理/系统设置','系统设置','1','','','icon-settings','','',60,'1','default','core','0','system','2018-12-04 11:21:40','system','2018-12-28 16:03:30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793832443748352','1069793832187895808','0,1069793827855179776,1069793832187895808,',30,'0000009000,0000000500,0000000030,','1',2,'系统管理/系统设置/菜单管理','菜单管理','1','/sys/menu/list','','icon-book-open','','sys:menu',80,'1','default','core','0','system','2018-12-04 11:21:40','system','2018-12-28 16:03:30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793832678629376','1069793832187895808','0,1069793827855179776,1069793832187895808,',40,'0000009000,0000000500,0000000040,','1',2,'系统管理/系统设置/模块管理','模块管理','1','/sys/module/list','','icon-grid','','sys:module',80,'1','default','core','0','system','2018-12-04 11:21:40','system','2018-12-28 16:03:30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793832879955968','1069793832187895808','0,1069793827855179776,1069793832187895808,',50,'0000009000,0000000500,0000000050,','1',2,'系统管理/系统设置/参数设置','参数设置','1','/sys/config/list','','icon-wrench','','sys:config',60,'1','default','core','0','system','2018-12-04 11:21:40','system','2018-12-28 16:03:30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793833098059776','1069793832187895808','0,1069793827855179776,1069793832187895808,',60,'0000009000,0000000500,0000000060,','0',2,'系统管理/系统设置/字典管理','字典管理','1','/sys/dictType/list','','icon-social-dropbox','','',60,'1','default','core','0','system','2018-12-04 11:21:40','system','2018-12-28 16:03:30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793833311969280','1069793833098059776','0,1069793827855179776,1069793832187895808,1069793833098059776,',30,'0000009000,0000000500,0000000060,0000000030,','1',3,'系统管理/系统设置/字典管理/类型查看','类型查看','2','','','','','sys:dictType:view',60,'1','default','core','0','system','2018-12-04 11:21:40','system','2018-12-28 16:03:30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793833517490176','1069793833098059776','0,1069793827855179776,1069793832187895808,1069793833098059776,',40,'0000009000,0000000500,0000000060,0000000040,','1',3,'系统管理/系统设置/字典管理/类型编辑','类型编辑','2','','','','','sys:dictType:edit',80,'1','default','core','0','system','2018-12-04 11:21:40','system','2018-12-28 16:03:30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793833735593984','1069793833098059776','0,1069793827855179776,1069793832187895808,1069793833098059776,',50,'0000009000,0000000500,0000000060,0000000050,','1',3,'系统管理/系统设置/字典管理/数据查看','数据查看','2','','','','','sys:dictData:view',60,'1','default','core','0','system','2018-12-04 11:21:40','system','2018-12-28 16:03:30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793833957892096','1069793833098059776','0,1069793827855179776,1069793832187895808,1069793833098059776,',60,'0000009000,0000000500,0000000060,0000000060,','1',3,'系统管理/系统设置/字典管理/数据编辑','数据编辑','2','','','','','sys:dictData:edit',60,'1','default','core','0','system','2018-12-04 11:21:40','system','2018-12-28 16:03:30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793834163412992','1069793832187895808','0,1069793827855179776,1069793832187895808,',70,'0000009000,0000000500,0000000070,','1',2,'系统管理/系统设置/行政区划','行政区划','1','/sys/area/list','','icon-map','','sys:area',60,'1','default','core','0','system','2018-12-04 11:21:41','system','2018-12-28 16:03:30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793834385711104','1069793832187895808','0,1069793827855179776,1069793832187895808,',80,'0000009000,0000000500,0000000080,','1',2,'系统管理/系统设置/国际化管理','国际化管理','1','/sys/lang/list','','icon-globe','','sys:lang',80,'1','default','core','0','system','2018-12-04 11:21:41','system','2018-12-28 16:03:30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793834670923776','1069793832187895808','0,1069793827855179776,1069793832187895808,',90,'0000009000,0000000500,0000000090,','1',2,'系统管理/系统设置/产品许可信息','产品许可信息','1','//licence','','icon-paper-plane','','',80,'1','default','core','0','system','2018-12-04 11:21:41','system','2018-12-28 16:03:30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793842593964032','1069793827855179776','0,1069793827855179776,',600,'0000009000,0000000600,','0',1,'系统管理/系统监控','系统监控','1','','','icon-ghost','','',60,'1','default','core','0','system','2018-12-04 11:21:44','system','2018-12-28 16:03:30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793855386591232','1069793842593964032','0,1069793827855179776,1069793842593964032,',40,'0000009000,0000000600,0000000040,','1',2,'系统管理/系统监控/访问日志','访问日志','1','/sys/log/list','','fa fa-bug','','sys:log',60,'1','default','core','0','system','2018-12-04 11:21:46','system','2018-12-28 16:03:30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793866518274048','1069793842593964032','0,1069793827855179776,1069793842593964032,',50,'0000009000,0000000600,0000000050,','1',2,'系统管理/系统监控/数据监控','数据监控','1','//druid','','icon-disc','','sys:state:druid',80,'1','default','core','0','system','2018-12-04 11:21:49','system','2018-12-28 16:03:30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793880212676608','1069793842593964032','0,1069793827855179776,1069793842593964032,',60,'0000009000,0000000600,0000000060,','1',2,'系统管理/系统监控/缓存监控','缓存监控','1','/state/cache/index','','icon-social-dribbble','','sys:stste:cache',80,'1','default','core','0','system','2018-12-04 11:21:52','system','2018-12-28 16:03:30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793890773934080','1069793842593964032','0,1069793827855179776,1069793842593964032,',70,'0000009000,0000000600,0000000070,','1',2,'系统管理/系统监控/服务器监控','服务器监控','1','/state/server/index','','icon-speedometer','','sys:state:server',80,'1','default','core','0','system','2018-12-04 11:21:55','system','2018-12-28 16:03:30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793904950681600','1069793842593964032','0,1069793827855179776,1069793842593964032,',80,'0000009000,0000000600,0000000080,','1',2,'系统管理/系统监控/作业监控','作业监控','1','/job/list','','icon-notebook','','sys:job',80,'1','default','core','0','system','2018-12-04 11:21:58','system','2018-12-28 16:03:30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793915469996032','1069793842593964032','0,1069793827855179776,1069793842593964032,',90,'0000009000,0000000600,0000000090,','1',2,'系统管理/系统监控/在线用户','在线用户','1','/sys/online/list','','icon-social-twitter','','sys:online',60,'1','default','core','0','system','2018-12-04 11:22:00','system','2018-12-28 16:03:30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793928417812480','1069793842593964032','0,1069793827855179776,1069793842593964032,',100,'0000009000,0000000600,0000000100,','1',2,'系统管理/系统监控/在线文档','在线文档','1','//swagger-ui.html','','icon-book-open','','sys:swagger',80,'1','default','core','0','system','2018-12-04 11:22:03','system','2018-12-28 16:03:30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793939721461760','1069793827855179776','0,1069793827855179776,',700,'0000009000,0000000700,','0',1,'系统管理/消息推送','消息推送','1','','','icon-envelope-letter','','',60,'1','default','core','0','system','2018-12-04 11:22:06','system','2018-12-28 16:03:30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793950437908480','1069793939721461760','0,1069793827855179776,1069793939721461760,',30,'0000009000,0000000700,0000000030,','1',2,'系统管理/消息推送/未完成消息','未完成消息','1','/msg/msgPush/list','','','','msg:msgPush',60,'1','default','core','0','system','2018-12-04 11:22:09','system','2018-12-28 16:03:30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793962064519168','1069793939721461760','0,1069793827855179776,1069793939721461760,',40,'0000009000,0000000700,0000000040,','1',2,'系统管理/消息推送/已完成消息','已完成消息','1','/msg/msgPush/list?pushed=true','','','','msg:msgPush',60,'1','default','core','0','system','2018-12-04 11:22:12','system','2018-12-28 16:03:30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793974819397632','1069793939721461760','0,1069793827855179776,1069793939721461760,',50,'0000009000,0000000700,0000000050,','1',2,'系统管理/消息推送/消息模板管理','消息模板管理','1','/msg/msgTemplate/list','','','','msg:msgTemplate',60,'1','default','core','0','system','2018-12-04 11:22:15','system','2018-12-28 16:03:30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793986395676672','1069793827855179776','0,1069793827855179776,',900,'0000009000,0000000900,','0',1,'系统管理/研发工具','研发工具','1','','','fa fa-code','','',80,'1','default','core','0','system','2018-12-04 11:22:18','system','2018-12-28 16:03:30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069793999674843136','1069793986395676672','0,1069793827855179776,1069793986395676672,',30,'0000009000,0000000900,0000000030,','1',2,'系统管理/研发工具/代码生成工具','代码生成工具','1','/gen/genTable/list','','fa fa-code','','gen:genTable',80,'1','default','core','0','system','2018-12-04 11:22:20','system','2018-12-28 16:03:30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069794011611832320','1069793986395676672','0,1069793827855179776,1069793986395676672,',100,'0000009000,0000000900,0000000100,','0',2,'系统管理/研发工具/代码生成实例','代码生成实例','1','','','icon-social-dropbox','','',80,'1','default','core','0','system','2018-12-04 11:22:24','system','2018-12-28 16:03:30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069794024601591808','1069794011611832320','0,1069793827855179776,1069793986395676672,1069794011611832320,',30,'0000009000,0000000900,0000000100,0000000030,','1',3,'系统管理/研发工具/代码生成实例/单表_主子表','单表/主子表','1','/test/testData/list','','','','test:testData',80,'1','default','core','0','system','2018-12-04 11:22:26','system','2018-12-28 16:03:30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069794036068818944','1069794011611832320','0,1069793827855179776,1069793986395676672,1069794011611832320,',40,'0000009000,0000000900,0000000100,0000000040,','1',3,'系统管理/研发工具/代码生成实例/树表_树结构表','树表/树结构表','1','/test/testTree/list','','','','test:testTree',80,'1','default','core','0','system','2018-12-04 11:22:30','system','2018-12-28 16:03:30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069794049570283520','1069793986395676672','0,1069793827855179776,1069793986395676672,',200,'0000009000,0000000900,0000000200,','0',2,'系统管理/研发工具/数据表格实例','数据表格实例','1','','','','','',80,'1','default','core','0','system','2018-12-04 11:22:32','system','2018-12-28 16:03:30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069794061255614464','1069794049570283520','0,1069793827855179776,1069793986395676672,1069794049570283520,',30,'0000009000,0000000900,0000000200,0000000030,','1',3,'系统管理/研发工具/数据表格实例/多表头分组小计合计','多表头分组小计合计','1','/demo/dataGrid/groupGrid','','','','',80,'1','default','core','0','system','2018-12-04 11:22:35','system','2018-12-28 16:03:30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069794074945822720','1069794049570283520','0,1069793827855179776,1069793986395676672,1069794049570283520,',40,'0000009000,0000000900,0000000200,0000000040,','1',3,'系统管理/研发工具/数据表格实例/编辑表格多行编辑','编辑表格多行编辑','1','/demo/dataGrid/editGrid','','','','',80,'1','default','core','0','system','2018-12-04 11:22:38','system','2018-12-28 16:03:30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069794085469331456','1069793986395676672','0,1069793827855179776,1069793986395676672,',300,'0000009000,0000000900,0000000300,','0',2,'系统管理/研发工具/表单组件实例','表单组件实例','1','','','','','',80,'1','default','core','0','system','2018-12-04 11:22:42','system','2018-12-28 16:03:30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069794102854721536','1069794085469331456','0,1069793827855179776,1069793986395676672,1069794085469331456,',30,'0000009000,0000000900,0000000300,0000000030,','1',3,'系统管理/研发工具/表单组件实例/组件应用实例','组件应用实例','1','/demo/form/editForm','','','','',80,'1','default','core','0','system','2018-12-04 11:22:45','system','2018-12-28 16:03:30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069794117505425408','1069794085469331456','0,1069793827855179776,1069793986395676672,1069794085469331456,',40,'0000009000,0000000900,0000000300,0000000040,','1',3,'系统管理/研发工具/表单组件实例/栅格布局实例','栅格布局实例','1','/demo/form/layoutForm','','','','',80,'1','default','core','0','system','2018-12-04 11:22:49','system','2018-12-28 16:03:30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069794133020155904','1069794085469331456','0,1069793827855179776,1069793986395676672,1069794085469331456,',50,'0000009000,0000000900,0000000300,0000000050,','1',3,'系统管理/研发工具/表单组件实例/表格表单实例','表格表单实例','1','/demo/form/tableForm','','','','',80,'1','default','core','0','system','2018-12-04 11:22:52','system','2018-12-28 16:03:30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069794147733774336','1069793986395676672','0,1069793827855179776,1069793986395676672,',400,'0000009000,0000000900,0000000400,','0',2,'系统管理/研发工具/前端界面实例','前端界面实例','1','','','','','',80,'1','default','core','0','system','2018-12-04 11:22:56','system','2018-12-28 16:03:30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069794163500163072','1069794147733774336','0,1069793827855179776,1069793986395676672,1069794147733774336,',30,'0000009000,0000000900,0000000400,0000000030,','1',3,'系统管理/研发工具/前端界面实例/图标样式查找','图标样式查找','1','//tags/iconselect','','','','',80,'1','default','core','0','system','2018-12-04 11:23:00','system','2018-12-28 16:03:30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069794177907597312','1069793827855179776','0,1069793827855179776,',999,'0000009000,0000000999,','0',1,'系统管理/JeeSite社区','JeeSite社区','1','','','fa fa-code','','',80,'1','default','core','0','system','2018-12-04 11:23:04','system','2018-12-28 16:03:30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069794195435593728','1069794177907597312','0,1069793827855179776,1069794177907597312,',30,'0000009000,0000000999,0000000030,','1',2,'系统管理/JeeSite社区/官方网站','官方网站','1','http://jeesite.com','_blank','','','',80,'1','default','core','0','system','2018-12-04 11:23:07','system','2018-12-28 16:03:30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069794208010117120','1069794177907597312','0,1069793827855179776,1069794177907597312,',50,'0000009000,0000000999,0000000050,','1',2,'系统管理/JeeSite社区/作者博客','作者博客','1','https://my.oschina.net/thinkgem','_blank','','','',80,'1','default','core','0','system','2018-12-04 11:23:11','system','2018-12-28 16:03:30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069794225638776832','1069794177907597312','0,1069793827855179776,1069794177907597312,',40,'0000009000,0000000999,0000000040,','1',2,'系统管理/JeeSite社区/问题反馈','问题反馈','1','https://gitee.com/thinkgem/jeesite4/issues','_blank','','','',80,'1','default','core','0','system','2018-12-04 11:23:15','system','2018-12-28 16:03:30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1069794240302063616','1069794177907597312','0,1069793827855179776,1069794177907597312,',60,'0000009000,0000000999,0000000060,','1',2,'系统管理/JeeSite社区/开源社区','开源社区','1','http://jeesite.net','_blank','','','',80,'1','default','core','0','system','2018-12-04 11:23:18','system','2018-12-28 16:03:30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `js_sys_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_sys_module`
--

DROP TABLE IF EXISTS `js_sys_module`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `js_sys_module` (
  `module_code` varchar(64) NOT NULL COMMENT '模块编码',
  `module_name` varchar(100) NOT NULL COMMENT '模块名称',
  `description` varchar(500) DEFAULT NULL COMMENT '模块描述',
  `main_class_name` varchar(500) DEFAULT NULL COMMENT '主类全名',
  `current_version` varchar(50) DEFAULT NULL COMMENT '当前版本',
  `upgrade_info` varchar(300) DEFAULT NULL COMMENT '升级信息',
  `status` char(1) NOT NULL DEFAULT '0' COMMENT '状态（0正常 1删除 2停用）',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) DEFAULT NULL COMMENT '备注信息',
  PRIMARY KEY (`module_code`),
  KEY `idx_sys_module_status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='模块表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_sys_module`
--

LOCK TABLES `js_sys_module` WRITE;
/*!40000 ALTER TABLE `js_sys_module` DISABLE KEYS */;
INSERT INTO `js_sys_module` VALUES ('cms','内容管理','网站、站点、栏目、文章、链接、评论、留言板','com.jeesite.modules.cms.web.CmsController','4.0.0',NULL,'0','system','2018-12-04 11:21:33','system','2018-12-04 11:21:33',NULL),('core','核心模块','用户、角色、组织、模块、菜单、字典、参数','com.jeesite.modules.sys.web.LoginController','4.1.1',NULL,'0','system','2018-12-04 11:21:33','system','2018-12-04 11:21:33',NULL),('jflow','JFlow工作流','适合OA、可视化的java工作流引擎、自定义表单引擎。','com.jeesite.modules.jflow.config.JflowConfig','4.0.6',NULL,'0','system','2018-08-13 21:47:17','system','2018-08-13 21:48:00',NULL);
/*!40000 ALTER TABLE `js_sys_module` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_sys_msg_inner`
--

DROP TABLE IF EXISTS `js_sys_msg_inner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `js_sys_msg_inner` (
  `id` varchar(64) NOT NULL COMMENT '编号',
  `msg_title` varchar(200) NOT NULL COMMENT '消息标题',
  `content_level` char(1) NOT NULL COMMENT '内容级别（1普通 2一般 3紧急）',
  `content_type` char(1) DEFAULT NULL COMMENT '内容类型（1公告 2新闻 3会议 4其它）',
  `msg_content` text NOT NULL COMMENT '消息内容',
  `receive_type` char(1) NOT NULL COMMENT '接受者类型（1用户 2部门 3角色 4岗位）',
  `receive_codes` text NOT NULL COMMENT '接受者字符串',
  `receive_names` text NOT NULL COMMENT '接受者名称字符串',
  `send_user_code` varchar(64) NOT NULL COMMENT '发送者用户编码',
  `send_user_name` varchar(100) NOT NULL COMMENT '发送者用户姓名',
  `send_date` datetime NOT NULL COMMENT '发送时间',
  `is_attac` char(1) DEFAULT NULL COMMENT '是否有附件',
  `notify_types` varchar(100) NOT NULL COMMENT '通知类型（PC APP 短信 邮件 微信）多选',
  `status` char(1) NOT NULL COMMENT '状态（0正常 1删除 4审核 5驳回 9草稿）',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) DEFAULT NULL COMMENT '备注信息',
  PRIMARY KEY (`id`),
  KEY `idx_sys_msg_inner_cb` (`create_by`),
  KEY `idx_sys_msg_inner_status` (`status`),
  KEY `idx_sys_msg_inner_cl` (`content_level`),
  KEY `idx_sys_msg_inner_sc` (`send_user_code`),
  KEY `idx_sys_msg_inner_sd` (`send_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='内部消息';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_sys_msg_inner`
--

LOCK TABLES `js_sys_msg_inner` WRITE;
/*!40000 ALTER TABLE `js_sys_msg_inner` DISABLE KEYS */;
/*!40000 ALTER TABLE `js_sys_msg_inner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_sys_msg_inner_record`
--

DROP TABLE IF EXISTS `js_sys_msg_inner_record`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `js_sys_msg_inner_record` (
  `id` varchar(64) NOT NULL COMMENT '编号',
  `msg_inner_id` varchar(64) NOT NULL COMMENT '所属消息',
  `receive_user_code` varchar(64) DEFAULT NULL COMMENT '接受者用户编码',
  `receive_user_name` varchar(100) NOT NULL COMMENT '接受者用户姓名',
  `read_status` char(1) NOT NULL COMMENT '读取状态（0未送达 1未读 2已读）',
  `read_date` datetime DEFAULT NULL COMMENT '阅读时间',
  `is_star` char(1) DEFAULT NULL COMMENT '是否标星',
  PRIMARY KEY (`id`),
  KEY `idx_sys_msg_inner_r_mi` (`msg_inner_id`),
  KEY `idx_sys_msg_inner_r_rc` (`receive_user_code`),
  KEY `idx_sys_msg_inner_r_ruc` (`receive_user_code`),
  KEY `idx_sys_msg_inner_r_status` (`read_status`),
  KEY `idx_sys_msg_inner_r_star` (`is_star`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='内部消息发送记录表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_sys_msg_inner_record`
--

LOCK TABLES `js_sys_msg_inner_record` WRITE;
/*!40000 ALTER TABLE `js_sys_msg_inner_record` DISABLE KEYS */;
/*!40000 ALTER TABLE `js_sys_msg_inner_record` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_sys_msg_push`
--

DROP TABLE IF EXISTS `js_sys_msg_push`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `js_sys_msg_push` (
  `id` varchar(64) NOT NULL COMMENT '编号',
  `msg_type` varchar(16) NOT NULL COMMENT '消息类型（PC APP 短信 邮件 微信）',
  `msg_title` varchar(200) NOT NULL COMMENT '消息标题',
  `msg_content` text NOT NULL COMMENT '消息内容',
  `biz_key` varchar(64) DEFAULT NULL COMMENT '业务主键',
  `biz_type` varchar(64) DEFAULT NULL COMMENT '业务类型',
  `receive_code` varchar(64) NOT NULL COMMENT '接受者账号',
  `receive_user_code` varchar(64) NOT NULL COMMENT '接受者用户编码',
  `receive_user_name` varchar(100) NOT NULL COMMENT '接受者用户姓名',
  `send_user_code` varchar(64) NOT NULL COMMENT '发送者用户编码',
  `send_user_name` varchar(100) NOT NULL COMMENT '发送者用户姓名',
  `send_date` datetime NOT NULL COMMENT '发送时间',
  `is_merge_push` char(1) DEFAULT NULL COMMENT '是否合并推送',
  `plan_push_date` datetime DEFAULT NULL COMMENT '计划推送时间',
  `push_number` int(11) DEFAULT NULL COMMENT '推送尝试次数',
  `push_return_code` varchar(200) DEFAULT NULL COMMENT '推送返回结果码',
  `push_return_msg_id` varchar(200) DEFAULT NULL COMMENT '推送返回消息编号',
  `push_return_content` text COMMENT '推送返回的内容信息',
  `push_status` char(1) DEFAULT NULL COMMENT '推送状态（0未推送 1成功  2失败）',
  `push_date` datetime DEFAULT NULL COMMENT '推送时间',
  `read_status` char(1) DEFAULT NULL COMMENT '读取状态（0未送达 1未读 2已读）',
  `read_date` datetime DEFAULT NULL COMMENT '读取时间',
  PRIMARY KEY (`id`),
  KEY `idx_sys_msg_push_type` (`msg_type`),
  KEY `idx_sys_msg_push_rc` (`receive_code`),
  KEY `idx_sys_msg_push_uc` (`receive_user_code`),
  KEY `idx_sys_msg_push_suc` (`send_user_code`),
  KEY `idx_sys_msg_push_pd` (`plan_push_date`),
  KEY `idx_sys_msg_push_ps` (`push_status`),
  KEY `idx_sys_msg_push_rs` (`read_status`),
  KEY `idx_sys_msg_push_bk` (`biz_key`),
  KEY `idx_sys_msg_push_bt` (`biz_type`),
  KEY `idx_sys_msg_push_imp` (`is_merge_push`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='消息推送表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_sys_msg_push`
--

LOCK TABLES `js_sys_msg_push` WRITE;
/*!40000 ALTER TABLE `js_sys_msg_push` DISABLE KEYS */;
/*!40000 ALTER TABLE `js_sys_msg_push` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_sys_msg_pushed`
--

DROP TABLE IF EXISTS `js_sys_msg_pushed`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `js_sys_msg_pushed` (
  `id` varchar(64) NOT NULL COMMENT '编号',
  `msg_type` varchar(16) NOT NULL COMMENT '消息类型（PC APP 短信 邮件 微信）',
  `msg_title` varchar(200) NOT NULL COMMENT '消息标题',
  `msg_content` text NOT NULL COMMENT '消息内容',
  `biz_key` varchar(64) DEFAULT NULL COMMENT '业务主键',
  `biz_type` varchar(64) DEFAULT NULL COMMENT '业务类型',
  `receive_code` varchar(64) NOT NULL COMMENT '接受者账号',
  `receive_user_code` varchar(64) NOT NULL COMMENT '接受者用户编码',
  `receive_user_name` varchar(100) NOT NULL COMMENT '接受者用户姓名',
  `send_user_code` varchar(64) NOT NULL COMMENT '发送者用户编码',
  `send_user_name` varchar(100) NOT NULL COMMENT '发送者用户姓名',
  `send_date` datetime NOT NULL COMMENT '发送时间',
  `is_merge_push` char(1) DEFAULT NULL COMMENT '是否合并推送',
  `plan_push_date` datetime DEFAULT NULL COMMENT '计划推送时间',
  `push_number` int(11) DEFAULT NULL COMMENT '推送尝试次数',
  `push_return_content` text COMMENT '推送返回的内容信息',
  `push_return_code` varchar(200) DEFAULT NULL COMMENT '推送返回结果码',
  `push_return_msg_id` varchar(200) DEFAULT NULL COMMENT '推送返回消息编号',
  `push_status` char(1) DEFAULT NULL COMMENT '推送状态（0未推送 1成功  2失败）',
  `push_date` datetime DEFAULT NULL COMMENT '推送时间',
  `read_status` char(1) DEFAULT NULL COMMENT '读取状态（0未送达 1未读 2已读）',
  `read_date` datetime DEFAULT NULL COMMENT '读取时间',
  PRIMARY KEY (`id`),
  KEY `idx_sys_msg_pushed_type` (`msg_type`),
  KEY `idx_sys_msg_pushed_rc` (`receive_code`),
  KEY `idx_sys_msg_pushed_uc` (`receive_user_code`),
  KEY `idx_sys_msg_pushed_suc` (`send_user_code`),
  KEY `idx_sys_msg_pushed_pd` (`plan_push_date`),
  KEY `idx_sys_msg_pushed_ps` (`push_status`),
  KEY `idx_sys_msg_pushed_rs` (`read_status`),
  KEY `idx_sys_msg_pushed_bk` (`biz_key`),
  KEY `idx_sys_msg_pushed_bt` (`biz_type`),
  KEY `idx_sys_msg_pushed_imp` (`is_merge_push`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='消息已推送表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_sys_msg_pushed`
--

LOCK TABLES `js_sys_msg_pushed` WRITE;
/*!40000 ALTER TABLE `js_sys_msg_pushed` DISABLE KEYS */;
/*!40000 ALTER TABLE `js_sys_msg_pushed` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_sys_msg_template`
--

DROP TABLE IF EXISTS `js_sys_msg_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `js_sys_msg_template` (
  `id` varchar(64) NOT NULL COMMENT '编号',
  `module_code` varchar(64) DEFAULT NULL COMMENT '归属模块',
  `tpl_key` varchar(100) NOT NULL COMMENT '模板键值',
  `tpl_name` varchar(100) NOT NULL COMMENT '模板名称',
  `tpl_type` varchar(16) NOT NULL COMMENT '模板类型',
  `tpl_content` text NOT NULL COMMENT '模板内容',
  `status` char(1) NOT NULL DEFAULT '0' COMMENT '状态（0正常 1删除 2停用）',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) DEFAULT NULL COMMENT '备注信息',
  PRIMARY KEY (`id`),
  KEY `idx_sys_msg_tpl_key` (`tpl_key`),
  KEY `idx_sys_msg_tpl_type` (`tpl_type`),
  KEY `idx_sys_msg_tpl_status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='消息模板';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_sys_msg_template`
--

LOCK TABLES `js_sys_msg_template` WRITE;
/*!40000 ALTER TABLE `js_sys_msg_template` DISABLE KEYS */;
/*!40000 ALTER TABLE `js_sys_msg_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_sys_office`
--

DROP TABLE IF EXISTS `js_sys_office`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `js_sys_office` (
  `office_code` varchar(64) NOT NULL COMMENT '机构编码',
  `parent_code` varchar(64) NOT NULL COMMENT '父级编号',
  `parent_codes` varchar(1000) NOT NULL COMMENT '所有父级编号',
  `tree_sort` decimal(10,0) NOT NULL COMMENT '本级排序号（升序）',
  `tree_sorts` varchar(1000) NOT NULL COMMENT '所有级别排序号',
  `tree_leaf` char(1) NOT NULL COMMENT '是否最末级',
  `tree_level` decimal(4,0) NOT NULL COMMENT '层次级别',
  `tree_names` varchar(1000) NOT NULL COMMENT '全节点名',
  `view_code` varchar(100) NOT NULL COMMENT '机构代码',
  `office_name` varchar(100) NOT NULL COMMENT '机构名称',
  `full_name` varchar(200) NOT NULL COMMENT '机构全称',
  `office_type` char(1) NOT NULL COMMENT '机构类型',
  `leader` varchar(100) DEFAULT NULL COMMENT '负责人',
  `phone` varchar(100) DEFAULT NULL COMMENT '办公电话',
  `address` varchar(255) DEFAULT NULL COMMENT '联系地址',
  `zip_code` varchar(100) DEFAULT NULL COMMENT '邮政编码',
  `email` varchar(300) DEFAULT NULL COMMENT '电子邮箱',
  `status` char(1) NOT NULL DEFAULT '0' COMMENT '状态（0正常 1删除 2停用）',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) DEFAULT NULL COMMENT '备注信息',
  `corp_code` varchar(64) NOT NULL DEFAULT '0' COMMENT '租户代码',
  `corp_name` varchar(100) NOT NULL DEFAULT 'JeeSite' COMMENT '租户名称',
  `extend_s1` varchar(500) DEFAULT NULL COMMENT '扩展 String 1',
  `extend_s2` varchar(500) DEFAULT NULL COMMENT '扩展 String 2',
  `extend_s3` varchar(500) DEFAULT NULL COMMENT '扩展 String 3',
  `extend_s4` varchar(500) DEFAULT NULL COMMENT '扩展 String 4',
  `extend_s5` varchar(500) DEFAULT NULL COMMENT '扩展 String 5',
  `extend_s6` varchar(500) DEFAULT NULL COMMENT '扩展 String 6',
  `extend_s7` varchar(500) DEFAULT NULL COMMENT '扩展 String 7',
  `extend_s8` varchar(500) DEFAULT NULL COMMENT '扩展 String 8',
  `extend_i1` decimal(19,0) DEFAULT NULL COMMENT '扩展 Integer 1',
  `extend_i2` decimal(19,0) DEFAULT NULL COMMENT '扩展 Integer 2',
  `extend_i3` decimal(19,0) DEFAULT NULL COMMENT '扩展 Integer 3',
  `extend_i4` decimal(19,0) DEFAULT NULL COMMENT '扩展 Integer 4',
  `extend_f1` decimal(19,4) DEFAULT NULL COMMENT '扩展 Float 1',
  `extend_f2` decimal(19,4) DEFAULT NULL COMMENT '扩展 Float 2',
  `extend_f3` decimal(19,4) DEFAULT NULL COMMENT '扩展 Float 3',
  `extend_f4` decimal(19,4) DEFAULT NULL COMMENT '扩展 Float 4',
  `extend_d1` datetime DEFAULT NULL COMMENT '扩展 Date 1',
  `extend_d2` datetime DEFAULT NULL COMMENT '扩展 Date 2',
  `extend_d3` datetime DEFAULT NULL COMMENT '扩展 Date 3',
  `extend_d4` datetime DEFAULT NULL COMMENT '扩展 Date 4',
  PRIMARY KEY (`office_code`),
  KEY `idx_sys_office_cc` (`corp_code`),
  KEY `idx_sys_office_pc` (`parent_code`),
  KEY `idx_sys_office_pcs` (`parent_codes`(255)),
  KEY `idx_sys_office_status` (`status`),
  KEY `idx_sys_office_ot` (`office_type`),
  KEY `idx_sys_office_vc` (`view_code`),
  KEY `idx_sys_office_ts` (`tree_sort`),
  KEY `idx_sys_office_tss` (`tree_sorts`(255))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='组织机构表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_sys_office`
--

LOCK TABLES `js_sys_office` WRITE;
/*!40000 ALTER TABLE `js_sys_office` DISABLE KEYS */;
INSERT INTO `js_sys_office` VALUES ('SD','0','0,',40,'0000000040,','0',0,'山东公司','SD','山东公司','山东公司','1',NULL,NULL,NULL,NULL,NULL,'0','system','2018-12-04 11:23:26','system','2018-12-04 11:23:26',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('SDJN','SD','0,SD,',30,'0000000040,0000000030,','0',1,'山东公司/济南公司','SDJN','济南公司','山东济南公司','2',NULL,NULL,NULL,NULL,NULL,'0','system','2018-12-04 11:23:27','system','2018-12-04 11:23:27',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('SDJN01','SDJN','0,SD,SDJN,',30,'0000000040,0000000030,0000000030,','1',2,'山东公司/济南公司/企管部','SDJN01','企管部','山东济南企管部','3',NULL,NULL,NULL,NULL,NULL,'0','system','2018-12-04 11:23:28','system','2018-12-04 11:23:28',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('SDJN02','SDJN','0,SD,SDJN,',40,'0000000040,0000000030,0000000040,','1',2,'山东公司/济南公司/财务部','SDJN02','财务部','山东济南财务部','3',NULL,NULL,NULL,NULL,NULL,'0','system','2018-12-04 11:23:29','system','2018-12-04 11:23:29',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('SDJN03','SDJN','0,SD,SDJN,',50,'0000000040,0000000030,0000000050,','1',2,'山东公司/济南公司/研发部','SDJN03','研发部','山东济南研发部','3',NULL,NULL,NULL,NULL,NULL,'0','system','2018-12-04 11:23:30','system','2018-12-04 11:23:30',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('SDQD','SD','0,SD,',40,'0000000040,0000000040,','0',1,'山东公司/青岛公司','SDQD','青岛公司','山东青岛公司','2',NULL,NULL,NULL,NULL,NULL,'0','system','2018-12-04 11:23:31','system','2018-12-04 11:23:31',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('SDQD01','SDQD','0,SD,SDQD,',30,'0000000040,0000000040,0000000030,','1',2,'山东公司/青岛公司/企管部','SDQD01','企管部','山东青岛企管部','3',NULL,NULL,NULL,NULL,NULL,'0','system','2018-12-04 11:23:31','system','2018-12-04 11:23:31',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('SDQD02','SDQD','0,SD,SDQD,',40,'0000000040,0000000040,0000000040,','1',2,'山东公司/青岛公司/财务部','SDQD02','财务部','山东青岛财务部','3',NULL,NULL,NULL,NULL,NULL,'0','system','2018-12-04 11:23:32','system','2018-12-04 11:23:32',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('SDQD03','SDQD','0,SD,SDQD,',50,'0000000040,0000000040,0000000050,','1',2,'山东公司/青岛公司/研发部','SDQD03','研发部','山东青岛研发部','3',NULL,NULL,NULL,NULL,NULL,'0','system','2018-12-04 11:23:33','system','2018-12-04 11:23:33',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `js_sys_office` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_sys_post`
--

DROP TABLE IF EXISTS `js_sys_post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `js_sys_post` (
  `post_code` varchar(64) NOT NULL COMMENT '岗位编码',
  `post_name` varchar(100) NOT NULL COMMENT '岗位名称',
  `post_type` varchar(100) DEFAULT NULL COMMENT '岗位分类（高管、中层、基层）',
  `post_sort` decimal(10,0) DEFAULT NULL COMMENT '岗位排序（升序）',
  `status` char(1) NOT NULL DEFAULT '0' COMMENT '状态（0正常 1删除 2停用）',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) DEFAULT NULL COMMENT '备注信息',
  `corp_code` varchar(64) NOT NULL DEFAULT '0' COMMENT '租户代码',
  `corp_name` varchar(100) NOT NULL DEFAULT 'JeeSite' COMMENT '租户名称',
  PRIMARY KEY (`post_code`),
  KEY `idx_sys_post_cc` (`corp_code`),
  KEY `idx_sys_post_status` (`status`),
  KEY `idx_sys_post_ps` (`post_sort`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='员工岗位表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_sys_post`
--

LOCK TABLES `js_sys_post` WRITE;
/*!40000 ALTER TABLE `js_sys_post` DISABLE KEYS */;
INSERT INTO `js_sys_post` VALUES ('ceo','总经理',NULL,1,'0','system','2018-12-04 11:23:36','system','2018-12-04 11:23:36',NULL,'0','JeeSite'),('cfo','财务经理',NULL,2,'0','system','2018-12-04 11:23:36','system','2018-12-04 11:23:36',NULL,'0','JeeSite'),('dept','部门经理',NULL,2,'0','system','2018-12-04 11:23:37','system','2018-12-04 11:23:37',NULL,'0','JeeSite'),('hrm','人力经理',NULL,2,'0','system','2018-12-04 11:23:36','system','2018-12-04 11:23:36',NULL,'0','JeeSite'),('user','普通员工',NULL,3,'0','system','2018-12-04 11:23:37','system','2018-12-04 11:23:37',NULL,'0','JeeSite');
/*!40000 ALTER TABLE `js_sys_post` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_sys_role`
--

DROP TABLE IF EXISTS `js_sys_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `js_sys_role` (
  `role_code` varchar(64) NOT NULL COMMENT '角色编码',
  `role_name` varchar(100) NOT NULL COMMENT '角色名称',
  `role_type` varchar(100) DEFAULT NULL COMMENT '角色分类（高管、中层、基层、其它）',
  `role_sort` decimal(10,0) DEFAULT NULL COMMENT '角色排序（升序）',
  `is_sys` char(1) DEFAULT NULL COMMENT '系统内置（1是 0否）',
  `user_type` varchar(16) DEFAULT NULL COMMENT '用户类型（employee员工 member会员）',
  `data_scope` char(1) DEFAULT NULL COMMENT '数据范围设置（0未设置  1全部数据 2自定义数据）',
  `status` char(1) NOT NULL DEFAULT '0' COMMENT '状态（0正常 1删除 2停用）',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) DEFAULT NULL COMMENT '备注信息',
  `corp_code` varchar(64) NOT NULL DEFAULT '0' COMMENT '租户代码',
  `corp_name` varchar(100) NOT NULL DEFAULT 'JeeSite' COMMENT '租户名称',
  PRIMARY KEY (`role_code`),
  KEY `idx_sys_role_cc` (`corp_code`),
  KEY `idx_sys_role_is` (`is_sys`),
  KEY `idx_sys_role_status` (`status`),
  KEY `idx_sys_role_rs` (`role_sort`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_sys_role`
--

LOCK TABLES `js_sys_role` WRITE;
/*!40000 ALTER TABLE `js_sys_role` DISABLE KEYS */;
INSERT INTO `js_sys_role` VALUES ('corpAdmin','系统管理员',NULL,200,'1','none','0','0','system','2018-12-04 11:21:39','system','2018-12-04 11:21:39','客户方使用的管理员角色，客户方管理员，集团管理员','0','JeeSite'),('default','默认角色',NULL,100,'1','none','0','0','system','2018-12-04 11:21:39','system','2018-12-04 11:21:39','非管理员用户，共有的默认角色，在参数配置里指定','0','JeeSite'),('dept','部门经理',NULL,40,'0','employee','0','0','system','2018-12-04 11:21:39','system','2018-12-04 11:21:39','部门经理','0','JeeSite'),('user','普通员工',NULL,50,'0','employee','0','0','system','2018-12-04 11:21:39','system','2018-12-04 11:21:39','普通员工','0','JeeSite');
/*!40000 ALTER TABLE `js_sys_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_sys_role_data_scope`
--

DROP TABLE IF EXISTS `js_sys_role_data_scope`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `js_sys_role_data_scope` (
  `role_code` varchar(64) NOT NULL COMMENT '控制角色编码',
  `ctrl_type` varchar(20) NOT NULL COMMENT '控制类型',
  `ctrl_data` varchar(64) NOT NULL COMMENT '控制数据',
  `ctrl_permi` varchar(64) NOT NULL COMMENT '控制权限',
  PRIMARY KEY (`role_code`,`ctrl_type`,`ctrl_data`,`ctrl_permi`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色数据权限表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_sys_role_data_scope`
--

LOCK TABLES `js_sys_role_data_scope` WRITE;
/*!40000 ALTER TABLE `js_sys_role_data_scope` DISABLE KEYS */;
/*!40000 ALTER TABLE `js_sys_role_data_scope` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_sys_role_menu`
--

DROP TABLE IF EXISTS `js_sys_role_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `js_sys_role_menu` (
  `role_code` varchar(64) NOT NULL COMMENT '角色编码',
  `menu_code` varchar(64) NOT NULL COMMENT '菜单编码',
  PRIMARY KEY (`role_code`,`menu_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色与菜单关联表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_sys_role_menu`
--

LOCK TABLES `js_sys_role_menu` WRITE;
/*!40000 ALTER TABLE `js_sys_role_menu` DISABLE KEYS */;
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin','1069793827855179776'),('corpAdmin','1069793828132003840'),('corpAdmin','1069793828333330432'),('corpAdmin','1069793828547239936'),('corpAdmin','1069793828735983616'),('corpAdmin','1069793828937310208'),('corpAdmin','1069793829147025408'),('corpAdmin','1069793829339963392'),('corpAdmin','1069793829541289984'),('corpAdmin','1069793829818114048'),('corpAdmin','1069793830040412160'),('corpAdmin','1069793830271098880'),('corpAdmin','1069793830438871040'),('corpAdmin','1069793830615031808'),('corpAdmin','1069793830791192576'),('corpAdmin','1069793830954770432'),('corpAdmin','1069793831114153984'),('corpAdmin','1069793831281926144'),('corpAdmin','1069793831453892608'),('corpAdmin','1069793831613276160'),('corpAdmin','1069793831781048320'),('corpAdmin','1069793831948820480'),('corpAdmin','1069793832187895808'),('corpAdmin','1069793832443748352'),('corpAdmin','1069793832678629376'),('corpAdmin','1069793832879955968'),('corpAdmin','1069793833098059776'),('corpAdmin','1069793833311969280'),('corpAdmin','1069793833517490176'),('corpAdmin','1069793833735593984'),('corpAdmin','1069793833957892096'),('corpAdmin','1069793834163412992'),('corpAdmin','1069793834385711104'),('corpAdmin','1069793834670923776'),('corpAdmin','1069793842593964032'),('corpAdmin','1069793855386591232'),('corpAdmin','1069793866518274048'),('corpAdmin','1069793880212676608'),('corpAdmin','1069793890773934080'),('corpAdmin','1069793904950681600'),('corpAdmin','1069793915469996032'),('corpAdmin','1069793928417812480'),('corpAdmin','1069793939721461760'),('corpAdmin','1069793950437908480'),('corpAdmin','1069793962064519168'),('corpAdmin','1069793974819397632'),('corpAdmin','1069793986395676672'),('corpAdmin','1069793999674843136'),('corpAdmin','1069794011611832320'),('corpAdmin','1069794024601591808'),('corpAdmin','1069794036068818944'),('corpAdmin','1069794049570283520'),('corpAdmin','1069794061255614464'),('corpAdmin','1069794074945822720'),('corpAdmin','1069794085469331456'),('corpAdmin','1069794102854721536'),('corpAdmin','1069794117505425408'),('corpAdmin','1069794133020155904'),('corpAdmin','1069794147733774336'),('corpAdmin','1069794163500163072'),('corpAdmin','1069794177907597312'),('corpAdmin','1069794195435593728'),('corpAdmin','1069794208010117120'),('corpAdmin','1069794225638776832'),('corpAdmin','1069794240302063616');
/*!40000 ALTER TABLE `js_sys_role_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_sys_user`
--

DROP TABLE IF EXISTS `js_sys_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `js_sys_user` (
  `user_code` varchar(100) NOT NULL COMMENT '用户编码',
  `login_code` varchar(100) NOT NULL COMMENT '登录账号',
  `user_name` varchar(100) NOT NULL COMMENT '用户昵称',
  `password` varchar(100) NOT NULL COMMENT '登录密码',
  `email` varchar(300) DEFAULT NULL COMMENT '电子邮箱',
  `mobile` varchar(100) DEFAULT NULL COMMENT '手机号码',
  `phone` varchar(100) DEFAULT NULL COMMENT '办公电话',
  `sex` char(1) DEFAULT NULL COMMENT '用户性别',
  `avatar` varchar(1000) DEFAULT NULL COMMENT '头像路径',
  `sign` varchar(200) DEFAULT NULL COMMENT '个性签名',
  `wx_openid` varchar(100) DEFAULT NULL COMMENT '绑定的微信号',
  `mobile_imei` varchar(100) DEFAULT NULL COMMENT '绑定的手机串号',
  `user_type` varchar(16) NOT NULL COMMENT '用户类型',
  `ref_code` varchar(64) DEFAULT NULL COMMENT '用户类型引用编号',
  `ref_name` varchar(100) DEFAULT NULL COMMENT '用户类型引用姓名',
  `mgr_type` char(1) NOT NULL COMMENT '管理员类型（0非管理员 1系统管理员  2二级管理员）',
  `pwd_security_level` decimal(1,0) DEFAULT NULL COMMENT '密码安全级别（0初始 1很弱 2弱 3安全 4很安全）',
  `pwd_update_date` datetime DEFAULT NULL COMMENT '密码最后更新时间',
  `pwd_update_record` varchar(1000) DEFAULT NULL COMMENT '密码修改记录',
  `pwd_question` varchar(200) DEFAULT NULL COMMENT '密保问题',
  `pwd_question_answer` varchar(200) DEFAULT NULL COMMENT '密保问题答案',
  `pwd_question_2` varchar(200) DEFAULT NULL COMMENT '密保问题2',
  `pwd_question_answer_2` varchar(200) DEFAULT NULL COMMENT '密保问题答案2',
  `pwd_question_3` varchar(200) DEFAULT NULL COMMENT '密保问题3',
  `pwd_question_answer_3` varchar(200) DEFAULT NULL COMMENT '密保问题答案3',
  `pwd_quest_update_date` datetime DEFAULT NULL COMMENT '密码问题修改时间',
  `last_login_ip` varchar(100) DEFAULT NULL COMMENT '最后登陆IP',
  `last_login_date` datetime DEFAULT NULL COMMENT '最后登陆时间',
  `freeze_date` datetime DEFAULT NULL COMMENT '冻结时间',
  `freeze_cause` varchar(200) DEFAULT NULL COMMENT '冻结原因',
  `user_weight` decimal(8,0) DEFAULT '0' COMMENT '用户权重（降序）',
  `status` char(1) NOT NULL COMMENT '状态（0正常 1删除 2停用 3冻结）',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) DEFAULT NULL COMMENT '备注信息',
  `corp_code` varchar(64) NOT NULL DEFAULT '0' COMMENT '租户代码',
  `corp_name` varchar(100) NOT NULL DEFAULT 'JeeSite' COMMENT '租户名称',
  `extend_s1` varchar(500) DEFAULT NULL COMMENT '扩展 String 1',
  `extend_s2` varchar(500) DEFAULT NULL COMMENT '扩展 String 2',
  `extend_s3` varchar(500) DEFAULT NULL COMMENT '扩展 String 3',
  `extend_s4` varchar(500) DEFAULT NULL COMMENT '扩展 String 4',
  `extend_s5` varchar(500) DEFAULT NULL COMMENT '扩展 String 5',
  `extend_s6` varchar(500) DEFAULT NULL COMMENT '扩展 String 6',
  `extend_s7` varchar(500) DEFAULT NULL COMMENT '扩展 String 7',
  `extend_s8` varchar(500) DEFAULT NULL COMMENT '扩展 String 8',
  `extend_i1` decimal(19,0) DEFAULT NULL COMMENT '扩展 Integer 1',
  `extend_i2` decimal(19,0) DEFAULT NULL COMMENT '扩展 Integer 2',
  `extend_i3` decimal(19,0) DEFAULT NULL COMMENT '扩展 Integer 3',
  `extend_i4` decimal(19,0) DEFAULT NULL COMMENT '扩展 Integer 4',
  `extend_f1` decimal(19,4) DEFAULT NULL COMMENT '扩展 Float 1',
  `extend_f2` decimal(19,4) DEFAULT NULL COMMENT '扩展 Float 2',
  `extend_f3` decimal(19,4) DEFAULT NULL COMMENT '扩展 Float 3',
  `extend_f4` decimal(19,4) DEFAULT NULL COMMENT '扩展 Float 4',
  `extend_d1` datetime DEFAULT NULL COMMENT '扩展 Date 1',
  `extend_d2` datetime DEFAULT NULL COMMENT '扩展 Date 2',
  `extend_d3` datetime DEFAULT NULL COMMENT '扩展 Date 3',
  `extend_d4` datetime DEFAULT NULL COMMENT '扩展 Date 4',
  `PinYin` varchar(50) DEFAULT NULL COMMENT '缺少字段出现异常，手工增加',
  PRIMARY KEY (`user_code`),
  KEY `idx_sys_user_lc` (`login_code`),
  KEY `idx_sys_user_email` (`email`(255)),
  KEY `idx_sys_user_mobile` (`mobile`),
  KEY `idx_sys_user_wo` (`wx_openid`),
  KEY `idx_sys_user_imei` (`mobile_imei`),
  KEY `idx_sys_user_rt` (`user_type`),
  KEY `idx_sys_user_rc` (`ref_code`),
  KEY `idx_sys_user_mt` (`mgr_type`),
  KEY `idx_sys_user_us` (`user_weight`),
  KEY `idx_sys_user_ud` (`update_date`),
  KEY `idx_sys_user_status` (`status`),
  KEY `idx_sys_user_cc` (`corp_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_sys_user`
--

LOCK TABLES `js_sys_user` WRITE;
/*!40000 ALTER TABLE `js_sys_user` DISABLE KEYS */;
INSERT INTO `js_sys_user` VALUES ('admin','admin','系统管理员','9239e39d2e46b7cf7ca7f9310d33212198300e090f860b01e5b73486',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'none',NULL,NULL,'1',1,'2018-12-04 11:23:25',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'0','system','2018-12-04 11:23:26','system','2018-12-04 11:23:26','客户方使用的系统管理员，用于一些常用的基础数据配置。','0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('system','system','超级管理员','cd3a8250d665854b2b27d3f569eee5fe38f7a4e87f7f439f2c0cb029',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'none',NULL,NULL,'0',1,'2018-12-04 11:23:21',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0:0:0:0:0:0:0:1','2018-12-28 16:04:19',NULL,NULL,0,'0','system','2018-12-04 11:23:22','system','2018-12-04 11:23:25','开发者使用的最高级别管理员，主要用于开发和调试。','0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('user10_e2ie','user10','用户10','07385d19059af813986b8d91dcd288144961228acead4cd629089d9a','user@test.com','18555555555','053188888888',NULL,NULL,NULL,NULL,NULL,'employee','user10_e2ie','用户10','0',0,'2018-12-04 11:23:52',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'0','system','2018-12-04 11:23:52','system','2018-12-04 11:23:52',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('user11_us21','user11','用户11','1012d9339d8ed0e0fce8cac6fc0ae00a0d7fd66c979317ad4dda1c01','user@test.com','18555555555','053188888888',NULL,NULL,NULL,NULL,NULL,'employee','user11_us21','用户11','0',0,'2018-12-04 11:23:53',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'0','system','2018-12-04 11:23:53','system','2018-12-04 11:23:53',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('user12_u9li','user12','用户12','57f62c62976e3147982d90713c4e8f33120365e2b79cfe4ad1ca7382','user@test.com','18555555555','053188888888',NULL,NULL,NULL,NULL,NULL,'employee','user12_u9li','用户12','0',0,'2018-12-04 11:23:55',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'0','system','2018-12-04 11:23:55','system','2018-12-04 11:23:55',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('user13_5ael','user13','用户13','088db68275719bb8d6173114d74100e5dd1d501f781e4c9348a27328','user@test.com','18555555555','053188888888',NULL,NULL,NULL,NULL,NULL,'employee','user13_5ael','用户13','0',0,'2018-12-04 11:23:57',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'0','system','2018-12-04 11:23:57','system','2018-12-04 11:23:57',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('user14_iyau','user14','用户14','75762a4da76cb3671165dc54de10783670b3a07450a442fee0fea44f','user@test.com','18555555555','053188888888',NULL,NULL,NULL,NULL,NULL,'employee','user14_iyau','用户14','0',0,'2018-12-04 11:23:58',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'0','system','2018-12-04 11:23:58','system','2018-12-04 11:23:58',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('user15_luzt','user15','用户15','7f26b6e1697a53d56ef499d4c0b8cac6b413b86d98a04ea8ca5e6934','user@test.com','18555555555','053188888888',NULL,NULL,NULL,NULL,NULL,'employee','user15_luzt','用户15','0',0,'2018-12-04 11:24:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'0','system','2018-12-04 11:24:00','system','2018-12-04 11:24:00',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('user16_tqgx','user16','用户16','c981662bd6326397ad089ee2276ecfc56e04f36c104b30b63958709e','user@test.com','18555555555','053188888888',NULL,NULL,NULL,NULL,NULL,'employee','user16_tqgx','用户16','0',0,'2018-12-04 11:24:01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'0','system','2018-12-04 11:24:01','system','2018-12-04 11:24:01',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('user17_pt7y','user17','用户17','e17091e0042492e4171936556b2b583ef9cec2515d612c9d330676c8','user@test.com','18555555555','053188888888',NULL,NULL,NULL,NULL,NULL,'employee','user17_pt7y','用户17','0',0,'2018-12-04 11:24:03',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'0','system','2018-12-04 11:24:03','system','2018-12-04 11:24:03',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('user18_3g45','user18','用户18','0cf2dddffe67d4d29dd18e95cd767c07a5b233290cc02057ae060b7d','user@test.com','18555555555','053188888888',NULL,NULL,NULL,NULL,NULL,'employee','user18_3g45','用户18','0',0,'2018-12-04 11:24:05',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'0','system','2018-12-04 11:24:05','system','2018-12-04 11:24:05',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('user19_ni2v','user19','用户19','736d4db9bd33ff76781a4c023b7af5e49c95dac130e22fc2e040668d','user@test.com','18555555555','053188888888',NULL,NULL,NULL,NULL,NULL,'employee','user19_ni2v','用户19','0',0,'2018-12-04 11:24:06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'0','system','2018-12-04 11:24:06','system','2018-12-04 11:24:06',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('user1_7ppw','user1','用户01','d02056bdfab61ca4c601b7d753e45c2a81b9f65870cea7f53eaf6f74','user@test.com','18555555555','053188888888',NULL,NULL,NULL,NULL,NULL,'employee','user1_7ppw','用户01','0',0,'2018-12-04 11:23:38',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'0','system','2018-12-04 11:23:38','system','2018-12-04 11:23:38',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('user20_z6bi','user20','用户20','e1df2194e3ce9185c40c28ba6146abbb7a20d393a1697711caa61d18','user@test.com','18555555555','053188888888',NULL,NULL,NULL,NULL,NULL,'employee','user20_z6bi','用户20','0',0,'2018-12-04 11:24:08',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'0','system','2018-12-04 11:24:08','system','2018-12-04 11:24:08',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('user21_szwr','user21','用户21','26306a2cc8dfd209b3b8a89a015e01c261e8421c1df7f8190594113c','user@test.com','18555555555','053188888888',NULL,NULL,NULL,NULL,NULL,'employee','user21_szwr','用户21','0',0,'2018-12-04 11:24:10',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'0','system','2018-12-04 11:24:10','system','2018-12-04 11:24:10',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('user22_u7dq','user22','用户22','de3689e2bb3c13118f9e69053e1be0b366a2d356b38976144c0d50d6','user@test.com','18555555555','053188888888',NULL,NULL,NULL,NULL,NULL,'employee','user22_u7dq','用户22','0',0,'2018-12-04 11:24:11',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'0','system','2018-12-04 11:24:11','system','2018-12-04 11:24:11',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('user23_btwa','user23','用户23','de17a08710ab6f84a27581d457fba810c696a552c8d2dca214390147','user@test.com','18555555555','053188888888',NULL,NULL,NULL,NULL,NULL,'employee','user23_btwa','用户23','0',0,'2018-12-04 11:24:13',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'0','system','2018-12-04 11:24:13','system','2018-12-04 11:24:13',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('user2_7nxn','user2','用户02','db17fe307a6bd2250c39a58196786c2a64060b16af09672b1f56c391','user@test.com','18555555555','053188888888',NULL,NULL,NULL,NULL,NULL,'employee','user2_7nxn','用户02','0',0,'2018-12-04 11:23:39',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'0','system','2018-12-04 11:23:39','system','2018-12-04 11:23:39',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('user3_ebwj','user3','用户03','2e5c86430a523b9ef3ab1fb9acd8c707ce7b3379d679ad4239370c3c','user@test.com','18555555555','053188888888',NULL,NULL,NULL,NULL,NULL,'employee','user3_ebwj','用户03','0',0,'2018-12-04 11:23:41',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'0','system','2018-12-04 11:23:41','system','2018-12-04 11:23:41',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('user4_62k1','user4','用户04','be24590d6c120bca578d32b16577b36c2c3f7ada20d441065eb6769a','user@test.com','18555555555','053188888888',NULL,NULL,NULL,NULL,NULL,'employee','user4_62k1','用户04','0',0,'2018-12-04 11:23:42',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'0','system','2018-12-04 11:23:42','system','2018-12-04 11:23:42',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('user5_x004','user5','用户05','0247cff1d943998d0163d575556ee407302b286d442d9c102eaf51aa','user@test.com','18555555555','053188888888',NULL,NULL,NULL,NULL,NULL,'employee','user5_x004','用户05','0',0,'2018-12-04 11:23:44',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'0','system','2018-12-04 11:23:44','system','2018-12-04 11:23:44',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('user6_hmvr','user6','用户06','e4f851dc534970560f8d873fcbe7293e9b9b8ddb13a0c5c87feb3fe4','user@test.com','18555555555','053188888888',NULL,NULL,NULL,NULL,NULL,'employee','user6_hmvr','用户06','0',0,'2018-12-04 11:23:45',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'0','system','2018-12-04 11:23:45','system','2018-12-04 11:23:45',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('user7_ptwy','user7','用户07','361cd731da78ca95860b776b78403fec1478214b068f8e2b5b0f7528','user@test.com','18555555555','053188888888',NULL,NULL,NULL,NULL,NULL,'employee','user7_ptwy','用户07','0',0,'2018-12-04 11:23:47',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'0','system','2018-12-04 11:23:47','system','2018-12-04 11:23:47',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('user8_as3r','user8','用户08','70c155334e227a1d6b269a34efc26dace01bf246c8c914f3aca3451a','user@test.com','18555555555','053188888888',NULL,NULL,NULL,NULL,NULL,'employee','user8_as3r','用户08','0',0,'2018-12-04 11:23:49',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'0','system','2018-12-04 11:23:49','system','2018-12-04 11:23:49',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('user9_eqac','user9','用户09','235027a136d54b8e01230295d146b40b9ecfd10f0f6ec970fdc00936','user@test.com','18555555555','053188888888',NULL,NULL,NULL,NULL,NULL,'employee','user9_eqac','用户09','0',0,'2018-12-04 11:23:50',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'0','system','2018-12-04 11:23:50','system','2018-12-04 11:23:50',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `js_sys_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_sys_user_copy`
--

DROP TABLE IF EXISTS `js_sys_user_copy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `js_sys_user_copy` (
  `user_code` varchar(100) NOT NULL COMMENT '用户编码',
  `login_code` varchar(100) NOT NULL COMMENT '登录账号',
  `user_name` varchar(100) NOT NULL COMMENT '用户昵称',
  `password` varchar(100) NOT NULL COMMENT '登录密码',
  `email` varchar(300) DEFAULT NULL COMMENT '电子邮箱',
  `mobile` varchar(100) DEFAULT NULL COMMENT '手机号码',
  `phone` varchar(100) DEFAULT NULL COMMENT '办公电话',
  `sex` char(1) DEFAULT NULL COMMENT '用户性别',
  `avatar` varchar(1000) DEFAULT NULL COMMENT '头像路径',
  `sign` varchar(200) DEFAULT NULL COMMENT '个性签名',
  `wx_openid` varchar(100) DEFAULT NULL COMMENT '绑定的微信号',
  `mobile_imei` varchar(100) DEFAULT NULL COMMENT '绑定的手机串号',
  `user_type` varchar(16) NOT NULL COMMENT '用户类型',
  `ref_code` varchar(64) DEFAULT NULL COMMENT '用户类型引用编号',
  `ref_name` varchar(100) DEFAULT NULL COMMENT '用户类型引用姓名',
  `mgr_type` char(1) NOT NULL COMMENT '管理员类型（0非管理员 1系统管理员  2二级管理员）',
  `pwd_security_level` decimal(1,0) DEFAULT NULL COMMENT '密码安全级别（0初始 1很弱 2弱 3安全 4很安全）',
  `pwd_update_date` datetime DEFAULT NULL COMMENT '密码最后更新时间',
  `pwd_update_record` varchar(1000) DEFAULT NULL COMMENT '密码修改记录',
  `pwd_question` varchar(200) DEFAULT NULL COMMENT '密保问题',
  `pwd_question_answer` varchar(200) DEFAULT NULL COMMENT '密保问题答案',
  `pwd_question_2` varchar(200) DEFAULT NULL COMMENT '密保问题2',
  `pwd_question_answer_2` varchar(200) DEFAULT NULL COMMENT '密保问题答案2',
  `pwd_question_3` varchar(200) DEFAULT NULL COMMENT '密保问题3',
  `pwd_question_answer_3` varchar(200) DEFAULT NULL COMMENT '密保问题答案3',
  `pwd_quest_update_date` datetime DEFAULT NULL COMMENT '密码问题修改时间',
  `last_login_ip` varchar(100) DEFAULT NULL COMMENT '最后登陆IP',
  `last_login_date` datetime DEFAULT NULL COMMENT '最后登陆时间',
  `freeze_date` datetime DEFAULT NULL COMMENT '冻结时间',
  `freeze_cause` varchar(200) DEFAULT NULL COMMENT '冻结原因',
  `user_weight` decimal(8,0) DEFAULT '0' COMMENT '用户权重（降序）',
  `status` char(1) NOT NULL COMMENT '状态（0正常 1删除 2停用 3冻结）',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) DEFAULT NULL COMMENT '备注信息',
  `corp_code` varchar(64) NOT NULL DEFAULT '0' COMMENT '租户代码',
  `corp_name` varchar(100) NOT NULL DEFAULT 'JeeSite' COMMENT '租户名称',
  `extend_s1` varchar(500) DEFAULT NULL COMMENT '扩展 String 1',
  `extend_s2` varchar(500) DEFAULT NULL COMMENT '扩展 String 2',
  `extend_s3` varchar(500) DEFAULT NULL COMMENT '扩展 String 3',
  `extend_s4` varchar(500) DEFAULT NULL COMMENT '扩展 String 4',
  `extend_s5` varchar(500) DEFAULT NULL COMMENT '扩展 String 5',
  `extend_s6` varchar(500) DEFAULT NULL COMMENT '扩展 String 6',
  `extend_s7` varchar(500) DEFAULT NULL COMMENT '扩展 String 7',
  `extend_s8` varchar(500) DEFAULT NULL COMMENT '扩展 String 8',
  `extend_i1` decimal(19,0) DEFAULT NULL COMMENT '扩展 Integer 1',
  `extend_i2` decimal(19,0) DEFAULT NULL COMMENT '扩展 Integer 2',
  `extend_i3` decimal(19,0) DEFAULT NULL COMMENT '扩展 Integer 3',
  `extend_i4` decimal(19,0) DEFAULT NULL COMMENT '扩展 Integer 4',
  `extend_f1` decimal(19,4) DEFAULT NULL COMMENT '扩展 Float 1',
  `extend_f2` decimal(19,4) DEFAULT NULL COMMENT '扩展 Float 2',
  `extend_f3` decimal(19,4) DEFAULT NULL COMMENT '扩展 Float 3',
  `extend_f4` decimal(19,4) DEFAULT NULL COMMENT '扩展 Float 4',
  `extend_d1` datetime DEFAULT NULL COMMENT '扩展 Date 1',
  `extend_d2` datetime DEFAULT NULL COMMENT '扩展 Date 2',
  `extend_d3` datetime DEFAULT NULL COMMENT '扩展 Date 3',
  `extend_d4` datetime DEFAULT NULL COMMENT '扩展 Date 4',
  PRIMARY KEY (`user_code`),
  KEY `idx_sys_user_lc` (`login_code`),
  KEY `idx_sys_user_mobile` (`mobile`),
  KEY `idx_sys_user_wo` (`wx_openid`),
  KEY `idx_sys_user_imei` (`mobile_imei`),
  KEY `idx_sys_user_rt` (`user_type`),
  KEY `idx_sys_user_rc` (`ref_code`),
  KEY `idx_sys_user_mt` (`mgr_type`),
  KEY `idx_sys_user_us` (`user_weight`),
  KEY `idx_sys_user_ud` (`update_date`),
  KEY `idx_sys_user_status` (`status`),
  KEY `idx_sys_user_cc` (`corp_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_sys_user_copy`
--

LOCK TABLES `js_sys_user_copy` WRITE;
/*!40000 ALTER TABLE `js_sys_user_copy` DISABLE KEYS */;
INSERT INTO `js_sys_user_copy` VALUES ('1234_utd9','1234','123','ae9d75b0ee5e8b858c312e2b2654bbdb7b6cfe0a33ae9111db80aa14','','','',NULL,NULL,NULL,NULL,NULL,'employee','1234_utd9','123','0',0,'2018-11-12 11:09:17',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'0','system','2018-11-12 11:09:17','system','2018-11-12 11:09:17','','0','JeeSite','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('admin','admin','系统管理员','39be66b88806beab3af760f26957e93cbf798a738731e9964846e4cd',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'none',NULL,NULL,'1',1,'2018-11-10 15:53:20',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'0','system','2018-11-10 15:53:20','system','2018-11-10 15:53:20','客户方使用的系统管理员，用于一些常用的基础数据配置。','0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('cfadmin_mgfk','cfadmin','cfcc','a8064566a176ccabd5c21d4a986c66769bf466f87e56223b203535ea','','','',NULL,NULL,NULL,NULL,NULL,'employee','cfadmin_mgfk','cfcc','0',0,'2018-11-13 11:59:12',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,50,'0','system','2018-11-13 11:59:12','system','2018-11-13 11:59:12','','0','JeeSite','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('system','system','超级管理员','7b842ba09a5e94cba519793e1af7b32edb601b404ea9e36ca5a5635e',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'none',NULL,NULL,'0',1,'2018-11-10 15:53:20',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'127.0.0.1','2018-11-15 14:36:26',NULL,NULL,0,'0','system','2018-11-10 15:53:20','system','2018-11-10 15:53:20','开发者使用的最高级别管理员，主要用于开发和调试。','0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('user10_vqtj','user10','用户10','e2c05bdb6c4ddcc7b85ed54dc2a9a3f0c69e245a03dc8b5db4c78417','user@test.com','18555555555','053188888888',NULL,NULL,NULL,NULL,NULL,'employee','user10_vqtj','用户10','0',0,'2018-11-10 15:53:27',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'0','system','2018-11-10 15:53:27','system','2018-11-10 15:53:27',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('user11_avct','user11','用户11','19cfd1edc011128ad4e0075710319f588c736d5dc250ef804c184c8b','user@test.com','18555555555','053188888888',NULL,NULL,NULL,NULL,NULL,'employee','user11_avct','用户11','0',0,'2018-11-10 15:53:28',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'0','system','2018-11-10 15:53:28','system','2018-11-10 15:53:28',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('user12_kqst','user12','用户12','e81ae9df49d9724346d36f142276c7eb0ac9a500ef94c2ea00191c7d','user@test.com','18555555555','053188888888',NULL,NULL,NULL,NULL,NULL,'employee','user12_kqst','用户12','0',0,'2018-11-10 15:53:28',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'0','system','2018-11-10 15:53:28','system','2018-11-10 15:53:28',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('user13_uj5u','user13','用户13','3cd03c508bdd0dcf28e7ab6bb60f536d8c01d339b048130f34672a84','user@test.com','18555555555','053188888888',NULL,NULL,NULL,NULL,NULL,'employee','user13_uj5u','用户13','0',0,'2018-11-10 15:53:29',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'0','system','2018-11-10 15:53:29','system','2018-11-10 15:53:29',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('user14_toi4','user14','用户14','35162e08cafdc31bf9c6c51faf5fa28fee9d49149dd69fc538de6bcb','user@test.com','18555555555','053188888888',NULL,NULL,NULL,NULL,NULL,'employee','user14_toi4','用户14','0',0,'2018-11-10 15:53:29',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'0','system','2018-11-10 15:53:29','system','2018-11-10 15:53:29',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('user15_1c93','user15','用户15','a6da1835e6c023021f217e4155fef0b9580f72c25552f97e9bf22178','user@test.com','18555555555','053188888888',NULL,NULL,NULL,NULL,NULL,'employee','user15_1c93','用户15','0',0,'2018-11-10 15:53:29',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'0','system','2018-11-10 15:53:29','system','2018-11-10 15:53:29',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('user16_llf7','user16','用户16','567c1cb008d0173ee572372ca6157f279973d37cbf4f69a454470fc3','user@test.com','18555555555','053188888888',NULL,NULL,NULL,NULL,NULL,'employee','user16_llf7','用户16','0',0,'2018-11-10 15:53:30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'0','system','2018-11-10 15:53:30','system','2018-11-10 15:53:30',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('user17_jqiv','user17','用户17','21077009d3470a519b2dfcd0cf5936ef813166bf678fcb74efef79f9','user@test.com','18555555555','053188888888',NULL,NULL,NULL,NULL,NULL,'employee','user17_jqiv','用户17','0',0,'2018-11-10 15:53:30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'0','system','2018-11-10 15:53:30','system','2018-11-10 15:53:30',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('user18_jsz1','user18','用户18','f7077ee1385a91e095d16b200dae5295d058a2e7c12d27c10247def5','user@test.com','18555555555','053188888888',NULL,NULL,NULL,NULL,NULL,'employee','user18_jsz1','用户18','0',0,'2018-11-10 15:53:31',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'0','system','2018-11-10 15:53:31','system','2018-11-10 15:53:31',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('user19_nlnh','user19','用户19','e483f11e284a55402260f44ad1f2331956640faff5149fc208212809','user@test.com','18555555555','053188888888',NULL,NULL,NULL,NULL,NULL,'employee','user19_nlnh','用户19','0',0,'2018-11-10 15:53:31',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'0','system','2018-11-10 15:53:31','system','2018-11-10 15:53:31',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('user1_e3yt','user1','用户01','9b31996ccbdbff6562ddbe967e5dfe76ca2508f323660512577cb160','user@test.com','18555555555','053188888888',NULL,NULL,NULL,NULL,NULL,'employee','user1_e3yt','用户01','0',0,'2018-11-10 15:53:24',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'0','system','2018-11-10 15:53:24','system','2018-11-10 15:53:24',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('user20_unq5','user20','用户20','28e49f938f00ac386ffeea0f61822e4f84117eccba9bfa63e6da28b9','user@test.com','18555555555','053188888888',NULL,NULL,NULL,NULL,NULL,'employee','user20_unq5','用户20','0',0,'2018-11-10 15:53:31',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'0','system','2018-11-10 15:53:31','system','2018-11-10 15:53:31',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('user21_fvtp','user21','用户21','8a4e29098d756a69e72aec72777e27d698937d590bb8d7c40536522d','user@test.com','18555555555','053188888888',NULL,NULL,NULL,NULL,NULL,'employee','user21_fvtp','用户21','0',0,'2018-11-10 15:53:32',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'0','system','2018-11-10 15:53:32','system','2018-11-10 15:53:32',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('user22_ojxy','user22','用户22','83312c07ef5c602141c7b8ac0a2a2f159d664cb29fe0aa0d059955d2','user@test.com','18555555555','053188888888',NULL,NULL,NULL,NULL,NULL,'employee','user22_ojxy','用户22','0',0,'2018-11-10 15:53:32',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'0','system','2018-11-10 15:53:32','system','2018-11-10 15:53:32',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('user23_cmzg','user23','用户23','63a75a919a027f4e2f8cc4c47e97c4b7005c9579d6a5f198fdadff4e','user@test.com','18555555555','053188888888',NULL,NULL,NULL,NULL,NULL,'employee','user23_cmzg','用户23','0',0,'2018-11-10 15:53:33',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'0','system','2018-11-10 15:53:33','system','2018-11-10 15:53:33',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('user2_able','user2','用户02','4fc43fb3271e18ec9fe918f069f44b09c657957ce8de5bb6eb0dcf42','user@test.com','18555555555','053188888888',NULL,NULL,NULL,NULL,NULL,'employee','user2_able','用户02','0',0,'2018-11-10 15:53:24',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'0','system','2018-11-10 15:53:24','system','2018-11-10 15:53:24',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('user3_feuc','user3','用户03','99be2e26a16e4cef062f84e8db16ab812b59ac77589f5e790942fe35','user@test.com','18555555555','053188888888',NULL,NULL,NULL,NULL,NULL,'employee','user3_feuc','用户03','0',0,'2018-11-10 15:53:25',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'0','system','2018-11-10 15:53:25','system','2018-11-10 15:53:25',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('user4_gkzb','user4','用户04','1888f3a14ac01624353271ea5ada9bade91d90601d7fcf7370d44c32','user@test.com','18555555555','053188888888',NULL,NULL,NULL,NULL,NULL,'employee','user4_gkzb','用户04','0',0,'2018-11-10 15:53:25',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'0','system','2018-11-10 15:53:25','system','2018-11-10 15:53:25',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('user5_pcgf','user5','用户05','e9712587e2884952c40dd450ac1e11e1d2e2ce3c2bcdef70ce0f1b4f','user@test.com','18555555555','053188888888',NULL,NULL,NULL,NULL,NULL,'employee','user5_pcgf','用户05','0',0,'2018-11-10 15:53:25',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'0','system','2018-11-10 15:53:25','system','2018-11-10 15:53:26',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('user6_7edw','user6','用户06','5bc8ba040bb8f21dbec1f8a73dbf231bd32fbc1737acacadeb166f74','user@test.com','18555555555','053188888888',NULL,NULL,NULL,NULL,NULL,'employee','user6_7edw','用户06','0',0,'2018-11-10 15:53:26',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'0','system','2018-11-10 15:53:26','system','2018-11-10 15:53:26',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('user7_w8p6','user7','用户07','2ece56f55956db19c0c6d72c6a6ac5c77f1275d1aa8b49341bebead4','user@test.com','18555555555','053188888888',NULL,NULL,NULL,NULL,NULL,'employee','user7_w8p6','用户07','0',0,'2018-11-10 15:53:26',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'0','system','2018-11-10 15:53:26','system','2018-11-10 15:53:26',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('user8_ou1c','user8','用户08','ed67438b463c57bad0356c784049197d592aa4e9fb8b039688e64d66','user@test.com','18555555555','053188888888',NULL,NULL,NULL,NULL,NULL,'employee','user8_ou1c','用户08','0',0,'2018-11-10 15:53:27',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'0','system','2018-11-10 15:53:27','system','2018-11-10 15:53:27',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('user9_tkec','user9','用户09','3344283109b60c0eefc8313f8810e1f0d7657fcb755a3cfd65e14e06','user@test.com','18555555555','053188888888',NULL,NULL,NULL,NULL,NULL,'employee','user9_tkec','用户09','0',0,'2018-11-10 15:53:27',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'0','system','2018-11-10 15:53:27','system','2018-11-10 15:53:27',NULL,'0','JeeSite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `js_sys_user_copy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_sys_user_data_scope`
--

DROP TABLE IF EXISTS `js_sys_user_data_scope`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `js_sys_user_data_scope` (
  `user_code` varchar(100) NOT NULL COMMENT '控制用户编码',
  `ctrl_type` varchar(20) NOT NULL COMMENT '控制类型',
  `ctrl_data` varchar(64) NOT NULL COMMENT '控制数据',
  `ctrl_permi` varchar(64) NOT NULL COMMENT '控制权限',
  PRIMARY KEY (`user_code`,`ctrl_type`,`ctrl_data`,`ctrl_permi`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户数据权限表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_sys_user_data_scope`
--

LOCK TABLES `js_sys_user_data_scope` WRITE;
/*!40000 ALTER TABLE `js_sys_user_data_scope` DISABLE KEYS */;
/*!40000 ALTER TABLE `js_sys_user_data_scope` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_sys_user_role`
--

DROP TABLE IF EXISTS `js_sys_user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `js_sys_user_role` (
  `user_code` varchar(100) NOT NULL COMMENT '用户编码',
  `role_code` varchar(64) NOT NULL COMMENT '角色编码',
  PRIMARY KEY (`user_code`,`role_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户与角色关联表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_sys_user_role`
--

LOCK TABLES `js_sys_user_role` WRITE;
/*!40000 ALTER TABLE `js_sys_user_role` DISABLE KEYS */;
INSERT INTO `js_sys_user_role` VALUES ('user10_e2ie','user'),('user11_us21','user'),('user12_u9li','user'),('user13_5ael','user'),('user14_iyau','dept'),('user15_luzt','dept'),('user16_tqgx','user'),('user17_pt7y','user'),('user18_3g45','dept'),('user19_ni2v','user'),('user1_7ppw','dept'),('user20_z6bi','user'),('user21_szwr','dept'),('user22_u7dq','user'),('user23_btwa','user'),('user2_7nxn','user'),('user3_ebwj','user'),('user4_62k1','dept'),('user5_x004','user'),('user6_hmvr','user'),('user7_ptwy','dept'),('user8_as3r','user'),('user9_eqac','user');
/*!40000 ALTER TABLE `js_sys_user_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ms_dutyandpower`
--

DROP TABLE IF EXISTS `ms_dutyandpower`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ms_dutyandpower` (
  `No` varchar(10) NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(500) DEFAULT NULL COMMENT '名称',
  `FK_Main` varchar(200) DEFAULT NULL COMMENT '制度编号',
  `StationName` varchar(500) DEFAULT NULL COMMENT '岗位',
  `Duty` varchar(1000) DEFAULT NULL COMMENT '职责',
  `PowerOfRight` varchar(1000) DEFAULT NULL COMMENT '权限',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='职责与权限';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ms_dutyandpower`
--

LOCK TABLES `ms_dutyandpower` WRITE;
/*!40000 ALTER TABLE `ms_dutyandpower` DISABLE KEYS */;
/*!40000 ALTER TABLE `ms_dutyandpower` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ms_nodedtl`
--

DROP TABLE IF EXISTS `ms_nodedtl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ms_nodedtl` (
  `FK_Node` int(11) NOT NULL COMMENT '节点 - 主键',
  `FK_ZhiDuDtl` varchar(100) NOT NULL COMMENT '节点 - 主键',
  PRIMARY KEY (`FK_Node`,`FK_ZhiDuDtl`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='制度章节';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ms_nodedtl`
--

LOCK TABLES `ms_nodedtl` WRITE;
/*!40000 ALTER TABLE `ms_nodedtl` DISABLE KEYS */;
/*!40000 ALTER TABLE `ms_nodedtl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ms_sort`
--

DROP TABLE IF EXISTS `ms_sort`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ms_sort` (
  `No` varchar(4) NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(200) DEFAULT NULL COMMENT '名称',
  `ParentNo` varchar(4) DEFAULT NULL COMMENT '父编号',
  `TreeNo` varchar(60) DEFAULT NULL COMMENT '树编号',
  `IsDir` int(11) DEFAULT NULL COMMENT '是否是目录',
  `Abbr` varchar(60) DEFAULT NULL COMMENT '简称',
  `Idx` int(11) DEFAULT NULL COMMENT '序号',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='目录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ms_sort`
--

LOCK TABLES `ms_sort` WRITE;
/*!40000 ALTER TABLE `ms_sort` DISABLE KEYS */;
/*!40000 ALTER TABLE `ms_sort` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ms_zhidu`
--

DROP TABLE IF EXISTS `ms_zhidu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ms_zhidu` (
  `No` varchar(5) NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(200) DEFAULT NULL COMMENT '名称',
  `FK_Sort` varchar(200) DEFAULT NULL COMMENT '类别',
  `WebPath` varchar(400) DEFAULT NULL COMMENT '路径',
  `RelDept` varchar(200) DEFAULT NULL COMMENT '发布单位',
  `ZDNo` varchar(200) DEFAULT NULL COMMENT '制度编号',
  `ZDVersion` varchar(200) DEFAULT NULL COMMENT '制度版本号',
  `ZDProperty` varchar(200) DEFAULT NULL COMMENT '文件属性',
  `ExternalNo` varchar(200) DEFAULT NULL COMMENT '外部系统编号',
  `OID` varchar(200) DEFAULT NULL COMMENT '流程编号',
  `IsDelete` int(11) DEFAULT NULL COMMENT '是否作废',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='制度';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ms_zhidu`
--

LOCK TABLES `ms_zhidu` WRITE;
/*!40000 ALTER TABLE `ms_zhidu` DISABLE KEYS */;
/*!40000 ALTER TABLE `ms_zhidu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ms_zhidudept`
--

DROP TABLE IF EXISTS `ms_zhidudept`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ms_zhidudept` (
  `No` varchar(5) NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(200) DEFAULT NULL COMMENT '名称',
  `FK_Dept` varchar(200) DEFAULT NULL COMMENT '部门',
  `ZDMax` varchar(400) DEFAULT NULL COMMENT '最大值',
  `ZDNo` varchar(200) DEFAULT NULL COMMENT '制度编号',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='制度';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ms_zhidudept`
--

LOCK TABLES `ms_zhidudept` WRITE;
/*!40000 ALTER TABLE `ms_zhidudept` DISABLE KEYS */;
/*!40000 ALTER TABLE `ms_zhidudept` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ms_zhidudtl`
--

DROP TABLE IF EXISTS `ms_zhidudtl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ms_zhidudtl` (
  `No` varchar(5) NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(200) DEFAULT NULL COMMENT '名称',
  `DocLevel` varchar(20) DEFAULT NULL COMMENT '文档级别',
  `ParagraphIndex` varchar(20) DEFAULT NULL COMMENT '文档级',
  `ParentParagraphIndex` varchar(20) DEFAULT NULL COMMENT '文档父级',
  `IsDir` int(11) DEFAULT NULL COMMENT '是否是章节',
  `Idx` int(11) DEFAULT NULL COMMENT '序号',
  `FK_Main` varchar(5) DEFAULT NULL COMMENT '制度',
  `DocText` text COMMENT 'DocText',
  `DocHtml` text COMMENT 'DocHtml',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='章节';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ms_zhidudtl`
--

LOCK TABLES `ms_zhidudtl` WRITE;
/*!40000 ALTER TABLE `ms_zhidudtl` DISABLE KEYS */;
/*!40000 ALTER TABLE `ms_zhidudtl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `port_dept`
--

DROP TABLE IF EXISTS `port_dept`;
/*!50001 DROP VIEW IF EXISTS `port_dept`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `port_dept` AS SELECT 
 1 AS `No`,
 1 AS `Name`,
 1 AS `NameOfPath`,
 1 AS `ParentNo`,
 1 AS `TreeNo`,
 1 AS `Leader`,
 1 AS `Tel`,
 1 AS `Idx`,
 1 AS `IsDir`,
 1 AS `OrgNo`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `port_deptduty`
--

DROP TABLE IF EXISTS `port_deptduty`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `port_deptduty` (
  `FK_Dept` varchar(15) NOT NULL COMMENT '部门 - 主键',
  `FK_Duty` varchar(100) NOT NULL COMMENT '职务,主外键:对应物理表:Port_Duty,表描述:职务',
  PRIMARY KEY (`FK_Dept`,`FK_Duty`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='部门职务';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `port_deptduty`
--

LOCK TABLES `port_deptduty` WRITE;
/*!40000 ALTER TABLE `port_deptduty` DISABLE KEYS */;
INSERT INTO `port_deptduty` VALUES ('100','01'),('100','02'),('1001','04'),('1002','04'),('1003','04'),('1004','04'),('1005','04');
/*!40000 ALTER TABLE `port_deptduty` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `port_deptemp`
--

DROP TABLE IF EXISTS `port_deptemp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `port_deptemp` (
  `MyPK` varchar(100) NOT NULL,
  `FK_Emp` varchar(100) DEFAULT NULL,
  `FK_Dept` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `port_deptemp`
--

LOCK TABLES `port_deptemp` WRITE;
/*!40000 ALTER TABLE `port_deptemp` DISABLE KEYS */;
/*!40000 ALTER TABLE `port_deptemp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `port_deptemp_bak`
--

DROP TABLE IF EXISTS `port_deptemp_bak`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `port_deptemp_bak` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `FK_Dept` varchar(50) DEFAULT NULL COMMENT '部门',
  `FK_Emp` varchar(100) DEFAULT NULL COMMENT '操作员',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `port_deptemp_bak`
--

LOCK TABLES `port_deptemp_bak` WRITE;
/*!40000 ALTER TABLE `port_deptemp_bak` DISABLE KEYS */;
INSERT INTO `port_deptemp_bak` VALUES ('1001_zhanghaicheng','1001','zhanghaicheng'),('1001_zhangyifan','1001','zhangyifan'),('1001_zhoushengyu','1001','zhoushengyu'),('1002_qifenglin','1002','qifenglin'),('1002_zhoutianjiao','1002','zhoutianjiao'),('1003_fuhui','1003','fuhui'),('1003_guoxiangbin','1003','guoxiangbin'),('1004_guobaogeng','1004','guobaogeng'),('1004_yangyilei','1004','yangyilei'),('1005_liping','1005','liping'),('1005_liyan','1005','liyan'),('100_zhoupeng','100','zhoupeng');
/*!40000 ALTER TABLE `port_deptemp_bak` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `port_deptempstation`
--

DROP TABLE IF EXISTS `port_deptempstation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `port_deptempstation` (
  `MyPK` varchar(100) NOT NULL,
  `FK_Dept` varchar(50) DEFAULT NULL,
  `FK_Station` varchar(50) DEFAULT NULL,
  `FK_Emp` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `port_deptempstation`
--

LOCK TABLES `port_deptempstation` WRITE;
/*!40000 ALTER TABLE `port_deptempstation` DISABLE KEYS */;
/*!40000 ALTER TABLE `port_deptempstation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `port_deptsearchscorp`
--

DROP TABLE IF EXISTS `port_deptsearchscorp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `port_deptsearchscorp` (
  `FK_Emp` varchar(50) NOT NULL COMMENT '操作员 - 主键',
  `FK_Dept` varchar(100) NOT NULL COMMENT '部门,主外键:对应物理表:Port_Dept,表描述:部门',
  PRIMARY KEY (`FK_Emp`,`FK_Dept`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='部门查询权限';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `port_deptsearchscorp`
--

LOCK TABLES `port_deptsearchscorp` WRITE;
/*!40000 ALTER TABLE `port_deptsearchscorp` DISABLE KEYS */;
/*!40000 ALTER TABLE `port_deptsearchscorp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `port_deptstation`
--

DROP TABLE IF EXISTS `port_deptstation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `port_deptstation` (
  `FK_Dept` varchar(15) NOT NULL COMMENT '部门 - 主键',
  `FK_Station` varchar(100) NOT NULL COMMENT '岗位,主外键:对应物理表:Port_Station,表描述:岗位',
  PRIMARY KEY (`FK_Dept`,`FK_Station`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='部门岗位对应';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `port_deptstation`
--

LOCK TABLES `port_deptstation` WRITE;
/*!40000 ALTER TABLE `port_deptstation` DISABLE KEYS */;
INSERT INTO `port_deptstation` VALUES ('100','01'),('1001','07'),('1002','08'),('1003','09'),('1004','10'),('1005','11');
/*!40000 ALTER TABLE `port_deptstation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `port_depttype`
--

DROP TABLE IF EXISTS `port_depttype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `port_depttype` (
  `No` varchar(2) NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(50) DEFAULT NULL COMMENT '名称',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='部门类型';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `port_depttype`
--

LOCK TABLES `port_depttype` WRITE;
/*!40000 ALTER TABLE `port_depttype` DISABLE KEYS */;
/*!40000 ALTER TABLE `port_depttype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `port_duty`
--

DROP TABLE IF EXISTS `port_duty`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `port_duty` (
  `No` varchar(2) NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(50) DEFAULT NULL COMMENT '名称',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='职务';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `port_duty`
--

LOCK TABLES `port_duty` WRITE;
/*!40000 ALTER TABLE `port_duty` DISABLE KEYS */;
INSERT INTO `port_duty` VALUES ('01','董事长'),('02','总经理'),('03','科长'),('04','科员'),('05','分公司总经理'),('20','其他');
/*!40000 ALTER TABLE `port_duty` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `port_emp`
--

DROP TABLE IF EXISTS `port_emp`;
/*!50001 DROP VIEW IF EXISTS `port_emp`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `port_emp` AS SELECT 
 1 AS `No`,
 1 AS `Name`,
 1 AS `Pass`,
 1 AS `FK_Dept`,
 1 AS `SID`,
 1 AS `Tel`,
 1 AS `Email`,
 1 AS `PinYin`,
 1 AS `SignType`,
 1 AS `Idx`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `port_empstation`
--

DROP TABLE IF EXISTS `port_empstation`;
/*!50001 DROP VIEW IF EXISTS `port_empstation`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `port_empstation` AS SELECT 
 1 AS `FK_Emp`,
 1 AS `FK_Station`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `port_helper`
--

DROP TABLE IF EXISTS `port_helper`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `port_helper` (
  `No` varchar(3) NOT NULL COMMENT '编号',
  `Name` text COMMENT '描述',
  `MyFileName` varchar(300) DEFAULT NULL COMMENT '附件或图片',
  `MyFilePath` varchar(300) DEFAULT NULL COMMENT 'MyFilePath',
  `MyFileExt` varchar(20) DEFAULT NULL COMMENT 'MyFileExt',
  `WebPath` varchar(300) DEFAULT NULL COMMENT 'WebPath',
  `MyFileH` int(11) DEFAULT '0' COMMENT 'MyFileH',
  `MyFileW` int(11) DEFAULT '0' COMMENT 'MyFileW',
  `MyFileSize` float DEFAULT NULL COMMENT 'MyFileSize',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `port_helper`
--

LOCK TABLES `port_helper` WRITE;
/*!40000 ALTER TABLE `port_helper` DISABLE KEYS */;
/*!40000 ALTER TABLE `port_helper` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `port_inc`
--

DROP TABLE IF EXISTS `port_inc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `port_inc` (
  `No` varchar(30) NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(60) DEFAULT NULL COMMENT '名称',
  `ParentNo` varchar(30) DEFAULT NULL COMMENT '父节点编号',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='独立组织';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `port_inc`
--

LOCK TABLES `port_inc` WRITE;
/*!40000 ALTER TABLE `port_inc` DISABLE KEYS */;
/*!40000 ALTER TABLE `port_inc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `port_station`
--

DROP TABLE IF EXISTS `port_station`;
/*!50001 DROP VIEW IF EXISTS `port_station`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `port_station` AS SELECT 
 1 AS `No`,
 1 AS `Name`,
 1 AS `FK_StationType`,
 1 AS `DutyReq`,
 1 AS `Makings`,
 1 AS `OrgNo`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `port_stationtype`
--

DROP TABLE IF EXISTS `port_stationtype`;
/*!50001 DROP VIEW IF EXISTS `port_stationtype`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `port_stationtype` AS SELECT 
 1 AS `No`,
 1 AS `Name`,
 1 AS `Idx`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `pub_day`
--

DROP TABLE IF EXISTS `pub_day`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pub_day` (
  `No` varchar(30) NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(60) DEFAULT NULL COMMENT '名称',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='日期';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pub_day`
--

LOCK TABLES `pub_day` WRITE;
/*!40000 ALTER TABLE `pub_day` DISABLE KEYS */;
/*!40000 ALTER TABLE `pub_day` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pub_nd`
--

DROP TABLE IF EXISTS `pub_nd`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pub_nd` (
  `No` varchar(30) NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(60) DEFAULT NULL COMMENT '名称',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='年度';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pub_nd`
--

LOCK TABLES `pub_nd` WRITE;
/*!40000 ALTER TABLE `pub_nd` DISABLE KEYS */;
/*!40000 ALTER TABLE `pub_nd` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pub_ny`
--

DROP TABLE IF EXISTS `pub_ny`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pub_ny` (
  `No` varchar(30) NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(60) DEFAULT NULL COMMENT '名称',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='年月';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pub_ny`
--

LOCK TABLES `pub_ny` WRITE;
/*!40000 ALTER TABLE `pub_ny` DISABLE KEYS */;
INSERT INTO `pub_ny` VALUES ('2019-09','2019-09');
/*!40000 ALTER TABLE `pub_ny` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pub_yf`
--

DROP TABLE IF EXISTS `pub_yf`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pub_yf` (
  `No` varchar(30) NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(60) DEFAULT NULL COMMENT '名称',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='月份';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pub_yf`
--

LOCK TABLES `pub_yf` WRITE;
/*!40000 ALTER TABLE `pub_yf` DISABLE KEYS */;
/*!40000 ALTER TABLE `pub_yf` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sheeteleright`
--

DROP TABLE IF EXISTS `sheeteleright`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sheeteleright` (
  `OID` int(11) NOT NULL DEFAULT '0',
  `RDT` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`OID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sheeteleright`
--

LOCK TABLES `sheeteleright` WRITE;
/*!40000 ALTER TABLE `sheeteleright` DISABLE KEYS */;
/*!40000 ALTER TABLE `sheeteleright` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `spgw`
--

DROP TABLE IF EXISTS `spgw`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `spgw` (
  `TXR` varchar(300) DEFAULT NULL COMMENT 'TXR',
  `BGSR` varchar(300) DEFAULT NULL COMMENT '审批人',
  `CWCR` varchar(300) DEFAULT NULL COMMENT '审核人',
  `GBCR` varchar(300) DEFAULT NULL COMMENT '审核人',
  `GHCR` varchar(300) DEFAULT NULL COMMENT '审核人',
  `PFRQ` varchar(50) DEFAULT NULL COMMENT '批复日期',
  `SQDW` varchar(300) DEFAULT NULL COMMENT '申请单位',
  `TZRQ` varchar(50) DEFAULT NULL COMMENT '日期',
  `TZYJ` varchar(300) DEFAULT NULL COMMENT '厅长审批',
  `BGSRQ` varchar(50) DEFAULT NULL COMMENT '日期',
  `BGSYJ` varchar(300) DEFAULT NULL COMMENT '办公室审核',
  `CWCRQ` varchar(50) DEFAULT NULL COMMENT '日期',
  `CWCYJ` varchar(300) DEFAULT NULL COMMENT '财务处审核',
  `GBCRQ` varchar(50) DEFAULT NULL COMMENT '时间',
  `GHCRQ` varchar(50) DEFAULT NULL COMMENT '日期',
  `GHCYJ` varchar(300) DEFAULT NULL COMMENT '规划处意见',
  `DJGLCR` varchar(300) DEFAULT NULL COMMENT '审核人',
  `Context` varchar(300) DEFAULT NULL COMMENT '批复内容',
  `DJGLCRQ` varchar(50) DEFAULT NULL COMMENT '日期',
  `DJGLCYJ` varchar(300) DEFAULT NULL COMMENT '地籍管理处意见',
  `STGBCYJ` varchar(300) DEFAULT NULL COMMENT '省厅耕保处意见',
  `JBXX_XMMC` varchar(300) DEFAULT NULL COMMENT 'JBXX_XMMC',
  `PiFuWenHao` varchar(300) DEFAULT NULL COMMENT '批复文号',
  `OID` int(11) NOT NULL COMMENT 'OID - 主键',
  `RDT` varchar(50) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`OID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='审批公文';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spgw`
--

LOCK TABLES `spgw` WRITE;
/*!40000 ALTER TABLE `spgw` DISABLE KEYS */;
/*!40000 ALTER TABLE `spgw` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_cfield`
--

DROP TABLE IF EXISTS `sys_cfield`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_cfield` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `EnsName` varchar(100) DEFAULT NULL COMMENT '实体类名称',
  `FK_Emp` varchar(100) DEFAULT NULL COMMENT '工作人员',
  `Attrs` text COMMENT '属性s',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='列选择';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_cfield`
--

LOCK TABLES `sys_cfield` WRITE;
/*!40000 ALTER TABLE `sys_cfield` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_cfield` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_contrast`
--

DROP TABLE IF EXISTS `sys_contrast`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_contrast` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `ContrastKey` varchar(20) DEFAULT NULL COMMENT '对比项目',
  `KeyVal1` varchar(20) DEFAULT NULL COMMENT 'KeyVal1',
  `KeyVal2` varchar(20) DEFAULT NULL COMMENT 'KeyVal2',
  `SortBy` varchar(20) DEFAULT NULL COMMENT 'SortBy',
  `KeyOfNum` varchar(20) DEFAULT NULL COMMENT 'KeyOfNum',
  `GroupWay` int(11) DEFAULT NULL COMMENT '求什么?SumAvg',
  `OrderWay` varchar(10) DEFAULT NULL COMMENT 'OrderWay',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='对比状态存储';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_contrast`
--

LOCK TABLES `sys_contrast` WRITE;
/*!40000 ALTER TABLE `sys_contrast` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_contrast` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_datarpt`
--

DROP TABLE IF EXISTS `sys_datarpt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_datarpt` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `ColCount` varchar(50) DEFAULT NULL COMMENT '列',
  `RowCount` varchar(50) DEFAULT NULL COMMENT '行',
  `Val` float DEFAULT NULL COMMENT '值',
  `RefOID` float DEFAULT NULL COMMENT '关联的值',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='报表数据存储模版';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_datarpt`
--

LOCK TABLES `sys_datarpt` WRITE;
/*!40000 ALTER TABLE `sys_datarpt` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_datarpt` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_defval`
--

DROP TABLE IF EXISTS `sys_defval`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_defval` (
  `OID` int(11) NOT NULL COMMENT 'OID - 主键',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT '实体',
  `AttrKey` varchar(50) DEFAULT NULL COMMENT '节点对应字段',
  `LB` int(11) DEFAULT NULL COMMENT '类别',
  `FK_Emp` varchar(100) DEFAULT NULL COMMENT '人员',
  `CurValue` text COMMENT '文本',
  PRIMARY KEY (`OID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='选择词汇';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_defval`
--

LOCK TABLES `sys_defval` WRITE;
/*!40000 ALTER TABLE `sys_defval` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_defval` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_docfile`
--

DROP TABLE IF EXISTS `sys_docfile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_docfile` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `FileName` varchar(200) DEFAULT NULL COMMENT '名称',
  `FileSize` int(11) DEFAULT NULL COMMENT '大小',
  `FileType` varchar(50) DEFAULT NULL COMMENT '文件类型',
  `D1` text COMMENT 'D1',
  `D2` text COMMENT 'D2',
  `D3` text COMMENT 'D3',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='备注字段文件管理者';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_docfile`
--

LOCK TABLES `sys_docfile` WRITE;
/*!40000 ALTER TABLE `sys_docfile` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_docfile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_domain`
--

DROP TABLE IF EXISTS `sys_domain`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_domain` (
  `No` varchar(30) NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(30) DEFAULT NULL COMMENT 'Name',
  `DBLink` varchar(130) DEFAULT NULL COMMENT 'DBLink',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='域';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_domain`
--

LOCK TABLES `sys_domain` WRITE;
/*!40000 ALTER TABLE `sys_domain` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_domain` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_encfg`
--

DROP TABLE IF EXISTS `sys_encfg`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_encfg` (
  `No` varchar(100) NOT NULL COMMENT '实体名称 - 主键',
  `GroupTitle` varchar(2000) DEFAULT NULL COMMENT '分组标签',
  `Url` varchar(500) DEFAULT NULL COMMENT '要打开的Url',
  `FJSavePath` varchar(100) DEFAULT NULL COMMENT '保存路径',
  `FJWebPath` varchar(100) DEFAULT NULL COMMENT '附件Web路径',
  `Datan` varchar(200) DEFAULT NULL COMMENT '字段数据分析方式',
  `UI` varchar(2000) DEFAULT NULL COMMENT 'UI设置',
  `AtPara` varchar(3000) DEFAULT NULL COMMENT 'AtPara',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='实体配置';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_encfg`
--

LOCK TABLES `sys_encfg` WRITE;
/*!40000 ALTER TABLE `sys_encfg` DISABLE KEYS */;
INSERT INTO `sys_encfg` VALUES ('BP.Sys.FrmUI.FrmAttachmentExt','@MyPK=基础信息,附件的基本配置.\n@DeleteWay=权限控制,控制附件的下载与上传权限.@IsRowLock=WebOffice属性,设置与公文有关系的属性配置.\n@IsToHeLiuHZ=流程相关,控制节点附件的分合流.',NULL,NULL,NULL,NULL,NULL,NULL),('BP.WF.Template.FlowExt','@No=基础信息,基础信息权限信息.@IsBatchStart=数据&表单,数据导入导出.@DesignerNo=设计者,流程开发设计者信息',NULL,NULL,NULL,NULL,NULL,NULL),('BP.WF.Template.FlowSheet','@No=基本配置@FlowRunWay=启动方式,配置工作流程如何自动发起，该选项要与流程服务一起工作才有效.@StartLimitRole=启动限制规则@StartGuideWay=发起前置导航@CFlowWay=延续流程@DTSWay=流程数据与业务数据同步@PStarter=轨迹查看权限',NULL,NULL,NULL,NULL,NULL,NULL),('BP.WF.Template.FrmNodeComponent','@NodeID=审核组件,适用于sdk表单审核组件与ccform上的审核组件属性设置.@SFLab=父子流程组件,在该节点上配置与显示父子流程.@FrmThreadLab=子线程组件,对合流节点有效，用于配置与现实子线程运行的情况。@FrmTrackLab=轨迹组件,用于显示流程运行的轨迹图.@FTCLab=流转自定义,在每个节点上自己控制节点的处理人.',NULL,NULL,NULL,NULL,NULL,NULL),('BP.WF.Template.MapDataExt','@No=基本属性@Designer=设计者信息',NULL,NULL,NULL,NULL,NULL,NULL),('BP.WF.Template.MapDtlExt','@No=基础信息,基础信息权限信息.@IsExp=数据导入导出,数据导入导出.@MTR=多表头,实现多表头.@IsEnableLink=超链接,显示在从表的右边.@IsCopyNDData=流程相关,与流程相关的配置非流程可以忽略.',NULL,NULL,NULL,NULL,NULL,NULL),('BP.WF.Template.MapFrmFool','@No=基础属性,基础属性.@Designer=设计者信息,设计者的单位信息，人员信息，可以上传到表单云.',NULL,NULL,NULL,NULL,NULL,NULL),('BP.WF.Template.NodeExt','@NodeID=基本配置@SendLab=按钮权限,控制工作节点可操作按钮.@RunModel=运行模式,分合流,父子流程@AutoJumpRole0=跳转,自动跳转规则当遇到该节点时如何让其自动的执行下一步.',NULL,NULL,NULL,NULL,NULL,NULL),('BP.WF.Template.NodeSheet','@NodeID=基本配置@FormType=表单@FWCSta=审核组件,适用于sdk表单审核组件与ccform上的审核组件属性设置.@SFSta=父子流程,对启动，查看父子流程的控件设置.@SendLab=按钮权限,控制工作节点可操作按钮.@RunModel=运行模式,分合流,父子流程@AutoJumpRole0=跳转,自动跳转规则当遇到该节点时如何让其自动的执行下一步.@MPhone_WorkModel=移动,与手机平板电脑相关的应用设置.@OfficeOpen=公文按钮,只有当该节点是公文流程时候有效',NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `sys_encfg` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_enum`
--

DROP TABLE IF EXISTS `sys_enum`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_enum` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `Lab` varchar(300) DEFAULT NULL COMMENT 'Lab',
  `EnumKey` varchar(100) DEFAULT NULL COMMENT 'EnumKey',
  `IntKey` int(11) DEFAULT NULL COMMENT 'Val',
  `Lang` varchar(10) DEFAULT NULL COMMENT '语言',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='枚举数据';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_enum`
--

LOCK TABLES `sys_enum` WRITE;
/*!40000 ALTER TABLE `sys_enum` DISABLE KEYS */;
INSERT INTO `sys_enum` VALUES ('ActionType_CH_0','GET','ActionType',0,'CH'),('ActionType_CH_1','POST','ActionType',1,'CH'),('AlertType_CH_0','短信','AlertType',0,'CH'),('AlertType_CH_1','邮件','AlertType',1,'CH'),('AlertType_CH_2','邮件与短信','AlertType',2,'CH'),('AlertType_CH_3','系统(内部)消息','AlertType',3,'CH'),('AlertWay_CH_0','不接收','AlertWay',0,'CH'),('AlertWay_CH_1','短信','AlertWay',1,'CH'),('AlertWay_CH_2','邮件','AlertWay',2,'CH'),('AlertWay_CH_3','内部消息','AlertWay',3,'CH'),('AlertWay_CH_4','QQ消息','AlertWay',4,'CH'),('AlertWay_CH_5','RTX消息','AlertWay',5,'CH'),('AlertWay_CH_6','MSN消息','AlertWay',6,'CH'),('AppModel_CH_0','BS系统','AppModel',0,'CH'),('AppModel_CH_1','CS系统','AppModel',1,'CH'),('AppType_CH_0','外部Url连接','AppType',0,'CH'),('AppType_CH_1','本地可执行文件','AppType',1,'CH'),('AthCtrlWay_CH_0','PK-主键','AthCtrlWay',0,'CH'),('AthCtrlWay_CH_1','FID-流程ID','AthCtrlWay',1,'CH'),('AthCtrlWay_CH_2','ParentID-父流程ID','AthCtrlWay',2,'CH'),('AthCtrlWay_CH_3','仅能查看自己上传的附件','AthCtrlWay',3,'CH'),('AthCtrlWay_CH_4','按照WorkID计算(对流程节点表单有效)','AthCtrlWay',4,'CH'),('AthSaveWay_CH_0','保存到IIS服务器','AthSaveWay',0,'CH'),('AthSaveWay_CH_1','保存到数据库','AthSaveWay',1,'CH'),('AthSaveWay_CH_2','ftp服务器','AthSaveWay',2,'CH'),('AthUploadWay_CH_0','继承模式','AthUploadWay',0,'CH'),('AthUploadWay_CH_1','协作模式','AthUploadWay',1,'CH'),('AuthorWay_CH_0','不授权','AuthorWay',0,'CH'),('AuthorWay_CH_1','全部流程授权','AuthorWay',1,'CH'),('AuthorWay_CH_2','指定流程授权','AuthorWay',2,'CH'),('BillFileType_CH_0','Word','BillFileType',0,'CH'),('BillFileType_CH_1','PDF','BillFileType',1,'CH'),('BillFileType_CH_2','Excel(未完成)','BillFileType',2,'CH'),('BillFileType_CH_3','Html(未完成)','BillFileType',3,'CH'),('BillFileType_CH_5','锐浪报表','BillFileType',5,'CH'),('BillOpenModel_CH_0','下载本地','BillOpenModel',0,'CH'),('BillOpenModel_CH_1','在线WebOffice打开','BillOpenModel',1,'CH'),('CancelRole_CH_0','上一步可以撤销','CancelRole',0,'CH'),('CancelRole_CH_1','不能撤销','CancelRole',1,'CH'),('CancelRole_CH_2','上一步与开始节点可以撤销','CancelRole',2,'CH'),('CancelRole_CH_3','指定的节点可以撤销','CancelRole',3,'CH'),('CCRole_CH_0','不能抄送','CCRole',0,'CH'),('CCRole_CH_1','手工抄送','CCRole',1,'CH'),('CCRole_CH_2','自动抄送','CCRole',2,'CH'),('CCRole_CH_3','手工与自动','CCRole',3,'CH'),('CCRole_CH_4','按表单SysCCEmps字段计算','CCRole',4,'CH'),('CCRole_CH_5','在发送前打开抄送窗口','CCRole',5,'CH'),('CCWriteTo_CH_0','写入抄送列表','CCWriteTo',0,'CH'),('CCWriteTo_CH_1','写入待办','CCWriteTo',1,'CH'),('CCWriteTo_CH_2','写入待办与抄送列表','CCWriteTo',2,'CH'),('CeShiMoShi_CH_0','强办模式','CeShiMoShi',0,'CH'),('CeShiMoShi_CH_1','协作模式','CeShiMoShi',1,'CH'),('CeShiMoShi_CH_2','队列模式','CeShiMoShi',2,'CH'),('CeShiMoShi_CH_3','共享模式','CeShiMoShi',3,'CH'),('CGDD_CH_0','本市','CGDD',0,'CH'),('CGDD_CH_1','外地','CGDD',1,'CH'),('CHAlertRole_CH_0','不提示','CHAlertRole',0,'CH'),('CHAlertRole_CH_1','每天1次','CHAlertRole',1,'CH'),('CHAlertRole_CH_2','每天2次','CHAlertRole',2,'CH'),('CHAlertWay_CH_0','邮件','CHAlertWay',0,'CH'),('CHAlertWay_CH_1','短信','CHAlertWay',1,'CH'),('CHAlertWay_CH_2','CCIM即时通讯','CHAlertWay',2,'CH'),('ChartType_CH_0','几何图形','ChartType',0,'CH'),('ChartType_CH_1','肖像图片','ChartType',1,'CH'),('CHSta_CH_0','及时完成','CHSta',0,'CH'),('CHSta_CH_1','按期完成','CHSta',1,'CH'),('CHSta_CH_2','逾期完成','CHSta',2,'CH'),('CHSta_CH_3','超期完成','CHSta',3,'CH'),('ColSpanAttrDT_CH_1','跨1个单元格','ColSpanAttrDT',1,'CH'),('ColSpanAttrDT_CH_3','跨3个单元格','ColSpanAttrDT',3,'CH'),('ColSpanAttrString_CH_1','跨1个单元格','ColSpanAttrString',1,'CH'),('ColSpanAttrString_CH_3','跨3个单元格','ColSpanAttrString',3,'CH'),('ColSpanAttrString_CH_4','跨4个单元格','ColSpanAttrString',4,'CH'),('CondModel_CH_0','由连接线条件控制','CondModel',0,'CH'),('CondModel_CH_2','发送按钮旁下拉框选择','CondModel',2,'CH'),('ConfirmKind_CH_0','当前单元格','ConfirmKind',0,'CH'),('ConfirmKind_CH_1','左方单元格','ConfirmKind',1,'CH'),('ConfirmKind_CH_2','上方单元格','ConfirmKind',2,'CH'),('ConfirmKind_CH_3','右方单元格','ConfirmKind',3,'CH'),('ConfirmKind_CH_4','下方单元格','ConfirmKind',4,'CH'),('ConnJudgeWay_CH_0','or','ConnJudgeWay',0,'CH'),('ConnJudgeWay_CH_1','and','ConnJudgeWay',1,'CH'),('CtrlWay_CH_0','单个','CtrlWay',0,'CH'),('CtrlWay_CH_1','多个','CtrlWay',1,'CH'),('CtrlWay_CH_2','指定','CtrlWay',2,'CH'),('DataStoreModel_CH_0','数据轨迹模式','DataStoreModel',0,'CH'),('DataStoreModel_CH_1','数据合并模式','DataStoreModel',1,'CH'),('DataType_CH_0','字符串','DataType',0,'CH'),('DataType_CH_1','整数','DataType',1,'CH'),('DataType_CH_2','浮点数','DataType',2,'CH'),('DataType_CH_3','日期','DataType',3,'CH'),('DataType_CH_4','日期时间','DataType',4,'CH'),('DataType_CH_5','外键','DataType',5,'CH'),('DataType_CH_6','枚举','DataType',6,'CH'),('DelEnable_CH_0','不能删除','DelEnable',0,'CH'),('DelEnable_CH_1','逻辑删除','DelEnable',1,'CH'),('DelEnable_CH_2','记录日志方式删除','DelEnable',2,'CH'),('DelEnable_CH_3','彻底删除','DelEnable',3,'CH'),('DelEnable_CH_4','让用户决定删除方式','DelEnable',4,'CH'),('DeleteWay_CH_0','不能删除','DeleteWay',0,'CH'),('DeleteWay_CH_1','删除所有','DeleteWay',1,'CH'),('DeleteWay_CH_2','只能删除自己上传的','DeleteWay',2,'CH'),('DocType_CH_0','正式公文','DocType',0,'CH'),('DocType_CH_1','便函','DocType',1,'CH'),('Draft_CH_0','无(不设草稿)','Draft',0,'CH'),('Draft_CH_1','保存到待办','Draft',1,'CH'),('Draft_CH_2','保存到草稿箱','Draft',2,'CH'),('DtlAddRecModel_CH_0','按设置的数量初始化空白行','DtlAddRecModel',0,'CH'),('DtlAddRecModel_CH_1','用按钮增加空白行','DtlAddRecModel',1,'CH'),('DtlOpenType_CH_0','操作员','DtlOpenType',0,'CH'),('DtlOpenType_CH_1','工作ID','DtlOpenType',1,'CH'),('DtlOpenType_CH_2','流程ID','DtlOpenType',2,'CH'),('DtlSaveModel_CH_0','自动存盘(失去焦点自动存盘)','DtlSaveModel',0,'CH'),('DtlSaveModel_CH_1','手动存盘(保存按钮触发存盘)','DtlSaveModel',1,'CH'),('DTSearchWay_CH_0','不启用','DTSearchWay',0,'CH'),('DTSearchWay_CH_1','按日期','DTSearchWay',1,'CH'),('DTSearchWay_CH_2','按日期时间','DTSearchWay',2,'CH'),('DTSWay_CH_0','不考核','DTSWay',0,'CH'),('DTSWay_CH_1','按照时效考核','DTSWay',1,'CH'),('DTSWay_CH_2','按照工作量考核','DTSWay',2,'CH'),('EditerType_CH_0','无编辑器','EditerType',0,'CH'),('EditerType_CH_1','Sina编辑器0','EditerType',1,'CH'),('EditerType_CH_2','FKEditer','EditerType',2,'CH'),('EditerType_CH_3','KindEditor','EditerType',3,'CH'),('EditerType_CH_4','百度的UEditor','EditerType',4,'CH'),('EnumUIContralType_CH_1','下拉框','EnumUIContralType',1,'CH'),('EnumUIContralType_CH_3','单选按钮','EnumUIContralType',3,'CH'),('EventType_CH_0','禁用','EventType',0,'CH'),('EventType_CH_1','执行URL','EventType',1,'CH'),('EventType_CH_2','执行CCFromRef.js','EventType',2,'CH'),('ExcelType_CH_0','普通文件数据提取','ExcelType',0,'CH'),('ExcelType_CH_1','流程附件数据提取','ExcelType',1,'CH'),('ExpType_CH_3','按照SQL计算','ExpType',3,'CH'),('ExpType_CH_4','按照参数计算','ExpType',4,'CH'),('FeiYongYuSuan_CH_0','1万元以下','FeiYongYuSuan',0,'CH'),('FeiYongYuSuan_CH_1','1-2万元','FeiYongYuSuan',1,'CH'),('FeiYongYuSuan_CH_2','2-5万元','FeiYongYuSuan',2,'CH'),('FeiYongYuSuan_CH_3','5-10万元','FeiYongYuSuan',3,'CH'),('FeiYongYuSuan_CH_4','10-20万元','FeiYongYuSuan',4,'CH'),('FeiYongYuSuan_CH_5','20-30万元','FeiYongYuSuan',5,'CH'),('FeiYongYuSuan_CH_6','30-50万元','FeiYongYuSuan',6,'CH'),('FeiYongYuSuan_CH_7','50-70万元','FeiYongYuSuan',7,'CH'),('FeiYongYuSuan_CH_8','70-100万元','FeiYongYuSuan',8,'CH'),('FeiYongYuSuan_CH_9','100万元','FeiYongYuSuan',9,'CH'),('FindLeader_CH_0','直接领导','FindLeader',0,'CH'),('FindLeader_CH_1','指定职务级别的领导','FindLeader',1,'CH'),('FindLeader_CH_2','指定职务的领导','FindLeader',2,'CH'),('FindLeader_CH_3','指定岗位的领导','FindLeader',3,'CH'),('FJOpen_CH_0','关闭附件','FJOpen',0,'CH'),('FJOpen_CH_1','操作员','FJOpen',1,'CH'),('FJOpen_CH_2','工作ID','FJOpen',2,'CH'),('FJOpen_CH_3','流程ID','FJOpen',3,'CH'),('FlowDeleteRole_CH_0','超级管理员可以删除','FlowDeleteRole',0,'CH'),('FlowDeleteRole_CH_1','分级管理员可以删除','FlowDeleteRole',1,'CH'),('FlowDeleteRole_CH_2','发起人可以删除','FlowDeleteRole',2,'CH'),('FlowDeleteRole_CH_3','节点启动删除按钮的操作员','FlowDeleteRole',3,'CH'),('FlowRunWay_CH_0','手工启动','FlowRunWay',0,'CH'),('FlowRunWay_CH_1','指定人员按时启动','FlowRunWay',1,'CH'),('FlowRunWay_CH_2','数据集按时启动','FlowRunWay',2,'CH'),('FlowRunWay_CH_3','触发式启动','FlowRunWay',3,'CH'),('FLRole_CH_0','按接受人','FLRole',0,'CH'),('FLRole_CH_1','按部门','FLRole',1,'CH'),('FLRole_CH_2','按岗位','FLRole',2,'CH'),('FrmSln_CH_0','默认方案','FrmSln',0,'CH'),('FrmSln_CH_1','只读方案','FrmSln',1,'CH'),('FrmSln_CH_2','自定义方案','FrmSln',2,'CH'),('FrmThreadSta_CH_0','禁用','FrmThreadSta',0,'CH'),('FrmThreadSta_CH_1','启用','FrmThreadSta',1,'CH'),('FrmType_CH_0','傻瓜表单','FrmType',0,'CH'),('FrmType_CH_1','自由表单','FrmType',1,'CH'),('FrmType_CH_2','Silverlight表单(已取消)','FrmType',2,'CH'),('FrmType_CH_3','嵌入式表单','FrmType',3,'CH'),('FrmType_CH_4','Word表单','FrmType',4,'CH'),('FrmType_CH_5','在线编辑模式Excel表单','FrmType',5,'CH'),('FrmType_CH_6','VSTO模式Excel表单','FrmType',6,'CH'),('FrmType_CH_7','实体类组件','FrmType',7,'CH'),('FrmUrlShowWay_CH_0','不显示','FrmUrlShowWay',0,'CH'),('FrmUrlShowWay_CH_1','自动大小','FrmUrlShowWay',1,'CH'),('FrmUrlShowWay_CH_2','指定大小','FrmUrlShowWay',2,'CH'),('FrmUrlShowWay_CH_3','新窗口','FrmUrlShowWay',3,'CH'),('FTCWorkModel_CH_0','简洁模式','FTCWorkModel',0,'CH'),('FTCWorkModel_CH_1','高级模式','FTCWorkModel',1,'CH'),('FWCAth_CH_0','不启用','FWCAth',0,'CH'),('FWCAth_CH_1','多附件','FWCAth',1,'CH'),('FWCAth_CH_2','单附件(暂不支持)','FWCAth',2,'CH'),('FWCAth_CH_3','图片附件(暂不支持)','FWCAth',3,'CH'),('FWCMsgShow_CH_0','都显示','FWCMsgShow',0,'CH'),('FWCMsgShow_CH_1','仅显示自己的意见','FWCMsgShow',1,'CH'),('FWCOrderModel_CH_0','按审批时间先后排序','FWCOrderModel',0,'CH'),('FWCOrderModel_CH_1','按照接受人员列表先后顺序(官职大小)','FWCOrderModel',1,'CH'),('FWCShowModel_CH_0','表格方式','FWCShowModel',0,'CH'),('FWCShowModel_CH_1','自由模式','FWCShowModel',1,'CH'),('FWCSta_CH_0','禁用','FWCSta',0,'CH'),('FWCSta_CH_1','启用','FWCSta',1,'CH'),('FWCSta_CH_2','只读','FWCSta',2,'CH'),('FYLX_CH_0','汽车票','FYLX',0,'CH'),('FYLX_CH_1','打的票','FYLX',1,'CH'),('FYLX_CH_2','火车票','FYLX',2,'CH'),('FYLX_CH_3','飞机票','FYLX',3,'CH'),('FYLX_CH_4','其它','FYLX',4,'CH'),('GengDiJianSheJiBenTiaoJian_CH_0','条件1','GengDiJianSheJiBenTiaoJian',0,'CH'),('GengDiJianSheJiBenTiaoJian_CH_1','条件2','GengDiJianSheJiBenTiaoJian',1,'CH'),('GengDiJianSheJiBenTiaoJian_CH_2','条件3','GengDiJianSheJiBenTiaoJian',2,'CH'),('HuiQianRole_CH_0','不启用','HuiQianRole',0,'CH'),('HuiQianRole_CH_1','协作(同事)模式','HuiQianRole',1,'CH'),('HuiQianRole_CH_4','组长(领导)模式','HuiQianRole',4,'CH'),('HungUpWay_CH_0','无限挂起','HungUpWay',0,'CH'),('HungUpWay_CH_1','按指定的时间解除挂起并通知我自己','HungUpWay',1,'CH'),('HungUpWay_CH_2','按指定的时间解除挂起并通知所有人','HungUpWay',2,'CH'),('ImgSrcType_CH_0','本地','ImgSrcType',0,'CH'),('ImgSrcType_CH_1','URL','ImgSrcType',1,'CH'),('ImpModel_CH_0','不导入','ImpModel',0,'CH'),('ImpModel_CH_1','按配置模式导入','ImpModel',1,'CH'),('ImpModel_CH_2','按照xls文件模版导入','ImpModel',2,'CH'),('IsAutoSendSubFlowOver_CH_0','不处理','IsAutoSendSubFlowOver',0,'CH'),('IsAutoSendSubFlowOver_CH_1','让父流程自动运行下一步','IsAutoSendSubFlowOver',1,'CH'),('IsAutoSendSubFlowOver_CH_2','结束父流程','IsAutoSendSubFlowOver',2,'CH'),('IsSigan_CH_0','无','IsSigan',0,'CH'),('IsSigan_CH_1','图片签名','IsSigan',1,'CH'),('IsSigan_CH_2','山东CA','IsSigan',2,'CH'),('IsSigan_CH_3','广东CA','IsSigan',3,'CH'),('jd_CH_0','未定义','jd',0,'CH'),('JiMiChengDu_CH_0','公开','JiMiChengDu',0,'CH'),('JiMiChengDu_CH_1','保密','JiMiChengDu',1,'CH'),('JiMiChengDu_CH_2','秘密','JiMiChengDu',2,'CH'),('JiMiChengDu_CH_3','机密','JiMiChengDu',3,'CH'),('JingJiLeiXing_CH_0','内资','JingJiLeiXing',0,'CH'),('JingJiLeiXing_CH_1','外资','JingJiLeiXing',1,'CH'),('JinJiChengDu_CH_0','平件','JinJiChengDu',0,'CH'),('JinJiChengDu_CH_1','紧急','JinJiChengDu',1,'CH'),('jjlx1_CH_0','内资','jjlx1',0,'CH'),('JMCD_CH_0','一般','JMCD',0,'CH'),('JMCD_CH_1','保密','JMCD',1,'CH'),('JMCD_CH_2','秘密','JMCD',2,'CH'),('JMCD_CH_3','机密','JMCD',3,'CH'),('JumpWay_CH_0','不能跳转','JumpWay',0,'CH'),('JumpWay_CH_1','只能向后跳转','JumpWay',1,'CH'),('JumpWay_CH_2','只能向前跳转','JumpWay',2,'CH'),('JumpWay_CH_3','任意节点跳转','JumpWay',3,'CH'),('JumpWay_CH_4','按指定规则跳转','JumpWay',4,'CH'),('LGType_CH_0','普通','LGType',0,'CH'),('LGType_CH_1','枚举','LGType',1,'CH'),('LGType_CH_2','外键','LGType',2,'CH'),('LGType_CH_3','打开系统页面','LGType',3,'CH'),('ListShowModel_CH_0','表格','ListShowModel',0,'CH'),('ListShowModel_CH_1','卡片','ListShowModel',1,'CH'),('MenuCtrlWay_CH_0','按照设置的控制','MenuCtrlWay',0,'CH'),('MenuCtrlWay_CH_1','任何人都可以使用','MenuCtrlWay',1,'CH'),('MenuCtrlWay_CH_2','Admin用户可以使用','MenuCtrlWay',2,'CH'),('MenuType_CH_0','系统根目录','MenuType',0,'CH'),('MenuType_CH_1','系统类别','MenuType',1,'CH'),('MenuType_CH_2','系统','MenuType',2,'CH'),('MenuType_CH_3','目录','MenuType',3,'CH'),('MenuType_CH_4','功能','MenuType',4,'CH'),('MenuType_CH_5','功能控制点','MenuType',5,'CH'),('MiMiDengJi_CH_0','无','MiMiDengJi',0,'CH'),('MiMiDengJi_CH_1','普通','MiMiDengJi',1,'CH'),('MiMiDengJi_CH_2','秘密','MiMiDengJi',2,'CH'),('MiMiDengJi_CH_3','机密','MiMiDengJi',3,'CH'),('MiMiDengJi_CH_4','绝密','MiMiDengJi',4,'CH'),('Model_CH_0','普通','Model',0,'CH'),('Model_CH_1','固定行','Model',1,'CH'),('MoveToShowWay_CH_0','不显示','MoveToShowWay',0,'CH'),('MoveToShowWay_CH_1','下拉列表0','MoveToShowWay',1,'CH'),('MoveToShowWay_CH_2','平铺','MoveToShowWay',2,'CH'),('MsgCtrl_CH_0','不发送','MsgCtrl',0,'CH'),('MsgCtrl_CH_1','按设置的下一步接受人自动发送（默认）','MsgCtrl',1,'CH'),('MsgCtrl_CH_2','由本节点表单系统字段(IsSendEmail,IsSendSMS)来决定','MsgCtrl',2,'CH'),('MsgCtrl_CH_3','由SDK开发者参数(IsSendEmail,IsSendSMS)来决定','MsgCtrl',3,'CH'),('MyDataType_CH_1','字符串String','MyDataType',1,'CH'),('MyDataType_CH_2','整数类型Int','MyDataType',2,'CH'),('MyDataType_CH_3','浮点类型AppFloat','MyDataType',3,'CH'),('MyDataType_CH_4','判断类型Boolean','MyDataType',4,'CH'),('MyDataType_CH_5','双精度类型Double','MyDataType',5,'CH'),('MyDataType_CH_6','日期型Date','MyDataType',6,'CH'),('MyDataType_CH_7','时间类型Datetime','MyDataType',7,'CH'),('MyDataType_CH_8','金额类型AppMoney','MyDataType',8,'CH'),('MyDeptRole_CH_0','仅部门领导可以查看','MyDeptRole',0,'CH'),('MyDeptRole_CH_1','部门下所有的人都可以查看','MyDeptRole',1,'CH'),('MyDeptRole_CH_2','本部门里指定岗位的人可以查看','MyDeptRole',2,'CH'),('OpenWay_CH_0','新窗口','OpenWay',0,'CH'),('OpenWay_CH_1','本窗口','OpenWay',1,'CH'),('OpenWay_CH_2','覆盖新窗口','OpenWay',2,'CH'),('PopValFormat_CH_0','No(仅编号)','PopValFormat',0,'CH'),('PopValFormat_CH_1','Name(仅名称)','PopValFormat',1,'CH'),('PopValFormat_CH_2','No,Name(编号与名称,比如zhangsan,张三;lisi,李四;)','PopValFormat',2,'CH'),('PrintDocEnable_CH_0','不打印','PrintDocEnable',0,'CH'),('PrintDocEnable_CH_1','打印网页','PrintDocEnable',1,'CH'),('PrintDocEnable_CH_2','打印RTF模板','PrintDocEnable',2,'CH'),('PrintDocEnable_CH_3','打印Word模版','PrintDocEnable',3,'CH'),('PRI_CH_0','低','PRI',0,'CH'),('PRI_CH_1','中','PRI',1,'CH'),('PRI_CH_2','高','PRI',2,'CH'),('PushWay_CH_0','按照指定节点的工作人员','PushWay',0,'CH'),('PushWay_CH_1','按照指定的工作人员','PushWay',1,'CH'),('PushWay_CH_2','按照指定的工作岗位','PushWay',2,'CH'),('PushWay_CH_3','按照指定的部门','PushWay',3,'CH'),('PushWay_CH_4','按照指定的SQL','PushWay',4,'CH'),('PushWay_CH_5','按照系统指定的字段','PushWay',5,'CH'),('QingJiaLeiXing_CH_0','事假','QingJiaLeiXing',0,'CH'),('QingJiaLeiXing_CH_1','病假','QingJiaLeiXing',1,'CH'),('QingJiaLeiXing_CH_2','婚假','QingJiaLeiXing',2,'CH'),('QRModel_CH_0','不生成','QRModel',0,'CH'),('QRModel_CH_1','生成二维码','QRModel',1,'CH'),('RBShowModel_CH_0','竖向','RBShowModel',0,'CH'),('RBShowModel_CH_3','横向','RBShowModel',3,'CH'),('ReadReceipts_CH_0','不回执','ReadReceipts',0,'CH'),('ReadReceipts_CH_1','自动回执','ReadReceipts',1,'CH'),('ReadReceipts_CH_2','由上一节点表单字段决定','ReadReceipts',2,'CH'),('ReadReceipts_CH_3','由SDK开发者参数决定','ReadReceipts',3,'CH'),('ReturnRole_CH_0','不能退回','ReturnRole',0,'CH'),('ReturnRole_CH_1','只能退回上一个节点','ReturnRole',1,'CH'),('ReturnRole_CH_2','可退回以前任意节点','ReturnRole',2,'CH'),('ReturnRole_CH_3','可退回指定的节点','ReturnRole',3,'CH'),('ReturnRole_CH_4','由流程图设计的退回路线决定','ReturnRole',4,'CH'),('ReturnSendModel_CH_0','从退回节点正常执行','ReturnSendModel',0,'CH'),('ReturnSendModel_CH_1','直接发送到当前节点','ReturnSendModel',1,'CH'),('ReturnSendModel_CH_2','直接发送到当前节点的下一个节点','ReturnSendModel',2,'CH'),('RunModel_CH_0','普通','RunModel',0,'CH'),('RunModel_CH_1','合流','RunModel',1,'CH'),('RunModel_CH_2','分流','RunModel',2,'CH'),('RunModel_CH_3','分合流','RunModel',3,'CH'),('RunModel_CH_4','子线程','RunModel',4,'CH'),('SaveModel_CH_0','仅节点表','SaveModel',0,'CH'),('SaveModel_CH_1','节点表与Rpt表','SaveModel',1,'CH'),('sbsy_CH_0','自有','sbsy',0,'CH'),('SearchUrlOpenType_CH_0','En.htm','SearchUrlOpenType',0,'CH'),('SearchUrlOpenType_CH_1','EnOnly.htm','SearchUrlOpenType',1,'CH'),('SearchUrlOpenType_CH_2','自定义url','SearchUrlOpenType',2,'CH'),('SelectorModel_CH_0','按岗位','SelectorModel',0,'CH'),('SelectorModel_CH_1','按部门','SelectorModel',1,'CH'),('SelectorModel_CH_2','按人员','SelectorModel',2,'CH'),('SelectorModel_CH_3','按SQL','SelectorModel',3,'CH'),('SelectorModel_CH_4','按SQL模版计算','SelectorModel',4,'CH'),('SelectorModel_CH_5','使用通用人员选择器','SelectorModel',5,'CH'),('SelectorModel_CH_6','部门与岗位的交集','SelectorModel',6,'CH'),('SelectorModel_CH_7','自定义Url','SelectorModel',7,'CH'),('SelectorModel_CH_8','使用通用部门岗位人员选择器','SelectorModel',8,'CH'),('SelectorModel_CH_9','按岗位智能计算(操作员所在部门)','SelectorModel',9,'CH'),('sex__CH_0','男','sex_',0,'CH'),('sex__CH_1','女','sex_',1,'CH'),('SFOpenType_CH_0','工作查看器','SFOpenType',0,'CH'),('SFOpenType_CH_1','傻瓜表单轨迹查看器','SFOpenType',1,'CH'),('SFShowCtrl_CH_0','可以看所有的子流程','SFShowCtrl',0,'CH'),('SFShowCtrl_CH_1','仅仅可以看自己发起的子流程','SFShowCtrl',1,'CH'),('SFShowModel_CH_0','表格方式','SFShowModel',0,'CH'),('SFShowModel_CH_1','自由模式','SFShowModel',1,'CH'),('SFSta_CH_0','禁用','SFSta',0,'CH'),('SFSta_CH_1','启用','SFSta',1,'CH'),('SFSta_CH_2','只读','SFSta',2,'CH'),('SharingType_CH_0','共享','SharingType',0,'CH'),('SharingType_CH_1','私有','SharingType',1,'CH'),('ShenFen_CH_0','工人','ShenFen',0,'CH'),('ShenFen_CH_1','农民','ShenFen',1,'CH'),('ShenFen_CH_2','干部','ShenFen',2,'CH'),('ShiFouYunXuZuoYe_CH_0','符合安全标准允许作业','ShiFouYunXuZuoYe',0,'CH'),('ShiFouYunXuZuoYe_CH_1','不符合安全标准需要整改','ShiFouYunXuZuoYe',1,'CH'),('ShowWhere_CH_0','树形表单','ShowWhere',0,'CH'),('ShowWhere_CH_1','工具栏','ShowWhere',1,'CH'),('SignType_CH_0','不签名','SignType',0,'CH'),('SignType_CH_1','图片签名','SignType',1,'CH'),('SignType_CH_2','电子签名','SignType',2,'CH'),('SQLType_CH_0','方向条件','SQLType',0,'CH'),('SQLType_CH_1','接受人规则','SQLType',1,'CH'),('SQLType_CH_2','下拉框数据过滤','SQLType',2,'CH'),('SQLType_CH_3','级联下拉框','SQLType',3,'CH'),('SQLType_CH_4','PopVal开窗返回值','SQLType',4,'CH'),('SQLType_CH_5','人员选择器人员选择范围','SQLType',5,'CH'),('SSOType_CH_0','SID验证','SSOType',0,'CH'),('SSOType_CH_1','连接','SSOType',1,'CH'),('SSOType_CH_2','表单提交','SSOType',2,'CH'),('SSOType_CH_3','不传值','SSOType',3,'CH'),('SubFlowStartWay_CH_0','不启动','SubFlowStartWay',0,'CH'),('SubFlowStartWay_CH_1','指定的字段启动','SubFlowStartWay',1,'CH'),('SubFlowStartWay_CH_2','按明细表启动','SubFlowStartWay',2,'CH'),('SubThreadType_CH_0','同表单','SubThreadType',0,'CH'),('SubThreadType_CH_1','异表单','SubThreadType',1,'CH'),('TabType_CH_0','本地表或视图','TabType',0,'CH'),('TabType_CH_1','通过一个SQL确定的一个外部数据源','TabType',1,'CH'),('TabType_CH_2','通过WebServices获得的一个数据源','TabType',2,'CH'),('Target_CH_0','新窗口','Target',0,'CH'),('Target_CH_1','本窗口','Target',1,'CH'),('Target_CH_2','父窗口','Target',2,'CH'),('TaskSta_CH_0','未开始','TaskSta',0,'CH'),('TaskSta_CH_1','进行中','TaskSta',1,'CH'),('TaskSta_CH_2','完成','TaskSta',2,'CH'),('TaskSta_CH_3','推迟','TaskSta',3,'CH'),('ThreadKillRole_CH_0','不能删除','ThreadKillRole',0,'CH'),('ThreadKillRole_CH_1','手工删除','ThreadKillRole',1,'CH'),('ThreadKillRole_CH_2','自动删除','ThreadKillRole',2,'CH'),('TimelineRole_CH_0','按节点(由节点属性来定义)','TimelineRole',0,'CH'),('TimelineRole_CH_1','按发起人(开始节点SysSDTOfFlow字段计算)','TimelineRole',1,'CH'),('TSpan_CH_0','本周','TSpan',0,'CH'),('TSpan_CH_1','上周','TSpan',1,'CH'),('TSpan_CH_2','上上周','TSpan',2,'CH'),('TSpan_CH_3','更早','TSpan',3,'CH'),('UIRowStyleGlo_CH_0','无风格','UIRowStyleGlo',0,'CH'),('UIRowStyleGlo_CH_1','交替风格','UIRowStyleGlo',1,'CH'),('UIRowStyleGlo_CH_2','鼠标移动','UIRowStyleGlo',2,'CH'),('UIRowStyleGlo_CH_3','交替并鼠标移动','UIRowStyleGlo',3,'CH'),('UploadFileCheck_CH_0','不控制','UploadFileCheck',0,'CH'),('UploadFileCheck_CH_1','上传附件个数不能为0','UploadFileCheck',1,'CH'),('UploadFileCheck_CH_2','每个类别下面的个数不能为0','UploadFileCheck',2,'CH'),('UrlSrcType_CH_0','自定义','UrlSrcType',0,'CH'),('UrlSrcType_CH_1','表单库','UrlSrcType',1,'CH'),('UserType_CH_0','普通用户','UserType',0,'CH'),('UserType_CH_1','管理员用户','UserType',1,'CH'),('UseSta_CH_0','禁用','UseSta',0,'CH'),('UseSta_CH_1','启用','UseSta',1,'CH'),('WFStateApp_CH_10','批处理','WFStateApp',10,'CH'),('WFStateApp_CH_2','运行中','WFStateApp',2,'CH'),('WFStateApp_CH_3','已完成','WFStateApp',3,'CH'),('WFStateApp_CH_4','挂起','WFStateApp',4,'CH'),('WFStateApp_CH_5','退回','WFStateApp',5,'CH'),('WFStateApp_CH_6','转发','WFStateApp',6,'CH'),('WFStateApp_CH_7','删除','WFStateApp',7,'CH'),('WFStateApp_CH_8','加签','WFStateApp',8,'CH'),('WFStateApp_CH_9','冻结','WFStateApp',9,'CH'),('WFState_CH_0','空白','WFState',0,'CH'),('WFState_CH_1','草稿','WFState',1,'CH'),('WFState_CH_10','批处理','WFState',10,'CH'),('WFState_CH_11','加签回复状态','WFState',11,'CH'),('WFState_CH_2','运行中','WFState',2,'CH'),('WFState_CH_3','已完成','WFState',3,'CH'),('WFState_CH_4','挂起','WFState',4,'CH'),('WFState_CH_5','退回','WFState',5,'CH'),('WFState_CH_6','转发','WFState',6,'CH'),('WFState_CH_7','删除','WFState',7,'CH'),('WFState_CH_8','加签','WFState',8,'CH'),('WFState_CH_9','冻结','WFState',9,'CH'),('WFSta_CH_0','运行中','WFSta',0,'CH'),('WFSta_CH_1','已完成','WFSta',1,'CH'),('WFSta_CH_2','其他','WFSta',2,'CH'),('WhenOverSize_CH_0','不处理','WhenOverSize',0,'CH'),('WhenOverSize_CH_1','向下顺增行','WhenOverSize',1,'CH'),('WhenOverSize_CH_2','次页显示','WhenOverSize',2,'CH'),('WhoExeIt_CH_0','操作员执行','WhoExeIt',0,'CH'),('WhoExeIt_CH_1','机器执行','WhoExeIt',1,'CH'),('WhoExeIt_CH_2','混合执行','WhoExeIt',2,'CH'),('WhoIsPK_CH_0','WorkID是主键','WhoIsPK',0,'CH'),('WhoIsPK_CH_1','FID是主键(干流程的WorkID)','WhoIsPK',1,'CH'),('WhoIsPK_CH_2','父流程ID是主键','WhoIsPK',2,'CH'),('WhoIsPK_CH_3','延续流程ID是主键','WhoIsPK',3,'CH'),('XB_CH_0','女','XB',0,'CH'),('XB_CH_1','男','XB',1,'CH'),('XingBie_CH_0','男','XingBie',0,'CH'),('XingBie_CH_1','女','XingBie',1,'CH'),('xqsj_CH_0','长期有效','xqsj',0,'CH'),('xqsj_CH_1','截止于','xqsj',1,'CH'),('xsq_CH_0','未绑定','xsq',0,'CH'),('YBFlowReturnRole_CH_0','不能退回','YBFlowReturnRole',0,'CH'),('YBFlowReturnRole_CH_1','退回到父流程的开始节点','YBFlowReturnRole',1,'CH'),('YBFlowReturnRole_CH_2','退回到父流程的任何节点','YBFlowReturnRole',2,'CH'),('YBFlowReturnRole_CH_3','退回父流程的启动节点','YBFlowReturnRole',3,'CH'),('YBFlowReturnRole_CH_4','可退回到指定的节点','YBFlowReturnRole',4,'CH'),('ZhuYaoShiChang_CH_0','国外','ZhuYaoShiChang',0,'CH'),('ZhuYaoShiChang_CH_1','国内','ZhuYaoShiChang',1,'CH'),('ZhuYaoShiChang_CH_2','内外并重','ZhuYaoShiChang',2,'CH'),('ZZMM_CH_0','少先队员','ZZMM',0,'CH'),('ZZMM_CH_1','团员','ZZMM',1,'CH'),('ZZMM_CH_2','党员','ZZMM',2,'CH');
/*!40000 ALTER TABLE `sys_enum` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_enummain`
--

DROP TABLE IF EXISTS `sys_enummain`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_enummain` (
  `No` varchar(40) NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(40) DEFAULT NULL COMMENT '名称',
  `CfgVal` varchar(1500) DEFAULT NULL COMMENT '配置信息',
  `Lang` varchar(10) DEFAULT NULL COMMENT '语言',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='枚举';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_enummain`
--

LOCK TABLES `sys_enummain` WRITE;
/*!40000 ALTER TABLE `sys_enummain` DISABLE KEYS */;
INSERT INTO `sys_enummain` VALUES ('CeShiMoShi','测试模式','@0=强办模式@1=协作模式@2=队列模式@3=共享模式','CH'),('CGDD','采购地点','@0=本市@1=外地','CH'),('FYLX','费用类型','@0=汽车票@1=打的票@2=火车票','CH'),('QingJiaLeiXing','请假类型','@0=事假@1=病假@2=婚假','CH'),('ShiFouYunXuZuoYe','是否允许作业','@0=符合安全标准允许作业@1=不符合安全标准需要整改','CH');
/*!40000 ALTER TABLE `sys_enummain` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_enver`
--

DROP TABLE IF EXISTS `sys_enver`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_enver` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `No` varchar(50) DEFAULT NULL COMMENT '实体类',
  `Name` varchar(100) DEFAULT NULL COMMENT '实体名',
  `PKValue` varchar(300) DEFAULT NULL COMMENT '主键值',
  `EVer` int(11) DEFAULT NULL COMMENT '版本号',
  `Rec` varchar(100) DEFAULT NULL COMMENT '修改人',
  `RDT` varchar(50) DEFAULT NULL COMMENT '修改日期',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='实体版本号';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_enver`
--

LOCK TABLES `sys_enver` WRITE;
/*!40000 ALTER TABLE `sys_enver` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_enver` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_enverdtl`
--

DROP TABLE IF EXISTS `sys_enverdtl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_enverdtl` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `EnName` varchar(200) DEFAULT NULL COMMENT '实体名',
  `EnVerPK` varchar(100) DEFAULT NULL COMMENT '版本主表PK',
  `AttrKey` varchar(100) DEFAULT NULL COMMENT '字段',
  `AttrName` varchar(200) DEFAULT NULL COMMENT '字段名',
  `OldVal` varchar(100) DEFAULT NULL COMMENT '旧值',
  `NewVal` varchar(100) DEFAULT NULL COMMENT '新值',
  `EnVer` int(11) DEFAULT NULL COMMENT '版本号(日期)',
  `RDT` varchar(50) DEFAULT NULL COMMENT '日期',
  `Rec` varchar(100) DEFAULT NULL COMMENT '版本号',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='实体修改明细';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_enverdtl`
--

LOCK TABLES `sys_enverdtl` WRITE;
/*!40000 ALTER TABLE `sys_enverdtl` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_enverdtl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_excelfield`
--

DROP TABLE IF EXISTS `sys_excelfield`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_excelfield` (
  `No` varchar(36) NOT NULL COMMENT '编号',
  `Name` varchar(50) DEFAULT NULL COMMENT '名称',
  `CellName` varchar(50) DEFAULT NULL COMMENT '单元格名称',
  `CellRow` int(11) DEFAULT '0' COMMENT '行号',
  `CellColumn` int(11) DEFAULT '0' COMMENT '列号',
  `FK_ExcelSheet` varchar(100) DEFAULT NULL COMMENT '所属ExcelSheet表',
  `Field` varchar(50) DEFAULT NULL COMMENT '存储字段名',
  `FK_ExcelTable` varchar(100) DEFAULT NULL COMMENT '存储数据表',
  `DataType` int(11) DEFAULT '0' COMMENT '值类型',
  `UIBindKey` varchar(100) DEFAULT NULL COMMENT '外键表/枚举',
  `UIRefKey` varchar(30) DEFAULT NULL COMMENT '外键表No',
  `UIRefKeyText` varchar(30) DEFAULT NULL COMMENT '外键表Name',
  `Validators` text COMMENT '校验器',
  `FK_ExcelFile` varchar(100) DEFAULT NULL COMMENT '所属Excel模板',
  `AtPara` text COMMENT '参数',
  `ConfirmKind` int(11) DEFAULT '0' COMMENT '单元格确认方式',
  `ConfirmCellCount` int(11) DEFAULT '1' COMMENT '单元格确认方向移动量',
  `ConfirmCellValue` varchar(200) DEFAULT NULL COMMENT '对应单元格值',
  `ConfirmRepeatIndex` int(11) DEFAULT '0' COMMENT '对应单元格值重复选定次序',
  `SkipIsNull` int(11) DEFAULT '0' COMMENT '不计非空',
  `SyncToField` varchar(100) DEFAULT NULL COMMENT '同步到字段',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_excelfield`
--

LOCK TABLES `sys_excelfield` WRITE;
/*!40000 ALTER TABLE `sys_excelfield` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_excelfield` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_excelfile`
--

DROP TABLE IF EXISTS `sys_excelfile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_excelfile` (
  `No` varchar(36) NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(50) DEFAULT NULL COMMENT '名称',
  `Mark` varchar(50) DEFAULT NULL COMMENT '标识',
  `ExcelType` int(11) DEFAULT NULL COMMENT '类型,枚举类型:0 普通文件数据提取;1 流程附件数据提取;',
  `Note` text COMMENT '上传说明',
  `MyFileName` varchar(300) DEFAULT NULL COMMENT '模板文件',
  `MyFilePath` varchar(300) DEFAULT NULL COMMENT 'MyFilePath',
  `MyFileExt` varchar(20) DEFAULT NULL COMMENT 'MyFileExt',
  `WebPath` varchar(300) DEFAULT NULL COMMENT 'WebPath',
  `MyFileH` int(11) DEFAULT '0' COMMENT 'MyFileH',
  `MyFileW` int(11) DEFAULT '0' COMMENT 'MyFileW',
  `MyFileSize` float DEFAULT NULL COMMENT 'MyFileSize',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Excel模板';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_excelfile`
--

LOCK TABLES `sys_excelfile` WRITE;
/*!40000 ALTER TABLE `sys_excelfile` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_excelfile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_excelsheet`
--

DROP TABLE IF EXISTS `sys_excelsheet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_excelsheet` (
  `No` varchar(36) NOT NULL COMMENT 'No',
  `Name` varchar(50) DEFAULT NULL COMMENT 'Sheet名称',
  `FK_ExcelFile` varchar(100) DEFAULT NULL COMMENT 'Excel模板',
  `SheetIndex` int(11) DEFAULT '0' COMMENT 'Sheet索引',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_excelsheet`
--

LOCK TABLES `sys_excelsheet` WRITE;
/*!40000 ALTER TABLE `sys_excelsheet` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_excelsheet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_exceltable`
--

DROP TABLE IF EXISTS `sys_exceltable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_exceltable` (
  `No` varchar(36) NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(50) DEFAULT NULL COMMENT '数据表名',
  `FK_ExcelFile` varchar(100) DEFAULT NULL COMMENT 'Excel模板,外键:对应物理表:Sys_ExcelFile,表描述:Excel模板',
  `IsDtl` int(11) DEFAULT NULL COMMENT '是否明细表',
  `Note` text COMMENT '数据表说明',
  `SyncToTable` varchar(100) DEFAULT NULL COMMENT '同步到表',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Excel数据表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_exceltable`
--

LOCK TABLES `sys_exceltable` WRITE;
/*!40000 ALTER TABLE `sys_exceltable` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_exceltable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_filemanager`
--

DROP TABLE IF EXISTS `sys_filemanager`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_filemanager` (
  `OID` int(11) NOT NULL COMMENT 'OID',
  `AttrFileName` varchar(50) DEFAULT NULL COMMENT '指定名称',
  `AttrFileNo` varchar(50) DEFAULT NULL COMMENT '指定编号',
  `EnName` varchar(50) DEFAULT NULL COMMENT '关联的表',
  `RefVal` varchar(50) DEFAULT NULL COMMENT '主键值',
  `WebPath` varchar(100) DEFAULT NULL COMMENT 'Web路径',
  `MyFileName` varchar(300) DEFAULT NULL COMMENT '文件名称',
  `MyFilePath` varchar(300) DEFAULT NULL COMMENT 'MyFilePath',
  `MyFileExt` varchar(20) DEFAULT NULL COMMENT 'MyFileExt',
  `MyFileH` int(11) DEFAULT '0' COMMENT 'MyFileH',
  `MyFileW` int(11) DEFAULT '0' COMMENT 'MyFileW',
  `MyFileSize` float DEFAULT NULL COMMENT 'MyFileSize',
  `RDT` varchar(50) DEFAULT NULL COMMENT '上传时间',
  `Rec` varchar(50) DEFAULT NULL COMMENT '上传人',
  `Doc` text COMMENT '内容',
  PRIMARY KEY (`OID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_filemanager`
--

LOCK TABLES `sys_filemanager` WRITE;
/*!40000 ALTER TABLE `sys_filemanager` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_filemanager` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_formtree`
--

DROP TABLE IF EXISTS `sys_formtree`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_formtree` (
  `No` varchar(10) NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(100) DEFAULT NULL COMMENT '名称',
  `ParentNo` varchar(100) DEFAULT NULL COMMENT '父节点No',
  `OrgNo` varchar(100) DEFAULT NULL COMMENT '组织编号',
  `Idx` int(11) DEFAULT NULL COMMENT 'Idx',
  `IsDir` int(11) DEFAULT NULL COMMENT '是否是目录?',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='表单树';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_formtree`
--

LOCK TABLES `sys_formtree` WRITE;
/*!40000 ALTER TABLE `sys_formtree` DISABLE KEYS */;
INSERT INTO `sys_formtree` VALUES ('01','表单元素','1',NULL,0,0),('1','表单库','0',NULL,0,0);
/*!40000 ALTER TABLE `sys_formtree` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_frmattachment`
--

DROP TABLE IF EXISTS `sys_frmattachment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_frmattachment` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT '表单ID',
  `NoOfObj` varchar(50) DEFAULT NULL COMMENT '附件标识',
  `FK_Node` int(11) DEFAULT NULL COMMENT '节点控制(对sln有效)',
  `AthRunModel` int(11) DEFAULT NULL COMMENT '运行模式,枚举类型:0 流水模式;1 固定模式;2 自定义页面;',
  `Name` varchar(50) DEFAULT NULL COMMENT '附件名称',
  `Exts` varchar(50) DEFAULT NULL COMMENT '文件格式',
  `NumOfUpload` int(11) DEFAULT NULL COMMENT '最低上传数量',
  `AthSaveWay` int(11) DEFAULT NULL COMMENT '保存方式,枚举类型:0 保存到IIS服务器;1 保存到数据库;2 ftp服务器;',
  `SaveTo` varchar(150) DEFAULT NULL COMMENT '保存到',
  `Sort` varchar(500) DEFAULT NULL COMMENT '类别',
  `IsTurn2Html` int(11) DEFAULT NULL COMMENT '是否转换成html(方便手机浏览)',
  `X` float DEFAULT NULL COMMENT 'X',
  `Y` float DEFAULT NULL COMMENT 'Y',
  `W` float DEFAULT NULL COMMENT '宽度',
  `H` float DEFAULT NULL COMMENT '高度',
  `DeleteWay` int(11) DEFAULT NULL COMMENT '附件删除规则,枚举类型:0 不能删除;1 删除所有;2 只能删除自己上传的;',
  `IsUpload` int(11) DEFAULT NULL COMMENT '是否可以上传',
  `IsDownload` int(11) DEFAULT NULL COMMENT '是否可以下载',
  `IsOrder` int(11) DEFAULT NULL COMMENT '是否可以排序',
  `IsAutoSize` int(11) DEFAULT NULL COMMENT '自动控制大小',
  `IsNote` int(11) DEFAULT NULL COMMENT '是否增加备注',
  `IsExpCol` int(11) DEFAULT NULL COMMENT '是否启用扩展列',
  `IsShowTitle` int(11) DEFAULT NULL COMMENT '是否显示标题列',
  `UploadType` int(11) DEFAULT NULL COMMENT '上传类型,枚举类型:0 单个;1 多个;2 指定;',
  `AthUploadWay` int(11) DEFAULT NULL COMMENT '控制上传控制方式,枚举类型:0 继承模式;1 协作模式;',
  `CtrlWay` int(11) DEFAULT NULL COMMENT '控制呈现控制方式,枚举类型:0 单个;1 多个;2 指定;',
  `IsRowLock` int(11) DEFAULT NULL COMMENT '是否启用锁定行',
  `IsWoEnableWF` int(11) DEFAULT NULL COMMENT '是否启用weboffice',
  `IsWoEnableSave` int(11) DEFAULT NULL COMMENT '是否启用保存',
  `IsWoEnableReadonly` int(11) DEFAULT NULL COMMENT '是否只读',
  `IsWoEnableRevise` int(11) DEFAULT NULL COMMENT '是否启用修订',
  `IsWoEnableViewKeepMark` int(11) DEFAULT NULL COMMENT '是否查看用户留痕',
  `IsWoEnablePrint` int(11) DEFAULT NULL COMMENT '是否打印',
  `IsWoEnableSeal` int(11) DEFAULT NULL COMMENT '是否启用签章',
  `IsWoEnableOver` int(11) DEFAULT NULL COMMENT '是否启用套红',
  `IsWoEnableTemplete` int(11) DEFAULT NULL COMMENT '是否启用公文模板',
  `IsWoEnableCheck` int(11) DEFAULT NULL COMMENT '是否自动写入审核信息',
  `IsWoEnableInsertFlow` int(11) DEFAULT NULL COMMENT '是否插入流程',
  `IsWoEnableInsertFengXian` int(11) DEFAULT NULL COMMENT '是否插入风险点',
  `IsWoEnableMarks` int(11) DEFAULT NULL COMMENT '是否启用留痕模式',
  `IsWoEnableDown` int(11) DEFAULT NULL COMMENT '是否启用下载',
  `IsToHeLiuHZ` int(11) DEFAULT NULL COMMENT '该附件是否要汇总到合流节点上去？(对子线程节点有效)',
  `IsHeLiuHuiZong` int(11) DEFAULT NULL COMMENT '是否是合流节点的汇总附件组件？(对合流节点有效)',
  `DataRefNoOfObj` varchar(150) DEFAULT NULL COMMENT '对应附件标识',
  `AtPara` varchar(3000) DEFAULT NULL COMMENT 'AtPara',
  `GroupID` int(11) DEFAULT NULL COMMENT 'GroupID',
  `GUID` varchar(128) DEFAULT NULL COMMENT 'GUID',
  `IsDelete` int(11) DEFAULT NULL COMMENT '附件删除规则(0=不能删除1=删除所有2=只能删除自己上传的)',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='附件';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_frmattachment`
--

LOCK TABLES `sys_frmattachment` WRITE;
/*!40000 ALTER TABLE `sys_frmattachment` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_frmattachment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_frmattachmentdb`
--

DROP TABLE IF EXISTS `sys_frmattachmentdb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_frmattachmentdb` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT 'FK_MapData',
  `FK_FrmAttachment` varchar(500) DEFAULT NULL COMMENT '附件编号',
  `NoOfObj` varchar(50) DEFAULT NULL COMMENT '附件标识',
  `RefPKVal` varchar(50) DEFAULT NULL COMMENT '实体主键',
  `FID` int(11) DEFAULT NULL COMMENT 'FID',
  `NodeID` varchar(50) DEFAULT NULL COMMENT '节点ID',
  `Sort` varchar(200) DEFAULT NULL COMMENT '类别',
  `FileFullName` varchar(700) DEFAULT NULL COMMENT '文件路径',
  `FileName` varchar(500) DEFAULT NULL COMMENT '名称',
  `FileExts` varchar(50) DEFAULT NULL COMMENT '扩展',
  `FileSize` float DEFAULT NULL COMMENT '文件大小',
  `RDT` varchar(50) DEFAULT NULL COMMENT '记录日期',
  `Rec` varchar(50) DEFAULT NULL COMMENT '记录人',
  `RecName` varchar(50) DEFAULT NULL COMMENT '记录人名字',
  `MyNote` text COMMENT '备注',
  `IsRowLock` int(11) DEFAULT NULL COMMENT '是否锁定行',
  `Idx` int(11) DEFAULT NULL COMMENT '排序',
  `UploadGUID` varchar(500) DEFAULT NULL COMMENT '上传GUID',
  `AtPara` varchar(3000) DEFAULT NULL COMMENT 'AtPara',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='附件数据存储';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_frmattachmentdb`
--

LOCK TABLES `sys_frmattachmentdb` WRITE;
/*!40000 ALTER TABLE `sys_frmattachmentdb` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_frmattachmentdb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_frmbtn`
--

DROP TABLE IF EXISTS `sys_frmbtn`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_frmbtn` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT '表单ID',
  `Text` text COMMENT '标签',
  `X` float DEFAULT NULL COMMENT 'X',
  `Y` float DEFAULT NULL COMMENT 'Y',
  `IsView` int(11) DEFAULT NULL COMMENT '是否可见',
  `IsEnable` int(11) DEFAULT NULL COMMENT '是否起用',
  `UAC` int(11) DEFAULT NULL COMMENT '控制类型',
  `UACContext` text COMMENT '控制内容',
  `EventType` int(11) DEFAULT NULL COMMENT '事件类型,枚举类型:0 禁用;1 执行URL;2 执行CCFromRef.js;',
  `EventContext` text COMMENT '事件内容',
  `MsgOK` varchar(500) DEFAULT NULL COMMENT '运行成功提示',
  `MsgErr` varchar(500) DEFAULT NULL COMMENT '运行失败提示',
  `GUID` varchar(128) DEFAULT NULL COMMENT 'GUID',
  `GroupID` int(11) DEFAULT NULL COMMENT '所在分组',
  `GroupIDText` varchar(50) DEFAULT NULL COMMENT '所在分组',
  `BtnType` int(11) DEFAULT '0',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='按钮';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_frmbtn`
--

LOCK TABLES `sys_frmbtn` WRITE;
/*!40000 ALTER TABLE `sys_frmbtn` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_frmbtn` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_frmele`
--

DROP TABLE IF EXISTS `sys_frmele`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_frmele` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT '表单ID',
  `EleType` varchar(50) DEFAULT NULL COMMENT '类型',
  `EleID` varchar(50) DEFAULT NULL COMMENT '控件ID值(对部分控件有效)',
  `EleName` varchar(200) DEFAULT NULL COMMENT '控件名称(对部分控件有效)',
  `X` float DEFAULT NULL COMMENT 'X',
  `Y` float DEFAULT NULL COMMENT 'Y',
  `H` float DEFAULT NULL COMMENT 'H',
  `W` float DEFAULT NULL COMMENT 'W',
  `Tag1` varchar(50) DEFAULT NULL COMMENT 'Tag1',
  `Tag2` varchar(50) DEFAULT NULL COMMENT 'Tag2',
  `Tag3` varchar(50) DEFAULT NULL COMMENT 'Tag3',
  `Tag4` varchar(50) DEFAULT NULL COMMENT 'Tag4',
  `GUID` varchar(128) DEFAULT NULL COMMENT 'GUID',
  `IsEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `AtPara` text COMMENT 'AtPara',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='表单元素扩展';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_frmele`
--

LOCK TABLES `sys_frmele` WRITE;
/*!40000 ALTER TABLE `sys_frmele` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_frmele` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_frmeledb`
--

DROP TABLE IF EXISTS `sys_frmeledb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_frmeledb` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT 'FK_MapData',
  `EleID` varchar(50) DEFAULT NULL COMMENT 'EleID',
  `RefPKVal` varchar(50) DEFAULT NULL COMMENT 'RefPKVal',
  `FID` int(11) DEFAULT NULL COMMENT 'FID',
  `Tag1` varchar(1000) DEFAULT NULL COMMENT 'Tag1',
  `Tag2` varchar(1000) DEFAULT NULL COMMENT 'Tag2',
  `Tag3` varchar(1000) DEFAULT NULL COMMENT 'Tag3',
  `Tag4` varchar(1000) DEFAULT NULL COMMENT 'Tag4',
  `Tag5` varchar(1000) DEFAULT NULL COMMENT 'Tag5',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='表单元素扩展DB';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_frmeledb`
--

LOCK TABLES `sys_frmeledb` WRITE;
/*!40000 ALTER TABLE `sys_frmeledb` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_frmeledb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_frmevent`
--

DROP TABLE IF EXISTS `sys_frmevent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_frmevent` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `FK_Event` varchar(400) DEFAULT NULL COMMENT '事件名称',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT '表单ID',
  `FK_Flow` varchar(100) DEFAULT NULL COMMENT '流程编号',
  `FK_Node` int(11) DEFAULT NULL COMMENT '节点ID',
  `EventDoType` int(11) DEFAULT NULL COMMENT '事件类型',
  `DoDoc` varchar(400) DEFAULT NULL COMMENT '执行内容',
  `MsgOK` varchar(400) DEFAULT NULL COMMENT '成功执行提示',
  `MsgError` varchar(400) DEFAULT NULL COMMENT '异常信息提示',
  `MsgCtrl` int(11) DEFAULT NULL COMMENT '消息发送控制,枚举类型:0 不发送;1 按设置的下一步接受人自动发送（默认）;2 由本节点表单系统字段(IsSendEmail,IsSendSMS)来决定;3 由SDK开发者参数(IsSendEmail,IsSendSMS)来决定;',
  `MailEnable` int(11) DEFAULT NULL COMMENT '是否启用邮件发送？(如果启用就要设置邮件模版，支持ccflow表达式。)',
  `MailTitle` varchar(200) DEFAULT NULL COMMENT '邮件标题模版',
  `MailDoc` text COMMENT '邮件内容模版',
  `SMSEnable` int(11) DEFAULT NULL COMMENT '是否启用短信发送？(如果启用就要设置短信模版，支持ccflow表达式。)',
  `SMSDoc` text COMMENT '短信内容模版',
  `MobilePushEnable` int(11) DEFAULT NULL COMMENT '是否推送到手机、pad端。',
  `AtPara` text COMMENT 'AtPara',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='事件';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_frmevent`
--

LOCK TABLES `sys_frmevent` WRITE;
/*!40000 ALTER TABLE `sys_frmevent` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_frmevent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_frmimg`
--

DROP TABLE IF EXISTS `sys_frmimg`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_frmimg` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT 'FK_MapData',
  `ImgAppType` int(11) DEFAULT NULL COMMENT '应用类型',
  `X` float DEFAULT NULL COMMENT 'X',
  `Y` float DEFAULT NULL COMMENT 'Y',
  `H` float DEFAULT NULL COMMENT 'H',
  `W` float DEFAULT NULL COMMENT 'W',
  `ImgURL` varchar(200) DEFAULT NULL COMMENT '装饰图片URL',
  `ImgPath` varchar(200) DEFAULT NULL COMMENT '装饰图片路径',
  `LinkURL` varchar(200) DEFAULT NULL COMMENT '连接到URL',
  `LinkTarget` varchar(200) DEFAULT NULL COMMENT '连接目标',
  `GUID` varchar(128) DEFAULT NULL COMMENT 'GUID',
  `Tag0` varchar(500) DEFAULT NULL COMMENT '参数',
  `ImgSrcType` int(11) DEFAULT NULL COMMENT '装饰图片来源,枚举类型:0 本地;1 URL;',
  `IsEdit` int(11) DEFAULT NULL COMMENT '是否可以编辑',
  `Name` varchar(500) DEFAULT NULL COMMENT '中文名称',
  `EnPK` varchar(500) DEFAULT NULL COMMENT '英文名称',
  `GroupID` varchar(50) DEFAULT NULL COMMENT '所在分组',
  `GroupIDText` varchar(50) DEFAULT NULL COMMENT '所在分组',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='装饰图片';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_frmimg`
--

LOCK TABLES `sys_frmimg` WRITE;
/*!40000 ALTER TABLE `sys_frmimg` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_frmimg` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_frmimgath`
--

DROP TABLE IF EXISTS `sys_frmimgath`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_frmimgath` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT '表单ID',
  `CtrlID` varchar(200) DEFAULT NULL COMMENT '控件ID',
  `Name` varchar(200) DEFAULT NULL COMMENT '中文名称',
  `X` float DEFAULT NULL COMMENT 'X',
  `Y` float DEFAULT NULL COMMENT 'Y',
  `H` float DEFAULT NULL COMMENT 'H',
  `W` float DEFAULT NULL COMMENT 'W',
  `IsEdit` int(11) DEFAULT NULL COMMENT '是否可编辑',
  `IsRequired` int(11) DEFAULT NULL COMMENT '是否必填项',
  `GUID` varchar(128) DEFAULT NULL COMMENT 'GUID',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='图片附件';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_frmimgath`
--

LOCK TABLES `sys_frmimgath` WRITE;
/*!40000 ALTER TABLE `sys_frmimgath` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_frmimgath` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_frmimgathdb`
--

DROP TABLE IF EXISTS `sys_frmimgathdb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_frmimgathdb` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT '附件ID',
  `FK_FrmImgAth` varchar(50) DEFAULT NULL COMMENT '图片附件编号',
  `RefPKVal` varchar(50) DEFAULT NULL COMMENT '实体主键',
  `FileFullName` varchar(700) DEFAULT NULL COMMENT '文件全路径',
  `FileName` varchar(500) DEFAULT NULL COMMENT '名称',
  `FileExts` varchar(50) DEFAULT NULL COMMENT '扩展名',
  `FileSize` float DEFAULT NULL COMMENT '文件大小',
  `RDT` varchar(50) DEFAULT NULL COMMENT '记录日期',
  `Rec` varchar(50) DEFAULT NULL COMMENT '记录人',
  `RecName` varchar(50) DEFAULT NULL COMMENT '记录人名字',
  `MyNote` text COMMENT '备注',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='剪切图片附件数据存储';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_frmimgathdb`
--

LOCK TABLES `sys_frmimgathdb` WRITE;
/*!40000 ALTER TABLE `sys_frmimgathdb` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_frmimgathdb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_frmlab`
--

DROP TABLE IF EXISTS `sys_frmlab`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_frmlab` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT 'FK_MapData',
  `Text` text COMMENT 'Label',
  `X` float DEFAULT NULL COMMENT 'X',
  `Y` float DEFAULT NULL COMMENT 'Y',
  `FontSize` int(11) DEFAULT NULL COMMENT '字体大小',
  `FontColor` varchar(50) DEFAULT NULL COMMENT '颜色',
  `FontName` varchar(50) DEFAULT NULL COMMENT '字体名称',
  `FontStyle` varchar(200) DEFAULT NULL COMMENT '字体风格',
  `FontWeight` varchar(50) DEFAULT NULL COMMENT '字体宽度',
  `IsBold` int(11) DEFAULT NULL COMMENT '是否粗体',
  `IsItalic` int(11) DEFAULT NULL COMMENT '是否斜体',
  `GUID` varchar(128) DEFAULT NULL COMMENT 'GUID',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='标签';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_frmlab`
--

LOCK TABLES `sys_frmlab` WRITE;
/*!40000 ALTER TABLE `sys_frmlab` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_frmlab` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_frmline`
--

DROP TABLE IF EXISTS `sys_frmline`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_frmline` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT '主表',
  `X1` float DEFAULT NULL COMMENT 'X1',
  `Y1` float DEFAULT NULL COMMENT 'Y1',
  `X2` float DEFAULT NULL COMMENT 'X2',
  `Y2` float DEFAULT NULL COMMENT 'Y2',
  `X` float DEFAULT NULL COMMENT 'X',
  `Y` float DEFAULT NULL COMMENT 'Y',
  `BorderWidth` float DEFAULT NULL COMMENT '宽度',
  `BorderColor` varchar(30) DEFAULT NULL COMMENT '颜色',
  `GUID` varchar(128) DEFAULT NULL COMMENT '初始的GUID',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='线';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_frmline`
--

LOCK TABLES `sys_frmline` WRITE;
/*!40000 ALTER TABLE `sys_frmline` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_frmline` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_frmlink`
--

DROP TABLE IF EXISTS `sys_frmlink`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_frmlink` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT '表单ID',
  `Text` varchar(500) DEFAULT NULL COMMENT '标签',
  `URL` varchar(500) DEFAULT NULL COMMENT 'URL',
  `Target` varchar(20) DEFAULT NULL COMMENT '连接目标',
  `X` float DEFAULT NULL COMMENT 'X',
  `Y` float DEFAULT NULL COMMENT 'Y',
  `FontSize` int(11) DEFAULT NULL COMMENT 'FontSize',
  `FontColor` varchar(50) DEFAULT NULL COMMENT 'FontColor',
  `FontName` varchar(50) DEFAULT NULL COMMENT 'FontName',
  `FontStyle` varchar(50) DEFAULT NULL COMMENT 'FontStyle',
  `IsBold` int(11) DEFAULT NULL COMMENT 'IsBold',
  `IsItalic` int(11) DEFAULT NULL COMMENT 'IsItalic',
  `GUID` varchar(128) DEFAULT NULL COMMENT 'GUID',
  `GroupID` varchar(50) DEFAULT NULL COMMENT '所在分组',
  `GroupIDText` varchar(50) DEFAULT NULL COMMENT '所在分组',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='超连接';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_frmlink`
--

LOCK TABLES `sys_frmlink` WRITE;
/*!40000 ALTER TABLE `sys_frmlink` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_frmlink` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_frmrb`
--

DROP TABLE IF EXISTS `sys_frmrb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_frmrb` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT '表单ID',
  `KeyOfEn` varchar(30) DEFAULT NULL COMMENT '字段',
  `EnumKey` varchar(30) DEFAULT NULL COMMENT '枚举值',
  `Lab` varchar(90) DEFAULT NULL COMMENT '标签',
  `IntKey` int(11) DEFAULT NULL COMMENT 'IntKey',
  `UIIsEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `X` float DEFAULT NULL COMMENT 'X',
  `Y` float DEFAULT NULL COMMENT 'Y',
  `Script` text COMMENT '要执行的脚本',
  `FieldsCfg` text COMMENT '配置信息@FieldName=Sta',
  `SetVal` varchar(200) DEFAULT NULL COMMENT '设置的值',
  `Tip` varchar(1000) DEFAULT NULL COMMENT '选择后提示的信息',
  `GUID` varchar(128) DEFAULT NULL COMMENT 'GUID',
  `AtPara` varchar(1000) DEFAULT NULL COMMENT 'AtPara',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='单选框';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_frmrb`
--

LOCK TABLES `sys_frmrb` WRITE;
/*!40000 ALTER TABLE `sys_frmrb` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_frmrb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_frmreportfield`
--

DROP TABLE IF EXISTS `sys_frmreportfield`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_frmreportfield` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT '表单编号',
  `KeyOfEn` varchar(100) DEFAULT NULL COMMENT '字段名',
  `Name` varchar(200) DEFAULT NULL COMMENT '显示中文名',
  `UIWidth` varchar(100) DEFAULT NULL COMMENT '宽度',
  `UIVisible` int(11) DEFAULT NULL COMMENT '是否显示',
  `Idx` int(11) DEFAULT NULL COMMENT '显示顺序',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='表单报表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_frmreportfield`
--

LOCK TABLES `sys_frmreportfield` WRITE;
/*!40000 ALTER TABLE `sys_frmreportfield` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_frmreportfield` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_frmrpt`
--

DROP TABLE IF EXISTS `sys_frmrpt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_frmrpt` (
  `No` varchar(20) NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(50) DEFAULT NULL COMMENT '描述',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT '主表',
  `PTable` varchar(30) DEFAULT NULL COMMENT '物理表',
  `SQLOfColumn` varchar(300) DEFAULT NULL COMMENT '列的数据源',
  `SQLOfRow` varchar(300) DEFAULT NULL COMMENT '行数据源',
  `RowIdx` int(11) DEFAULT NULL COMMENT '位置',
  `GroupID` int(11) DEFAULT NULL COMMENT 'GroupID',
  `IsShowSum` int(11) DEFAULT NULL COMMENT 'IsShowSum',
  `IsShowIdx` int(11) DEFAULT NULL COMMENT 'IsShowIdx',
  `IsCopyNDData` int(11) DEFAULT NULL COMMENT 'IsCopyNDData',
  `IsHLDtl` int(11) DEFAULT NULL COMMENT '是否是合流汇总',
  `IsReadonly` int(11) DEFAULT NULL COMMENT 'IsReadonly',
  `IsShowTitle` int(11) DEFAULT NULL COMMENT 'IsShowTitle',
  `IsView` int(11) DEFAULT NULL COMMENT '是否可见',
  `IsExp` int(11) DEFAULT NULL COMMENT 'IsExp',
  `IsImp` int(11) DEFAULT NULL COMMENT 'IsImp',
  `IsInsert` int(11) DEFAULT NULL COMMENT 'IsInsert',
  `IsDelete` int(11) DEFAULT NULL COMMENT 'IsDelete',
  `IsUpdate` int(11) DEFAULT NULL COMMENT 'IsUpdate',
  `IsEnablePass` int(11) DEFAULT NULL COMMENT '是否启用通过审核功能?',
  `IsEnableAthM` int(11) DEFAULT NULL COMMENT '是否启用多附件',
  `IsEnableM2M` int(11) DEFAULT NULL COMMENT '是否启用M2M',
  `IsEnableM2MM` int(11) DEFAULT NULL COMMENT '是否启用M2M',
  `WhenOverSize` int(11) DEFAULT NULL COMMENT 'WhenOverSize,枚举类型:0 不处理;1 向下顺增行;2 次页显示;',
  `DtlOpenType` int(11) DEFAULT NULL COMMENT '数据开放类型,枚举类型:0 操作员;1 工作ID;2 流程ID;',
  `EditModel` int(11) DEFAULT '0' COMMENT '显示格式',
  `X` float DEFAULT NULL COMMENT 'X',
  `Y` float DEFAULT NULL COMMENT 'Y',
  `H` float DEFAULT NULL COMMENT 'H',
  `W` float DEFAULT NULL COMMENT 'W',
  `FrmW` float DEFAULT NULL COMMENT 'FrmW',
  `FrmH` float DEFAULT NULL COMMENT 'FrmH',
  `MTR` varchar(3000) DEFAULT NULL COMMENT '多表头列',
  `GUID` varchar(128) DEFAULT NULL COMMENT 'GUID',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='纬度报表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_frmrpt`
--

LOCK TABLES `sys_frmrpt` WRITE;
/*!40000 ALTER TABLE `sys_frmrpt` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_frmrpt` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_frmsln`
--

DROP TABLE IF EXISTS `sys_frmsln`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_frmsln` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `FK_Flow` varchar(4) DEFAULT NULL COMMENT '流程编号',
  `FK_Node` int(11) DEFAULT NULL COMMENT '节点',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT '表单ID',
  `KeyOfEn` varchar(200) DEFAULT NULL COMMENT '字段',
  `Name` varchar(500) DEFAULT NULL COMMENT '字段名',
  `EleType` varchar(20) DEFAULT NULL COMMENT '类型',
  `UIIsEnable` int(11) DEFAULT NULL COMMENT '是否可用',
  `UIVisible` int(11) DEFAULT NULL COMMENT '是否可见',
  `IsSigan` int(11) DEFAULT NULL COMMENT '是否签名',
  `IsNotNull` int(11) DEFAULT NULL COMMENT '是否为空',
  `RegularExp` varchar(500) DEFAULT NULL COMMENT '正则表达式',
  `IsWriteToFlowTable` int(11) DEFAULT NULL COMMENT '是否写入流程表',
  `IsWriteToGenerWorkFlow` int(11) DEFAULT '0' COMMENT '是否写入流程注册表',
  `DefVal` varchar(200) DEFAULT NULL COMMENT '默认值',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='表单字段方案';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_frmsln`
--

LOCK TABLES `sys_frmsln` WRITE;
/*!40000 ALTER TABLE `sys_frmsln` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_frmsln` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_glovar`
--

DROP TABLE IF EXISTS `sys_glovar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_glovar` (
  `No` varchar(50) NOT NULL COMMENT '键 - 主键',
  `Name` varchar(120) DEFAULT NULL COMMENT '名称',
  `Val` text COMMENT '值',
  `GroupKey` varchar(120) DEFAULT NULL COMMENT '分组值',
  `Note` text COMMENT '说明',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='全局变量';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_glovar`
--

LOCK TABLES `sys_glovar` WRITE;
/*!40000 ALTER TABLE `sys_glovar` DISABLE KEYS */;
INSERT INTO `sys_glovar` VALUES ('0','选择系统约定默认值',NULL,'DefVal',NULL),('@CurrWorker','当前工作可处理人员',NULL,'DefVal',NULL),('@FK_ND','当前年度',NULL,'DefVal',NULL),('@FK_YF','当前月份',NULL,'DefVal',NULL),('@WebUser.FK_Dept','登陆人员部门编号',NULL,'DefVal',NULL),('@WebUser.FK_DeptFullName','登陆人员部门全称',NULL,'DefVal',NULL),('@WebUser.FK_DeptName','登陆人员部门名称',NULL,'DefVal',NULL),('@WebUser.Name','登陆人员名称',NULL,'DefVal',NULL),('@WebUser.No','登陆人员账号',NULL,'DefVal',NULL),('@yyyy年MM月dd日','当前日期(yyyy年MM月dd日)',NULL,'DefVal',NULL),('@yyyy年MM月dd日HH时mm分','当前日期(yyyy年MM月dd日HH时mm分)',NULL,'DefVal',NULL),('@yy年MM月dd日','当前日期(yy年MM月dd日)',NULL,'DefVal',NULL),('@yy年MM月dd日HH时mm分','当前日期(yy年MM月dd日HH时mm分)',NULL,'DefVal',NULL),('DTSTodoStaPM2018-12-28','时效调度 WFTodoSta PM 调度','DTSTodoStaPM2018-12-28','WF','时效调度PMDTSTodoStaPM2018-12-28');
/*!40000 ALTER TABLE `sys_glovar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_groupenstemplate`
--

DROP TABLE IF EXISTS `sys_groupenstemplate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_groupenstemplate` (
  `OID` int(11) NOT NULL COMMENT 'OID - 主键',
  `EnName` varchar(500) DEFAULT NULL COMMENT '表称',
  `Name` varchar(500) DEFAULT NULL COMMENT '报表名',
  `EnsName` varchar(90) DEFAULT NULL COMMENT '报表类名',
  `OperateCol` varchar(90) DEFAULT NULL COMMENT '操作属性',
  `Attrs` varchar(90) DEFAULT NULL COMMENT '运算属性',
  `Rec` varchar(90) DEFAULT NULL COMMENT '记录人',
  PRIMARY KEY (`OID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='报表模板';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_groupenstemplate`
--

LOCK TABLES `sys_groupenstemplate` WRITE;
/*!40000 ALTER TABLE `sys_groupenstemplate` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_groupenstemplate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_groupfield`
--

DROP TABLE IF EXISTS `sys_groupfield`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_groupfield` (
  `OID` int(11) NOT NULL COMMENT 'OID - 主键',
  `Lab` varchar(500) DEFAULT NULL COMMENT '标签',
  `FrmID` varchar(200) DEFAULT NULL COMMENT '表单ID',
  `CtrlType` varchar(50) DEFAULT NULL COMMENT '控件类型',
  `CtrlID` varchar(500) DEFAULT NULL COMMENT '控件ID',
  `Idx` int(11) DEFAULT NULL COMMENT '顺序号',
  `GUID` varchar(128) DEFAULT NULL COMMENT 'GUID',
  `AtPara` varchar(3000) DEFAULT NULL COMMENT 'AtPara',
  PRIMARY KEY (`OID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='傻瓜表单分组';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_groupfield`
--

LOCK TABLES `sys_groupfield` WRITE;
/*!40000 ALTER TABLE `sys_groupfield` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_groupfield` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_m2m`
--

DROP TABLE IF EXISTS `sys_m2m`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_m2m` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT 'FK_MapData',
  `M2MNo` varchar(20) DEFAULT NULL COMMENT 'M2MNo',
  `EnOID` int(11) DEFAULT NULL COMMENT '实体OID',
  `DtlObj` varchar(20) DEFAULT NULL COMMENT 'DtlObj(对于m2mm有效)',
  `Doc` text COMMENT '内容',
  `ValsName` text COMMENT 'ValsName',
  `ValsSQL` text COMMENT 'ValsSQL',
  `NumSelected` int(11) DEFAULT NULL COMMENT '选择数',
  `GUID` varchar(128) DEFAULT NULL COMMENT 'GUID',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='M2M数据存储';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_m2m`
--

LOCK TABLES `sys_m2m` WRITE;
/*!40000 ALTER TABLE `sys_m2m` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_m2m` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_mapattr`
--

DROP TABLE IF EXISTS `sys_mapattr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_mapattr` (
  `MyPK` varchar(200) NOT NULL COMMENT '实体标识 - 主键',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT '实体标识',
  `KeyOfEn` varchar(200) DEFAULT NULL COMMENT '属性',
  `Name` varchar(200) DEFAULT NULL COMMENT '描述',
  `DefVal` varchar(4000) DEFAULT NULL COMMENT '默认值',
  `UIContralType` int(11) DEFAULT NULL COMMENT '控件',
  `MyDataType` int(11) DEFAULT NULL COMMENT '数据类型',
  `LGType` int(11) DEFAULT NULL COMMENT '逻辑类型,枚举类型:0 普通;1 枚举;2 外键;3 打开系统页面;',
  `UIWidth` float DEFAULT NULL COMMENT '宽度',
  `UIHeight` float DEFAULT NULL COMMENT '高度',
  `MinLen` int(11) DEFAULT NULL COMMENT '最小长度',
  `MaxLen` int(11) DEFAULT NULL COMMENT '最大长度',
  `UIBindKey` varchar(100) DEFAULT NULL COMMENT '绑定的信息',
  `UIRefKey` varchar(30) DEFAULT NULL COMMENT '绑定的Key',
  `UIRefKeyText` varchar(30) DEFAULT NULL COMMENT '绑定的Text',
  `UIVisible` int(11) DEFAULT NULL COMMENT '是否可见',
  `UIIsEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `UIIsLine` int(11) DEFAULT NULL COMMENT '是否单独栏显示',
  `UIIsInput` int(11) DEFAULT NULL COMMENT '是否必填字段',
  `IsRichText` int(11) DEFAULT NULL COMMENT '富文本',
  `IsSupperText` int(11) DEFAULT NULL COMMENT '富文本',
  `FontSize` int(11) DEFAULT NULL COMMENT '富文本',
  `IsSigan` int(11) DEFAULT NULL COMMENT '签字？',
  `X` float DEFAULT NULL COMMENT 'X',
  `Y` float DEFAULT NULL COMMENT 'Y',
  `GUID` varchar(128) DEFAULT NULL COMMENT 'GUID',
  `Tag` varchar(100) DEFAULT NULL COMMENT '标识（存放临时数据）',
  `EditType` int(11) DEFAULT NULL COMMENT '编辑类型',
  `Tip` varchar(4000) DEFAULT NULL COMMENT '激活提示',
  `ColSpan` int(11) DEFAULT NULL COMMENT '单元格数量',
  `GroupID` varchar(100) DEFAULT NULL COMMENT '显示的分组',
  `IsEnableInAPP` int(11) DEFAULT NULL COMMENT '是否在移动端中显示',
  `Idx` int(11) DEFAULT NULL COMMENT '序号',
  `AtPara` text COMMENT 'AtPara',
  `DefValText` varchar(50) DEFAULT NULL COMMENT '默认值（选中）',
  `RBShowModel` int(11) DEFAULT NULL COMMENT '单选按钮的展现方式,枚举类型:0 竖向;3 横向;',
  `IsEnableJS` int(11) DEFAULT NULL COMMENT '是否启用JS高级设置？',
  `GroupIDText` varchar(50) DEFAULT NULL COMMENT '显示的分组',
  `ExtDefVal` varchar(50) DEFAULT NULL COMMENT '系统默认值',
  `ExtDefValText` varchar(50) DEFAULT NULL COMMENT '系统默认值',
  `ExtIsSum` int(11) DEFAULT NULL COMMENT '是否显示合计(对从表有效)',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='实体属性';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_mapattr`
--

LOCK TABLES `sys_mapattr` WRITE;
/*!40000 ALTER TABLE `sys_mapattr` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_mapattr` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_mapdata`
--

DROP TABLE IF EXISTS `sys_mapdata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_mapdata` (
  `No` varchar(200) NOT NULL COMMENT 'null - 主键',
  `Name` varchar(500) DEFAULT NULL COMMENT 'null',
  `OfficeOpenLab` varchar(50) DEFAULT NULL COMMENT '打开本地标签',
  `OfficeOpenEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `OfficeOpenTemplateLab` varchar(50) DEFAULT NULL COMMENT '打开模板标签',
  `OfficeOpenTemplateEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `OfficeSaveLab` varchar(50) DEFAULT NULL COMMENT '保存标签',
  `OfficeSaveEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `OfficeAcceptLab` varchar(50) DEFAULT NULL COMMENT '接受修订标签',
  `OfficeAcceptEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `OfficeRefuseLab` varchar(50) DEFAULT NULL COMMENT '拒绝修订标签',
  `OfficeRefuseEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `OfficeOverLab` varchar(50) DEFAULT NULL COMMENT '套红按钮标签',
  `OfficeOverEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `OfficeMarksEnable` int(11) DEFAULT NULL COMMENT '是否查看用户留痕',
  `OfficePrintLab` varchar(50) DEFAULT NULL COMMENT '打印按钮标签',
  `OfficePrintEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `OfficeSealLab` varchar(50) DEFAULT NULL COMMENT '签章按钮标签',
  `OfficeSealEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `OfficeInsertFlowLab` varchar(50) DEFAULT NULL COMMENT '插入流程标签',
  `OfficeInsertFlowEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `OfficeNodeInfo` int(11) DEFAULT NULL COMMENT '是否记录节点信息',
  `OfficeReSavePDF` int(11) DEFAULT NULL COMMENT '是否该自动保存为PDF',
  `OfficeDownLab` varchar(50) DEFAULT NULL COMMENT '下载按钮标签',
  `OfficeDownEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `OfficeIsMarks` int(11) DEFAULT NULL COMMENT '是否进入留痕模式',
  `OfficeTemplate` varchar(100) DEFAULT NULL COMMENT '指定文档模板',
  `OfficeIsParent` int(11) DEFAULT NULL COMMENT '是否使用父流程的文档',
  `OfficeTHEnable` int(11) DEFAULT NULL COMMENT '是否自动套红',
  `OfficeTHTemplate` varchar(200) DEFAULT NULL COMMENT '自动套红模板',
  `PTable` varchar(500) DEFAULT NULL COMMENT '物理表',
  `IsTemplate` int(11) DEFAULT NULL COMMENT '是否是表单模版',
  `AtPara` text COMMENT 'AtPara',
  `FormEventEntity` varchar(100) DEFAULT NULL COMMENT '事件实体',
  `EnPK` varchar(200) DEFAULT NULL COMMENT '实体主键',
  `PTableModel` int(11) DEFAULT NULL COMMENT '表存储模式',
  `URL` varchar(500) DEFAULT NULL COMMENT 'Url',
  `Dtls` varchar(500) DEFAULT NULL COMMENT '从表',
  `FrmW` int(11) DEFAULT NULL COMMENT '系统表单宽度',
  `FrmH` int(11) DEFAULT NULL COMMENT '系统表单高度',
  `TableCol` int(11) DEFAULT NULL COMMENT '表单显示列数',
  `Tag` varchar(500) DEFAULT NULL COMMENT 'Tag',
  `FK_FrmSort` varchar(500) DEFAULT NULL COMMENT '表单类别',
  `FK_FormTree` varchar(500) DEFAULT NULL COMMENT '表单树',
  `FrmType` int(11) DEFAULT NULL COMMENT '表单类型',
  `AppType` int(11) DEFAULT NULL COMMENT '应用类型',
  `DBSrc` varchar(100) DEFAULT NULL COMMENT '数据源,外键:对应物理表:Sys_SFDBSrc,表描述:数据源',
  `BodyAttr` varchar(100) DEFAULT NULL COMMENT '表单Body属性',
  `Note` varchar(4000) DEFAULT NULL COMMENT '备注',
  `Designer` varchar(500) DEFAULT NULL COMMENT '设计者',
  `DesignerUnit` varchar(500) DEFAULT NULL COMMENT '单位',
  `DesignerContact` varchar(500) DEFAULT NULL COMMENT '联系方式',
  `Idx` int(11) DEFAULT NULL COMMENT '顺序号',
  `GUID` varchar(128) DEFAULT NULL COMMENT 'GUID',
  `Ver` varchar(30) DEFAULT NULL COMMENT '版本号',
  `FlowCtrls` varchar(200) DEFAULT NULL COMMENT '流程控件',
  `FK_Flow` varchar(50) DEFAULT NULL COMMENT '独立表单属性:FK_Flow',
  `DBURL` int(11) DEFAULT NULL COMMENT 'DBURL',
  `MyFileName` varchar(300) DEFAULT NULL COMMENT '表单模版',
  `MyFilePath` varchar(300) DEFAULT NULL COMMENT 'MyFilePath',
  `MyFileExt` varchar(20) DEFAULT NULL COMMENT 'MyFileExt',
  `WebPath` varchar(300) DEFAULT NULL COMMENT 'WebPath',
  `MyFileH` int(11) DEFAULT NULL COMMENT 'MyFileH',
  `MyFileW` int(11) DEFAULT NULL COMMENT 'MyFileW',
  `MyFileSize` float(11,2) DEFAULT NULL COMMENT 'MyFileSize',
  `RightViewWay` int(11) DEFAULT NULL COMMENT '报表查看权限控制方式',
  `RightViewTag` text COMMENT '报表查看权限控制Tag',
  `RightDeptWay` int(11) DEFAULT NULL COMMENT '部门数据查看控制方式',
  `RightDeptTag` text COMMENT '部门数据查看控制Tag',
  `TemplaterVer` varchar(30) DEFAULT NULL COMMENT '模版编号',
  `DBSave` varchar(50) DEFAULT '' COMMENT 'Excel数据文件存储',
  `TableWidth` int(11) DEFAULT NULL COMMENT '傻瓜表单宽度',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统表单';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_mapdata`
--

LOCK TABLES `sys_mapdata` WRITE;
/*!40000 ALTER TABLE `sys_mapdata` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_mapdata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_mapdtl`
--

DROP TABLE IF EXISTS `sys_mapdtl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_mapdtl` (
  `No` varchar(100) NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(200) DEFAULT NULL COMMENT '名称',
  `Alias` varchar(200) DEFAULT NULL COMMENT '别名',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT '表单ID',
  `PTable` varchar(200) DEFAULT NULL COMMENT '存储表',
  `GroupField` varchar(300) DEFAULT NULL COMMENT '分组字段',
  `RefPK` varchar(100) DEFAULT NULL COMMENT '关联的主键',
  `FEBD` varchar(100) DEFAULT NULL COMMENT '事件类实体类',
  `Model` int(11) DEFAULT NULL COMMENT '工作模式,枚举类型:0 普通;1 固定行;',
  `RowsOfList` int(11) DEFAULT NULL COMMENT '初始化行数',
  `IsEnableGroupField` int(11) DEFAULT NULL COMMENT '是否启用分组字段',
  `IsShowSum` int(11) DEFAULT NULL COMMENT '是否显示合计？',
  `IsShowIdx` int(11) DEFAULT NULL COMMENT '是否显示序号？',
  `IsCopyNDData` int(11) DEFAULT NULL COMMENT '是否允许copy节点数据',
  `IsHLDtl` int(11) DEFAULT NULL COMMENT '是否是合流汇总',
  `IsReadonly` int(11) DEFAULT NULL COMMENT '是否只读？',
  `IsShowTitle` int(11) DEFAULT NULL COMMENT '是否显示标题？',
  `IsView` int(11) DEFAULT NULL COMMENT '是否可见？',
  `IsInsert` int(11) DEFAULT NULL COMMENT '是否可以插入行？',
  `IsDelete` int(11) DEFAULT NULL COMMENT '是否可以删除行？',
  `IsUpdate` int(11) DEFAULT NULL COMMENT '是否可以更新？',
  `IsEnablePass` int(11) DEFAULT NULL COMMENT '是否启用通过审核功能?',
  `IsEnableAthM` int(11) DEFAULT NULL COMMENT '是否启用多附件',
  `IsEnableM2M` int(11) DEFAULT NULL COMMENT '是否启用M2M',
  `IsEnableM2MM` int(11) DEFAULT NULL COMMENT '是否启用M2M',
  `WhenOverSize` int(11) DEFAULT NULL COMMENT '超出行数,枚举类型:0 不处理;1 向下顺增行;2 次页显示;',
  `DtlOpenType` int(11) DEFAULT NULL COMMENT '数据开放类型,枚举类型:0 操作员;1 工作ID;2 流程ID;',
  `ListShowModel` int(11) DEFAULT NULL COMMENT '列表数据显示格式,枚举类型:0 表格;1 卡片;',
  `EditModel` int(11) DEFAULT NULL COMMENT '编辑数据方式,枚举类型:0 表格模式;1 傻瓜表单;2 自由表单;',
  `X` float DEFAULT NULL COMMENT '距左',
  `Y` float DEFAULT NULL COMMENT '距上',
  `H` float DEFAULT NULL COMMENT '高度',
  `W` float DEFAULT NULL COMMENT '宽度',
  `FrmW` float DEFAULT NULL COMMENT '表单宽度',
  `FrmH` float DEFAULT NULL COMMENT '表单高度',
  `MTR` varchar(4000) DEFAULT NULL COMMENT '请书写html标记,以《TR》开头，以《/TR》结尾。',
  `FilterSQLExp` varchar(200) DEFAULT NULL COMMENT '过滤数据SQL表达式',
  `FK_Node` int(11) DEFAULT NULL COMMENT '节点(用户独立表单权限控制)',
  `ShowCols` varchar(500) DEFAULT NULL COMMENT '显示的列',
  `IsExp` int(11) DEFAULT NULL COMMENT '是否可以导出？(导出到Excel,Txt,html类型文件.)',
  `ImpModel` int(11) DEFAULT NULL COMMENT '导入方式,枚举类型:0 不导入;1 按配置模式导入;2 按照xls文件模版导入;',
  `ImpSQLSearch` varchar(4000) DEFAULT NULL COMMENT '查询SQL(SQL里必须包含@Key关键字.)',
  `ImpSQLInit` varchar(4000) DEFAULT NULL COMMENT '初始化SQL(初始化表格的时候的SQL数据,可以为空)',
  `ImpSQLFullOneRow` varchar(4000) DEFAULT NULL COMMENT '数据填充一行数据的SQL(必须包含@Key关键字,为选择的主键)',
  `ImpSQLNames` varchar(900) DEFAULT NULL COMMENT '列的中文名称',
  `ColAutoExp` varchar(200) DEFAULT NULL COMMENT '列自动计算',
  `GUID` varchar(128) DEFAULT NULL COMMENT 'GUID',
  `AtPara` varchar(300) DEFAULT NULL COMMENT 'AtPara',
  `IsEnableLink` int(11) DEFAULT NULL COMMENT '是否启用超链接',
  `LinkLabel` varchar(50) DEFAULT NULL COMMENT '超连接标签',
  `LinkTarget` varchar(10) DEFAULT NULL COMMENT '连接目标',
  `LinkUrl` varchar(200) DEFAULT NULL COMMENT '连接URL',
  `SubThreadWorker` varchar(50) DEFAULT NULL COMMENT '子线程处理人字段',
  `SubThreadWorkerText` varchar(50) DEFAULT NULL COMMENT '子线程处理人字段',
  `ImpSQLFull` varchar(500) DEFAULT '',
  `PTableModel` int(11) DEFAULT '0',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='明细';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_mapdtl`
--

LOCK TABLES `sys_mapdtl` WRITE;
/*!40000 ALTER TABLE `sys_mapdtl` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_mapdtl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_mapext`
--

DROP TABLE IF EXISTS `sys_mapext`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_mapext` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT '主表',
  `ExtType` varchar(30) DEFAULT NULL COMMENT '类型',
  `DoWay` int(11) DEFAULT NULL COMMENT '执行方式',
  `AttrOfOper` varchar(30) DEFAULT NULL COMMENT '操作的Attr',
  `AttrsOfActive` varchar(900) DEFAULT NULL COMMENT '激活的字段',
  `Doc` text COMMENT '内容',
  `Tag` varchar(2000) DEFAULT NULL COMMENT 'Tag',
  `Tag1` varchar(2000) DEFAULT NULL COMMENT 'Tag1',
  `Tag2` varchar(2000) DEFAULT NULL COMMENT 'Tag2',
  `Tag3` varchar(2000) DEFAULT NULL COMMENT 'Tag3',
  `Tag4` varchar(2000) DEFAULT NULL COMMENT 'Tag4',
  `H` int(11) DEFAULT '500' COMMENT '高度',
  `W` int(11) DEFAULT '400' COMMENT '宽度',
  `DBType` int(11) DEFAULT NULL COMMENT '数据类型',
  `FK_DBSrc` varchar(100) DEFAULT NULL COMMENT '数据源',
  `PRI` int(11) DEFAULT '0' COMMENT 'PRI',
  `AtPara` text COMMENT '参数',
  `DBSrc` varchar(20) DEFAULT '',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='业务逻辑';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_mapext`
--

LOCK TABLES `sys_mapext` WRITE;
/*!40000 ALTER TABLE `sys_mapext` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_mapext` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_mapframe`
--

DROP TABLE IF EXISTS `sys_mapframe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_mapframe` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT '表单ID',
  `Name` varchar(200) DEFAULT NULL COMMENT '名称',
  `URL` varchar(3000) DEFAULT NULL COMMENT 'URL',
  `UrlSrcType` int(11) DEFAULT NULL COMMENT 'URL来源,枚举类型:0 自定义;1 表单库;',
  `FrmID` varchar(50) DEFAULT NULL COMMENT '表单表单',
  `FrmIDText` varchar(50) DEFAULT NULL COMMENT '表单表单',
  `Y` varchar(20) DEFAULT NULL COMMENT 'Y',
  `X` varchar(20) DEFAULT NULL COMMENT 'x',
  `W` varchar(20) DEFAULT NULL COMMENT '宽度',
  `H` varchar(20) DEFAULT NULL COMMENT '高度',
  `IsAutoSize` int(11) DEFAULT NULL COMMENT '是否自动设置大小',
  `EleType` varchar(50) DEFAULT NULL COMMENT '类型',
  `GUID` varchar(128) DEFAULT NULL COMMENT 'GUID',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='框架';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_mapframe`
--

LOCK TABLES `sys_mapframe` WRITE;
/*!40000 ALTER TABLE `sys_mapframe` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_mapframe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_mapm2m`
--

DROP TABLE IF EXISTS `sys_mapm2m`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_mapm2m` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT '主表',
  `NoOfObj` varchar(20) DEFAULT NULL COMMENT '编号',
  `Name` varchar(200) DEFAULT NULL COMMENT '名称',
  `DBOfLists` text COMMENT '列表数据源(对一对多对多模式有效）',
  `DBOfObjs` text COMMENT 'DBOfObjs',
  `DBOfGroups` text COMMENT 'DBOfGroups',
  `H` float DEFAULT NULL COMMENT 'H',
  `W` float DEFAULT NULL COMMENT 'W',
  `X` float DEFAULT NULL COMMENT 'X',
  `Y` float DEFAULT NULL COMMENT 'Y',
  `ShowWay` int(11) DEFAULT NULL COMMENT '显示方式',
  `M2MType` int(11) DEFAULT NULL COMMENT '类型',
  `RowIdx` int(11) DEFAULT NULL COMMENT '位置',
  `GroupID` int(11) DEFAULT NULL COMMENT '分组ID',
  `Cols` int(11) DEFAULT NULL COMMENT '记录呈现列数',
  `IsDelete` int(11) DEFAULT NULL COMMENT '可删除否',
  `IsInsert` int(11) DEFAULT NULL COMMENT '可插入否',
  `IsCheckAll` int(11) DEFAULT NULL COMMENT '是否显示选择全部',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='多选';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_mapm2m`
--

LOCK TABLES `sys_mapm2m` WRITE;
/*!40000 ALTER TABLE `sys_mapm2m` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_mapm2m` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_rptdept`
--

DROP TABLE IF EXISTS `sys_rptdept`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_rptdept` (
  `FK_Rpt` varchar(15) NOT NULL COMMENT '报表 - 主键',
  `FK_Dept` varchar(100) NOT NULL COMMENT '部门,主外键:对应物理表:Port_Dept,表描述:部门',
  PRIMARY KEY (`FK_Rpt`,`FK_Dept`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='报表部门对应信息';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_rptdept`
--

LOCK TABLES `sys_rptdept` WRITE;
/*!40000 ALTER TABLE `sys_rptdept` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_rptdept` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_rptemp`
--

DROP TABLE IF EXISTS `sys_rptemp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_rptemp` (
  `FK_Rpt` varchar(15) NOT NULL COMMENT '报表 - 主键',
  `FK_Emp` varchar(100) NOT NULL COMMENT '人员,主外键:对应物理表:Port_Emp,表描述:用户',
  PRIMARY KEY (`FK_Rpt`,`FK_Emp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='报表人员对应信息';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_rptemp`
--

LOCK TABLES `sys_rptemp` WRITE;
/*!40000 ALTER TABLE `sys_rptemp` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_rptemp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_rptstation`
--

DROP TABLE IF EXISTS `sys_rptstation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_rptstation` (
  `FK_Rpt` varchar(15) NOT NULL COMMENT '报表 - 主键',
  `FK_Station` varchar(100) NOT NULL COMMENT '岗位,主外键:对应物理表:Port_Station,表描述:岗位',
  PRIMARY KEY (`FK_Rpt`,`FK_Station`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='报表岗位对应信息';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_rptstation`
--

LOCK TABLES `sys_rptstation` WRITE;
/*!40000 ALTER TABLE `sys_rptstation` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_rptstation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_rpttemplate`
--

DROP TABLE IF EXISTS `sys_rpttemplate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_rpttemplate` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `EnsName` varchar(500) DEFAULT NULL COMMENT '类名',
  `FK_Emp` varchar(20) DEFAULT NULL COMMENT '操作员',
  `D1` varchar(90) DEFAULT NULL COMMENT 'D1',
  `D2` varchar(90) DEFAULT NULL COMMENT 'D2',
  `D3` varchar(90) DEFAULT NULL COMMENT 'D3',
  `AlObjs` varchar(90) DEFAULT NULL COMMENT '要分析的对象',
  `Height` int(11) DEFAULT NULL COMMENT 'Height',
  `Width` int(11) DEFAULT NULL COMMENT 'Width',
  `IsSumBig` int(11) DEFAULT NULL COMMENT '是否显示大合计',
  `IsSumLittle` int(11) DEFAULT NULL COMMENT '是否显示小合计',
  `IsSumRight` int(11) DEFAULT NULL COMMENT '是否显示右合计',
  `PercentModel` int(11) DEFAULT NULL COMMENT '比率显示方式',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='报表模板';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_rpttemplate`
--

LOCK TABLES `sys_rpttemplate` WRITE;
/*!40000 ALTER TABLE `sys_rpttemplate` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_rpttemplate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_serial`
--

DROP TABLE IF EXISTS `sys_serial`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_serial` (
  `CfgKey` varchar(100) NOT NULL COMMENT 'CfgKey - 主键',
  `IntVal` int(11) DEFAULT NULL COMMENT '属性',
  PRIMARY KEY (`CfgKey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='序列号';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_serial`
--

LOCK TABLES `sys_serial` WRITE;
/*!40000 ALTER TABLE `sys_serial` DISABLE KEYS */;
INSERT INTO `sys_serial` VALUES ('BP.WF.Template.FlowSort',106),('Demo_Resume',279),('OID',1214),('UpdataCCFlowVer',1214175317),('Ver',20180614);
/*!40000 ALTER TABLE `sys_serial` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_sfdbsrc`
--

DROP TABLE IF EXISTS `sys_sfdbsrc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_sfdbsrc` (
  `No` varchar(20) NOT NULL COMMENT '数据源编号(必须是英文) - 主键',
  `Name` varchar(30) DEFAULT NULL COMMENT '数据源名称',
  `DBSrcType` int(11) DEFAULT NULL COMMENT '数据源类型,枚举类型:0 应用系统主数据库(默认);1 SQLServer数据库;2 Oracle数据库;3 MySQL数据库;4 Informix数据库;50 Dubbo服务;100 WebService数据源;',
  `UserID` varchar(30) DEFAULT NULL COMMENT '数据库登录用户ID',
  `Password` varchar(30) DEFAULT NULL COMMENT '数据库登录用户密码',
  `IP` varchar(500) DEFAULT NULL COMMENT 'IP地址/数据库实例名',
  `DBName` varchar(30) DEFAULT NULL COMMENT '数据库名称/Oracle保持为空',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='数据源';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_sfdbsrc`
--

LOCK TABLES `sys_sfdbsrc` WRITE;
/*!40000 ALTER TABLE `sys_sfdbsrc` DISABLE KEYS */;
INSERT INTO `sys_sfdbsrc` VALUES ('local','本机数据源(默认)',0,'','','','');
/*!40000 ALTER TABLE `sys_sfdbsrc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_sftable`
--

DROP TABLE IF EXISTS `sys_sftable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_sftable` (
  `No` varchar(200) NOT NULL COMMENT '表英文名称 - 主键',
  `Name` varchar(200) DEFAULT NULL COMMENT '表中文名称',
  `SrcType` int(11) DEFAULT NULL COMMENT '数据表类型,枚举类型:0 本地的类;1 创建表;2 表或视图;3 SQL查询表;4 WebServices;5 微服务Handler外部数据源;6 JavaScript外部数据源;',
  `CodeStruct` int(11) DEFAULT NULL COMMENT '字典表类型,枚举类型:0 普通的编码表(具有No,Name);1 树结构(具有No,Name,ParentNo);2 行政机构编码表(编码以两位编号标识级次树形关系);',
  `RootVal` varchar(200) DEFAULT NULL COMMENT '根目录值',
  `FK_Val` varchar(200) DEFAULT NULL COMMENT '默认创建的字段名',
  `TableDesc` varchar(200) DEFAULT NULL COMMENT '表描述',
  `DefVal` varchar(200) DEFAULT NULL COMMENT '默认值',
  `FK_SFDBSrc` varchar(100) DEFAULT NULL COMMENT '数据源,外键:对应物理表:Sys_SFDBSrc,表描述:数据源',
  `SrcTable` varchar(200) DEFAULT NULL COMMENT '数据源表',
  `ColumnValue` varchar(200) DEFAULT NULL COMMENT '显示的值(编号列)',
  `ColumnText` varchar(200) DEFAULT NULL COMMENT '显示的文字(名称列)',
  `ParentValue` varchar(200) DEFAULT NULL COMMENT '父级值(父级列)',
  `SelectStatement` varchar(1000) DEFAULT NULL COMMENT '查询语句',
  `RDT` varchar(50) DEFAULT NULL COMMENT '加入日期',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='字典表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_sftable`
--

LOCK TABLES `sys_sftable` WRITE;
/*!40000 ALTER TABLE `sys_sftable` DISABLE KEYS */;
INSERT INTO `sys_sftable` VALUES ('BP.Port.Depts','部门',0,0,NULL,'FK_Dept','部门','','local',NULL,NULL,NULL,NULL,NULL,NULL),('BP.Port.Emps','人员',0,0,NULL,'FK_Emp','系统中的操作员','','local',NULL,NULL,NULL,NULL,NULL,NULL),('BP.Port.Stations','岗位',0,0,NULL,'FK_Station','工作岗位','','local',NULL,NULL,NULL,NULL,NULL,NULL),('BP.Pub.Days','日',0,0,NULL,'FK_Day','1-31日','','local',NULL,NULL,NULL,NULL,NULL,NULL),('BP.Pub.NYs','年月',0,0,NULL,'FK_NY','年度与月份','','local',NULL,NULL,NULL,NULL,NULL,NULL),('BP.Pub.YFs','月',0,0,NULL,'FK_YF','1-12月','','local',NULL,NULL,NULL,NULL,NULL,NULL),('CN_City','城市',1,0,NULL,'FK_City','中国的市级城市','','local',NULL,NULL,NULL,NULL,NULL,NULL),('CN_PQ','地区',1,0,NULL,'FK_DQ','华北、西北、西南。。。','','local',NULL,NULL,NULL,NULL,NULL,NULL),('CN_SF','省份',1,0,NULL,'FK_SF','中国的省份。','','local',NULL,NULL,NULL,NULL,NULL,NULL),('Demo_BanJi','班级',1,0,NULL,'FK_BJ','班级','','local',NULL,NULL,NULL,NULL,NULL,NULL),('Demo_Student','学生',1,0,NULL,'FK_Student','学生','','local',NULL,NULL,NULL,NULL,NULL,NULL),('Port_Dept','部门',1,0,NULL,'FK_Dept','部门','','local',NULL,NULL,NULL,NULL,NULL,NULL),('Port_Emp','操作员',1,0,NULL,'FK_Emp','操作员','','local',NULL,NULL,NULL,NULL,NULL,NULL),('Port_Station','岗位',1,0,NULL,'FK_Station','岗位','','local',NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `sys_sftable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_sms`
--

DROP TABLE IF EXISTS `sys_sms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_sms` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `Sender` varchar(200) DEFAULT NULL COMMENT '发送人(可以为空)',
  `SendTo` varchar(200) DEFAULT NULL COMMENT '发送给(可以为空)',
  `RDT` varchar(50) DEFAULT NULL COMMENT '写入时间',
  `Mobile` varchar(30) DEFAULT NULL COMMENT '手机号(可以为空)',
  `MobileSta` int(11) DEFAULT NULL COMMENT '消息状态',
  `MobileInfo` varchar(1000) DEFAULT NULL COMMENT '短信信息',
  `Email` varchar(200) DEFAULT NULL COMMENT 'Email(可以为空)',
  `EmailSta` int(11) DEFAULT NULL COMMENT 'EmaiSta消息状态',
  `EmailTitle` varchar(3000) DEFAULT NULL COMMENT '标题',
  `EmailDoc` text COMMENT '内容',
  `SendDT` varchar(50) DEFAULT NULL COMMENT '发送时间',
  `IsRead` int(11) DEFAULT NULL COMMENT '是否读取?',
  `IsAlert` int(11) DEFAULT NULL COMMENT '是否提示?',
  `MsgFlag` varchar(200) DEFAULT NULL COMMENT '消息标记(用于防止发送重复)',
  `MsgType` varchar(200) DEFAULT NULL COMMENT '消息类型(CC抄送,Todolist待办,Return退回,Etc其他消息...)',
  `AtPara` varchar(500) DEFAULT NULL COMMENT 'AtPara',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='消息';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_sms`
--

LOCK TABLES `sys_sms` WRITE;
/*!40000 ALTER TABLE `sys_sms` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_sms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_userlogt`
--

DROP TABLE IF EXISTS `sys_userlogt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_userlogt` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `FK_Emp` varchar(30) DEFAULT NULL COMMENT '用户',
  `IP` varchar(200) DEFAULT NULL COMMENT 'IP',
  `LogFlag` varchar(300) DEFAULT NULL COMMENT 'Flag',
  `Docs` varchar(300) DEFAULT NULL COMMENT 'Docs',
  `RDT` varchar(20) DEFAULT NULL COMMENT '记录日期',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户日志';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_userlogt`
--

LOCK TABLES `sys_userlogt` WRITE;
/*!40000 ALTER TABLE `sys_userlogt` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_userlogt` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_userregedit`
--

DROP TABLE IF EXISTS `sys_userregedit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_userregedit` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `EnsName` varchar(100) DEFAULT NULL COMMENT '实体类名称',
  `FK_Emp` varchar(100) DEFAULT NULL COMMENT '用户',
  `Attrs` text COMMENT '属性s',
  `CfgKey` varchar(200) DEFAULT NULL COMMENT '键',
  `Vals` varchar(2000) DEFAULT NULL COMMENT '值',
  `GenerSQL` varchar(2000) DEFAULT NULL COMMENT 'GenerSQL',
  `Paras` varchar(2000) DEFAULT NULL COMMENT 'Paras',
  `NumKey` varchar(300) DEFAULT NULL COMMENT '分析的Key',
  `OrderBy` varchar(300) DEFAULT NULL COMMENT 'OrderBy',
  `OrderWay` varchar(300) DEFAULT NULL COMMENT 'OrderWay',
  `SearchKey` varchar(300) DEFAULT NULL COMMENT 'SearchKey',
  `MVals` varchar(2000) DEFAULT NULL COMMENT 'MVals',
  `IsPic` int(11) DEFAULT NULL COMMENT '是否图片',
  `DTFrom` varchar(20) DEFAULT NULL COMMENT '查询时间从',
  `DTTo` varchar(20) DEFAULT NULL COMMENT '到',
  `AtPara` text COMMENT 'AtPara',
  `FK_MapData` varchar(100) DEFAULT '' COMMENT '实体',
  `AttrKey` varchar(50) DEFAULT '' COMMENT '节点对应字段',
  `LB` int(11) DEFAULT '0' COMMENT '类别',
  `CurValue` text COMMENT '文本',
  `ContrastKey` varchar(20) DEFAULT '' COMMENT '对比项目',
  `KeyVal1` varchar(20) DEFAULT '' COMMENT 'KeyVal1',
  `KeyVal2` varchar(20) DEFAULT '' COMMENT 'KeyVal2',
  `SortBy` varchar(20) DEFAULT '' COMMENT 'SortBy',
  `KeyOfNum` varchar(20) DEFAULT '' COMMENT 'KeyOfNum',
  `GroupWay` int(11) DEFAULT '1' COMMENT '求什么?SumAvg',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户注册表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_userregedit`
--

LOCK TABLES `sys_userregedit` WRITE;
/*!40000 ALTER TABLE `sys_userregedit` DISABLE KEYS */;
INSERT INTO `sys_userregedit` VALUES ('admin_BP.WF.Data.MyStartFlows_SearchAttrs',NULL,'',NULL,'','','','','','','','','',0,'','','@RecCount=0','','',0,NULL,'','','','','',1);
/*!40000 ALTER TABLE `sys_userregedit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_wfsealdata`
--

DROP TABLE IF EXISTS `sys_wfsealdata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_wfsealdata` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `OID` varchar(200) DEFAULT NULL COMMENT 'OID',
  `FK_Node` varchar(200) DEFAULT NULL COMMENT 'FK_Node',
  `FK_MapData` varchar(300) DEFAULT NULL COMMENT 'FK_MapData',
  `SealData` text COMMENT 'SealData',
  `RDT` varchar(20) DEFAULT NULL COMMENT '记录日期',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='签名信息';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_wfsealdata`
--

LOCK TABLES `sys_wfsealdata` WRITE;
/*!40000 ALTER TABLE `sys_wfsealdata` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_wfsealdata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `test_data`
--

DROP TABLE IF EXISTS `test_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `test_data` (
  `id` varchar(64) NOT NULL COMMENT '编号',
  `test_input` varchar(200) DEFAULT NULL COMMENT '单行文本',
  `test_textarea` varchar(200) DEFAULT NULL COMMENT '多行文本',
  `test_select` varchar(10) DEFAULT NULL COMMENT '下拉框',
  `test_select_multiple` varchar(200) DEFAULT NULL COMMENT '下拉多选',
  `test_radio` varchar(10) DEFAULT NULL COMMENT '单选框',
  `test_checkbox` varchar(200) DEFAULT NULL COMMENT '复选框',
  `test_date` datetime DEFAULT NULL COMMENT '日期选择',
  `test_datetime` datetime DEFAULT NULL COMMENT '日期时间',
  `test_user_code` varchar(64) DEFAULT NULL COMMENT '用户选择',
  `test_office_code` varchar(64) DEFAULT NULL COMMENT '机构选择',
  `test_area_code` varchar(64) DEFAULT NULL COMMENT '区域选择',
  `test_area_name` varchar(100) DEFAULT NULL COMMENT '区域名称',
  `status` char(1) NOT NULL DEFAULT '0' COMMENT '状态（0正常 1删除 2停用）',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) DEFAULT NULL COMMENT '备注信息',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='测试数据';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `test_data`
--

LOCK TABLES `test_data` WRITE;
/*!40000 ALTER TABLE `test_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `test_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `test_data_child`
--

DROP TABLE IF EXISTS `test_data_child`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `test_data_child` (
  `id` varchar(64) NOT NULL COMMENT '编号',
  `test_sort` int(11) DEFAULT NULL COMMENT '排序号',
  `test_data_id` varchar(64) DEFAULT NULL COMMENT '父表主键',
  `test_input` varchar(200) DEFAULT NULL COMMENT '单行文本',
  `test_textarea` varchar(200) DEFAULT NULL COMMENT '多行文本',
  `test_select` varchar(10) DEFAULT NULL COMMENT '下拉框',
  `test_select_multiple` varchar(200) DEFAULT NULL COMMENT '下拉多选',
  `test_radio` varchar(10) DEFAULT NULL COMMENT '单选框',
  `test_checkbox` varchar(200) DEFAULT NULL COMMENT '复选框',
  `test_date` datetime DEFAULT NULL COMMENT '日期选择',
  `test_datetime` datetime DEFAULT NULL COMMENT '日期时间',
  `test_user_code` varchar(64) DEFAULT NULL COMMENT '用户选择',
  `test_office_code` varchar(64) DEFAULT NULL COMMENT '机构选择',
  `test_area_code` varchar(64) DEFAULT NULL COMMENT '区域选择',
  `test_area_name` varchar(100) DEFAULT NULL COMMENT '区域名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='测试数据子表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `test_data_child`
--

LOCK TABLES `test_data_child` WRITE;
/*!40000 ALTER TABLE `test_data_child` DISABLE KEYS */;
/*!40000 ALTER TABLE `test_data_child` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `test_tree`
--

DROP TABLE IF EXISTS `test_tree`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `test_tree` (
  `tree_code` varchar(64) NOT NULL COMMENT '节点编码',
  `parent_code` varchar(64) NOT NULL COMMENT '父级编号',
  `parent_codes` varchar(1000) NOT NULL COMMENT '所有父级编号',
  `tree_sort` decimal(10,0) NOT NULL COMMENT '本级排序号（升序）',
  `tree_sorts` varchar(1000) NOT NULL COMMENT '所有级别排序号',
  `tree_leaf` char(1) NOT NULL COMMENT '是否最末级',
  `tree_level` decimal(4,0) NOT NULL COMMENT '层次级别',
  `tree_names` varchar(1000) NOT NULL COMMENT '全节点名',
  `tree_name` varchar(200) NOT NULL COMMENT '节点名称',
  `status` char(1) NOT NULL DEFAULT '0' COMMENT '状态（0正常 1删除 2停用）',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) DEFAULT NULL COMMENT '备注信息',
  PRIMARY KEY (`tree_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='测试树表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `test_tree`
--

LOCK TABLES `test_tree` WRITE;
/*!40000 ALTER TABLE `test_tree` DISABLE KEYS */;
/*!40000 ALTER TABLE `test_tree` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ty_ywdy`
--

DROP TABLE IF EXISTS `ty_ywdy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ty_ywdy` (
  `No` varchar(4) NOT NULL COMMENT '功能编号',
  `Name` varchar(300) DEFAULT NULL COMMENT '名称',
  `ParentNo` varchar(300) DEFAULT NULL COMMENT '名称',
  `Idx` int(11) DEFAULT '0' COMMENT '顺序号',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ty_ywdy`
--

LOCK TABLES `ty_ywdy` WRITE;
/*!40000 ALTER TABLE `ty_ywdy` DISABLE KEYS */;
/*!40000 ALTER TABLE `ty_ywdy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `v_flowstarter`
--

DROP TABLE IF EXISTS `v_flowstarter`;
/*!50001 DROP VIEW IF EXISTS `v_flowstarter`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_flowstarter` AS SELECT 
 1 AS `FK_Flow`,
 1 AS `FlowName`,
 1 AS `FK_Emp`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `v_flowstarterbpm`
--

DROP TABLE IF EXISTS `v_flowstarterbpm`;
/*!50001 DROP VIEW IF EXISTS `v_flowstarterbpm`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_flowstarterbpm` AS SELECT 
 1 AS `FK_Flow`,
 1 AS `FlowName`,
 1 AS `FK_Emp`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `v_todolist`
--

DROP TABLE IF EXISTS `v_todolist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `v_todolist` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `FK_FlowSort` varchar(100) DEFAULT NULL COMMENT '类别,外键:对应物理表:WF_FlowSort,表描述:流程类别',
  `FK_Flow` varchar(100) DEFAULT NULL COMMENT '流程,外键:对应物理表:WF_Flow,表描述:流程',
  `TodoSta0` int(11) DEFAULT NULL COMMENT '待办中',
  `TodoSta1` int(11) DEFAULT NULL COMMENT '预警中',
  `TodoSta2` int(11) DEFAULT NULL COMMENT '逾期中',
  `TodoSta3` int(11) DEFAULT NULL COMMENT '正常办结',
  `TodoSta4` int(11) DEFAULT NULL COMMENT '超期办结',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='流程统计';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `v_todolist`
--

LOCK TABLES `v_todolist` WRITE;
/*!40000 ALTER TABLE `v_todolist` DISABLE KEYS */;
/*!40000 ALTER TABLE `v_todolist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `v_totalch`
--

DROP TABLE IF EXISTS `v_totalch`;
/*!50001 DROP VIEW IF EXISTS `v_totalch`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_totalch` AS SELECT 
 1 AS `FK_Emp`,
 1 AS `AllNum`,
 1 AS `ASNum`,
 1 AS `CSNum`,
 1 AS `JiShi`,
 1 AS `ANQI`,
 1 AS `YuQi`,
 1 AS `ChaoQi`,
 1 AS `WCRate`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `v_totalchweek`
--

DROP TABLE IF EXISTS `v_totalchweek`;
/*!50001 DROP VIEW IF EXISTS `v_totalchweek`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_totalchweek` AS SELECT 
 1 AS `FK_Emp`,
 1 AS `WeekNum`,
 1 AS `FK_NY`,
 1 AS `AllNum`,
 1 AS `ASNum`,
 1 AS `CSNum`,
 1 AS `JiShi`,
 1 AS `AnQi`,
 1 AS `YuQi`,
 1 AS `ChaoQi`,
 1 AS `WCRate`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `v_totalchyf`
--

DROP TABLE IF EXISTS `v_totalchyf`;
/*!50001 DROP VIEW IF EXISTS `v_totalchyf`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_totalchyf` AS SELECT 
 1 AS `FK_Emp`,
 1 AS `FK_NY`,
 1 AS `AllNum`,
 1 AS `ASNum`,
 1 AS `CSNum`,
 1 AS `JiShi`,
 1 AS `AnQi`,
 1 AS `YuQi`,
 1 AS `ChaoQi`,
 1 AS `WCRate`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `wf_accepterrole`
--

DROP TABLE IF EXISTS `wf_accepterrole`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wf_accepterrole` (
  `OID` int(11) NOT NULL COMMENT 'OID - 主键',
  `Name` varchar(200) DEFAULT NULL COMMENT 'null',
  `FK_Node` varchar(100) DEFAULT NULL COMMENT '节点',
  `FK_Mode` int(11) DEFAULT NULL COMMENT '模式类型',
  `Tag0` varchar(999) DEFAULT NULL COMMENT 'Tag0',
  `Tag1` varchar(999) DEFAULT NULL COMMENT 'Tag1',
  `Tag2` varchar(999) DEFAULT NULL COMMENT 'Tag2',
  `Tag3` varchar(999) DEFAULT NULL COMMENT 'Tag3',
  `Tag4` varchar(999) DEFAULT NULL COMMENT 'Tag4',
  `Tag5` varchar(999) DEFAULT NULL COMMENT 'Tag5',
  PRIMARY KEY (`OID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='接受人规则';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_accepterrole`
--

LOCK TABLES `wf_accepterrole` WRITE;
/*!40000 ALTER TABLE `wf_accepterrole` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_accepterrole` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_bill`
--

DROP TABLE IF EXISTS `wf_bill`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wf_bill` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `WorkID` int(11) DEFAULT NULL COMMENT '工作ID',
  `FID` int(11) DEFAULT NULL COMMENT 'FID',
  `FK_Flow` varchar(4) DEFAULT NULL COMMENT '流程',
  `FK_BillType` varchar(300) DEFAULT NULL COMMENT '单据类型',
  `Title` varchar(900) DEFAULT NULL COMMENT '标题',
  `FK_Starter` varchar(50) DEFAULT NULL COMMENT '发起人',
  `StartDT` varchar(50) DEFAULT NULL COMMENT '发起时间',
  `Url` varchar(2000) DEFAULT NULL COMMENT 'Url',
  `FullPath` varchar(2000) DEFAULT NULL COMMENT 'FullPath',
  `FK_Emp` varchar(100) DEFAULT NULL COMMENT '打印人,外键:对应物理表:Port_Emp,表描述:用户',
  `RDT` varchar(50) DEFAULT NULL COMMENT '打印时间',
  `FK_Dept` varchar(100) DEFAULT NULL COMMENT '隶属部门,外键:对应物理表:Port_Dept,表描述:部门',
  `FK_NY` varchar(100) DEFAULT NULL COMMENT '隶属年月,外键:对应物理表:Pub_NY,表描述:年月',
  `Emps` text COMMENT 'Emps',
  `FK_Node` varchar(30) DEFAULT NULL COMMENT '节点',
  `FK_Bill` varchar(500) DEFAULT NULL COMMENT 'FK_Bill',
  `MyNum` int(11) DEFAULT NULL COMMENT '个数',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='单据';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_bill`
--

LOCK TABLES `wf_bill` WRITE;
/*!40000 ALTER TABLE `wf_bill` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_bill` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_billtemplate`
--

DROP TABLE IF EXISTS `wf_billtemplate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wf_billtemplate` (
  `No` varchar(190) NOT NULL COMMENT 'No - 主键',
  `Name` varchar(200) DEFAULT NULL COMMENT 'Name',
  `TempFilePath` varchar(200) DEFAULT NULL COMMENT '模板路径',
  `NodeID` int(11) DEFAULT NULL COMMENT 'NodeID',
  `BillFileType` int(11) DEFAULT NULL COMMENT '生成的文件类型,枚举类型:0 Word;1 PDF;2 Excel(未完成);3 Html(未完成);5 锐浪报表;',
  `BillOpenModel` int(11) DEFAULT NULL COMMENT '生成的文件打开方式,枚举类型:0 下载本地;1 在线WebOffice打开;',
  `QRModel` int(11) DEFAULT NULL COMMENT '二维码生成方式,枚举类型:0 不生成;1 生成二维码;',
  `FK_BillType` varchar(4) DEFAULT NULL COMMENT '单据类型',
  `IDX` varchar(200) DEFAULT NULL COMMENT 'IDX',
  `ExpField` varchar(800) DEFAULT NULL COMMENT '要排除的字段',
  `ReplaceVal` varchar(3000) DEFAULT NULL COMMENT '要替换的值',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='单据模板';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_billtemplate`
--

LOCK TABLES `wf_billtemplate` WRITE;
/*!40000 ALTER TABLE `wf_billtemplate` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_billtemplate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_billtype`
--

DROP TABLE IF EXISTS `wf_billtype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wf_billtype` (
  `No` varchar(2) NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(50) DEFAULT NULL COMMENT '名称',
  `FK_Flow` varchar(50) DEFAULT NULL COMMENT '流程',
  `IDX` int(11) DEFAULT NULL COMMENT 'IDX',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='单据类型';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_billtype`
--

LOCK TABLES `wf_billtype` WRITE;
/*!40000 ALTER TABLE `wf_billtype` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_billtype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_ccdept`
--

DROP TABLE IF EXISTS `wf_ccdept`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wf_ccdept` (
  `FK_Node` int(11) NOT NULL COMMENT '节点,主外键:对应物理表:WF_Node,表描述:节点',
  `FK_Dept` varchar(100) NOT NULL COMMENT '部门,主外键:对应物理表:Port_Dept,表描述:部门',
  PRIMARY KEY (`FK_Node`,`FK_Dept`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='抄送部门';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_ccdept`
--

LOCK TABLES `wf_ccdept` WRITE;
/*!40000 ALTER TABLE `wf_ccdept` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_ccdept` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_ccemp`
--

DROP TABLE IF EXISTS `wf_ccemp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wf_ccemp` (
  `FK_Node` int(11) NOT NULL COMMENT '节点 - 主键',
  `FK_Emp` varchar(100) NOT NULL COMMENT '人员,主外键:对应物理表:Port_Emp,表描述:用户',
  PRIMARY KEY (`FK_Node`,`FK_Emp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='抄送人员';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_ccemp`
--

LOCK TABLES `wf_ccemp` WRITE;
/*!40000 ALTER TABLE `wf_ccemp` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_ccemp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_cclist`
--

DROP TABLE IF EXISTS `wf_cclist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wf_cclist` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `Title` varchar(500) DEFAULT NULL COMMENT '标题',
  `Sta` int(11) DEFAULT NULL COMMENT '状态',
  `FK_Flow` varchar(3) DEFAULT NULL COMMENT '流程编号',
  `FlowName` varchar(200) DEFAULT NULL COMMENT '流程名称',
  `FK_Node` int(11) DEFAULT NULL COMMENT '节点',
  `NodeName` varchar(500) DEFAULT NULL COMMENT '节点名称',
  `WorkID` int(11) DEFAULT NULL COMMENT '工作ID',
  `FID` int(11) DEFAULT NULL COMMENT 'FID',
  `Doc` text COMMENT '内容',
  `Rec` varchar(50) DEFAULT NULL COMMENT '抄送人员',
  `RDT` varchar(50) DEFAULT NULL COMMENT '记录日期',
  `CCTo` varchar(50) DEFAULT NULL COMMENT '抄送给',
  `CCToName` varchar(50) DEFAULT NULL COMMENT '抄送给(人员名称)',
  `CCToDept` varchar(50) DEFAULT NULL COMMENT '抄送到部门',
  `CCToDeptName` varchar(600) DEFAULT NULL COMMENT '抄送给部门名称',
  `CDT` varchar(50) DEFAULT NULL COMMENT '打开时间',
  `PFlowNo` varchar(100) DEFAULT NULL COMMENT '父流程编号',
  `PWorkID` int(11) DEFAULT NULL COMMENT '父流程WorkID',
  `InEmpWorks` int(11) DEFAULT NULL COMMENT '是否加入待办列表',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='抄送列表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_cclist`
--

LOCK TABLES `wf_cclist` WRITE;
/*!40000 ALTER TABLE `wf_cclist` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_cclist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_ccstation`
--

DROP TABLE IF EXISTS `wf_ccstation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wf_ccstation` (
  `FK_Node` int(11) NOT NULL COMMENT '节点,主外键:对应物理表:WF_Node,表描述:节点',
  `FK_Station` varchar(100) NOT NULL COMMENT '工作岗位,主外键:对应物理表:Port_Station,表描述:岗位',
  PRIMARY KEY (`FK_Node`,`FK_Station`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='抄送岗位';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_ccstation`
--

LOCK TABLES `wf_ccstation` WRITE;
/*!40000 ALTER TABLE `wf_ccstation` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_ccstation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_ch`
--

DROP TABLE IF EXISTS `wf_ch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wf_ch` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `WorkID` int(11) DEFAULT NULL COMMENT '工作ID',
  `FID` int(11) DEFAULT NULL COMMENT 'FID',
  `Title` varchar(900) DEFAULT NULL COMMENT '标题',
  `FK_Flow` varchar(100) DEFAULT NULL COMMENT '流程',
  `FK_FlowT` varchar(200) DEFAULT NULL COMMENT '流程名称',
  `FK_Node` int(11) DEFAULT NULL COMMENT '节点',
  `FK_NodeT` varchar(200) DEFAULT NULL COMMENT '节点名称',
  `Sender` varchar(200) DEFAULT NULL COMMENT '发送人',
  `SenderT` varchar(200) DEFAULT NULL COMMENT '发送人名称',
  `FK_Emp` varchar(100) DEFAULT NULL COMMENT '当事人',
  `FK_EmpT` varchar(200) DEFAULT NULL COMMENT '当事人名称',
  `GroupEmps` varchar(400) DEFAULT NULL COMMENT '相关当事人',
  `GroupEmpsNames` varchar(900) DEFAULT NULL COMMENT '相关当事人名称',
  `GroupEmpsNum` int(11) DEFAULT NULL COMMENT '相关当事人数量',
  `DTFrom` varchar(50) DEFAULT NULL COMMENT '任务下达时间',
  `DTTo` varchar(50) DEFAULT NULL COMMENT '任务处理时间',
  `SDT` varchar(50) DEFAULT NULL COMMENT '应完成日期',
  `FK_Dept` varchar(100) DEFAULT NULL COMMENT '隶属部门',
  `FK_DeptT` varchar(500) DEFAULT NULL COMMENT '部门名称',
  `FK_NY` varchar(100) DEFAULT NULL COMMENT '隶属月份',
  `DTSWay` int(11) DEFAULT NULL COMMENT '考核方式,枚举类型:0 不考核;1 按照时效考核;2 按照工作量考核;',
  `TimeLimit` varchar(50) DEFAULT NULL COMMENT '规定限期',
  `OverMinutes` float DEFAULT NULL COMMENT '逾期分钟',
  `UseDays` float DEFAULT NULL COMMENT '实际使用天',
  `OverDays` float DEFAULT NULL COMMENT '逾期天',
  `CHSta` int(11) DEFAULT NULL COMMENT '状态',
  `WeekNum` int(11) DEFAULT NULL COMMENT '第几周',
  `Points` float DEFAULT NULL COMMENT '总扣分',
  `MyNum` int(11) DEFAULT NULL COMMENT '个数',
  `UseMinutes` float(50,2) DEFAULT NULL COMMENT '实际使用分钟',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='时效考核';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_ch`
--

LOCK TABLES `wf_ch` WRITE;
/*!40000 ALTER TABLE `wf_ch` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_ch` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_cheval`
--

DROP TABLE IF EXISTS `wf_cheval`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wf_cheval` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `Title` varchar(500) DEFAULT NULL COMMENT '标题',
  `FK_Flow` varchar(7) DEFAULT NULL COMMENT '流程编号',
  `FlowName` varchar(100) DEFAULT NULL COMMENT '流程名称',
  `WorkID` int(11) DEFAULT NULL COMMENT '工作ID',
  `FK_Node` int(11) DEFAULT NULL COMMENT '评价节点',
  `NodeName` varchar(100) DEFAULT NULL COMMENT '节点名称',
  `Rec` varchar(50) DEFAULT NULL COMMENT '评价人',
  `RecName` varchar(50) DEFAULT NULL COMMENT '评价人名称',
  `RDT` varchar(50) DEFAULT NULL COMMENT '评价日期',
  `EvalEmpNo` varchar(50) DEFAULT NULL COMMENT '被考核的人员编号',
  `EvalEmpName` varchar(50) DEFAULT NULL COMMENT '被考核的人员名称',
  `EvalCent` varchar(20) DEFAULT NULL COMMENT '评价分值',
  `EvalNote` varchar(20) DEFAULT NULL COMMENT '评价内容',
  `FK_Dept` varchar(50) DEFAULT NULL COMMENT '部门',
  `DeptName` varchar(100) DEFAULT NULL COMMENT '部门名称',
  `FK_NY` varchar(7) DEFAULT NULL COMMENT '年月',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='工作质量评价';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_cheval`
--

LOCK TABLES `wf_cheval` WRITE;
/*!40000 ALTER TABLE `wf_cheval` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_cheval` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_cond`
--

DROP TABLE IF EXISTS `wf_cond`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wf_cond` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `CondType` int(11) DEFAULT NULL COMMENT '条件类型',
  `DataFrom` int(11) DEFAULT NULL COMMENT '条件数据来源0表单,1岗位(对方向条件有效)',
  `FK_Flow` varchar(60) DEFAULT NULL COMMENT '流程',
  `NodeID` int(11) DEFAULT NULL COMMENT '发生的事件MainNode',
  `FK_Node` int(11) DEFAULT NULL COMMENT '节点ID',
  `FK_Attr` varchar(80) DEFAULT NULL COMMENT '属性',
  `AttrKey` varchar(60) DEFAULT NULL COMMENT '属性键',
  `AttrName` varchar(500) DEFAULT NULL COMMENT '中文名称',
  `FK_Operator` varchar(60) DEFAULT NULL COMMENT '运算符号',
  `OperatorValue` text COMMENT '要运算的值',
  `OperatorValueT` text COMMENT '要运算的值T',
  `ToNodeID` int(11) DEFAULT NULL COMMENT 'ToNodeID（对方向条件有效）',
  `ConnJudgeWay` int(11) DEFAULT NULL COMMENT '条件关系,枚举类型:0 or;1 and;',
  `MyPOID` int(11) DEFAULT NULL COMMENT 'MyPOID',
  `PRI` int(11) DEFAULT NULL COMMENT '计算优先级',
  `CondOrAnd` int(11) DEFAULT NULL COMMENT '方向条件类型',
  `Note` varchar(500) DEFAULT NULL COMMENT '备注',
  `AtPara` varchar(2000) DEFAULT NULL COMMENT 'AtPara',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='流程条件';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_cond`
--

LOCK TABLES `wf_cond` WRITE;
/*!40000 ALTER TABLE `wf_cond` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_cond` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_dataapply`
--

DROP TABLE IF EXISTS `wf_dataapply`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wf_dataapply` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `WorkID` int(11) DEFAULT NULL COMMENT '工作ID',
  `NodeId` int(11) DEFAULT NULL COMMENT 'NodeId',
  `RunState` int(11) DEFAULT NULL COMMENT '运行状态0,没有提交，1，提交申请执行审批中，2，审核完毕。',
  `ApplyDays` int(11) DEFAULT NULL COMMENT '申请天数',
  `ApplyData` varchar(50) DEFAULT NULL COMMENT '申请日期',
  `Applyer` varchar(100) DEFAULT NULL COMMENT '申请人,外键:对应物理表:Port_Emp,表描述:用户',
  `ApplyNote1` text COMMENT '申请原因',
  `ApplyNote2` text COMMENT '申请备注',
  `Checker` varchar(100) DEFAULT NULL COMMENT '审批人,外键:对应物理表:Port_Emp,表描述:用户',
  `CheckerData` varchar(50) DEFAULT NULL COMMENT '审批日期',
  `CheckerDays` int(11) DEFAULT NULL COMMENT '批准天数',
  `CheckerNote1` text COMMENT '审批意见',
  `CheckerNote2` text COMMENT '审批备注',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='追加时间申请';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_dataapply`
--

LOCK TABLES `wf_dataapply` WRITE;
/*!40000 ALTER TABLE `wf_dataapply` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_dataapply` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_deptflowsearch`
--

DROP TABLE IF EXISTS `wf_deptflowsearch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wf_deptflowsearch` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `FK_Emp` varchar(50) DEFAULT NULL COMMENT '操作员',
  `FK_Flow` varchar(50) DEFAULT NULL COMMENT '流程编号',
  `FK_Dept` varchar(100) DEFAULT NULL COMMENT '部门编号',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='流程部门数据查询权限';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_deptflowsearch`
--

LOCK TABLES `wf_deptflowsearch` WRITE;
/*!40000 ALTER TABLE `wf_deptflowsearch` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_deptflowsearch` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_direction`
--

DROP TABLE IF EXISTS `wf_direction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wf_direction` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `FK_Flow` varchar(10) DEFAULT NULL COMMENT '流程',
  `Node` int(11) DEFAULT NULL COMMENT '从节点',
  `ToNode` int(11) DEFAULT NULL COMMENT '到节点',
  `IsCanBack` int(11) DEFAULT NULL COMMENT '是否可以原路返回(对后退线有效)',
  `Dots` varchar(300) DEFAULT NULL COMMENT '轨迹信息',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='节点方向信息';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_direction`
--

LOCK TABLES `wf_direction` WRITE;
/*!40000 ALTER TABLE `wf_direction` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_direction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_directionstation`
--

DROP TABLE IF EXISTS `wf_directionstation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wf_directionstation` (
  `FK_Direction` int(11) NOT NULL DEFAULT '0' COMMENT '节点',
  `FK_Station` varchar(100) NOT NULL COMMENT '工作岗位',
  PRIMARY KEY (`FK_Direction`,`FK_Station`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_directionstation`
--

LOCK TABLES `wf_directionstation` WRITE;
/*!40000 ALTER TABLE `wf_directionstation` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_directionstation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_emp`
--

DROP TABLE IF EXISTS `wf_emp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wf_emp` (
  `No` varchar(50) NOT NULL COMMENT 'No - 主键',
  `Name` varchar(50) DEFAULT NULL COMMENT 'Name',
  `FK_Dept` varchar(100) DEFAULT NULL COMMENT 'FK_Dept',
  `OrgNo` varchar(100) DEFAULT NULL COMMENT '组织,外键:对应物理表:Port_Inc,表描述:独立组织',
  `UseSta` int(11) DEFAULT NULL COMMENT '用户状态0禁用,1正常.',
  `MyNum` int(11) DEFAULT '1' COMMENT '个数',
  `UserType` int(11) DEFAULT NULL COMMENT '用户状态,枚举类型:0 普通用户;1 管理员用户;',
  `RootOfFlow` varchar(100) DEFAULT NULL COMMENT '流程权限节点,外键:对应物理表:WF_FlowSort,表描述:流程类别',
  `RootOfForm` varchar(100) DEFAULT NULL COMMENT '表单权限节点,外键:对应物理表:Sys_FormTree,表描述:表单树',
  `RootOfDept` varchar(100) DEFAULT NULL COMMENT '组织结构权限节点,外键:对应物理表:Port_Dept,表描述:部门',
  `Tel` varchar(50) DEFAULT NULL COMMENT 'Tel',
  `Email` varchar(50) DEFAULT NULL COMMENT 'Email',
  `TM` varchar(50) DEFAULT NULL COMMENT '即时通讯号',
  `AlertWay` int(11) DEFAULT NULL COMMENT '收听方式,枚举类型:0 不接收;1 短信;2 邮件;3 内部消息;4 QQ消息;5 RTX消息;6 MSN消息;',
  `Author` varchar(50) DEFAULT NULL COMMENT '授权人',
  `AuthorDate` varchar(50) DEFAULT NULL COMMENT '授权日期',
  `AuthorWay` int(11) DEFAULT NULL COMMENT '授权方式',
  `AuthorToDate` varchar(50) DEFAULT NULL COMMENT '授权到日期',
  `AuthorFlows` text COMMENT '可以执行的授权流程',
  `Stas` varchar(3000) DEFAULT NULL COMMENT '岗位s',
  `Depts` varchar(100) DEFAULT NULL COMMENT 'Deptss',
  `FtpUrl` varchar(50) DEFAULT NULL COMMENT 'FtpUrl',
  `Msg` text COMMENT 'Msg',
  `Style` text COMMENT 'Style',
  `StartFlows` text COMMENT '可以发起的流程',
  `SPass` varchar(200) DEFAULT '' COMMENT '图片签名密码',
  `Idx` int(11) DEFAULT NULL COMMENT 'Idx',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='操作员';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_emp`
--

LOCK TABLES `wf_emp` WRITE;
/*!40000 ALTER TABLE `wf_emp` DISABLE KEYS */;
INSERT INTO `wf_emp` VALUES ('admin','系统管理员','SD',NULL,1,1,3,'','','','82374939-601','admin@ccflow.org','',3,'','',0,'','','','','','','','','',0);
/*!40000 ALTER TABLE `wf_emp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `wf_empworks`
--

DROP TABLE IF EXISTS `wf_empworks`;
/*!50001 DROP VIEW IF EXISTS `wf_empworks`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `wf_empworks` AS SELECT 
 1 AS `PRI`,
 1 AS `WorkID`,
 1 AS `IsRead`,
 1 AS `Starter`,
 1 AS `StarterName`,
 1 AS `WFState`,
 1 AS `FK_Dept`,
 1 AS `DeptName`,
 1 AS `FK_Flow`,
 1 AS `FlowName`,
 1 AS `PWorkID`,
 1 AS `PFlowNo`,
 1 AS `FK_Node`,
 1 AS `NodeName`,
 1 AS `WorkerDept`,
 1 AS `Title`,
 1 AS `RDT`,
 1 AS `ADT`,
 1 AS `SDT`,
 1 AS `FK_Emp`,
 1 AS `FID`,
 1 AS `FK_FlowSort`,
 1 AS `SysType`,
 1 AS `SDTOfNode`,
 1 AS `PressTimes`,
 1 AS `GuestNo`,
 1 AS `GuestName`,
 1 AS `BillNo`,
 1 AS `FlowNote`,
 1 AS `TodoEmps`,
 1 AS `TodoEmpsNum`,
 1 AS `TodoSta`,
 1 AS `TaskSta`,
 1 AS `ListType`,
 1 AS `Sender`,
 1 AS `AtPara`,
 1 AS `MyNum`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `wf_findworkerrole`
--

DROP TABLE IF EXISTS `wf_findworkerrole`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wf_findworkerrole` (
  `OID` int(11) NOT NULL COMMENT 'OID - 主键',
  `Name` varchar(200) DEFAULT NULL COMMENT 'Name',
  `FK_Node` int(11) DEFAULT NULL COMMENT '节点ID',
  `SortVal0` varchar(200) DEFAULT NULL COMMENT 'SortVal0',
  `SortText0` varchar(200) DEFAULT NULL COMMENT 'SortText0',
  `SortVal1` varchar(200) DEFAULT NULL COMMENT 'SortVal1',
  `SortText1` varchar(200) DEFAULT NULL COMMENT 'SortText1',
  `SortVal2` varchar(200) DEFAULT NULL COMMENT 'SortText2',
  `SortText2` varchar(200) DEFAULT NULL COMMENT 'SortText2',
  `SortVal3` varchar(200) DEFAULT NULL COMMENT 'SortVal3',
  `SortText3` varchar(200) DEFAULT NULL COMMENT 'SortText3',
  `TagVal0` varchar(1000) DEFAULT NULL COMMENT 'TagVal0',
  `TagVal1` varchar(1000) DEFAULT NULL COMMENT 'TagVal1',
  `TagVal2` varchar(1000) DEFAULT NULL COMMENT 'TagVal2',
  `TagVal3` varchar(1000) DEFAULT NULL COMMENT 'TagVal3',
  `TagText0` varchar(1000) DEFAULT NULL COMMENT 'TagText0',
  `TagText1` varchar(1000) DEFAULT NULL COMMENT 'TagText1',
  `TagText2` varchar(1000) DEFAULT NULL COMMENT 'TagText2',
  `TagText3` varchar(1000) DEFAULT NULL COMMENT 'TagText3',
  `IsEnable` int(11) DEFAULT NULL COMMENT '是否可用',
  `Idx` int(11) DEFAULT NULL COMMENT 'IDX',
  PRIMARY KEY (`OID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='找人规则';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_findworkerrole`
--

LOCK TABLES `wf_findworkerrole` WRITE;
/*!40000 ALTER TABLE `wf_findworkerrole` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_findworkerrole` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_flow`
--

DROP TABLE IF EXISTS `wf_flow`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wf_flow` (
  `No` varchar(200) NOT NULL COMMENT '编号 - 主键',
  `FK_FlowSort` varchar(100) DEFAULT NULL COMMENT '流程类别,外键:对应物理表:WF_FlowSort,表描述:流程类别',
  `Name` varchar(500) DEFAULT NULL COMMENT '名称',
  `FlowMark` varchar(150) DEFAULT NULL COMMENT '流程标记',
  `FlowEventEntity` varchar(150) DEFAULT NULL COMMENT '流程事件实体',
  `TitleRole` varchar(150) DEFAULT NULL COMMENT '标题生成规则',
  `IsCanStart` int(11) DEFAULT NULL COMMENT '可以独立启动否？(独立启动的流程可以显示在发起流程列表里)',
  `IsFullSA` int(11) DEFAULT NULL COMMENT '是否自动计算未来的处理人？',
  `IsAutoSendSubFlowOver` int(11) DEFAULT NULL COMMENT '(为子流程时)在流程结束时，是否检查所有子流程完成后，让父流程自动发送到下一步。',
  `IsGuestFlow` int(11) DEFAULT NULL COMMENT '是否外部用户参与流程(非组织结构人员参与的流程)',
  `FlowAppType` int(11) DEFAULT NULL COMMENT '流程应用类型,枚举类型:0 业务流程;1 工程类(项目组流程);2 公文流程(VSTO);',
  `TimelineRole` int(11) DEFAULT NULL COMMENT '时效性规则,枚举类型:0 按节点(由节点属性来定义);1 按发起人(开始节点SysSDTOfFlow字段计算);',
  `Draft` int(11) DEFAULT NULL COMMENT '草稿规则,枚举类型:0 无(不设草稿);1 保存到待办;2 保存到草稿箱;',
  `FlowDeleteRole` int(11) DEFAULT NULL COMMENT '流程实例删除规则,枚举类型:0 超级管理员可以删除;1 分级管理员可以删除;2 发起人可以删除;3 节点启动删除按钮的操作员;',
  `HelpUrl` varchar(300) DEFAULT NULL COMMENT '帮助文档',
  `SysType` varchar(100) DEFAULT NULL COMMENT '系统类型',
  `Tester` varchar(300) DEFAULT NULL COMMENT '发起测试人',
  `NodeAppType` varchar(50) DEFAULT NULL COMMENT '业务类型枚举(可为Null)',
  `NodeAppTypeText` varchar(50) DEFAULT NULL COMMENT '业务类型枚举(可为Null)',
  `ChartType` int(11) DEFAULT NULL COMMENT '节点图形类型,枚举类型:0 几何图形;1 肖像图片;',
  `IsBatchStart` int(11) DEFAULT NULL COMMENT '是否可以批量发起流程？(如果是就要设置发起的需要填写的字段,多个用逗号分开)',
  `BatchStartFields` varchar(500) DEFAULT NULL COMMENT '发起字段s',
  `HistoryFields` varchar(500) DEFAULT NULL COMMENT '历史查看字段',
  `IsResetData` int(11) DEFAULT NULL COMMENT '是否启用开始节点数据重置按钮？',
  `IsLoadPriData` int(11) DEFAULT NULL COMMENT '是否自动装载上一笔数据？',
  `IsDBTemplate` int(11) DEFAULT NULL COMMENT '是否启用数据模版？',
  `IsStartInMobile` int(11) DEFAULT NULL COMMENT '是否可以在手机里启用？(如果发起表单特别复杂就不要在手机里启用了)',
  `IsMD5` int(11) DEFAULT NULL COMMENT '是否是数据加密流程(MD5数据加密防篡改)',
  `DataStoreModel` int(11) DEFAULT NULL COMMENT '流程数据存储模式,枚举类型:0 数据轨迹模式;1 数据合并模式;',
  `PTable` varchar(30) DEFAULT NULL COMMENT '流程数据存储表',
  `FlowNoteExp` varchar(500) DEFAULT NULL COMMENT '备注的表达式',
  `BillNoFormat` varchar(50) DEFAULT NULL COMMENT '单据编号格式',
  `DesignerNo` varchar(50) DEFAULT NULL COMMENT '设计者编号',
  `DesignerName` varchar(100) DEFAULT NULL COMMENT '设计者名称',
  `Note` text COMMENT '流程描述',
  `FlowRunWay` int(11) DEFAULT NULL COMMENT '启动方式,枚举类型:0 手工启动;1 指定人员按时启动;2 数据集按时启动;3 触发式启动;',
  `RunObj` varchar(4000) DEFAULT NULL COMMENT '运行内容',
  `RunSQL` varchar(2000) DEFAULT NULL COMMENT '流程结束执行后执行的SQL',
  `NumOfBill` int(11) DEFAULT NULL COMMENT '是否有单据',
  `NumOfDtl` int(11) DEFAULT NULL COMMENT 'NumOfDtl',
  `AvgDay` float(11,2) DEFAULT NULL COMMENT '平均运行用天',
  `Idx` int(11) DEFAULT NULL COMMENT '显示顺序号(在发起列表中)',
  `Paras` varchar(2000) DEFAULT NULL COMMENT '参数',
  `DRCtrlType` int(11) DEFAULT NULL COMMENT '部门查询权限控制方式',
  `StartLimitRole` int(11) DEFAULT NULL COMMENT '启动限制规则,枚举类型:0 不限制;1 每人每天一次;2 每人每周一次;3 每人每月一次;4 每人每季一次;5 每人每年一次;6 发起的列不能重复,(多个列可以用逗号分开);7 设置的SQL数据源为空,或者返回结果为零时可以启动.;8 设置的SQL数据源为空,或者返回结果为零时不可以启动.;',
  `StartLimitPara` varchar(500) DEFAULT NULL COMMENT '规则参数',
  `StartLimitAlert` varchar(500) DEFAULT NULL COMMENT '限制提示',
  `StartLimitWhen` int(11) DEFAULT NULL COMMENT '提示时间',
  `StartGuideWay` int(11) DEFAULT NULL COMMENT '前置导航方式,枚举类型:0 无;1 按系统的URL-(父子流程)单条模式;2 按系统的URL-(子父流程)多条模式;3 按系统的URL-(实体记录,未完成)单条模式;4 按系统的URL-(实体记录,未完成)多条模式;5 从开始节点Copy数据;10 按自定义的Url;11 按用户输入参数;',
  `StartGuideLink` varchar(200) DEFAULT NULL COMMENT '右侧的连接',
  `StartGuideLab` varchar(200) DEFAULT NULL COMMENT '连接标签',
  `StartGuidePara1` varchar(500) DEFAULT NULL COMMENT '参数1',
  `StartGuidePara2` varchar(500) DEFAULT NULL COMMENT '参数2',
  `StartGuidePara3` varchar(500) DEFAULT NULL COMMENT '参数3',
  `Ver` varchar(20) DEFAULT NULL COMMENT '版本号',
  `DType` int(11) DEFAULT NULL COMMENT '设计类型0=ccbpm,1=bpmn',
  `AtPara` varchar(1000) DEFAULT NULL COMMENT 'AtPara',
  `DTSWay` int(11) DEFAULT NULL COMMENT '同步方式,枚举类型:0 不考核;1 按照时效考核;2 按照工作量考核;',
  `DTSDBSrc` varchar(200) DEFAULT NULL COMMENT '数据库,外键:对应物理表:Sys_SFDBSrc,表描述:数据源',
  `DTSBTable` varchar(200) DEFAULT NULL COMMENT '业务表名',
  `DTSBTablePK` varchar(50) DEFAULT NULL COMMENT '业务表主键',
  `DTSTime` int(11) DEFAULT NULL COMMENT '执行同步时间点,枚举类型:0 所有的节点发送后;1 指定的节点发送后;2 当流程结束时;',
  `DTSSpecNodes` varchar(200) DEFAULT NULL COMMENT '指定的节点ID',
  `DTSField` int(11) DEFAULT NULL COMMENT '要同步的字段计算方式,枚举类型:0 字段名相同;1 按设置的字段匹配;',
  `DTSFields` varchar(2000) DEFAULT NULL COMMENT '要同步的字段s,中间用逗号分开.',
  `MyDeptRole` int(11) DEFAULT NULL COMMENT '本部门发起的流程,枚举类型:0 仅部门领导可以查看;1 部门下所有的人都可以查看;2 本部门里指定岗位的人可以查看;',
  `PStarter` int(11) DEFAULT NULL COMMENT '发起人可看(必选)',
  `PWorker` int(11) DEFAULT NULL COMMENT '参与人可看(必选)',
  `PCCer` int(11) DEFAULT NULL COMMENT '被抄送人可看(必选)',
  `PMyDept` int(11) DEFAULT NULL COMMENT '本部门人可看',
  `PPMyDept` int(11) DEFAULT NULL COMMENT '直属上级部门可看(比如:我是)',
  `PPDept` int(11) DEFAULT NULL COMMENT '上级部门可看',
  `PSameDept` int(11) DEFAULT NULL COMMENT '平级部门可看',
  `PSpecDept` int(11) DEFAULT NULL COMMENT '指定部门可看',
  `PSpecDeptExt` varchar(200) DEFAULT NULL COMMENT '部门编号',
  `PSpecSta` int(11) DEFAULT NULL COMMENT '指定的岗位可看',
  `PSpecStaExt` varchar(200) DEFAULT NULL COMMENT '岗位编号',
  `PSpecGroup` int(11) DEFAULT NULL COMMENT '指定的权限组可看',
  `PSpecGroupExt` varchar(200) DEFAULT NULL COMMENT '权限组',
  `PSpecEmp` int(11) DEFAULT NULL COMMENT '指定的人员可看',
  `PSpecEmpExt` varchar(200) DEFAULT NULL COMMENT '指定的人员编号',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='节点标签';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_flow`
--

LOCK TABLES `wf_flow` WRITE;
/*!40000 ALTER TABLE `wf_flow` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_flow` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_flowemp`
--

DROP TABLE IF EXISTS `wf_flowemp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wf_flowemp` (
  `FK_Flow` varchar(100) NOT NULL COMMENT 'FK_Flow,主外键:对应物理表:WF_Flow,表描述:流程',
  `FK_Emp` varchar(100) NOT NULL COMMENT '人员,主外键:对应物理表:Port_Emp,表描述:用户',
  PRIMARY KEY (`FK_Flow`,`FK_Emp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='流程岗位属性信息';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_flowemp`
--

LOCK TABLES `wf_flowemp` WRITE;
/*!40000 ALTER TABLE `wf_flowemp` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_flowemp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_flowformtree`
--

DROP TABLE IF EXISTS `wf_flowformtree`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wf_flowformtree` (
  `No` varchar(10) NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(100) DEFAULT NULL COMMENT '名称',
  `ParentNo` varchar(100) DEFAULT NULL COMMENT '父节点No',
  `Idx` int(11) DEFAULT NULL COMMENT 'Idx',
  `FK_Flow` varchar(20) DEFAULT NULL COMMENT '流程编号',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='独立表单树';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_flowformtree`
--

LOCK TABLES `wf_flowformtree` WRITE;
/*!40000 ALTER TABLE `wf_flowformtree` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_flowformtree` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_flownode`
--

DROP TABLE IF EXISTS `wf_flownode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wf_flownode` (
  `FK_Flow` varchar(20) NOT NULL COMMENT '流程编号 - 主键',
  `FK_Node` varchar(20) NOT NULL COMMENT '节点 - 主键',
  PRIMARY KEY (`FK_Flow`,`FK_Node`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='流程抄送节点';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_flownode`
--

LOCK TABLES `wf_flownode` WRITE;
/*!40000 ALTER TABLE `wf_flownode` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_flownode` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_flowsort`
--

DROP TABLE IF EXISTS `wf_flowsort`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wf_flowsort` (
  `No` varchar(100) NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(100) DEFAULT NULL COMMENT '名称',
  `ParentNo` varchar(100) DEFAULT NULL COMMENT '父节点No',
  `OrgNo` varchar(50) DEFAULT NULL COMMENT '组织编号(0为系统组织)',
  `Idx` int(11) DEFAULT NULL COMMENT 'Idx',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='流程类别';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_flowsort`
--

LOCK TABLES `wf_flowsort` WRITE;
/*!40000 ALTER TABLE `wf_flowsort` DISABLE KEYS */;
INSERT INTO `wf_flowsort` VALUES ('01.','线性流程','99','0',0),('99','流程树','0','0',0);
/*!40000 ALTER TABLE `wf_flowsort` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_frmnode`
--

DROP TABLE IF EXISTS `wf_frmnode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wf_frmnode` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `FK_Frm` varchar(200) DEFAULT NULL COMMENT '表单ID',
  `FK_Node` int(11) DEFAULT NULL COMMENT '节点编号',
  `FK_Flow` varchar(20) DEFAULT NULL COMMENT '流程编号',
  `OfficeOpenLab` varchar(50) DEFAULT NULL COMMENT '打开本地标签',
  `OfficeOpenEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `OfficeOpenTemplateLab` varchar(50) DEFAULT NULL COMMENT '打开模板标签',
  `OfficeOpenTemplateEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `OfficeSaveLab` varchar(50) DEFAULT NULL COMMENT '保存标签',
  `OfficeSaveEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `OfficeAcceptLab` varchar(50) DEFAULT NULL COMMENT '接受修订标签',
  `OfficeAcceptEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `OfficeRefuseLab` varchar(50) DEFAULT NULL COMMENT '拒绝修订标签',
  `OfficeRefuseEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `OfficeOverLab` varchar(50) DEFAULT NULL COMMENT '套红按钮标签',
  `OfficeOverEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `OfficeMarksEnable` int(11) DEFAULT NULL COMMENT '是否查看用户留痕',
  `OfficePrintLab` varchar(50) DEFAULT NULL COMMENT '打印按钮标签',
  `OfficePrintEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `OfficeSealLab` varchar(50) DEFAULT NULL COMMENT '签章按钮标签',
  `OfficeSealEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `OfficeInsertFlowLab` varchar(50) DEFAULT NULL COMMENT '插入流程标签',
  `OfficeInsertFlowEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `OfficeNodeInfo` int(11) DEFAULT NULL COMMENT '是否记录节点信息',
  `OfficeReSavePDF` int(11) DEFAULT NULL COMMENT '是否该自动保存为PDF',
  `OfficeDownLab` varchar(50) DEFAULT NULL COMMENT '下载按钮标签',
  `OfficeDownEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `OfficeIsMarks` int(11) DEFAULT NULL COMMENT '是否进入留痕模式',
  `OfficeTemplate` varchar(100) DEFAULT NULL COMMENT '指定文档模板',
  `OfficeIsParent` int(11) DEFAULT NULL COMMENT '是否使用父流程的文档',
  `OfficeTHEnable` int(11) DEFAULT NULL COMMENT '是否自动套红',
  `OfficeTHTemplate` varchar(200) DEFAULT NULL COMMENT '自动套红模板',
  `FrmSln` int(11) DEFAULT NULL COMMENT '表单控制方案',
  `FrmType` varchar(20) DEFAULT NULL COMMENT '表单类型',
  `IsPrint` int(11) DEFAULT NULL COMMENT '是否可以打印',
  `IsEnableLoadData` int(11) DEFAULT NULL COMMENT '是否启用装载填充事件',
  `IsDefaultOpen` int(11) DEFAULT NULL COMMENT '是否默认打开',
  `Idx` int(11) DEFAULT NULL COMMENT '顺序号',
  `WhoIsPK` int(11) DEFAULT NULL COMMENT '谁是主键？',
  `Is1ToN` int(11) DEFAULT NULL COMMENT '是否1变N？',
  `HuiZong` varchar(300) DEFAULT NULL COMMENT '子线程要汇总的数据表',
  `FrmEnableRole` int(11) DEFAULT NULL COMMENT '表单启用规则',
  `FrmEnableExp` varchar(4000) DEFAULT NULL COMMENT '启用的表达式',
  `TempleteFile` varchar(500) DEFAULT NULL COMMENT '模版文件',
  `IsEnable` int(11) DEFAULT NULL COMMENT '是否显示',
  `GuanJianZiDuan` varchar(20) DEFAULT NULL COMMENT '关键字段',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='节点表单';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_frmnode`
--

LOCK TABLES `wf_frmnode` WRITE;
/*!40000 ALTER TABLE `wf_frmnode` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_frmnode` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_generfh`
--

DROP TABLE IF EXISTS `wf_generfh`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wf_generfh` (
  `OID` int(11) NOT NULL COMMENT '流程ID - 主键',
  `FID` int(11) DEFAULT NULL COMMENT 'FID',
  `Title` text COMMENT '标题',
  `GroupKey` varchar(3000) DEFAULT NULL COMMENT '分组主键',
  `FK_Flow` varchar(500) DEFAULT NULL COMMENT '流程',
  `ToEmpsMsg` text COMMENT '接受人员',
  `FK_Node` int(11) DEFAULT NULL COMMENT '停留节点',
  `WFState` int(11) DEFAULT NULL COMMENT 'WFState',
  `RDT` varchar(50) DEFAULT NULL COMMENT 'RDT',
  PRIMARY KEY (`OID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='分合流程控制';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_generfh`
--

LOCK TABLES `wf_generfh` WRITE;
/*!40000 ALTER TABLE `wf_generfh` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_generfh` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_generworkerlist`
--

DROP TABLE IF EXISTS `wf_generworkerlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wf_generworkerlist` (
  `WorkID` int(11) NOT NULL COMMENT '工作ID - 主键',
  `FK_Emp` varchar(20) NOT NULL COMMENT '人员 - 主键',
  `FK_Node` int(11) NOT NULL COMMENT '节点ID - 主键',
  `FID` int(11) DEFAULT NULL COMMENT '流程ID',
  `FK_EmpText` varchar(30) DEFAULT NULL COMMENT '人员名称',
  `FK_NodeText` varchar(100) DEFAULT NULL COMMENT '节点名称',
  `FK_Flow` varchar(3) DEFAULT NULL COMMENT '流程',
  `FK_Dept` varchar(100) DEFAULT NULL COMMENT '使用部门',
  `SDT` varchar(50) DEFAULT NULL COMMENT '应完成日期',
  `DTOfWarning` varchar(50) DEFAULT NULL COMMENT '警告日期',
  `RDT` varchar(50) DEFAULT NULL COMMENT '记录时间',
  `CDT` varchar(50) DEFAULT NULL COMMENT '完成时间',
  `IsEnable` int(11) DEFAULT NULL COMMENT '是否可用',
  `IsRead` int(11) DEFAULT NULL COMMENT '是否读取',
  `IsPass` int(11) DEFAULT NULL COMMENT '是否通过(对合流节点有效)',
  `WhoExeIt` int(11) DEFAULT NULL COMMENT '谁执行它',
  `Sender` varchar(200) DEFAULT NULL COMMENT '发送人',
  `PRI` int(11) DEFAULT NULL COMMENT '优先级',
  `PressTimes` int(11) DEFAULT NULL COMMENT '催办次数',
  `DTOfHungUp` varchar(50) DEFAULT NULL COMMENT '挂起时间',
  `DTOfUnHungUp` varchar(50) DEFAULT NULL COMMENT '预计解除挂起时间',
  `HungUpTimes` int(11) DEFAULT NULL COMMENT '挂起次数',
  `GuestNo` varchar(30) DEFAULT NULL COMMENT '外部用户编号',
  `GuestName` varchar(100) DEFAULT NULL COMMENT '外部用户名称',
  `AtPara` text COMMENT 'AtPara',
  PRIMARY KEY (`WorkID`,`FK_Emp`,`FK_Node`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='工作者';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_generworkerlist`
--

LOCK TABLES `wf_generworkerlist` WRITE;
/*!40000 ALTER TABLE `wf_generworkerlist` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_generworkerlist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_generworkflow`
--

DROP TABLE IF EXISTS `wf_generworkflow`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wf_generworkflow` (
  `WorkID` int(11) NOT NULL COMMENT 'WorkID - 主键',
  `FID` int(11) DEFAULT NULL COMMENT '流程ID',
  `FK_FlowSort` varchar(100) DEFAULT NULL COMMENT '流程类别',
  `SysType` varchar(100) DEFAULT NULL COMMENT '系统类别',
  `FK_Flow` varchar(100) DEFAULT NULL COMMENT '流程',
  `FlowName` varchar(100) DEFAULT NULL COMMENT '流程名称',
  `Title` varchar(1000) DEFAULT NULL COMMENT '标题',
  `WFSta` int(11) DEFAULT NULL COMMENT '状态,枚举类型:0 运行中;1 已完成;2 其他;',
  `WFState` int(11) DEFAULT NULL COMMENT '流程状态,枚举类型:0 空白;1 草稿;2 运行中;3 已完成;4 挂起;5 退回;6 转发;7 删除;8 加签;9 冻结;10 批处理;11 加签回复状态;',
  `Starter` varchar(200) DEFAULT NULL COMMENT '发起人',
  `StarterName` varchar(200) DEFAULT NULL COMMENT '发起人名称',
  `Sender` varchar(200) DEFAULT NULL COMMENT '发送人',
  `RDT` varchar(50) DEFAULT NULL COMMENT '记录日期',
  `FK_Node` int(11) DEFAULT NULL COMMENT '节点',
  `NodeName` varchar(100) DEFAULT NULL COMMENT '节点名称',
  `FK_Dept` varchar(100) DEFAULT NULL COMMENT '部门',
  `DeptName` varchar(100) DEFAULT NULL COMMENT '部门名称',
  `PRI` int(11) DEFAULT NULL COMMENT '优先级',
  `SDTOfNode` varchar(50) DEFAULT NULL COMMENT '节点应完成时间',
  `SDTOfFlow` varchar(50) DEFAULT NULL COMMENT '流程应完成时间',
  `PFlowNo` varchar(3) DEFAULT NULL COMMENT '父流程编号',
  `PWorkID` int(11) DEFAULT NULL COMMENT '父流程ID',
  `PNodeID` int(11) DEFAULT NULL COMMENT '父流程调用节点',
  `PFID` int(11) DEFAULT NULL COMMENT '父流程调用的PFID',
  `PEmp` varchar(32) DEFAULT NULL COMMENT '子流程的调用人',
  `GuestNo` varchar(100) DEFAULT NULL COMMENT '客户编号',
  `GuestName` varchar(100) DEFAULT NULL COMMENT '客户名称',
  `BillNo` varchar(100) DEFAULT NULL COMMENT '单据编号',
  `FlowNote` text COMMENT '备注',
  `TodoEmps` text COMMENT '待办人员',
  `TodoEmpsNum` int(11) DEFAULT NULL COMMENT '待办人员数量',
  `TaskSta` int(11) DEFAULT NULL COMMENT '共享状态',
  `AtPara` varchar(2000) DEFAULT NULL COMMENT '参数(流程运行设置临时存储的参数)',
  `Emps` text COMMENT '参与人',
  `GUID` varchar(36) DEFAULT NULL COMMENT 'GUID',
  `FK_NY` varchar(100) DEFAULT NULL COMMENT '年月',
  `WeekNum` int(11) DEFAULT NULL COMMENT '周次',
  `TSpan` int(11) DEFAULT NULL COMMENT '时间间隔',
  `TodoSta` int(11) DEFAULT NULL COMMENT '待办状态',
  `MyNum` int(11) DEFAULT NULL COMMENT '个数',
  PRIMARY KEY (`WorkID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='流程实例';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_generworkflow`
--

LOCK TABLES `wf_generworkflow` WRITE;
/*!40000 ALTER TABLE `wf_generworkflow` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_generworkflow` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_hungup`
--

DROP TABLE IF EXISTS `wf_hungup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wf_hungup` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `FK_Node` int(11) DEFAULT NULL COMMENT '节点ID',
  `WorkID` int(11) DEFAULT NULL COMMENT 'WorkID',
  `HungUpWay` int(11) DEFAULT NULL COMMENT '挂起方式,枚举类型:0 无限挂起;1 按指定的时间解除挂起并通知我自己;2 按指定的时间解除挂起并通知所有人;',
  `Note` text COMMENT '挂起原因(标题与内容支持变量)',
  `Rec` varchar(50) DEFAULT NULL COMMENT '挂起人',
  `DTOfHungUp` varchar(50) DEFAULT NULL COMMENT '挂起时间',
  `DTOfUnHungUp` varchar(50) DEFAULT NULL COMMENT '实际解除挂起时间',
  `DTOfUnHungUpPlan` varchar(50) DEFAULT NULL COMMENT '预计解除挂起时间',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='挂起';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_hungup`
--

LOCK TABLES `wf_hungup` WRITE;
/*!40000 ALTER TABLE `wf_hungup` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_hungup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_labnote`
--

DROP TABLE IF EXISTS `wf_labnote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wf_labnote` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `Name` varchar(3000) DEFAULT NULL COMMENT 'null',
  `FK_Flow` varchar(100) DEFAULT NULL COMMENT '流程',
  `X` int(11) DEFAULT NULL COMMENT 'X坐标',
  `Y` int(11) DEFAULT NULL COMMENT 'Y坐标',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='标签';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_labnote`
--

LOCK TABLES `wf_labnote` WRITE;
/*!40000 ALTER TABLE `wf_labnote` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_labnote` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_node`
--

DROP TABLE IF EXISTS `wf_node`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wf_node` (
  `NodeID` int(11) NOT NULL COMMENT 'NodeID - 主键',
  `Step` int(11) DEFAULT NULL COMMENT '步骤(无计算意义)',
  `FK_Flow` varchar(150) DEFAULT NULL COMMENT '流程编号',
  `Name` varchar(200) DEFAULT NULL COMMENT '节点名称',
  `Tip` varchar(100) DEFAULT NULL COMMENT '操作提示',
  `WhoExeIt` int(11) DEFAULT NULL COMMENT '谁执行它,枚举类型:0 操作员执行;1 机器执行;2 混合执行;',
  `ReadReceipts` int(11) DEFAULT NULL COMMENT '已读回执',
  `CondModel` int(11) DEFAULT NULL COMMENT '方向条件控制规则,枚举类型:0 由连接线条件控制;2 发送按钮旁下拉框选择;',
  `CancelRole` int(11) DEFAULT NULL COMMENT '撤销规则,枚举类型:0 上一步可以撤销;1 不能撤销;2 上一步与开始节点可以撤销;3 指定的节点可以撤销;',
  `CancelDisWhenRead` int(11) DEFAULT NULL COMMENT '对方已经打开就不能撤销',
  `IsTask` int(11) DEFAULT NULL COMMENT '允许分配工作否?',
  `IsRM` int(11) DEFAULT NULL COMMENT '是否启用投递路径自动记忆功能?',
  `DTFrom` varchar(50) DEFAULT NULL COMMENT '生命周期从',
  `DTTo` varchar(50) DEFAULT NULL COMMENT '生命周期到',
  `IsBUnit` int(11) DEFAULT NULL COMMENT '是否是节点模版（业务单元）?',
  `FocusField` varchar(50) DEFAULT NULL COMMENT '焦点字段',
  `SaveModel` int(11) DEFAULT NULL COMMENT '保存方式,枚举类型:0 仅节点表;1 节点表与Rpt表;',
  `IsGuestNode` int(11) DEFAULT NULL COMMENT '是否是外部用户执行的节点(非组织结构人员参与处理工作的节点)?',
  `NodeAppType` int(11) DEFAULT NULL COMMENT '节点业务类型',
  `FWCSta` int(11) DEFAULT NULL COMMENT '节点状态',
  `SelfParas` varchar(500) DEFAULT NULL COMMENT '自定义参数',
  `RunModel` int(11) DEFAULT NULL COMMENT '节点类型,枚举类型:0 普通;1 合流;2 分流;3 分合流;4 子线程;',
  `SubThreadType` int(11) DEFAULT NULL COMMENT '子线程类型,枚举类型:0 同表单;1 异表单;',
  `PassRate` float DEFAULT NULL COMMENT '完成通过率',
  `SubFlowStartWay` int(11) DEFAULT NULL COMMENT '子线程启动方式,枚举类型:0 不启动;1 指定的字段启动;2 按明细表启动;',
  `SubFlowStartParas` varchar(100) DEFAULT NULL COMMENT '启动参数',
  `ThreadIsCanDel` int(11) DEFAULT NULL COMMENT '是否可以删除子线程(当前节点已经发送出去的线程，并且当前节点是分流，或者分合流有效，在子线程退回后的操作)？',
  `ThreadIsCanShift` int(11) DEFAULT NULL COMMENT '是否可以移交子线程(当前节点已经发送出去的线程，并且当前节点是分流，或者分合流有效，在子线程退回后的操作)？',
  `IsAllowRepeatEmps` int(11) DEFAULT NULL COMMENT '是否允许子线程接受人员重复(仅当分流点向子线程发送时有效)?',
  `AutoRunEnable` int(11) DEFAULT NULL COMMENT '是否启用自动运行？(仅当分流点向子线程发送时有效)',
  `AutoRunParas` varchar(100) DEFAULT NULL COMMENT '自动运行SQL',
  `AutoJumpRole0` int(11) DEFAULT NULL COMMENT '处理人就是发起人',
  `AutoJumpRole1` int(11) DEFAULT NULL COMMENT '处理人已经出现过',
  `AutoJumpRole2` int(11) DEFAULT NULL COMMENT '处理人与上一步相同',
  `WhenNoWorker` int(11) DEFAULT NULL COMMENT '(是)找不到人就跳转,(否)提示错误.',
  `SendLab` varchar(50) DEFAULT NULL COMMENT '发送按钮标签',
  `SendJS` varchar(999) DEFAULT NULL COMMENT '按钮JS函数',
  `SaveLab` varchar(50) DEFAULT NULL COMMENT '保存按钮标签',
  `SaveEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `ThreadLab` varchar(50) DEFAULT NULL COMMENT '子线程按钮标签',
  `ThreadEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `ThreadKillRole` int(11) DEFAULT NULL COMMENT '子线程删除方式,枚举类型:0 不能删除;1 手工删除;2 自动删除;',
  `JumpWayLab` varchar(50) DEFAULT NULL COMMENT '跳转按钮标签',
  `JumpWay` int(11) DEFAULT NULL COMMENT '跳转规则,枚举类型:0 不能跳转;1 只能向后跳转;2 只能向前跳转;3 任意节点跳转;4 按指定规则跳转;',
  `JumpToNodes` varchar(200) DEFAULT NULL COMMENT '可跳转的节点',
  `ReturnLab` varchar(50) DEFAULT NULL COMMENT '退回按钮标签',
  `ReturnRole` int(11) DEFAULT NULL COMMENT '退回规则,枚举类型:0 不能退回;1 只能退回上一个节点;2 可退回以前任意节点;3 可退回指定的节点;4 由流程图设计的退回路线决定;',
  `ReturnAlert` varchar(999) DEFAULT NULL COMMENT '被退回后信息提示',
  `IsBackTracking` int(11) DEFAULT NULL COMMENT '是否可以原路返回(启用退回功能才有效)',
  `ReturnField` varchar(50) DEFAULT NULL COMMENT '退回信息填写字段',
  `ReturnReasonsItems` varchar(999) DEFAULT NULL COMMENT '退回原因',
  `CCLab` varchar(50) DEFAULT NULL COMMENT '抄送按钮标签',
  `CCRole` int(11) DEFAULT NULL COMMENT '抄送规则,枚举类型:0 不能抄送;1 手工抄送;2 自动抄送;3 手工与自动;4 按表单SysCCEmps字段计算;',
  `CCWriteTo` int(11) DEFAULT NULL COMMENT '抄送写入规则,枚举类型:0 写入抄送列表;1 写入待办;2 写入待办与抄送列表;',
  `ShiftLab` varchar(50) DEFAULT NULL COMMENT '移交按钮标签',
  `ShiftEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `DelLab` varchar(50) DEFAULT NULL COMMENT '删除按钮标签',
  `DelEnable` int(11) DEFAULT NULL COMMENT '删除规则,枚举类型:0 不能删除;1 逻辑删除;2 记录日志方式删除;3 彻底删除;4 让用户决定删除方式;',
  `EndFlowLab` varchar(50) DEFAULT NULL COMMENT '结束流程按钮标签',
  `EndFlowEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `PrintHtmlLab` varchar(50) DEFAULT NULL COMMENT '打印Html标签',
  `PrintHtmlEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `PrintPDFLab` varchar(50) DEFAULT NULL COMMENT '打印pdf标签',
  `PrintPDFEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `PrintZipLab` varchar(50) DEFAULT NULL COMMENT '打包下载zip按钮标签',
  `PrintZipEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `PrintDocLab` varchar(50) DEFAULT NULL COMMENT '打印单据按钮标签',
  `PrintDocEnable` int(11) DEFAULT NULL COMMENT '打印方式,枚举类型:0 不打印;1 打印网页;2 打印RTF模板;3 打印Word模版;',
  `TrackLab` varchar(50) DEFAULT NULL COMMENT '轨迹按钮标签',
  `TrackEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `HungLab` varchar(50) DEFAULT NULL COMMENT '挂起按钮标签',
  `HungEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `SearchLab` varchar(50) DEFAULT NULL COMMENT '查询按钮标签',
  `SearchEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `WorkCheckLab` varchar(50) DEFAULT NULL COMMENT '审核按钮标签',
  `WorkCheckEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `AskforLab` varchar(50) DEFAULT NULL COMMENT '加签按钮标签',
  `AskforEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `HuiQianLab` varchar(50) DEFAULT NULL COMMENT '会签标签',
  `HuiQianRole` int(11) DEFAULT NULL COMMENT '会签模式,枚举类型:0 不启用;1 协作(同事)模式;4 组长(领导)模式;',
  `TCLab` varchar(50) DEFAULT NULL COMMENT '流转自定义',
  `TCEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `WebOffice` varchar(50) DEFAULT NULL COMMENT '文档按钮标签',
  `WebOfficeEnable` int(11) DEFAULT NULL COMMENT '文档启用方式',
  `PRILab` varchar(50) DEFAULT NULL COMMENT '重要性',
  `PRIEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `CHLab` varchar(50) DEFAULT NULL COMMENT '节点时限',
  `CHEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `AllotLab` varchar(50) DEFAULT NULL COMMENT '分配按钮标签',
  `AllotEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `FocusLab` varchar(50) DEFAULT NULL COMMENT '关注',
  `FocusEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `ConfirmLab` varchar(50) DEFAULT NULL COMMENT '确认按钮标签',
  `ConfirmEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `CCIsAttr` int(11) DEFAULT '0' COMMENT '按表单字段抄送',
  `CCFormAttr` varchar(100) DEFAULT '' COMMENT '抄送人员字段',
  `CCIsStations` int(11) DEFAULT NULL COMMENT '按照岗位抄送',
  `CCStaWay` int(11) DEFAULT '0' COMMENT '抄送岗位计算方式',
  `CCIsDepts` int(11) DEFAULT NULL COMMENT '按照部门抄送',
  `CCIsEmps` int(11) DEFAULT NULL COMMENT '按照人员抄送',
  `CCIsSQLs` int(11) DEFAULT NULL COMMENT '按照SQL抄送',
  `CCSQL` varchar(200) DEFAULT NULL COMMENT 'SQL表达式',
  `CCTitle` varchar(100) DEFAULT NULL COMMENT '抄送标题',
  `CCDoc` text COMMENT '抄送内容(标题与内容支持变量)',
  `FWCLab` varchar(200) DEFAULT NULL COMMENT '显示标签',
  `FWCShowModel` int(11) DEFAULT NULL COMMENT '显示方式,枚举类型:0 表格方式;1 自由模式;',
  `FWCType` int(11) DEFAULT NULL COMMENT '审核组件,枚举类型:0 审核组件;1 日志组件;2 周报组件;3 月报组件;',
  `FWCNodeName` varchar(100) DEFAULT NULL COMMENT '节点意见名称',
  `FWCAth` int(11) DEFAULT NULL COMMENT '附件上传,枚举类型:0 不启用;1 多附件;2 单附件(暂不支持);3 图片附件(暂不支持);',
  `FWCTrackEnable` int(11) DEFAULT NULL COMMENT '轨迹图是否显示？',
  `FWCListEnable` int(11) DEFAULT NULL COMMENT '历史审核信息是否显示？(否,仅出现意见框)',
  `FWCIsShowAllStep` int(11) DEFAULT NULL COMMENT '在轨迹表里是否显示所有的步骤？',
  `FWCOpLabel` varchar(50) DEFAULT NULL COMMENT '操作名词(审核/审阅/批示)',
  `FWCDefInfo` varchar(50) DEFAULT NULL COMMENT '默认审核信息',
  `SigantureEnabel` int(11) DEFAULT NULL COMMENT '操作人是否显示为图片签名？',
  `FWCIsFullInfo` int(11) DEFAULT NULL COMMENT '如果用户未审核是否按照默认意见填充？',
  `FWC_X` float(11,2) DEFAULT NULL COMMENT '位置X',
  `FWC_Y` float(11,2) DEFAULT NULL COMMENT '位置Y',
  `FWC_H` float(11,2) DEFAULT NULL COMMENT '高度(0=100%)',
  `FWC_W` float(11,2) DEFAULT NULL COMMENT '宽度(0=100%)',
  `FWCFields` varchar(50) DEFAULT NULL COMMENT '审批格式字段',
  `FWCIsShowTruck` int(11) DEFAULT NULL COMMENT '是否显示未审核的轨迹？',
  `FWCIsShowReturnMsg` int(11) DEFAULT NULL COMMENT '是否显示退回信息？',
  `FWCOrderModel` int(11) DEFAULT NULL COMMENT '协作模式下操作员显示顺序,枚举类型:0 按审批时间先后排序;1 按照接受人员列表先后顺序(官职大小);',
  `FWCMsgShow` int(11) DEFAULT NULL COMMENT '审核意见显示方式,枚举类型:0 都显示;1 仅显示自己的意见;',
  `SFLab` varchar(200) DEFAULT NULL COMMENT '显示标签',
  `SFSta` int(11) DEFAULT NULL COMMENT '父子流程状态,枚举类型:0 禁用;1 启用;2 只读;',
  `SFShowModel` int(11) DEFAULT NULL COMMENT '显示方式,枚举类型:0 表格方式;1 自由模式;',
  `SFCaption` varchar(100) DEFAULT NULL COMMENT '连接标题',
  `SFDefInfo` varchar(50) DEFAULT NULL COMMENT '可启动的子流程',
  `SFActiveFlows` varchar(100) DEFAULT NULL COMMENT '可触发的子流程',
  `SF_X` float(11,2) DEFAULT NULL COMMENT '位置X',
  `SF_Y` float(11,2) DEFAULT NULL COMMENT '位置Y',
  `SF_H` float(11,2) DEFAULT NULL COMMENT '高度',
  `SF_W` float(11,2) DEFAULT NULL COMMENT '宽度',
  `SFFields` varchar(50) DEFAULT NULL COMMENT '审批格式字段',
  `SFShowCtrl` int(11) DEFAULT NULL COMMENT '显示控制方式,枚举类型:0 可以看所有的子流程;1 仅仅可以看自己发起的子流程;',
  `SFOpenType` int(11) DEFAULT NULL COMMENT '打开子流程显示,枚举类型:0 工作查看器;1 傻瓜表单轨迹查看器;',
  `FrmThreadLab` varchar(200) DEFAULT NULL COMMENT '显示标签',
  `FrmThreadSta` int(11) DEFAULT NULL COMMENT '组件状态,枚举类型:0 禁用;1 启用;',
  `FrmThread_X` float(11,2) DEFAULT NULL COMMENT '位置X',
  `FrmThread_Y` float(11,2) DEFAULT NULL COMMENT '位置Y',
  `FrmThread_H` float(11,2) DEFAULT NULL COMMENT '高度',
  `FrmThread_W` float(11,2) DEFAULT NULL COMMENT '宽度',
  `FrmTrackLab` varchar(200) DEFAULT NULL COMMENT '显示标签',
  `FrmTrackSta` int(11) DEFAULT NULL COMMENT '组件状态,枚举类型:0 禁用;1 标准风格;2 华东院风格;3 华夏银行风格;',
  `FrmTrack_X` float(11,2) DEFAULT NULL COMMENT '位置X',
  `FrmTrack_Y` float(11,2) DEFAULT NULL COMMENT '位置Y',
  `FrmTrack_H` float(11,2) DEFAULT NULL COMMENT '高度',
  `FrmTrack_W` float(11,2) DEFAULT NULL COMMENT '宽度',
  `CheckNodes` varchar(800) DEFAULT NULL COMMENT '工作节点s',
  `DeliveryWay` int(11) DEFAULT NULL COMMENT '访问规则',
  `FTCLab` varchar(50) DEFAULT NULL COMMENT '显示标签',
  `FTCSta` int(11) DEFAULT NULL COMMENT '组件状态,枚举类型:0 禁用;1 只读;2 可设置人员;',
  `FTCWorkModel` int(11) DEFAULT NULL COMMENT '工作模式,枚举类型:0 简洁模式;1 高级模式;',
  `FTC_X` float(11,2) DEFAULT NULL COMMENT '位置X',
  `FTC_Y` float(11,2) DEFAULT NULL COMMENT '位置Y',
  `FTC_H` float(11,2) DEFAULT NULL COMMENT '高度',
  `FTC_W` float(11,2) DEFAULT NULL COMMENT '宽度',
  `SelectAccepterLab` varchar(50) DEFAULT '接受人' COMMENT '接受人按钮标签',
  `SelectAccepterEnable` int(11) DEFAULT '0' COMMENT '方式',
  `BatchLab` varchar(50) DEFAULT '批量审核' COMMENT '批量审核标签',
  `BatchEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `OfficeOpenLab` varchar(50) DEFAULT '打开本地' COMMENT '打开本地标签',
  `OfficeOpenEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `OfficeOpenTemplateLab` varchar(50) DEFAULT '打开模板' COMMENT '打开模板标签',
  `OfficeOpenTemplateEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `OfficeSaveLab` varchar(50) DEFAULT '保存' COMMENT '保存标签',
  `OfficeSaveEnable` int(11) DEFAULT '1' COMMENT '是否启用',
  `OfficeAcceptLab` varchar(50) DEFAULT '接受修订' COMMENT '接受修订标签',
  `OfficeAcceptEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `OfficeRefuseLab` varchar(50) DEFAULT '拒绝修订' COMMENT '拒绝修订标签',
  `OfficeRefuseEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `OfficeOverLab` varchar(50) DEFAULT '套红' COMMENT '套红标签',
  `OfficeOverEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `OfficeMarksEnable` int(11) DEFAULT '1' COMMENT '是否查看用户留痕',
  `OfficePrintLab` varchar(50) DEFAULT '打印' COMMENT '打印标签',
  `OfficePrintEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `OfficeSealLab` varchar(50) DEFAULT '签章' COMMENT '签章标签',
  `OfficeSealEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `OfficeInsertFlowLab` varchar(50) DEFAULT '插入流程' COMMENT '插入流程标签',
  `OfficeInsertFlowEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `OfficeNodeInfo` int(11) DEFAULT '0' COMMENT '是否记录节点信息',
  `OfficeReSavePDF` int(11) DEFAULT '0' COMMENT '是否该自动保存为PDF',
  `OfficeDownLab` varchar(50) DEFAULT '下载' COMMENT '下载按钮标签',
  `OfficeDownEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `OfficeIsMarks` int(11) DEFAULT '1' COMMENT '是否进入留痕模式',
  `OfficeTemplate` varchar(100) DEFAULT '' COMMENT '指定文档模板',
  `OfficeIsParent` int(11) DEFAULT '1' COMMENT '是否使用父流程的文档',
  `OfficeTHEnable` int(11) DEFAULT '0' COMMENT '是否自动套红',
  `OfficeTHTemplate` varchar(200) DEFAULT '' COMMENT '自动套红模板',
  `SelectorModel` int(11) DEFAULT NULL COMMENT '显示方式,枚举类型:0 按岗位;1 按部门;2 按人员;3 按SQL;4 按SQL模版计算;5 使用通用人员选择器;6 部门与岗位的交集;7 自定义Url;8 使用通用部门岗位人员选择器;',
  `FK_SQLTemplate` varchar(50) DEFAULT NULL COMMENT 'SQL模版',
  `FK_SQLTemplateText` varchar(50) DEFAULT NULL COMMENT 'SQL模版',
  `IsAutoLoadEmps` int(11) DEFAULT NULL COMMENT '是否自动加载上一次选择的人员？',
  `IsSimpleSelector` int(11) DEFAULT NULL COMMENT '是否单项选择(只能选择一个人)？',
  `IsEnableDeptRange` int(11) DEFAULT '0' COMMENT '是否启用部门搜索范围限定(对使用通用人员选择器有效)？',
  `IsEnableStaRange` int(11) DEFAULT '0' COMMENT '是否启用岗位搜索范围限定(对使用通用人员选择器有效)？',
  `SelectorP1` text COMMENT '分组参数:可以为空,比如:SELECT No,Name,ParentNo FROM  Port_Dept',
  `SelectorP2` text COMMENT '操作员数据源:比如:SELECT No,Name,FK_Dept FROM  Port_Emp',
  `SelectorP3` text COMMENT '默认选择的数据源:比如:SELECT FK_Emp FROM  WF_GenerWorkerList WHERE FK_Node=102 AND WorkID=@WorkID',
  `SelectorP4` text COMMENT '强制选择的数据源:比如:SELECT FK_Emp FROM  WF_GenerWorkerList WHERE FK_Node=102 AND WorkID=@WorkID',
  `X` int(11) DEFAULT NULL COMMENT 'X坐标',
  `Y` int(11) DEFAULT NULL COMMENT 'Y坐标',
  `OfficeOpen` varchar(50) DEFAULT '打开本地' COMMENT '打开本地标签',
  `OfficeOpenTemplate` varchar(50) DEFAULT '打开模板' COMMENT '打开模板标签',
  `OfficeSave` varchar(50) DEFAULT '保存' COMMENT '保存标签',
  `OfficeAccept` varchar(50) DEFAULT '接受修订' COMMENT '接受修订标签',
  `OfficeRefuse` varchar(50) DEFAULT '拒绝修订' COMMENT '拒绝修订标签',
  `OfficeOver` varchar(50) DEFAULT '套红按钮' COMMENT '套红按钮标签',
  `OfficeMarks` int(11) DEFAULT '1' COMMENT '是否查看用户留痕',
  `OfficeReadOnly` int(11) DEFAULT '0' COMMENT '是否只读',
  `OfficePrint` varchar(50) DEFAULT '打印按钮' COMMENT '打印按钮标签',
  `OfficeSeal` varchar(50) DEFAULT '签章按钮' COMMENT '签章按钮标签',
  `OfficeInsertFlow` varchar(50) DEFAULT '插入流程' COMMENT '插入流程标签',
  `OfficeIsTrueTH` int(11) DEFAULT '0' COMMENT '是否自动套红',
  `WebOfficeFrmModel` int(11) DEFAULT '0' COMMENT '表单工作方式',
  `ICON` varchar(70) DEFAULT NULL COMMENT '节点ICON图片路径',
  `NodeWorkType` int(11) DEFAULT NULL COMMENT '节点类型',
  `FlowName` varchar(200) DEFAULT NULL COMMENT '流程名',
  `FrmAttr` varchar(300) DEFAULT NULL COMMENT 'FrmAttr',
  `TimeLimit` float(11,2) DEFAULT NULL COMMENT '限期(天)',
  `TWay` int(11) DEFAULT NULL COMMENT '时间计算方式',
  `TAlertRole` int(11) DEFAULT NULL COMMENT '逾期提醒规则',
  `TAlertWay` int(11) DEFAULT NULL COMMENT '逾期提醒方式',
  `WarningDay` float(11,2) DEFAULT NULL COMMENT '工作预警(天)',
  `WAlertRole` int(11) DEFAULT NULL COMMENT '预警提醒规则',
  `WAlertWay` int(11) DEFAULT NULL COMMENT '预警提醒方式',
  `TCent` float(11,2) DEFAULT NULL COMMENT '扣分(每延期1小时)',
  `CHWay` int(11) DEFAULT NULL COMMENT '考核方式',
  `IsEval` int(11) DEFAULT NULL COMMENT '是否工作质量考核',
  `OutTimeDeal` int(11) DEFAULT NULL COMMENT '超时处理方式',
  `DoOutTime` varchar(300) DEFAULT NULL COMMENT '超时处理内容',
  `Doc` varchar(100) DEFAULT NULL COMMENT '描述',
  `IsExpSender` int(11) DEFAULT NULL COMMENT '本节点接收人不允许包含上一步发送人',
  `DeliveryParas` varchar(600) DEFAULT NULL COMMENT '访问规则设置',
  `NodeFrmID` varchar(50) DEFAULT NULL COMMENT '节点表单ID',
  `IsCanDelFlow` int(11) DEFAULT NULL COMMENT '是否可以删除流程',
  `TodolistModel` int(11) DEFAULT NULL COMMENT '多人处理规则',
  `TeamLeaderConfirmRole` int(11) DEFAULT NULL COMMENT '组长确认规则',
  `TeamLeaderConfirmDoc` varchar(100) DEFAULT NULL COMMENT '组长确认设置内容',
  `IsHandOver` int(11) DEFAULT NULL COMMENT '是否可以移交',
  `BlockModel` int(11) DEFAULT NULL COMMENT '阻塞模式',
  `BlockExp` varchar(200) DEFAULT NULL COMMENT '阻塞表达式',
  `BlockAlert` varchar(100) DEFAULT NULL COMMENT '被阻塞提示信息',
  `BatchRole` int(11) DEFAULT NULL COMMENT '批处理',
  `BatchListCount` int(11) DEFAULT NULL COMMENT '批处理数量',
  `BatchParas` varchar(500) DEFAULT NULL COMMENT '参数',
  `FormType` int(11) DEFAULT NULL COMMENT '表单类型',
  `FormUrl` varchar(300) DEFAULT NULL COMMENT '表单URL',
  `TurnToDeal` int(11) DEFAULT NULL COMMENT '转向处理',
  `TurnToDealDoc` varchar(200) DEFAULT NULL COMMENT '发送后提示信息',
  `NodePosType` int(11) DEFAULT NULL COMMENT '位置',
  `IsCCFlow` int(11) DEFAULT NULL COMMENT '是否有流程完成条件',
  `HisStas` varchar(3000) DEFAULT NULL COMMENT '岗位',
  `HisDeptStrs` varchar(3000) DEFAULT NULL COMMENT '部门',
  `HisToNDs` varchar(50) DEFAULT NULL COMMENT '转到的节点',
  `HisBillIDs` varchar(50) DEFAULT NULL COMMENT '单据IDs',
  `HisSubFlows` varchar(30) DEFAULT NULL COMMENT 'HisSubFlows',
  `PTable` varchar(100) DEFAULT NULL COMMENT '物理表',
  `ShowSheets` varchar(100) DEFAULT NULL COMMENT '显示的表单',
  `GroupStaNDs` varchar(100) DEFAULT NULL COMMENT '岗位分组节点',
  `RefOneFrmTreeType` varchar(100) DEFAULT NULL COMMENT '独立表单类型',
  `AtPara` varchar(500) DEFAULT NULL COMMENT 'AtPara',
  `TSpanHour` float(50,2) DEFAULT NULL COMMENT '小时',
  PRIMARY KEY (`NodeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='选择器';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_node`
--

LOCK TABLES `wf_node` WRITE;
/*!40000 ALTER TABLE `wf_node` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_node` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_nodecancel`
--

DROP TABLE IF EXISTS `wf_nodecancel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wf_nodecancel` (
  `FK_Node` int(11) NOT NULL COMMENT '节点 - 主键',
  `CancelTo` int(11) NOT NULL COMMENT '撤销到 - 主键',
  PRIMARY KEY (`FK_Node`,`CancelTo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='可撤销的节点';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_nodecancel`
--

LOCK TABLES `wf_nodecancel` WRITE;
/*!40000 ALTER TABLE `wf_nodecancel` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_nodecancel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_nodedept`
--

DROP TABLE IF EXISTS `wf_nodedept`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wf_nodedept` (
  `FK_Node` int(11) NOT NULL COMMENT '节点 - 主键',
  `FK_Dept` varchar(100) NOT NULL COMMENT '部门,主外键:对应物理表:Port_Dept,表描述:部门',
  PRIMARY KEY (`FK_Node`,`FK_Dept`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='节点部门';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_nodedept`
--

LOCK TABLES `wf_nodedept` WRITE;
/*!40000 ALTER TABLE `wf_nodedept` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_nodedept` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_nodeemp`
--

DROP TABLE IF EXISTS `wf_nodeemp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wf_nodeemp` (
  `FK_Node` int(11) NOT NULL COMMENT 'Node - 主键',
  `FK_Emp` varchar(100) NOT NULL COMMENT '到人员,主外键:对应物理表:Port_Emp,表描述:用户',
  PRIMARY KEY (`FK_Node`,`FK_Emp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='节点人员';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_nodeemp`
--

LOCK TABLES `wf_nodeemp` WRITE;
/*!40000 ALTER TABLE `wf_nodeemp` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_nodeemp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_nodeflow`
--

DROP TABLE IF EXISTS `wf_nodeflow`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wf_nodeflow` (
  `FK_Node` int(11) NOT NULL COMMENT '节点 - 主键',
  `FK_Flow` varchar(100) NOT NULL COMMENT '子流程,主外键:对应物理表:WF_Flow,表描述:流程',
  PRIMARY KEY (`FK_Node`,`FK_Flow`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='节点调用子流程';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_nodeflow`
--

LOCK TABLES `wf_nodeflow` WRITE;
/*!40000 ALTER TABLE `wf_nodeflow` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_nodeflow` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_nodereturn`
--

DROP TABLE IF EXISTS `wf_nodereturn`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wf_nodereturn` (
  `FK_Node` int(11) NOT NULL COMMENT '节点 - 主键',
  `ReturnTo` int(11) NOT NULL COMMENT '退回到 - 主键',
  `Dots` varchar(300) DEFAULT NULL COMMENT '轨迹信息',
  PRIMARY KEY (`FK_Node`,`ReturnTo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='可退回的节点';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_nodereturn`
--

LOCK TABLES `wf_nodereturn` WRITE;
/*!40000 ALTER TABLE `wf_nodereturn` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_nodereturn` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_nodestation`
--

DROP TABLE IF EXISTS `wf_nodestation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wf_nodestation` (
  `FK_Node` int(11) NOT NULL COMMENT '节点 - 主键',
  `FK_Station` varchar(100) NOT NULL COMMENT '工作岗位,主外键:对应物理表:Port_Station,表描述:岗位',
  PRIMARY KEY (`FK_Node`,`FK_Station`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='节点岗位';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_nodestation`
--

LOCK TABLES `wf_nodestation` WRITE;
/*!40000 ALTER TABLE `wf_nodestation` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_nodestation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_nodesubflow`
--

DROP TABLE IF EXISTS `wf_nodesubflow`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wf_nodesubflow` (
  `OID` int(11) NOT NULL COMMENT 'OID - 主键',
  `FK_Node` int(11) DEFAULT NULL COMMENT '节点',
  `FK_Flow` varchar(100) DEFAULT NULL COMMENT '延续子流程,外键:对应物理表:WF_Flow,表描述:流程',
  `Idx` int(11) DEFAULT NULL COMMENT '显示顺序',
  `ExpType` int(11) DEFAULT NULL COMMENT '表达式类型,枚举类型:3 按照SQL计算;4 按照参数计算;',
  `CondExp` varchar(500) DEFAULT NULL COMMENT '条件表达式',
  `YBFlowReturnRole` int(11) DEFAULT NULL COMMENT '退回方式,枚举类型:0 不能退回;1 退回到父流程的开始节点;2 退回到父流程的任何节点;3 退回父流程的启动节点;4 可退回到指定的节点;',
  `ReturnToNode` varchar(50) DEFAULT NULL COMMENT '要退回的节点',
  `ReturnToNodeText` varchar(50) DEFAULT NULL COMMENT '要退回的节点',
  PRIMARY KEY (`OID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='延续子流程';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_nodesubflow`
--

LOCK TABLES `wf_nodesubflow` WRITE;
/*!40000 ALTER TABLE `wf_nodesubflow` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_nodesubflow` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_nodetoolbar`
--

DROP TABLE IF EXISTS `wf_nodetoolbar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wf_nodetoolbar` (
  `OID` int(11) NOT NULL COMMENT 'OID - 主键',
  `Title` varchar(100) DEFAULT NULL COMMENT '标题',
  `Target` varchar(100) DEFAULT NULL COMMENT '目标',
  `Url` varchar(500) DEFAULT NULL COMMENT '连接',
  `ShowWhere` int(11) DEFAULT NULL COMMENT '显示位置,枚举类型:0 树形表单;1 工具栏;',
  `Idx` int(11) DEFAULT NULL COMMENT '显示顺序',
  `FK_Node` int(11) DEFAULT NULL COMMENT '节点',
  `MyFileName` varchar(300) DEFAULT NULL COMMENT '图标',
  `MyFilePath` varchar(300) DEFAULT NULL COMMENT 'MyFilePath',
  `MyFileExt` varchar(20) DEFAULT NULL COMMENT 'MyFileExt',
  `WebPath` varchar(300) DEFAULT NULL COMMENT 'WebPath',
  `MyFileH` int(11) DEFAULT NULL COMMENT 'MyFileH',
  `MyFileW` int(11) DEFAULT NULL COMMENT 'MyFileW',
  `MyFileSize` float DEFAULT NULL COMMENT 'MyFileSize',
  PRIMARY KEY (`OID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='自定义工具栏';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_nodetoolbar`
--

LOCK TABLES `wf_nodetoolbar` WRITE;
/*!40000 ALTER TABLE `wf_nodetoolbar` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_nodetoolbar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_pushmsg`
--

DROP TABLE IF EXISTS `wf_pushmsg`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wf_pushmsg` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `FK_Flow` varchar(3) DEFAULT NULL COMMENT '流程',
  `FK_Node` int(11) DEFAULT NULL COMMENT '节点',
  `FK_Event` varchar(15) DEFAULT NULL COMMENT '事件类型',
  `PushWay` int(11) DEFAULT NULL COMMENT '推送方式,枚举类型:0 按照指定节点的工作人员;1 按照指定的工作人员;2 按照指定的工作岗位;3 按照指定的部门;4 按照指定的SQL;5 按照系统指定的字段;',
  `PushDoc` text COMMENT '推送保存内容',
  `Tag` varchar(500) DEFAULT NULL COMMENT 'Tag',
  `SMSPushWay` int(11) DEFAULT NULL COMMENT '短信发送方式',
  `SMSField` varchar(100) DEFAULT NULL COMMENT '短信字段',
  `SMSDoc` text COMMENT '短信内容模版',
  `SMSNodes` varchar(100) DEFAULT NULL COMMENT 'SMS节点s',
  `MailPushWay` int(11) DEFAULT NULL COMMENT '邮件发送方式',
  `MailAddress` varchar(100) DEFAULT NULL COMMENT '邮件字段',
  `MailTitle` varchar(200) DEFAULT NULL COMMENT '邮件标题模版',
  `MailDoc` text COMMENT '邮件内容模版',
  `MailNodes` varchar(100) DEFAULT NULL COMMENT 'Mail节点s',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='消息推送';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_pushmsg`
--

LOCK TABLES `wf_pushmsg` WRITE;
/*!40000 ALTER TABLE `wf_pushmsg` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_pushmsg` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_rememberme`
--

DROP TABLE IF EXISTS `wf_rememberme`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wf_rememberme` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `FK_Node` int(11) DEFAULT NULL COMMENT '节点',
  `FK_Emp` varchar(30) DEFAULT NULL COMMENT '当前操作人员',
  `Objs` text COMMENT '分配人员',
  `ObjsExt` text COMMENT '分配人员Ext',
  `Emps` text COMMENT '所有的工作人员',
  `EmpsExt` text COMMENT '工作人员Ext',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='记忆我';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_rememberme`
--

LOCK TABLES `wf_rememberme` WRITE;
/*!40000 ALTER TABLE `wf_rememberme` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_rememberme` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_returnwork`
--

DROP TABLE IF EXISTS `wf_returnwork`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wf_returnwork` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `WorkID` int(11) DEFAULT NULL COMMENT 'WorkID',
  `ReturnNode` int(11) DEFAULT NULL COMMENT '退回节点',
  `ReturnNodeName` varchar(200) DEFAULT NULL COMMENT '退回节点名称',
  `Returner` varchar(20) DEFAULT NULL COMMENT '退回人',
  `ReturnerName` varchar(200) DEFAULT NULL COMMENT '退回人名称',
  `ReturnToNode` int(11) DEFAULT NULL COMMENT 'ReturnToNode',
  `ReturnToEmp` text COMMENT '退回给',
  `BeiZhu` text COMMENT '退回原因',
  `RDT` varchar(50) DEFAULT NULL COMMENT '退回日期',
  `IsBackTracking` int(11) DEFAULT NULL COMMENT '是否要原路返回?',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='退回轨迹';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_returnwork`
--

LOCK TABLES `wf_returnwork` WRITE;
/*!40000 ALTER TABLE `wf_returnwork` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_returnwork` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_selectaccper`
--

DROP TABLE IF EXISTS `wf_selectaccper`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wf_selectaccper` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `FK_Node` int(11) DEFAULT NULL COMMENT '接受人节点',
  `WorkID` int(11) DEFAULT NULL COMMENT 'WorkID',
  `FK_Emp` varchar(20) DEFAULT NULL COMMENT 'FK_Emp',
  `EmpName` varchar(20) DEFAULT NULL COMMENT 'EmpName',
  `DeptName` varchar(400) DEFAULT NULL COMMENT '部门名称',
  `AccType` int(11) DEFAULT NULL COMMENT '类型(@0=接受人@1=抄送人)',
  `Rec` varchar(20) DEFAULT NULL COMMENT '记录人',
  `Info` varchar(200) DEFAULT NULL COMMENT '办理意见信息',
  `IsRemember` int(11) DEFAULT NULL COMMENT '以后发送是否按本次计算',
  `Idx` int(11) DEFAULT NULL COMMENT '顺序号(可以用于流程队列审核模式)',
  `Tag` varchar(200) DEFAULT NULL COMMENT '维度信息Tag',
  `TimeLimit` int(11) DEFAULT NULL COMMENT '时限-天',
  `TSpanHour` float DEFAULT NULL COMMENT '时限-小时',
  `ADT` varchar(50) DEFAULT NULL COMMENT '到达日期(计划)',
  `SDT` varchar(50) DEFAULT NULL COMMENT '应完成日期(计划)',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='选择接受/抄送人信息';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_selectaccper`
--

LOCK TABLES `wf_selectaccper` WRITE;
/*!40000 ALTER TABLE `wf_selectaccper` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_selectaccper` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_selectinfo`
--

DROP TABLE IF EXISTS `wf_selectinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wf_selectinfo` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `AcceptNodeID` int(11) DEFAULT NULL COMMENT '接受节点',
  `WorkID` int(11) DEFAULT NULL COMMENT '工作ID',
  `InfoLeft` varchar(200) DEFAULT NULL COMMENT 'InfoLeft',
  `InfoCenter` varchar(200) DEFAULT NULL COMMENT 'InfoCenter',
  `InfoRight` varchar(200) DEFAULT NULL COMMENT 'InfoLeft',
  `AccType` int(11) DEFAULT NULL COMMENT '类型(@0=接受人@1=抄送人)',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='选择接受/抄送人节点信息';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_selectinfo`
--

LOCK TABLES `wf_selectinfo` WRITE;
/*!40000 ALTER TABLE `wf_selectinfo` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_selectinfo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_shiftwork`
--

DROP TABLE IF EXISTS `wf_shiftwork`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wf_shiftwork` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `WorkID` int(11) DEFAULT NULL COMMENT '工作ID',
  `FK_Node` int(11) DEFAULT NULL COMMENT 'FK_Node',
  `FK_Emp` varchar(40) DEFAULT NULL COMMENT '移交人',
  `FK_EmpName` varchar(40) DEFAULT NULL COMMENT '移交人名称',
  `ToEmp` varchar(40) DEFAULT NULL COMMENT '移交给',
  `ToEmpName` varchar(40) DEFAULT NULL COMMENT '移交给名称',
  `RDT` varchar(50) DEFAULT NULL COMMENT '移交时间',
  `Note` varchar(2000) DEFAULT NULL COMMENT '移交原因',
  `IsRead` int(11) DEFAULT NULL COMMENT '是否读取？',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='移交记录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_shiftwork`
--

LOCK TABLES `wf_shiftwork` WRITE;
/*!40000 ALTER TABLE `wf_shiftwork` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_shiftwork` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_sqltemplate`
--

DROP TABLE IF EXISTS `wf_sqltemplate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wf_sqltemplate` (
  `No` varchar(3) NOT NULL COMMENT '编号 - 主键',
  `SQLType` int(11) DEFAULT NULL COMMENT '模版SQL类型,枚举类型:0 方向条件;1 接受人规则;2 下拉框数据过滤;3 级联下拉框;4 PopVal开窗返回值;5 人员选择器人员选择范围;',
  `Name` varchar(200) DEFAULT NULL COMMENT 'SQL说明',
  `Docs` text COMMENT 'SQL模版',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='SQL模板';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_sqltemplate`
--

LOCK TABLES `wf_sqltemplate` WRITE;
/*!40000 ALTER TABLE `wf_sqltemplate` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_sqltemplate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_task`
--

DROP TABLE IF EXISTS `wf_task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wf_task` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `FK_Flow` varchar(200) DEFAULT NULL COMMENT '流程编号',
  `Starter` varchar(200) DEFAULT NULL COMMENT '发起人',
  `Paras` text COMMENT '参数',
  `TaskSta` int(11) DEFAULT NULL COMMENT '任务状态',
  `Msg` text COMMENT '消息',
  `StartDT` varchar(20) DEFAULT NULL COMMENT '发起时间',
  `RDT` varchar(20) DEFAULT NULL COMMENT '插入数据时间',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='任务';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_task`
--

LOCK TABLES `wf_task` WRITE;
/*!40000 ALTER TABLE `wf_task` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_task` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_testapi`
--

DROP TABLE IF EXISTS `wf_testapi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wf_testapi` (
  `No` varchar(92) NOT NULL COMMENT '编号',
  `Name` varchar(50) DEFAULT NULL COMMENT '名称',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_testapi`
--

LOCK TABLES `wf_testapi` WRITE;
/*!40000 ALTER TABLE `wf_testapi` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_testapi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_testcase`
--

DROP TABLE IF EXISTS `wf_testcase`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wf_testcase` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `FK_Flow` varchar(100) DEFAULT NULL COMMENT '流程编号',
  `ParaType` varchar(100) DEFAULT NULL COMMENT '参数类型',
  `Vals` varchar(500) DEFAULT NULL COMMENT '值s',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_testcase`
--

LOCK TABLES `wf_testcase` WRITE;
/*!40000 ALTER TABLE `wf_testcase` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_testcase` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_testsample`
--

DROP TABLE IF EXISTS `wf_testsample`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wf_testsample` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `Name` varchar(50) DEFAULT NULL COMMENT '测试名称',
  `FK_API` varchar(100) DEFAULT NULL COMMENT '测试的API',
  `FK_Ver` varchar(100) DEFAULT NULL COMMENT '测试的版本',
  `DTFrom` varchar(50) DEFAULT NULL COMMENT '从',
  `DTTo` varchar(50) DEFAULT NULL COMMENT '到',
  `TimeUse` float DEFAULT NULL COMMENT '用时(毫秒)',
  `TimesPerSecond` float DEFAULT NULL COMMENT '每秒跑多少个?',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_testsample`
--

LOCK TABLES `wf_testsample` WRITE;
/*!40000 ALTER TABLE `wf_testsample` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_testsample` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_testver`
--

DROP TABLE IF EXISTS `wf_testver`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wf_testver` (
  `No` varchar(92) NOT NULL COMMENT '编号',
  `Name` varchar(50) DEFAULT NULL COMMENT '名称',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_testver`
--

LOCK TABLES `wf_testver` WRITE;
/*!40000 ALTER TABLE `wf_testver` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_testver` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_transfercustom`
--

DROP TABLE IF EXISTS `wf_transfercustom`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wf_transfercustom` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `WorkID` int(11) DEFAULT NULL COMMENT 'WorkID',
  `FK_Node` int(11) DEFAULT NULL COMMENT '节点ID',
  `Worker` varchar(200) DEFAULT NULL COMMENT '处理人(多个人用逗号分开)',
  `WorkerName` varchar(200) DEFAULT NULL COMMENT '处理人(多个人用逗号分开)',
  `SubFlowNo` varchar(3) DEFAULT NULL COMMENT '要经过的子流程编号',
  `PlanDT` varchar(50) DEFAULT NULL COMMENT '计划完成日期',
  `TodolistModel` int(11) DEFAULT NULL COMMENT '多人工作处理模式',
  `Idx` int(11) DEFAULT NULL COMMENT '顺序号',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='自定义运行路径';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_transfercustom`
--

LOCK TABLES `wf_transfercustom` WRITE;
/*!40000 ALTER TABLE `wf_transfercustom` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_transfercustom` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_turnto`
--

DROP TABLE IF EXISTS `wf_turnto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wf_turnto` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `TurnToType` int(11) DEFAULT NULL COMMENT '条件类型',
  `FK_Flow` varchar(60) DEFAULT NULL COMMENT '流程',
  `FK_Node` int(11) DEFAULT NULL COMMENT '节点ID',
  `FK_Attr` varchar(80) DEFAULT NULL COMMENT '属性外键Sys_MapAttr',
  `AttrKey` varchar(80) DEFAULT NULL COMMENT '键值',
  `AttrT` varchar(80) DEFAULT NULL COMMENT '属性名称',
  `FK_Operator` varchar(60) DEFAULT NULL COMMENT '运算符号',
  `OperatorValue` varchar(60) DEFAULT NULL COMMENT '要运算的值',
  `OperatorValueT` varchar(60) DEFAULT NULL COMMENT '要运算的值T',
  `TurnToURL` varchar(700) DEFAULT NULL COMMENT '要转向的URL',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='转向条件';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_turnto`
--

LOCK TABLES `wf_turnto` WRITE;
/*!40000 ALTER TABLE `wf_turnto` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_turnto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wf_workflowdeletelog`
--

DROP TABLE IF EXISTS `wf_workflowdeletelog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wf_workflowdeletelog` (
  `OID` int(11) NOT NULL COMMENT 'OID - 主键',
  `FID` int(11) DEFAULT NULL COMMENT 'FID',
  `FK_Dept` varchar(100) DEFAULT NULL COMMENT '部门,外键:对应物理表:Port_Dept,表描述:部门',
  `Title` varchar(100) DEFAULT NULL COMMENT '标题',
  `FlowStarter` varchar(100) DEFAULT NULL COMMENT '发起人',
  `FlowStartRDT` varchar(50) DEFAULT NULL COMMENT '发起时间',
  `FK_NY` varchar(100) DEFAULT NULL COMMENT '年月,外键:对应物理表:Pub_NY,表描述:年月',
  `FK_Flow` varchar(100) DEFAULT NULL COMMENT '流程,外键:对应物理表:WF_Flow,表描述:流程',
  `FlowEnderRDT` varchar(50) DEFAULT NULL COMMENT '最后处理时间',
  `FlowEndNode` int(11) DEFAULT NULL COMMENT '结束节点',
  `FlowDaySpan` float DEFAULT NULL COMMENT '跨度(天)',
  `FlowEmps` varchar(100) DEFAULT NULL COMMENT '参与人',
  `Oper` varchar(20) DEFAULT NULL COMMENT '删除人员',
  `OperDept` varchar(20) DEFAULT NULL COMMENT '删除人员部门',
  `OperDeptName` varchar(200) DEFAULT NULL COMMENT '删除人员名称',
  `DeleteNote` text COMMENT '删除原因',
  `DeleteDT` varchar(50) DEFAULT NULL COMMENT '删除日期',
  PRIMARY KEY (`OID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='流程删除日志';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wf_workflowdeletelog`
--

LOCK TABLES `wf_workflowdeletelog` WRITE;
/*!40000 ALTER TABLE `wf_workflowdeletelog` DISABLE KEYS */;
/*!40000 ALTER TABLE `wf_workflowdeletelog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Final view structure for view `port_dept`
--

/*!50001 DROP VIEW IF EXISTS `port_dept`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `port_dept` AS select `o`.`office_code` AS `No`,`o`.`office_name` AS `Name`,'' AS `NameOfPath`,(case when (`o`.`parent_code` = '0') then '0' else `o`.`parent_code` end) AS `ParentNo`,'' AS `TreeNo`,'admin' AS `Leader`,'' AS `Tel`,`o`.`tree_sort` AS `Idx`,(case when (`o`.`tree_leaf` = '0') then 1 else 0 end) AS `IsDir`,'' AS `OrgNo` from `js_sys_office` `o` where (`o`.`status` = '0') */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `port_emp`
--

/*!50001 DROP VIEW IF EXISTS `port_emp`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `port_emp` AS select `u`.`user_code` AS `No`,`u`.`user_name` AS `Name`,`u`.`password` AS `Pass`,(case when (`e`.`office_code` <> '') then `e`.`office_code` else (select `oo`.`office_code` from `js_sys_office` `oo` where (`oo`.`parent_code` = '0') limit 1) end) AS `FK_Dept`,`u`.`extend_s1` AS `SID`,`u`.`mobile` AS `Tel`,`u`.`email` AS `Email`,`u`.`PinYin` AS `PinYin`,0 AS `SignType`,0 AS `Idx` from (`js_sys_user` `u` left join `js_sys_employee` `e` on((`e`.`emp_code` = `u`.`ref_code`))) where (((`u`.`status` = '0') and (`u`.`user_type` = 'employee') and (`e`.`status` = '0')) or (`u`.`login_code` = 'system') or (`u`.`login_code` = 'admin')) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `port_empstation`
--

/*!50001 DROP VIEW IF EXISTS `port_empstation`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `port_empstation` AS select `ur`.`user_code` AS `FK_Emp`,`ur`.`role_code` AS `FK_Station` from `js_sys_user_role` `ur` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `port_station`
--

/*!50001 DROP VIEW IF EXISTS `port_station`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `port_station` AS select `r`.`role_code` AS `No`,`r`.`role_name` AS `Name`,`r`.`role_type` AS `FK_StationType`,'' AS `DutyReq`,'' AS `Makings`,'' AS `OrgNo` from `js_sys_role` `r` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `port_stationtype`
--

/*!50001 DROP VIEW IF EXISTS `port_stationtype`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `port_stationtype` AS select `d`.`dict_value` AS `No`,`d`.`dict_label` AS `Name`,'' AS `Idx` from `js_sys_dict_data` `d` where (`d`.`dict_type` = 'sys_role_type') */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_flowstarter`
--

/*!50001 DROP VIEW IF EXISTS `v_flowstarter`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_flowstarter` AS select `a`.`FK_Flow` AS `FK_Flow`,`a`.`FlowName` AS `FlowName`,`c`.`FK_Emp` AS `FK_Emp` from ((`wf_node` `a` join `wf_nodestation` `b`) join `port_empstation` `c`) where ((`a`.`NodePosType` = 0) and ((`a`.`WhoExeIt` = 0) or (`a`.`WhoExeIt` = 2)) and (`a`.`NodeID` = `b`.`FK_Node`) and (`b`.`FK_Station` = `c`.`FK_Station`) and ((`a`.`DeliveryWay` = 0) or (`a`.`DeliveryWay` = 14))) union select `a`.`FK_Flow` AS `FK_Flow`,`a`.`FlowName` AS `FlowName`,`c`.`No` AS `No` from ((`wf_node` `a` join `wf_nodedept` `b`) join `port_emp` `c`) where ((`a`.`NodePosType` = 0) and ((`a`.`WhoExeIt` = 0) or (`a`.`WhoExeIt` = 2)) and (`a`.`NodeID` = `b`.`FK_Node`) and (`b`.`FK_Dept` = `c`.`FK_Dept`) and (`a`.`DeliveryWay` = 1)) union select `a`.`FK_Flow` AS `FK_Flow`,`a`.`FlowName` AS `FlowName`,`b`.`FK_Emp` AS `FK_Emp` from (`wf_node` `a` join `wf_nodeemp` `b`) where ((`a`.`NodePosType` = 0) and ((`a`.`WhoExeIt` = 0) or (`a`.`WhoExeIt` = 2)) and (`a`.`NodeID` = `b`.`FK_Node`) and (`a`.`DeliveryWay` = 3)) union select `a`.`FK_Flow` AS `FK_Flow`,`a`.`FlowName` AS `FlowName`,`b`.`No` AS `FK_Emp` from (`wf_node` `a` join `port_emp` `b`) where ((`a`.`NodePosType` = 0) and ((`a`.`WhoExeIt` = 0) or (`a`.`WhoExeIt` = 2)) and (`a`.`DeliveryWay` = 4)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_flowstarterbpm`
--

/*!50001 DROP VIEW IF EXISTS `v_flowstarterbpm`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_flowstarterbpm` AS select `a`.`FK_Flow` AS `FK_Flow`,`a`.`FlowName` AS `FlowName`,`c`.`FK_Emp` AS `FK_Emp` from ((`wf_node` `a` join `wf_nodestation` `b`) join `port_deptempstation` `c`) where ((`a`.`NodePosType` = 0) and ((`a`.`WhoExeIt` = 0) or (`a`.`WhoExeIt` = 2)) and (`a`.`NodeID` = `b`.`FK_Node`) and (`b`.`FK_Station` = `c`.`FK_Station`) and ((`a`.`DeliveryWay` = 0) or (`a`.`DeliveryWay` = 14))) union select `a`.`FK_Flow` AS `FK_Flow`,`a`.`FlowName` AS `FlowName`,`c`.`FK_Emp` AS `FK_Emp` from ((`wf_node` `a` join `wf_nodedept` `b`) join `port_deptemp` `c`) where ((`a`.`NodePosType` = 0) and ((`a`.`WhoExeIt` = 0) or (`a`.`WhoExeIt` = 2)) and (`a`.`NodeID` = `b`.`FK_Node`) and (`b`.`FK_Dept` = `c`.`FK_Dept`) and (`a`.`DeliveryWay` = 1)) union select `a`.`FK_Flow` AS `FK_Flow`,`a`.`FlowName` AS `FlowName`,`b`.`FK_Emp` AS `FK_Emp` from (`wf_node` `a` join `wf_nodeemp` `b`) where ((`a`.`NodePosType` = 0) and ((`a`.`WhoExeIt` = 0) or (`a`.`WhoExeIt` = 2)) and (`a`.`NodeID` = `b`.`FK_Node`) and (`a`.`DeliveryWay` = 3)) union select `a`.`FK_Flow` AS `FK_Flow`,`a`.`FlowName` AS `FlowName`,`b`.`No` AS `FK_Emp` from (`wf_node` `a` join `port_emp` `b`) where ((`a`.`NodePosType` = 0) and ((`a`.`WhoExeIt` = 0) or (`a`.`WhoExeIt` = 2)) and (`a`.`DeliveryWay` = 4)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_totalch`
--

/*!50001 DROP VIEW IF EXISTS `v_totalch`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_totalch` AS select `wf_ch`.`FK_Emp` AS `FK_Emp`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`)) AS `AllNum`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` <= 1) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`))) AS `ASNum`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` >= 2) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`))) AS `CSNum`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` = 0) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`))) AS `JiShi`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` = 1) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`))) AS `ANQI`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` = 2) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`))) AS `YuQi`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` = 3) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`))) AS `ChaoQi`,round((((select cast(count(`a`.`MyPK`) as decimal(10,0)) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` <= 1) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`))) / (select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`))) * 100),2) AS `WCRate` from `wf_ch` group by `wf_ch`.`FK_Emp` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_totalchweek`
--

/*!50001 DROP VIEW IF EXISTS `v_totalchweek`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_totalchweek` AS select `wf_ch`.`FK_Emp` AS `FK_Emp`,`wf_ch`.`WeekNum` AS `WeekNum`,`wf_ch`.`FK_NY` AS `FK_NY`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`WeekNum` = `wf_ch`.`WeekNum`))) AS `AllNum`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` <= 1) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`WeekNum` = `wf_ch`.`WeekNum`))) AS `ASNum`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` >= 2) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`WeekNum` = `wf_ch`.`WeekNum`))) AS `CSNum`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` = 0) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`WeekNum` = `wf_ch`.`WeekNum`))) AS `JiShi`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` = 1) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`WeekNum` = `wf_ch`.`WeekNum`))) AS `AnQi`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` = 2) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`WeekNum` = `wf_ch`.`WeekNum`))) AS `YuQi`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` = 3) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`WeekNum` = `wf_ch`.`WeekNum`))) AS `ChaoQi`,round((((select cast(count(`a`.`MyPK`) as decimal(10,0)) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` <= 1) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`WeekNum` = `wf_ch`.`WeekNum`))) / (select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`WeekNum` = `wf_ch`.`WeekNum`)))) * 100),2) AS `WCRate` from `wf_ch` group by `wf_ch`.`FK_Emp`,`wf_ch`.`WeekNum`,`wf_ch`.`FK_NY` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_totalchyf`
--

/*!50001 DROP VIEW IF EXISTS `v_totalchyf`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_totalchyf` AS select `wf_ch`.`FK_Emp` AS `FK_Emp`,`wf_ch`.`FK_NY` AS `FK_NY`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`FK_NY` = `wf_ch`.`FK_NY`))) AS `AllNum`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` <= 1) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`FK_NY` = `wf_ch`.`FK_NY`))) AS `ASNum`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` >= 2) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`FK_NY` = `wf_ch`.`FK_NY`))) AS `CSNum`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` = 0) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`FK_NY` = `wf_ch`.`FK_NY`))) AS `JiShi`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` = 1) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`FK_NY` = `wf_ch`.`FK_NY`))) AS `AnQi`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` = 2) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`FK_NY` = `wf_ch`.`FK_NY`))) AS `YuQi`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` = 3) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`FK_NY` = `wf_ch`.`FK_NY`))) AS `ChaoQi`,round((((select cast(count(`a`.`MyPK`) as decimal(10,0)) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` <= 1) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`FK_NY` = `wf_ch`.`FK_NY`))) / (select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`FK_NY` = `wf_ch`.`FK_NY`)))) * 100),2) AS `WCRate` from `wf_ch` group by `wf_ch`.`FK_Emp`,`wf_ch`.`FK_NY` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `wf_empworks`
--

/*!50001 DROP VIEW IF EXISTS `wf_empworks`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `wf_empworks` AS select `a`.`PRI` AS `PRI`,`a`.`WorkID` AS `WorkID`,`b`.`IsRead` AS `IsRead`,`a`.`Starter` AS `Starter`,`a`.`StarterName` AS `StarterName`,`a`.`WFState` AS `WFState`,`a`.`FK_Dept` AS `FK_Dept`,`a`.`DeptName` AS `DeptName`,`a`.`FK_Flow` AS `FK_Flow`,`a`.`FlowName` AS `FlowName`,`a`.`PWorkID` AS `PWorkID`,`a`.`PFlowNo` AS `PFlowNo`,`b`.`FK_Node` AS `FK_Node`,`b`.`FK_NodeText` AS `NodeName`,`b`.`FK_Dept` AS `WorkerDept`,`a`.`Title` AS `Title`,`a`.`RDT` AS `RDT`,`b`.`RDT` AS `ADT`,`b`.`SDT` AS `SDT`,`b`.`FK_Emp` AS `FK_Emp`,`b`.`FID` AS `FID`,`a`.`FK_FlowSort` AS `FK_FlowSort`,`a`.`SysType` AS `SysType`,`a`.`SDTOfNode` AS `SDTOfNode`,`b`.`PressTimes` AS `PressTimes`,`a`.`GuestNo` AS `GuestNo`,`a`.`GuestName` AS `GuestName`,`a`.`BillNo` AS `BillNo`,`a`.`FlowNote` AS `FlowNote`,`a`.`TodoEmps` AS `TodoEmps`,`a`.`TodoEmpsNum` AS `TodoEmpsNum`,`a`.`TodoSta` AS `TodoSta`,`a`.`TaskSta` AS `TaskSta`,0 AS `ListType`,`a`.`Sender` AS `Sender`,`a`.`AtPara` AS `AtPara`,1 AS `MyNum` from (`wf_generworkflow` `a` join `wf_generworkerlist` `b`) where ((`b`.`IsEnable` = 1) and (`b`.`IsPass` = 0) and (`a`.`WorkID` = `b`.`WorkID`) and (`a`.`FK_Node` = `b`.`FK_Node`) and (`a`.`WFState` <> 0)) union select `a`.`PRI` AS `PRI`,`a`.`WorkID` AS `WorkID`,`b`.`Sta` AS `IsRead`,`a`.`Starter` AS `Starter`,`a`.`StarterName` AS `StarterName`,2 AS `WFState`,`a`.`FK_Dept` AS `FK_Dept`,`a`.`DeptName` AS `DeptName`,`a`.`FK_Flow` AS `FK_Flow`,`a`.`FlowName` AS `FlowName`,`a`.`PWorkID` AS `PWorkID`,`a`.`PFlowNo` AS `PFlowNo`,`b`.`FK_Node` AS `FK_Node`,`b`.`NodeName` AS `NodeName`,`b`.`CCToDept` AS `WorkerDept`,`a`.`Title` AS `Title`,`a`.`RDT` AS `RDT`,`b`.`RDT` AS `ADT`,`b`.`RDT` AS `SDT`,`b`.`CCTo` AS `FK_Emp`,`b`.`FID` AS `FID`,`a`.`FK_FlowSort` AS `FK_FlowSort`,`a`.`SysType` AS `SysType`,`a`.`SDTOfNode` AS `SDTOfNode`,0 AS `PressTimes`,`a`.`GuestNo` AS `GuestNo`,`a`.`GuestName` AS `GuestName`,`a`.`BillNo` AS `BillNo`,`a`.`FlowNote` AS `FlowNote`,`a`.`TodoEmps` AS `TodoEmps`,`a`.`TodoEmpsNum` AS `TodoEmpsNum`,0 AS `TodoSta`,0 AS `TaskSta`,1 AS `ListType`,`b`.`Rec` AS `Sender`,('@IsCC=1' or `a`.`AtPara`) AS `AtPara`,1 AS `MyNum` from (`wf_generworkflow` `a` join `wf_cclist` `b`) where ((`a`.`WorkID` = `b`.`WorkID`) and (`b`.`Sta` <= 1) and (`b`.`InEmpWorks` = 1) and (`a`.`WFState` <> 0)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-28 16:30:44
